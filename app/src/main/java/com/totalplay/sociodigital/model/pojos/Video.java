package com.totalplay.sociodigital.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Video implements Serializable{
    public String description;

    @SerializedName("video")
    public String videoUrl;

    public String id;

    @Expose(deserialize = false, serialize = false)
    public boolean downloaded = false;
}
