package com.totalplay.sociodigital.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PhotoEvidence extends RealmObject {
    private String path;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    @SerializedName("imagen")
    private String photo;
    @PrimaryKey
    private String id;
    private String folder;
    @SerializedName("fecha")
    private String date;
    @SerializedName("nombre")
    public String name;
    @Expose(deserialize = false, serialize = false)
    private boolean synced;

    public PhotoEvidence() {
        id = UUID.randomUUID().toString();
        synced = false;
    }

    public PhotoEvidence(String photo) {
        this.photo = photo;
        id = UUID.randomUUID().toString();
        synced = false;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}

