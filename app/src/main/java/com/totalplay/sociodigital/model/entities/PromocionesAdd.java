package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class PromocionesAdd {
    @SerializedName("promociones")
    public ArrayList<Promotion> mPromotions = new ArrayList<>();

    public PromocionesAdd(ArrayList<Promotion> promotions) {
        mPromotions = promotions;
    }
}
