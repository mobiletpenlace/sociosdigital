package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class BillingFact {
    @SerializedName("calle")
    public String street;
    @SerializedName("ciudad")
    public String city;
    @SerializedName("codigoPostalFacturacion")
    public String cpBilling;
    @SerializedName("colonia")
    public String colony;
    @SerializedName("delegacionMunicipio")
    public String delegation;
    @SerializedName("estado")
    public String state;
    @SerializedName("mismaDirInst")
    public String dirInst;
    @SerializedName("numExterior")
    public String externalNumber;
    @SerializedName("numInterior")
    public String internalNumber;

    public BillingFact(String street, String city, String cpBilling, String colony, String delegation, String state, String dirInst, String externalNumber, String internalNumber) {
        this.street = street;
        this.city = city;
        this.cpBilling = cpBilling;
        this.colony = colony;
        this.delegation = delegation;
        this.state = state;
        this.dirInst = dirInst;
        this.externalNumber = externalNumber;
        this.internalNumber = internalNumber;
    }
}
