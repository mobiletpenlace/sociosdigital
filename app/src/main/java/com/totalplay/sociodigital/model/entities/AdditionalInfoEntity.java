package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class AdditionalInfoEntity extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("TipoIdentificacion")
    public String typeID;
    @SerializedName("IdentificacionOficial")
    public String infoID;
}
