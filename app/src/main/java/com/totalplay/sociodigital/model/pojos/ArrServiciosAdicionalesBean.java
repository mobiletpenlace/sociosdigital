package com.totalplay.sociodigital.model.pojos;

import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ArrServiciosAdicionalesBean extends RealmObject implements PlanDetailResponse.AdicionalesBean {
    public String Id;
    public String Nombre;
    public String CantidadEquiposAutomaticos;
    public String Comentario;
    public String MaximoAgregar;
    public String Nombrecomercial;
    public String NombreEditable;
    public String PlanId;
    public String Precio;
    public String SeActiva;
    public String SeFactura;
    public String ServicioId;
    public String SRV_Mode;
    public String TipoServicio;
    public String TipoTelefonia;
    public boolean isChecked = false;
    public RealmList<ArrProductosIncluidosBean> ArrProductosIncluidos;
    //url imagen
    public String ImgIconoApp;

    @Override
    public int getAdicionalType() {
        return PlanDetailResponse.AdicionalesBean.SERVICE;
    }
}
