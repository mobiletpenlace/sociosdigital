package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class DireccionBean extends RealmObject implements Serializable {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("CodigoPostal")
    public String zipCode;
    @SerializedName("Colonia")
    public String colony;
    @SerializedName("Delegacion")
    public String delegation;
    @SerializedName("Ciudad")
    public String city;
    @SerializedName("Estado")
    public String state;
    @SerializedName("Calle")
    public String street;
    @SerializedName("NumeroExterior")
    public String noExt;
    @SerializedName("NumeroInterior")
    public String noInt;
    @SerializedName("EntreCalles")
    public String betweenStreets;
    @SerializedName("ReferenciaUrbana")
    public String urbanReference;
    @SerializedName("AutorizacionSinCobertura")
    public String authorizationWhitoutCoverage;
    @SerializedName("Cluster")
    public String cluster;
    @SerializedName("Plaza")
    public String place;
    @SerializedName("Distrito")
    public String district;
    @SerializedName("Factibilidad")
    public String feasibility;
    @SerializedName("Factible")
    public String feasible;
    @SerializedName("TipoCobertura")
    public String typeCoverage;
    @SerializedName("Region")
    public String region;
    @SerializedName("RegionId")
    public String idRegion;
    @SerializedName("zona")
    public String zone;
    @SerializedName("Latitude")
    public String latitude;
    @SerializedName("Longitude")
    public String longitude;
}
