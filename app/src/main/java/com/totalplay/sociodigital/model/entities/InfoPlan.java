package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class InfoPlan {
    @SerializedName("consultaAddons")
    public String mConsultAddons;
    @SerializedName("consultaPromo")
    public String mConsultPromo;
    @SerializedName("consultaServ")
    public String mConsultServ;
    @SerializedName("cluster")
    public String mCluster;
    @SerializedName("codigoPostal")
    public String mPostalCode;
    @SerializedName("colonia")
    public String mColony;
    @SerializedName("distrito")
    public String mDistrict;
    @SerializedName("metodoPago")
    public String payMethod;
    @SerializedName("plaza")
    public String place;
    @SerializedName("subcanalVenta")
    public String subChannelSale;

    public InfoPlan(String consultAddons,
                    String consultPromo,
                    String consultServ,
                    String cluster,
                    String postalCode,
                    String colony,
                    String district,
                    String payMethod,
                    String place,
                    String subChannelSale) {
        mConsultAddons = consultAddons;
        mConsultPromo = consultPromo;
        mConsultServ = consultServ;
        mCluster = cluster;
        mPostalCode = postalCode;
        mColony = colony;
        mDistrict = district;
        this.payMethod = payMethod;
        this.place = place;
        this.subChannelSale = subChannelSale;
    }
}
