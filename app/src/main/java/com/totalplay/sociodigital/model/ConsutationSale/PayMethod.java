package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class PayMethod {
    @SerializedName("anio")
    public String vigenceYear;
    @SerializedName("apellidoMaternoTitular")
    public String ownerSecondLastName;
    @SerializedName("apellidoPaternoTitular")
    public String ownerLastName;
    @SerializedName("digitosTarjeta")
    public String cardNumber;
    @SerializedName("mes")
    public String vigenceMonth;
    @SerializedName("metodoPago")
    public String payMethod;
    @SerializedName("nombreTitularTarjeta")
    public String ownerName;
    @SerializedName("tipoTarjeta")
    public String typeCard;

    public PayMethod(String vigenceYear, String ownerSecondLastName, String ownerLastName, String cardNumber, String vigenceMonth, String payMethod, String ownerName, String typeCard) {
        this.vigenceYear = vigenceYear;
        this.ownerSecondLastName = ownerSecondLastName;
        this.ownerLastName = ownerLastName;
        this.cardNumber = cardNumber;
        this.vigenceMonth = vigenceMonth;
        this.payMethod = payMethod;
        this.ownerName = ownerName;
        this.typeCard = typeCard;
    }
}
