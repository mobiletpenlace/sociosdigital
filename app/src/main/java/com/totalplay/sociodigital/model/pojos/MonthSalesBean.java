package com.totalplay.sociodigital.model.pojos;

public class MonthSalesBean  {
    public String mMont = "";
    public String mDay = "";

    public MonthSalesBean(String mMont, String mDay) {
        this.mMont = mMont;
        this.mDay = mDay;
    }
}
