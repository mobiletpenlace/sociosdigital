package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * Socio Digital
 */

public class ProductDetail {
    @SerializedName("Id")
    public String idProductDetail;
    @SerializedName("nombre")
    public String nameProductDetail;

    public ProductDetail(String idProductDetail, String nameProductDetail) {
        this.idProductDetail = idProductDetail;
        this.nameProductDetail = nameProductDetail;
    }
}
