package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ContactEntity extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("TipoPersona")
    public String typePerson;
    @SerializedName("Nombre")
    public String name;
    @SerializedName("ApellidoPaterno")
    public String fatherLastName;
    @SerializedName("ApellidoMaterno")
    public String motherLastName;
    @SerializedName("Telefono")
    public String phone;
    @SerializedName("OtroTelefono")
    public String otherPhone;
    @SerializedName("Celular")
    public String cellphone;
    @SerializedName("CorreoElectronico")
    public String email;
    @SerializedName("OtroCorreoElectronico")
    public String otherEmail;
    @SerializedName("RFC")
    public String rfc;
    @SerializedName("RazonSocial")
    public String socialReason;
    @SerializedName("FechaNacimiento")
    public String birthDay;
    @SerializedName("MedioContacto")
    public String medioContacto;
}
