package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioTP
 */

public class SaleTP {
    @SerializedName("datosAdicionales")
    public AdditionalData mAdditionalData;
    @SerializedName("dirFact")
    public BillingFact mBillingFact;
    @SerializedName("docs")
    public Documents mDocuments;
    @SerializedName("metodoPago")
    public PayMethod mPayMethod;
    @SerializedName("planSeleccionadoId")
    public String plainSelectedId;
    @SerializedName("prospecto")
    public ProspectInfo mProspectInfo;
    @SerializedName("sitio")
    public SiteInfo mSiteInfo;
    @SerializedName("informacionAdicionales")
    public AdditionalsInformation mAdditionalsInformation;

    public SaleTP(AdditionalData additionalData,
                  BillingFact billingFact,
                  Documents documents,
                  PayMethod payMethod,
                  String plainSelectedId,
                  ProspectInfo prospectInfo,
                  SiteInfo siteInfo,
                  AdditionalsInformation additionalsInformation) {
        mAdditionalData = additionalData;
        mBillingFact = billingFact;
        mDocuments = documents;
        mPayMethod = payMethod;
        this.plainSelectedId = plainSelectedId;
        mProspectInfo = prospectInfo;
        mSiteInfo = siteInfo;
        mAdditionalsInformation = additionalsInformation;
    }
}
