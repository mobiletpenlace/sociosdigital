package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class QuotationPlainServiceProduct extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();


    @SerializedName("Cantidad")
    public String quantity;
    @SerializedName("CantidadDN")
    public String quantityDN;
    @SerializedName("CantidadTroncal")
    public String quantityTrunk;
    @SerializedName("Descuento")
    public String discount;
    @SerializedName("DP_Producto")
    public String detailProduct;
    @SerializedName("DP_PromocionServicioProducto")
    public String detailPromotionProductService;
    @SerializedName("DP_ServicioProducto")
    public String detailProductService;
    @SerializedName("EsCargoUnico")
    public String isUniqueCharge;
    @SerializedName("EsDescuento")
    public String isDiscount;
    @SerializedName("EsProductoAdicional")
    public String isAdditionalProduct;
    @SerializedName("EsProntoPago")
    public String isSoonPay;
    @SerializedName("EsServicioRepetido")
    public String isServiceRepeated;
    @SerializedName("EstatusActivacion")
    public String statusActivation;
    @SerializedName("EstatusActivacionDescripcion")
    public String statusActivationDescription;
    @SerializedName("FechaFin")
    public String dateEnd;
    @SerializedName("FechaInicio")
    public String dateStart;
    @SerializedName("GeneraDNs")
    public String generateDn;
    @SerializedName("Impuesto2")
    public String tax2;
    @SerializedName("Impuesto1")
    public String tax1;
    @SerializedName("MontoImpuestos")
    public String amountTax;
    @SerializedName("NombreProducto")
    public String nameProduct;
    @SerializedName("PrecioUnitario")
    public String unitaryPrice;
    @SerializedName("PrecioUnitario_ProntoPago")
    public String unitarySoonPrice;
    @SerializedName("PrecioUnitarioBase")
    public String baseUnitaryPrice;
    @SerializedName("ProductoPadre")
    public String fatherProduct;
    @SerializedName("TieneIPDinamica")
    public String isDynamicIP;
    @SerializedName("TieneIPFija")
    public String isFixedIp;
    @SerializedName("TipoIp")
    public String typeIp;
    @SerializedName("TipoProducto")
    public String typeProduct;
    @SerializedName("VelocidadBajada")
    public String speedDown;
    @SerializedName("VelocidadSubida")
    public String speedUp;
    @SerializedName("DP_PromocionPlan")
    public String detailPromotionPlain;
    @SerializedName("MesInicio")
    public String mesInicio;
}
