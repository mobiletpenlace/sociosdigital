package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by imacbookpro on 10/09/18.
 * SocioDigital
 */

public class DataSaleMonth implements Serializable {
    @SerializedName("mes")
    public String month;
    @SerializedName("porContactar")
    public String toContact;
    @SerializedName("porInstalar")
    public String toInstall;
    @SerializedName("NoExitoso")
    public String noSuccessful;
    @SerializedName("perdidos")
    public String notLocatable;
    @SerializedName("instalados")
    public String instalated;
}
