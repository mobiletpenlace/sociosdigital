package com.totalplay.sociodigital.model.PmPlanesSocios;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class PlanTotalPlay  implements Serializable{
    @SerializedName("detallePlan")
    public String planDetail;
    @SerializedName("familiaPaquete")
    public String familyPackage;
    @SerializedName("id")
    public String id;
    @SerializedName("imagenPaquete")
    public String packageImage;
    @SerializedName("megas")
    public String megas;
    @SerializedName("nombrePaquete")
    public String namePackage;
    @SerializedName("plazo")
    public String plazo;
    @SerializedName("precioLista")
    public String listPrice;
    @SerializedName("precioProntoPago")
    public String earlyPrice;
    @SerializedName("tipoPlan")
    public String typePlan;
    @SerializedName("tipoOferta")
    public String typeOffert;
}
