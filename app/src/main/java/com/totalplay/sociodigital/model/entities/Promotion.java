package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by imacbookpro on 08/09/18.
 * sociodigital
 */

public class Promotion implements Serializable {
    @SerializedName("Id")
    public String idPromotion;
    @SerializedName("imagen")
    public String imagePromotion;
    @SerializedName("nombre")
    public String namePromotion;
}
