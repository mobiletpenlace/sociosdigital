package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class ProductsAdd {
    @SerializedName("productos")
    public ArrayList<ProductDetail> mProductDetails;

    public ProductsAdd(ArrayList<ProductDetail> productDetails) {
        mProductDetails = productDetails;
    }
}
