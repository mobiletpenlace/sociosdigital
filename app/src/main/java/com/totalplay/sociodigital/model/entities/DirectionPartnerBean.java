package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;
import com.resources.utils.CryptoUtils;

import io.realm.RealmObject;

public class DirectionPartnerBean extends RealmObject {
    @SerializedName("CodigoPostal")
    public String mZipCode = CryptoUtils.crypt("");

    @SerializedName("Colonia")
    public String mColony ="";

    @SerializedName("Delegacion")
    public String mDelegation = CryptoUtils.crypt("");

    @SerializedName("Ciudad")
    public String mCity = CryptoUtils.crypt("");

    @SerializedName("Estado")
    public String mState = CryptoUtils.crypt("");

    @SerializedName("Calle")
    public String mStreet = "";

    @SerializedName("NumInterior")
    public String mInternalNumber = CryptoUtils.crypt("");

    @SerializedName("NumExterior")
    public String mExteriorNumber = CryptoUtils.crypt("");

    @SerializedName("EntreCalle")
    public String mBetweenStreet = CryptoUtils.crypt("");

    @SerializedName("Referencias")
    public String mReference = CryptoUtils.crypt("");


    public DirectionPartnerBean() {
    }

    public DirectionPartnerBean(String mZipCode, String mColony, String mDelegation, String mCity, String mState, String mStreet, String mInternalNumber, String mExteriorNumber, String mBetweenStreet, String mReference) {

        this.mZipCode = mZipCode;
        this.mColony = mColony;
        this.mDelegation = mDelegation;
        this.mCity = mCity;
        this.mState = mState;
        this.mStreet = mStreet;
        this.mInternalNumber = mInternalNumber;
        this.mExteriorNumber = mExteriorNumber;
        this.mBetweenStreet = mBetweenStreet;
        this.mReference = mReference;
    }
}
