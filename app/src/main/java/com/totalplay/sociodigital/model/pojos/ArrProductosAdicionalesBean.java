package com.totalplay.sociodigital.model.pojos;

import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;

import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ArrProductosAdicionalesBean extends RealmObject implements PlanDetailResponse.AdicionalesBean {

    public String Id;
    public String Nombre;
    public String AgrupacionAddon;
    public String CantidadDN;
    public String CantidadTroncal;
    public String Comentario;
    public String EsAutomaticoCiudad;
    public String EsCargoUnico;
    public String Estatus;
    public String EsVisible;
    public String IdBrmArrear;
    public String IdBrmCU;
    public String IdBrmForward;
    public String IEPS;
    public String IVA;
    public String MaximoAgregar;
    public String NombreEditable;
    public String PlanDescuentoId;
    public String Plazo;
    public String PrecioBase;
    public String PrecioEditable;
    public String PrecioProntoPago;
    public String ProductoPadre;
    public String TipoProducto;
    public String TipoProductoTexto;
    public String IdProducto;
    public String NameProducto;
    public String ProductoId;
    public String Ciudad;
    public String VelocidadSubida;
    public String VelocidadBajada;
    public String TieneIPDinamica;
    public String TieneIPFija;
    public String TieneSTBAdicional;
    public String EsCCTV;
    public String EsWiFi;
    public String Cantidad;
    public String EstatusProducto;
    public String FechaInicio;
    public String FechaFin;
    public String ComentarioProducto;
    public String EsProntoPago;
    public boolean isChecked;
    //url imagen
    public String URLApp__c;

    @Override
    public int getAdicionalType() {
        return PlanDetailResponse.AdicionalesBean.PRODUCT;
    }
}
