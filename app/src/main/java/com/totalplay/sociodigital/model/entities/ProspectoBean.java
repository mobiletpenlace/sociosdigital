package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ProspectoBean extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("CuentaBRM")
    public String brmAccount;
    @SerializedName("IdOportunidad")
    public String idOpportunity;
    @SerializedName("IdCuenta")
    public String idAccount;
    @SerializedName("IdCuentaFactura")
    public String idBillingAccount;
    @SerializedName("IdContacto")
    public String idContact;
    @SerializedName("IdCotizacion")
    public String idQuotation;
    @SerializedName("IdSitio")
    public String idSite;
    @SerializedName("PlazoContrato")
    public String timeContract = "18";
    @SerializedName("Vendedor")
    public SellerEntity seller;
    @SerializedName("Direccion")
    public DireccionBean direccionBean;
    @SerializedName("Contacto")
    public ContactEntity contact;
    @SerializedName("Propuesta")
    public PropuestaBean proposal;
    @SerializedName("Firma")
    public SignatureEntity signature;
}
