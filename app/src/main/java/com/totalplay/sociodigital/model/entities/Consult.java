package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class Consult {
    @SerializedName("idPlan")
    public String idPlan;
    @SerializedName("info")
    public InfoPlan mInfoPlan;
    @SerializedName("estimuloFiscal")
    public String estimFiscal;

    public Consult(String idPlan, InfoPlan infoPlan, String estimFiscal) {
        this.idPlan = idPlan;
        mInfoPlan = infoPlan;
        this.estimFiscal =  estimFiscal;
    }
}
