package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class Additional  implements Serializable{
    @SerializedName("Id")
    public String id;
    @SerializedName("imagen")
    public String image;
    @SerializedName("maximoAgregar")
    public String maxAdd;
    @SerializedName("nombre")
    public String name;
    @SerializedName("precio")
    public String price;
}

