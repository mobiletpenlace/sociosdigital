package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class CardPartnerBean extends RealmObject{
    @SerializedName("NumeroTarjeta")
    public String mCardNumber = "";

    @SerializedName("NombreTitular")
    public String mNombreTitular = "";

    @SerializedName("LastNameTitular")
    public String mLastNameTitular = "";

    @SerializedName("FechaVencimientoAnio")
    public String mFechaVencimientoAnio = "";

    @SerializedName("FechaVencimientoMes")
    public String mFechaVencimientoMes = "";

    @SerializedName("CCTYPE")
    public String mCCTYPE = "";

    public CardPartnerBean() {
    }

    public CardPartnerBean(String mNombreTitular, String mLastNameTitular, String mCardNumber, String mFechaVencimientoMes, String mFechaVencimientoAnio, String mCCTYPE) {
        this.mCardNumber = mCardNumber;
        this.mNombreTitular = mNombreTitular;
        this.mFechaVencimientoMes = mFechaVencimientoMes;
        this.mFechaVencimientoAnio = mFechaVencimientoAnio;
        this.mCCTYPE =  mCCTYPE;
        this.mLastNameTitular = mLastNameTitular;
    }
}
