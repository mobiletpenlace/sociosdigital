package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseRequestInterface;
import com.resources.utils.CryptoUtils;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class UserDateBean extends RealmObject implements WSBaseRequestInterface {

    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();

    @SerializedName("Canal")
    public String mCanal = CryptoUtils.crypt("Socios TP");//"Socios TP";

    @SerializedName("NoEmpleadoSocio")
    public String mNoParter = "";

    @SerializedName("subCanal")
    public String mSubCanal = "";
    @SerializedName("NombreEdificioNegocio")
    public String mNameTypeSeller = "";
    @SerializedName("NumeroCoach")
    public String mNumberCoach = CryptoUtils.crypt("");

    @SerializedName("Nombre")
    public String mName = "";
    @SerializedName("ApPaterno")
    public String mLastName = "";
    @SerializedName("ApMaterno")
    public String mSecondLastName = "";

    @SerializedName("RFC")
    public String mRFC = "";
    @SerializedName("CorreoElectronico")
    public String mEmail = "";
    @SerializedName("Telefono")
    public String mPhone = "";

    @SerializedName("NoCuenta")
    public String mAccountNumber = "";

    @SerializedName("pathSelfie")
    public String mPathSelfie = "";

    public String mSelfie = "";
    @SerializedName("IdentificacionFrente")
    public String mIdentificationFront = "";
    @SerializedName("IdentificacionReverso")
    public String mIdentificationBack = "";
    @SerializedName("ComprobanteDomicilio")
    public String mVoucher = "";
    @SerializedName("FirmaDigital")
    public String mDigitalSignature = "";

    @SerializedName("TarjetaSocio")
    public CardPartnerBean mCardPartnerBean;

    @SerializedName("DomicilioEdificioNegocio")
    public DirectionBuildingBusinnessBean mDirectionBuildingBusinnessBean = new DirectionBuildingBusinnessBean();

    @SerializedName("DomicilioSocio")
    public DirectionPartnerBean mDirectionPartnerBean = new DirectionPartnerBean();

    public String mNameSelfie = "";

    @SerializedName("IdSystem")
    public String idSystem;
    @SerializedName("IdRoll")
    public String idRoll;
}
