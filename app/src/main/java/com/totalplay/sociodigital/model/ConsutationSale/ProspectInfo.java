package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * Socio Digital
 */

public class ProspectInfo {
    @SerializedName("calle")
    public String propectStreet;
    @SerializedName("celular")
    public String propectCellphone;
    @SerializedName("ciudad")
    public String prospectCity;
    @SerializedName("codigoPostal")
    public String prospectCP;
    @SerializedName("colonia")
    public String prospectColony;
    @SerializedName("company")
    public String prospectCompany;
    @SerializedName("correo")
    public String prospectEmail;
    @SerializedName("delegacionMunicipio")
    public String prospectDelegation;
    @SerializedName("estado")
    public String prospectState;
    @SerializedName("fechaNacimiento")
    public String prospectDataBirth;
    @SerializedName("firstName")
    public String prospectName;
    @SerializedName("lastName")
    public String prospectLastName;
    @SerializedName("midleName")
    public String prospectMidleName;
    @SerializedName("numExterior")
    public String prospectExternalNumber;
    @SerializedName("numInterior")
    public String prospectInternalNumber;
    @SerializedName("otroCoreo")
    public String prospectOtherEmail;
    @SerializedName("otroTelefono")
    public String prospectOtherPhone;
    @SerializedName("refCalle")
    public String prospectStreetReferences;
    @SerializedName("refUrbana")
    public String prospectUrbanReferences;
    @SerializedName("rfc")
    public String prospectRFC;
    @SerializedName("telefono")
    public String prospectPhone;
    @SerializedName("tipoPersona")
    public String prospectTypePerson;
    @SerializedName("idEmpleado")
    public String prospectIdEmployed;
    @SerializedName("canalEmpleado")
    public String prospectChannelEmployed;
    @SerializedName("subcanalEmpleado")
    public String prospectSubchannelEmployed;
    @SerializedName("estimuloFiscal")
    public String estimFiscal;

    public ProspectInfo(String propectStreet,
                        String propectCellphone,
                        String prospectCity,
                        String prospectCP,
                        String prospectColony,
                        String prospectCompany,
                        String prospectEmail,
                        String prospectDelegation,
                        String prospectState,
                        String prospectDataBirth,
                        String prospectName,
                        String prospectLastName,
                        String prospectMidleName,
                        String prospectExternalNumber,
                        String prospectInternalNumber,
                        String prospectOtherEmail,
                        String prospectOtherPhone,
                        String prospectStreetReferences,
                        String prospectUrbanReferences,
                        String prospectRFC,
                        String prospectPhone,
                        String prospectTypePerson,
                        String prospectIdEmployed,
                        String prospectChannelEmployed,
                        String prospectSubchannelEmployed,
                        String estimFiscal) {
        this.propectStreet = propectStreet;
        this.propectCellphone = propectCellphone;
        this.prospectCity = prospectCity;
        this.prospectCP = prospectCP;
        this.prospectColony = prospectColony;
        this.prospectCompany = prospectCompany;
        this.prospectEmail = prospectEmail;
        this.prospectDelegation = prospectDelegation;
        this.prospectState = prospectState;
        this.prospectDataBirth = prospectDataBirth;
        this.prospectName = prospectName;
        this.prospectLastName = prospectLastName;
        this.prospectMidleName = prospectMidleName;
        this.prospectExternalNumber = prospectExternalNumber;
        this.prospectInternalNumber = prospectInternalNumber;
        this.prospectOtherEmail = prospectOtherEmail;
        this.prospectOtherPhone = prospectOtherPhone;
        this.prospectStreetReferences = prospectStreetReferences;
        this.prospectUrbanReferences = prospectUrbanReferences;
        this.prospectRFC = prospectRFC;
        this.prospectPhone = prospectPhone;
        this.prospectTypePerson = prospectTypePerson;
        this.prospectIdEmployed = prospectIdEmployed;
        this.prospectChannelEmployed = prospectChannelEmployed;
        this.prospectSubchannelEmployed = prospectSubchannelEmployed;
        this.estimFiscal = estimFiscal;
    }
}
