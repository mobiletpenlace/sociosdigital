package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 07/09/18.
 * Socio Digital
 */

public class Documents {
    @SerializedName("documentosWRP")
    ArrayList<DocumentEvidence> mDocumentEvidences;

    public Documents(ArrayList<DocumentEvidence> documentEvidences) {
        mDocumentEvidences = documentEvidences;
    }
}
