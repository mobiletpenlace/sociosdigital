package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class InfoAddons {
    @SerializedName("precioListaPaquete")
    public String priceListPackage;
    @SerializedName("precioProntoPaquete")
    public String priceEarlyPackage;
    @SerializedName("productosAdd")
    public ProductsAdd mProductsAdd;
    @SerializedName("promocionesAdd")
    public PromocionesAdd mPromocionesAdd;
}
