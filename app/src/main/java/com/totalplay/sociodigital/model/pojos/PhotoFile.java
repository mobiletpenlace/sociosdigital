package com.totalplay.sociodigital.model.pojos;

public class PhotoFile {
    public String nameImage;
    public String nameFromRealm;
    public String pathSave;
    public boolean isSync;

    public PhotoFile(String nameImgage, String nameFromRealm, boolean isSync) {
        this.nameImage = nameImgage;
        this.nameFromRealm = nameFromRealm;
        this.isSync =  isSync;

    }
}
