package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ReferredEntity extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid;
    @SerializedName("Nombre")
    public String name;
    @SerializedName("ApellidoPaterno")
    public String lastNameFather;
    @SerializedName("ApellidoMaterno")
    public String lastNameMother;
    @SerializedName("Parentesco")
    public String relationship;
    @SerializedName("Telefono")
    public String phone;

    public ReferredEntity() {
        this.uuid = UUID.randomUUID().toString();
    }
}
