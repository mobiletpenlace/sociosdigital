package com.totalplay.sociodigital.model.pojos;

import com.google.gson.annotations.SerializedName;

public class Response{
    @SerializedName("Result")
    public String mResult;

    @SerializedName("ResultId")
    public String mResultId;

    @SerializedName("Description")
    public String mDescription;
}
