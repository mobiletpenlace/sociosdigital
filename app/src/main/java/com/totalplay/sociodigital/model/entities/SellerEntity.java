package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class SellerEntity extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("IdEmpleado")
    public String idEmployee;
    @SerializedName("Canal")
    public String chanel;
    @SerializedName("SubCanal")
    public String subChanel;
    @SerializedName("FolioContrato")
    public String ticketContract;
    @SerializedName("IdDispositivo")
    public String idDevice;
    @SerializedName("IdDistrital")
    public String idDistrict;
    @SerializedName("IdEmpleadoDistrital")
    public String idEmployeeDistrict;
    @SerializedName("OrigenApp")
    public String isApp;
    @SerializedName("EsComisionable")
    public String isCommission;
    @SerializedName("IMEI")
    public String imei;
    @SerializedName("ModeloCelular")
    public String deviceModel;
    @SerializedName("Empleado_Distribuidor")
    public String employee;
    @SerializedName("Usuario_Logueado")
    public String loggedUser;
    @SerializedName("VentaExpress")
    public String saleExpress;
    @SerializedName("AprobarVentaExpress")
    public String approvalSaleExpress;
}
