package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * Socio Digital
 */

public class SiteInfo {
    @SerializedName("categoryService")
    public String categoryService;
    @SerializedName("cluster")
    public String cluster;
    @SerializedName("distrito")
    public String district;
    @SerializedName("factibilidad")
    public String factibility;
    @SerializedName("latitud")
    public String latitude;
    @SerializedName("longitud")
    public String logitude;
    @SerializedName("plaza")
    public String place;
    @SerializedName("region")
    public String region;
    @SerializedName("regionId")
    public String regionId;
    @SerializedName("tipoCobertura")
    public String coverageType;
    @SerializedName("zona")
    public String zone;

    public SiteInfo(String categoryService,
                    String cluster,
                    String district,
                    String factibility,
                    String latitude,
                    String logitude,
                    String place,
                    String region,
                    String regionId,
                    String coverageType,
                    String zone) {
        this.categoryService = categoryService;
        this.cluster = cluster;
        this.district = district;
        this.factibility = factibility;
        this.latitude = latitude;
        this.logitude = logitude;
        this.place = place;
        this.region = region;
        this.regionId = regionId;
        this.coverageType = coverageType;
        this.zone = zone;
    }
}
