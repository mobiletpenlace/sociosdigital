package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * Socio Digital
 */

public class DocumentEvidence {
    @SerializedName("archivoB64")
    public String dataImageB64;
    @SerializedName("nombreArchivo")
    public String nameFile;
    @SerializedName("tipoDocumento")
    public String typeFile;

    public DocumentEvidence(String dataImageB64, String nameFile, String typeFile) {
        this.dataImageB64 = dataImageB64;
        this.nameFile = nameFile;
        this.typeFile = typeFile;
    }
}
