package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class AdditionalData {
    @SerializedName("tipoIdentificacion")
    public String identificationType;
    @SerializedName("medioContacto")
    public String meansContact;
    @SerializedName("identificacionOficial")
    public String officialIdentification;

    public AdditionalData(String identificationType, String meansContact, String officialIdentification) {
        this.identificationType = identificationType;
        this.meansContact = meansContact;
        this.officialIdentification = officialIdentification;
    }
}
