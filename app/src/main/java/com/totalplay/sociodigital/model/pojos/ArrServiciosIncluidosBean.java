package com.totalplay.sociodigital.model.pojos;

import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ArrServiciosIncluidosBean {
    public String Id;
    public String Nombre;
    public String CantidadEquiposAutomaticos;
    public String Comentario;
    public String MaximoAgregar;
    public String Nombrecomercial;
    public String NombreEditable;
    public String PlanId;
    public String Precio;
    public String SeActiva;
    public String SeFactura;
    //campo nuevo
    public String Orden;

    public String ServicioId;
    public String SRV_Mode;
    public String TipoServicio;
    public String TipoTelefonia;
    public List<ArrProductosIncluidosBean> ArrProductosIncluidos;
}
