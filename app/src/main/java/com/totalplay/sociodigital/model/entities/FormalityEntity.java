package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseRequestInterface;
import com.totalplay.sociodigital.background.ws.request.LoginRequest;
import com.totalplay.sociodigital.model.pojos.ArrProductosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosAdicionalesBean;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class FormalityEntity extends RealmObject implements WSBaseRequestInterface {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("Login")
    public LoginRequest loginRequest;
    @SerializedName("Prospecto")
    public ProspectoBean prospectoBean;
    public Double listPrice;
    public Double priceSoonPaymen;
    public String brmAccount;

    @Expose(serialize = false, deserialize = false)
    public RealmList<ArrServiciosAdicionalesBean> addonServices = new RealmList<>();
    @Expose(serialize = false, deserialize = false)
    public RealmList<ArrProductosAdicionalesBean> addonProducts = new RealmList<>();
}
