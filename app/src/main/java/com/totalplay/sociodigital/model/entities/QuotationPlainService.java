package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class QuotationPlainService extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("CategoriaServicio")
    public String categoryService;
    @SerializedName("DN_Principal")
    public String principalDN;
    @SerializedName("DNsActivados")
    public String activatedDNs;
    @SerializedName("DP_PlanServicio")
    public String detailPlainService;
    @SerializedName("EsServicioAdicional")
    public String isAdditionalService;
    @SerializedName("EstatusActivacion")
    public String activationStatus;
    @SerializedName("IdExterno_ws")
    public String idExtWs;
    @SerializedName("IpFijasActivadas")
    public String ipFixed;
    @SerializedName("NombrePlan")
    public String namePlain;
    @SerializedName("NombreServicio")
    public String nameService;
    @SerializedName("SRV_Mode")
    public String srvMode;
    @SerializedName("Tipo")
    public String type;
    @SerializedName("DP_PromocionPlan")
    public String detailPromotionPlain;
    @SerializedName("Cot_ServicioProducto")
    public RealmList<QuotationPlainServiceProduct> quotationPlainServiceProducts;
}
