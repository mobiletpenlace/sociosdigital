package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class PropuestaBean extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("Oferta")
    public String offer;
    @SerializedName("TipoPago")
    public String typePayment = "Prepago";
    @SerializedName("Cot_SitioPlan")
    public QuotationSitePlainEntity quotationSitePlain;
    @SerializedName("Cot_PlanPromocion")
    public QuotationPromotionPlain quotationPromotionPlain;
    @SerializedName("Cot_PlanServicio")
    public RealmList<QuotationPlainService> quotationPlainService;
}
