package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class QuotationSitePlainEntity extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();

    @SerializedName("DescuentoCargoUnico")
    public String discountUniqueCharge;
    @SerializedName("DescuentoRenta")
    public String rentDiscount;
    @SerializedName("DN_Principal")
    public String principalDn;
    @SerializedName("DP_Plan")
    public String plain;
    @SerializedName("DP_DescuentoLimiteCargoUnico")
    public String detailLimitDiscountUniqueCharge;
    @SerializedName("DP_DescuentoLimiteRenta")
    public String detailLimitDiscount;
    @SerializedName("EstatusActivacion")
    public String activationStatus;
    @SerializedName("NombrePlan")
    public String plainName;
    @SerializedName("PrecioProntoPago")
    public String priceSoonPayment;
    @SerializedName("Ticket")
    public String ticket;
}
