package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 10/09/18.
 * Socio Digital
 */

public class ConverageInput {
    @SerializedName("TipoCliente")
    public String typeClient;
    @SerializedName("latitud")
    public String latitude;
    @SerializedName("longitud")
    public String longitude;

    public ConverageInput(String typeClient, String latitude, String longitude) {
        this.typeClient = typeClient;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
