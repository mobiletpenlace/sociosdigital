package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class DireccionFacturacionBean extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("MismaDireccionInstalacion")
    public String sameDirection = "true";
    @SerializedName("FolioContrato")
    public String ticketContract;
    @SerializedName("Calle")
    public String street;
    @SerializedName("NumeroExterior")
    public String noExt;
    @SerializedName("NumeroInterior")
    public String noInt;
    @SerializedName("ColoniaCP")
    public String zipCodeColony;
    @SerializedName("Colonia")
    public String colony;
    @SerializedName("DelegacionMunicipio")
    public String delegation;
    @SerializedName("Ciudad")
    public String city;
    @SerializedName("Estado")
    public String state;
    @SerializedName("CodigoPostal")
    public String zipCode;
}
