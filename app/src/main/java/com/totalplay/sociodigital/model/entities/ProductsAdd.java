package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class ProductsAdd {
    @SerializedName("adicional")
    public ArrayList<Additional> mAdditionals =  new ArrayList<>();
    @SerializedName("Agrupacion")
    public String agroup;
    @SerializedName("Index")
    public String index;

    public ProductsAdd(ArrayList<Additional> additionals, String agroup, String index) {
        mAdditionals = additionals;
        this.agroup = agroup;
        this.index = index;
    }
}
