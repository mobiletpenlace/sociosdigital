package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class Promotional {
    @SerializedName("Id")
    public String promitionalId;
    @SerializedName("nombre")
    public String promotionalName;

    public Promotional(String promitionalId, String promotionalName) {
        this.promitionalId = promitionalId;
        this.promotionalName = promotionalName;
    }
}
