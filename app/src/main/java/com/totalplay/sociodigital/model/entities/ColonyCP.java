package com.totalplay.sociodigital.model.entities;

import java.io.Serializable;

public class ColonyCP implements Serializable {
    public String nameColony;
    public String appFiscalEstim;

    public ColonyCP(String nameColony, String appFiscalEstim) {
        this.nameColony = nameColony;
        this.appFiscalEstim = appFiscalEstim;
    }
}
