package com.totalplay.sociodigital.model.ConsutationSale;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class PromotionalsAdd {
    @SerializedName("promociones")
    ArrayList<Promotional> mPromotionals;

    public PromotionalsAdd(ArrayList<Promotional> promotionals) {
        mPromotionals = promotionals;
    }
}
