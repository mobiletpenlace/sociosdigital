package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class ConsultPlain {
    @SerializedName("plazaTP")
    public String placeTp;
    @SerializedName("canalFront")
    public String channelFront;

    public ConsultPlain(String placeTp, String channelFront) {
        this.placeTp = placeTp;
        this.channelFront = channelFront;
    }
}
