package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class DetailPlainPromotion extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid;

    @SerializedName("Id")
    public String id;


    public DetailPlainPromotion() {
        this.uuid = UUID.randomUUID().toString();
    }
}
