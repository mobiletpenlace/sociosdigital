package com.totalplay.sociodigital.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseRequestInterface;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class InfoSocioBean extends RealmObject implements WSBaseRequestInterface {

    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();

    @SerializedName("NoEmpleado")
    public String mNoEmpleado = "";

    @SerializedName("Nombre")
    public String mNombre = "";

    @SerializedName("Ciudad")
    public String mCiudad = "";

    @SerializedName("Canal")
    public String mCanal = "";

    @SerializedName("Subcanal")
    public String mSubCanal = "";

    @SerializedName("PathSelfie")
    public String mPathSelfie = "";

    @SerializedName("Id")
    public String idOportunity = "";

    public String mAccessKey = "";

    public String mIsAnyCardRegister = "0";
}

