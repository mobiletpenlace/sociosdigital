package com.totalplay.sociodigital.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class PayMethodEntity extends RealmObject {
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid;

    @SerializedName("IdTarjetaRepo")
    public String idCardRepo;
    @SerializedName("Metodo")
    public String method;
    @SerializedName("TipoTarjeta")
    public String typeCard;
    @SerializedName("NombreTitular")
    public String nameOwner;
    @SerializedName("ApellidoPaternoTitular")
    public String lastNameFatherOwner;
    @SerializedName("ApellidoMaternoTitular")
    public String lastNameMotherOwner;
    @SerializedName("NumeroTarjeta")
    public String cardNumber;
    @SerializedName("VencimientoMes")
    public String expiryMonth;
    @SerializedName("VencimientoAnio")
    public String expiryYear;


    public PayMethodEntity() {
        uuid = UUID.randomUUID().toString();
    }
}
