package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 28/08/18.
 * SocioDigital
 */

public class ProductMasterPlainsRequest extends BaseRequest {
    @SerializedName("Info")
    public Information information;

    public static class Information{
        @SerializedName("Tipo")
        public String type;
        @SerializedName("Plaza")
        public String city;

        public Information(String type, String city){
            this.type = type; this.city = city;
        }
    }
}
