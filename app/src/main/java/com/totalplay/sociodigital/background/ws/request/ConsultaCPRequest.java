package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class ConsultaCPRequest extends BaseRequest {
    @SerializedName("CodigoPostal")
    public String postalCode;

    public ConsultaCPRequest(String cp) {
        this.postalCode =  cp;
    }
}
