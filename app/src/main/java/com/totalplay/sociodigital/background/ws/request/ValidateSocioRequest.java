package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 25/09/18.
 * SocioDigital
 */

public class ValidateSocioRequest extends BaseRequest {
    @SerializedName("NoEmpleado")
    public String noEmployed;

    public ValidateSocioRequest(String noEmployed) {
        this.noEmployed = noEmployed;
    }
}
