package com.totalplay.sociodigital.background.ws.querymaps.time;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class Viewport {
    /**
     * northeast : {"lat":19.3178559802915,"lng":-99.1607759697085}
     * southwest : {"lat":19.3151580197085,"lng":-99.1634739302915}
     */

    @SerializedName("northeast")
    private Northeast northeast;
    @SerializedName("southwest")
    private Southwest southwest;

    public Viewport() {
        this.northeast = new Northeast();
        this.southwest = new Southwest();
    }

    public Northeast getNortheast() {
        return northeast;
    }

    public void setNortheast(Northeast northeast) {
        this.northeast = northeast;
    }

    public Southwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }

    public static class Northeast {
        /**
         * lat : 19.3178559802915
         * lng : -99.1607759697085
         */


        @SerializedName("lat")
        private double lat;
        @SerializedName("lng")
        private double lng;

        public Northeast() {
            this.lat = 0.0d;
            this.lng = 0.0d;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public static class Southwest {
        @SerializedName("lat")
        private double lat;
        @SerializedName("lng")
        private double lng;

        /**
         * lat : 19.3151580197085
         * lng : -99.1634739302915
         */
        public Southwest() {
            this.lat = 0.0d;
            this.lng = 0.0d;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }
}
