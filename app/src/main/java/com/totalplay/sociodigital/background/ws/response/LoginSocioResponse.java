package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.model.pojos.Response;

public class LoginSocioResponse extends BaseResponse {
    @SerializedName("InfoSocio")
    public InfoSocioBean mInfoSocioBean = new InfoSocioBean();

    @SerializedName("Response")
    public Response mResponse = new Response();
}
