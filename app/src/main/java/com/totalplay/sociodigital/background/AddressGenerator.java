package com.totalplay.sociodigital.background;

import android.location.Address;
import android.util.Log;

import com.totalplay.sociodigital.background.ws.querymaps.ResultMaps;
import com.totalplay.sociodigital.background.ws.querymaps.time.AddressComponents;

import java.io.Serializable;
import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class AddressGenerator implements Serializable {
    private String numExt;
    public String colony;
    private String geolocation;
    public String lat;
    public String lng;
    public String street;
    public String zipCode;
    public String state;
    public String delegation;
    public String city;

    public AddressGenerator() {
        this.street = "";
        this.zipCode = "";
        this.numExt = "";
        this.delegation = "";
        this.state = "";
        this.colony = "";
        this.city = "";
        this.geolocation = "";
        this.lat = "";
        this.lng = "";

    }


    public void initAddressFromList(List<Address> addresses) {
        for (int i = 0; i < addresses.size(); i++) {
            if (colony == null || colony.isEmpty())
                colony = addresses.get(i).getAddressLine(1);
            if (city == null || city.isEmpty())
                city = addresses.get(i).getLocality();
            if (delegation == null || delegation.isEmpty())
                delegation = addresses.get(i).getSubAdminArea();
            if (state == null || state.isEmpty())
                state = addresses.get(i).getAdminArea();
            if (zipCode == null || zipCode.isEmpty()) {
                try {
                    Log.e("Code: ",addresses.get(i).getAddressLine(0)); //2 -> 0
                    String cade = addresses.get(i).getPostalCode();
                    int pCode = Integer.valueOf(cade);//addresses.get(i).getAddressLine(0).substring(0, 5));

                    if (pCode > 0) {
                        zipCode = addresses.get(i).getPostalCode();//addresses.get(i).getAddressLine(1).substring(0, 5);
                    }
                } catch (Exception e) {
                    zipCode = "";
                }
            }
            if (street == null || street.isEmpty())
                street = addresses.get(i).getThoroughfare();
        }
    }


    public void initAddress(ResultMaps result) {
        for (int i = 0; i < result.getResults().size(); i++) {
            geolocation = result.getResults().get(i).getGeometry().getLocation().toString();
            lat = String.valueOf(result.getResults().get(i).getGeometry().getLocation().getLat());
            lng = String.valueOf(result.getResults().get(i).getGeometry().getLocation().getLng());
            for (int k = 0; k < result.getResults().size(); k++) {

                for (AddressComponents component : result.getResults().get(k).getAddressComponents()) {
                    for (int j = 0; j < component.getTypes().size(); j++) {
                        switch (component.getTypes().get(j)) {
                            case "street_number":
                                if (numExt.isEmpty())
                                    numExt = component.getLongName().toUpperCase();
                                break;
                            case "route":
                                if (street.isEmpty())
                                    street = component.getLongName().toUpperCase();
                                break;
                            case "locality":
                                if (state.isEmpty()) {
                                    state = component.getLongName().toUpperCase();
                                    if (component.getTypes().size() > 1) {
                                        if (component.getTypes().get(1) != null && component.getTypes().get(1).equals("political"))
                                            city = component.getLongName().toUpperCase();
                                    }
                                }
                                break;
                            case "postal_code":
                                if (zipCode.isEmpty())
                                    zipCode = component.getLongName().toUpperCase();
                                break;
                            case "administrative_area_level_1":
                                if (state.isEmpty())
                                    state = component.getLongName().toUpperCase();
                                if (j > 1 && delegation.isEmpty()) {
                                    delegation = component.getLongName().toUpperCase();
                                }
                                break;
                            case "administrative_area_level_2":
                                if (delegation.isEmpty())
                                    delegation = component.getLongName().toUpperCase();
                                break;
                            case "administrative_area_level_3":
                                if (delegation.isEmpty())
                                    delegation = component.getLongName().toUpperCase();
                                break;
                            case "sublocality_level_1":
                                if (colony.isEmpty())
                                    colony = component.getLongName().toUpperCase();
                                break;
                        }
                    }
                }
            }
        }
    }
}
