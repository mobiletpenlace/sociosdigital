package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseRequest;

/**
 * Created by imacbookpro on 28/08/18.
 * SocioDigital
 */

public class BaseRequest extends WSBaseRequest {
    @SerializedName("Login")
    public LoginRequest loginRequest = new LoginRequest();
}
