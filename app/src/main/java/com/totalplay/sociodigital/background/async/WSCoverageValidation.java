package com.totalplay.sociodigital.background.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.resources.utils.Prefs;
import com.totalplay.sociodigital.background.AddressGenerator;
import com.totalplay.sociodigital.background.ws.request.ValidateCoverageRequest;
import com.totalplay.sociodigital.background.ws.response.ValidateCoverageResponse;
import com.totalplay.sociodigital.library.background.BaseWSManager;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.model.entities.ConverageInput;
import com.totalplay.sociodigital.model.entities.DireccionBean;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class WSCoverageValidation extends AsyncTask<String, Integer, ValidateCoverageResponse> {
    private AddressGenerator addressGenerator;
    private ProgressDialog progress;
    private CoverageValidateListener listener;
    private String responseFeasibility;
    private String responseFailure;
    private Intent intent;

    private BaseWSManager mWsManager;

    public WSCoverageValidation(Context context, AddressGenerator addressGenerator, CoverageValidateListener listener) {
        this.addressGenerator = addressGenerator;
        progress = new ProgressDialog(context);
        this.listener = listener;
        progress.setTitle("Enviando Información");
        progress.setMessage("Validando cobertura, espere un momento por favor");
        progress.setCancelable(false);
        //progress.show();

        mWsManager = WSManager.init(context).settings();
    }

    @Override
    protected ValidateCoverageResponse doInBackground(String... _params) {
        ValidateCoverageResponse coverageResponse = null;
        try {
            Log.e("Latitude:->",addressGenerator.lat);
            Log.e("Longitude:->",addressGenerator.lng);
            if (!addressGenerator.lat.isEmpty() && !addressGenerator.lng.isEmpty() && addressGenerator.lat !=null && addressGenerator.lng != null && addressGenerator != null) {
                ConverageInput converageInput =  new ConverageInput("RESIDENCIAL",addressGenerator.lat, addressGenerator.lng);
                ValidateCoverageRequest validateCoverageRequest = new ValidateCoverageRequest(converageInput);
                coverageResponse = mWsManager.requestWsSync(ValidateCoverageResponse.class, WSManager.WS.VALIDATE_COVERAGE, validateCoverageRequest);

                if (coverageResponse.restFaultElement != null) {
                    responseFailure = coverageResponse.restFaultElement.summary;
                    coverageResponse.direccionBean = null;
                } else {
                    ValidateCoverageResponse.Feasibility feasibility = coverageResponse.feasibility;
                    responseFeasibility = coverageResponse.feasibility.factibility;
                    if (feasibility != null) {
                        if (feasibility.factibility != null && feasibility.factibility.equals("1")){
                            coverageResponse.direccionBean = new DireccionBean();
                            coverageResponse.direccionBean.cluster = feasibility.cluster != null ? feasibility.cluster : "";
                            coverageResponse.direccionBean.district = feasibility.district != null ? feasibility.district : "";
                            coverageResponse.direccionBean.authorizationWhitoutCoverage = ("false");
                            coverageResponse.direccionBean.place = feasibility.city != null ? feasibility.city : "";
                            //Prefs.instance().putString("savePlace",coverageResponse.direccionBean.place);
                            coverageResponse.direccionBean.idRegion = feasibility.regionId != null ? feasibility.regionId : "";
                            coverageResponse.direccionBean.region = feasibility.region != null ? feasibility.region : "";
                            coverageResponse.direccionBean.feasibility = "0";
                            coverageResponse.direccionBean.feasible = feasibility.factibility;
                            coverageResponse.direccionBean.zone = feasibility.zone != null ? feasibility.zone : "";
                            coverageResponse.direccionBean.latitude = addressGenerator.lat;
                            coverageResponse.direccionBean.longitude = addressGenerator.lng;
                        } else {
                            coverageResponse.direccionBean = null;
                            responseFailure = coverageResponse.feasibility.factibility;
                        }
                    } else {
                        coverageResponse.direccionBean = null;

                    }
                }

            } else {
                responseFailure = "Lo sentimos, por el momento no contamos con cobertura en tu ubicacion";
                return  coverageResponse;
            }

        } catch (Exception e) {
            Log.e(SocioDigitalKeys.DEBUG, e.getMessage());
            return coverageResponse;
        }
        return coverageResponse;


    }


    protected void onPostExecute(ValidateCoverageResponse result) {
        Log.e(SocioDigitalKeys.DEBUG, "Termino Asyntask");
        progress.cancel();
        if (result != null) {
            listener.onSuccessCoverage(result, responseFeasibility);
        } else {
            listener.onErrorCoverage(result, responseFailure);
        }

    }

    @Override
    protected void onPreExecute() {
        Log.e(SocioDigitalKeys.DEBUG, "Inicio Asyntask");
        super.onPreExecute();
    }

    public interface CoverageValidateListener {
        void onSuccessCoverage(ValidateCoverageResponse entity, String feasibility);

        void onErrorCoverage(ValidateCoverageResponse entity, String feasibility);
    }
}
