package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;

import io.realm.RealmObject;

/**
 * Created by imacbookpro on 28/08/18.
 * SocioDigital
 */

public class LoginRequest extends RealmObject {
    @SerializedName("User")
    public String user;
    @SerializedName("UserId")
    public String userId;
    @SerializedName("userId")
    public String userId2;
    @SerializedName("Password")
    public String secretWord;
    @SerializedName("Ip")
    public String ip;
    @SerializedName("ip")
    public String ip2;

    public LoginRequest() {
        this.user = SocioDigitalKeys.USER;
        this.secretWord = SocioDigitalKeys.SECRET_WORD;
        this.ip = SocioDigitalKeys.IP;
        this.ip2 =  SocioDigitalKeys.IP;
        this.userId =  SocioDigitalKeys.USER;
        this.userId2 = SocioDigitalKeys.USER;
    }

    public LoginRequest(String user, String secretWord, String ip) {
        this.user = user;
        this.secretWord = secretWord;
        this.ip = ip;
    }
}
