package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ACDRequest extends BaseRequest {
    @SerializedName("IdOportunidad")
    public String opportunityId;

    public ACDRequest(String opportunityId) {
        this.opportunityId = opportunityId;
    }
}
