package com.totalplay.sociodigital.background.ws.response;

import com.totalplay.sociodigital.model.pojos.Video;

import java.util.ArrayList;

public class VideosResponse {
    public ArrayList<Video> videos;
}
