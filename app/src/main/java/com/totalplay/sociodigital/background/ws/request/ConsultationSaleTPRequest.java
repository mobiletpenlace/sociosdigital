package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.ConsutationSale.SaleTP;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class ConsultationSaleTPRequest extends BaseRequest {

    @SerializedName("ventaTP")
    public SaleTP mSaleTP;

    public ConsultationSaleTPRequest(SaleTP saleTP) {
        mSaleTP = saleTP;
    }
}
