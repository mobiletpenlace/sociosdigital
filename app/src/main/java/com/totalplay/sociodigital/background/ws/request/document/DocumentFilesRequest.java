package com.totalplay.sociodigital.background.ws.request.document;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseRequestInterface;
import com.totalplay.sociodigital.background.ws.request.LoginRequest;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class DocumentFilesRequest extends RealmObject implements WSBaseRequestInterface {
    @SerializedName("Login")
    public LoginRequest login;
    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();
    @Expose(serialize = false, deserialize = false)
    public String idLocalStorage;
    @SerializedName("DocumentosAdjuntos")
    public DocumentListBean documentAdd;
}
