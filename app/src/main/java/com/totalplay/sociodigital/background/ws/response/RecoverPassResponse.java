package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

/**
 * Created by imacbookpro on 13/09/18
 * SocioDigital
 */

public class RecoverPassResponse extends WSBaseResponse {

    @SerializedName("Result")
    public Result mResult;

    public class Result{
        @SerializedName("Result")
        public String result;
        @SerializedName("IdResult")
        public String idResult;
        @SerializedName("Description")
        public String description;
    }
}
