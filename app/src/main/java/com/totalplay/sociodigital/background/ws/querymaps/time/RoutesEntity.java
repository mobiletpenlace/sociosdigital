package com.totalplay.sociodigital.background.ws.querymaps.time;

import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class RoutesEntity {
    private BoundsEntity bounds;
    private String copyrights;

    private OverviewPolylineEntity overview_polyline;
    private String summary;
    private List<LegsEntity> legs;
    private List<?> warnings;
    private List<?> waypoint_order;

    public BoundsEntity getBounds() {
        return bounds;
    }

    public void setBounds(BoundsEntity bounds) {
        this.bounds = bounds;
    }

    public String getCopyrights() {
        return copyrights;
    }

    public void setCopyrights(String copyrights) {
        this.copyrights = copyrights;
    }

    public OverviewPolylineEntity getOverview_polyline() {
        return overview_polyline;
    }

    public void setOverview_polyline(OverviewPolylineEntity overview_polyline) {
        this.overview_polyline = overview_polyline;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<LegsEntity> getLegs() {
        return legs;
    }

    public void setLegs(List<LegsEntity> legs) {
        this.legs = legs;
    }

    public List<?> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<?> warnings) {
        this.warnings = warnings;
    }

    public List<?> getWaypoint_order() {
        return waypoint_order;
    }

    public void setWaypoint_order(List<?> waypoint_order) {
        this.waypoint_order = waypoint_order;
    }

    public static class BoundsEntity {

        private NortheastEntity northeast;

        private SouthwestEntity southwest;

        public NortheastEntity getNortheast() {
            return northeast;
        }

        public void setNortheast(NortheastEntity northeast) {
            this.northeast = northeast;
        }

        public SouthwestEntity getSouthwest() {
            return southwest;
        }

        public void setSouthwest(SouthwestEntity southwest) {
            this.southwest = southwest;
        }

        public static class NortheastEntity {
            private double lat;
            private double lng;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }
        }

        public static class SouthwestEntity {
            private double lat;
            private double lng;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }
        }
    }

    public static class OverviewPolylineEntity {
        private String points;

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }
    }

    public static class LegsEntity {

        private DistanceEntity distance;
        private DurationEntity duration;
        private String end_address;

        private EndLocationEntity end_location;
        private String start_address;

        private StartLocationEntity start_location;

        private List<StepsEntity> steps;
        private List<?> via_waypoint;

        public DistanceEntity getDistance() {
            return distance;
        }

        public void setDistance(DistanceEntity distance) {
            this.distance = distance;
        }

        public DurationEntity getDuration() {
            return duration;
        }

        public void setDuration(DurationEntity duration) {
            this.duration = duration;
        }

        public String getEnd_address() {
            return end_address;
        }

        public void setEnd_address(String end_address) {
            this.end_address = end_address;
        }

        public EndLocationEntity getEnd_location() {
            return end_location;
        }

        public void setEnd_location(EndLocationEntity end_location) {
            this.end_location = end_location;
        }

        public String getStart_address() {
            return start_address;
        }

        public void setStart_address(String start_address) {
            this.start_address = start_address;
        }

        public StartLocationEntity getStart_location() {
            return start_location;
        }

        public void setStart_location(StartLocationEntity start_location) {
            this.start_location = start_location;
        }

        public List<StepsEntity> getSteps() {
            return steps;
        }

        public void setSteps(List<StepsEntity> steps) {
            this.steps = steps;
        }

        public List<?> getVia_waypoint() {
            return via_waypoint;
        }

        public void setVia_waypoint(List<?> via_waypoint) {
            this.via_waypoint = via_waypoint;
        }

        public static class DistanceEntity {
            private String text;
            private int value;

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }
        }

        public static class DurationEntity {
            private String text;
            private int value;

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }
        }

        public static class EndLocationEntity {
            private double lat;
            private double lng;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }
        }

        public static class StartLocationEntity {
            private double lat;
            private double lng;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }
        }

        public static class StepsEntity {

            private DistanceEntity distance;
            private DurationEntity duration;

            private EndLocationEntity end_location;
            private String html_instructions;

            private PolylineEntity polyline;

            private StartLocationEntity start_location;
            private String travel_mode;

            public DistanceEntity getDistance() {
                return distance;
            }

            public void setDistance(DistanceEntity distance) {
                this.distance = distance;
            }

            public DurationEntity getDuration() {
                return duration;
            }

            public void setDuration(DurationEntity duration) {
                this.duration = duration;
            }

            public EndLocationEntity getEnd_location() {
                return end_location;
            }

            public void setEnd_location(EndLocationEntity end_location) {
                this.end_location = end_location;
            }

            public String getHtml_instructions() {
                return html_instructions;
            }

            public void setHtml_instructions(String html_instructions) {
                this.html_instructions = html_instructions;
            }

            public PolylineEntity getPolyline() {
                return polyline;
            }

            public void setPolyline(PolylineEntity polyline) {
                this.polyline = polyline;
            }

            public StartLocationEntity getStart_location() {
                return start_location;
            }

            public void setStart_location(StartLocationEntity start_location) {
                this.start_location = start_location;
            }

            public String getTravel_mode() {
                return travel_mode;
            }

            public void setTravel_mode(String travel_mode) {
                this.travel_mode = travel_mode;
            }

            public static class DistanceEntity {
                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public int getValue() {
                    return value;
                }

                public void setValue(int value) {
                    this.value = value;
                }
            }

            public static class DurationEntity {
                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public int getValue() {
                    return value;
                }

                public void setValue(int value) {
                    this.value = value;
                }
            }

            public static class EndLocationEntity {
                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class PolylineEntity {
                private String points;

                public String getPoints() {
                    return points;
                }

                public void setPoints(String points) {
                    this.points = points;
                }
            }

            public static class StartLocationEntity {
                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }
        }
    }
}
