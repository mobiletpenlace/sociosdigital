package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class NoCoverageRequest extends BaseRequest {
    @SerializedName("ClientData")
    public ClientData clientData;

    public NoCoverageRequest(String name, String email, String phone, String zipCode, String colony ){
        this.clientData = new ClientData(name, email, phone, zipCode, colony);
    }

    public static class ClientData {
        public ClientData(String name, String email, String phone, String zipCode, String colony){
            this.name = name;
            this.email = email;
            this.phone = phone;
            this.zipCode =  zipCode;
            this.coliny =  colony;
        }

        @SerializedName("Name")
        public String name;
        @SerializedName("Email")
        public String email;
        @SerializedName("Phone")
        public String phone;
        @SerializedName("ZipCode")
        public String zipCode;
        @SerializedName("Colony")
        public String coliny;
    }
}
