package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

/**
 * Created by imacbookpro on 25/09/18.
 * SocioDigital
 */

public class ValidateSocioResponse extends WSBaseResponse {
    @SerializedName("Result")
    public Result mResult;
    @SerializedName("Id")
    public String id;
    @SerializedName("Description")
    public String nameSocio;

    public class Result{
        @SerializedName("IdResult")
        public String idResult;
        @SerializedName("Result")
        public String result;
        @SerializedName("ResultDescription")
        public String resultDescription;
    }


}
