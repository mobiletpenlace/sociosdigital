package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;

public class RegisterSellerResponse extends BaseResponse {
    @SerializedName("IdSocio")
    public String mIdPartner = "";

    @SerializedName("Result")
    public ResultResponse mResult;


    public class ResultResponse{
        @SerializedName("idResult")
        public String mIdResult;

        @SerializedName("Result")
        public String mRlt;

        @SerializedName("ResultDescription")
        public String mResultDescription;

    }
}
