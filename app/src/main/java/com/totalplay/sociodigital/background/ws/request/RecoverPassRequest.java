package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 13/09/18.
 * Socio Digital
 */

public class RecoverPassRequest extends BaseRequest {
    @SerializedName("IdSocio")
    public String idSocio;

    public RecoverPassRequest(String idSocio) {
        this.idSocio = idSocio;
    }
}
