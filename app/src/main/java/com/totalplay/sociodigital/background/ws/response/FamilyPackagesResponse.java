package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class FamilyPackagesResponse extends BaseResponse {
    @SerializedName("Result")
    public String result;
    @SerializedName("Descrption")
    public String description;
    @SerializedName("Planes")
    public ArrayList<Plans> plans = new ArrayList<>();

    public static class Plans {
        @SerializedName("IdPlan")
        public String planId;
        @SerializedName("NombrePlan")
        public String planName;
        @SerializedName("Comentario")
        public String commentary;
        @SerializedName("PrecioProntoPago")
        public String amountPP;
        @SerializedName("PrecioLista")
        public String ammountL;
        @SerializedName("InternetMbps")
        public String internetMbps;
        @SerializedName("CanalesTV")
        public String tvChannels;
        @SerializedName("CanalesHD")
        public String hdChannels;
        @SerializedName("TelefoniasLineas")
        public String telephoneLines;
        @SerializedName("DetallePlan")
        public String planDetail;
        @SerializedName("URLSlsf")
        public String imgPlan2;
        @SerializedName("URLSlsfPromo")
        public String imgPromo;
        @SerializedName("URLApp")
        public String imgPlan;
        @SerializedName("DetallePlanMC_c")
        public String detallePlanMcc;
    }
}
