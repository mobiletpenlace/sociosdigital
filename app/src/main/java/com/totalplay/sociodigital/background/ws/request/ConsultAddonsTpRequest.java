package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.entities.Consult;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class ConsultAddonsTpRequest extends BaseRequest {
    @SerializedName("consultar")
    public Consult mConsult;

    public ConsultAddonsTpRequest(Consult consult) {
        mConsult = consult;
    }
}
