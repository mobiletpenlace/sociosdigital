package com.totalplay.sociodigital.background.ws.response;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ResultBean {
    private String Result;
    private String IdResult;
    private String Description;

    public ResultBean(String result, String idResult, String description) {
        Result = result;
        IdResult = idResult;
        Description = description;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String Result) {
        this.Result = Result;
    }

    public String getIdResult() {
        return IdResult;
    }

    public void setIdResult(String IdResult) {
        this.IdResult = IdResult;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }
}
