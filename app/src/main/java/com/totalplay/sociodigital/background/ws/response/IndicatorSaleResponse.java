package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;
import com.totalplay.sociodigital.model.entities.DataSaleMonth;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 10/09/18.
 * SocioDigital
 */

public class IndicatorSaleResponse extends WSBaseResponse{
    @SerializedName("Result")
    public Result mResult;
    @SerializedName("Datos")
    public ArrayList<DataSaleMonth> mDataSaleMonths = new ArrayList<>();

    public class Result{
        @SerializedName("result")
        public String result;
        @SerializedName("resultId")
        public String resultId;
        @SerializedName("resultDescription")
        public String resultDescription;
    }
}
