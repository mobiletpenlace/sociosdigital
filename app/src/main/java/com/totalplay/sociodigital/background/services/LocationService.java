package com.totalplay.sociodigital.background.services;

import android.Manifest;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.totalplay.sociodigital.library.utils.Constants;

import java.util.concurrent.TimeUnit;
import static com.google.android.gms.location.LocationServices.FusedLocationApi;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class LocationService extends IntentService {
    private static final String TAG = LocationService.class.getSimpleName();

    private static final String ACTION_LOCATION_UPDATED = "location_updated";
    private static final String ACTION_REQUEST_LOCATION = "request_location";
    public static Location mLastLocation;
    private static LocationService instance = null;
    private static GoogleApiClient googleApiClient;

    public LocationService() {
        super(TAG);
    }

    public static IntentFilter getLocationUpdatedIntentFilter() {
        return new IntentFilter(LocationService.ACTION_LOCATION_UPDATED);
    }

    public static void requestLocation(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        intent.setAction(LocationService.ACTION_REQUEST_LOCATION);
        context.startService(intent);
    }
    public static void stopRequestLocation(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        context.stopService(intent);
    }

    public static boolean isInstanceCreated() {
        return instance != null;
    }

    public static LocationService getInstance() {
        return instance;
    }

    public static Location getLastLocation() {
        return mLastLocation;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        if (ACTION_REQUEST_LOCATION.equals(action)) {
            requestLocationInternal();
        } else if (ACTION_LOCATION_UPDATED.equals(action)) {
            locationUpdated(intent);
        }
    }

    /**
     * Called when a location update is requested
     */
    private void requestLocationInternal() {
        Log.v(TAG, ACTION_REQUEST_LOCATION);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();

        // It's OK to use blockingConnect() here as we are running in an
        // IntentService that executes work on a separate (background) thread.
        ConnectionResult connectionResult = googleApiClient.blockingConnect(
                Constants.GOOGLE_API_CLIENT_TIMEOUT_S, TimeUnit.SECONDS);

        if (connectionResult.isSuccess() && googleApiClient.isConnected()) {

            Intent locationUpdatedIntent = new Intent(this, LocationService.class);
            locationUpdatedIntent.setAction(ACTION_LOCATION_UPDATED);

            // Send last known location out first if available
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                Intent lastLocationIntent = new Intent(locationUpdatedIntent);
                lastLocationIntent.putExtra(
                        FusedLocationProviderApi.KEY_LOCATION_CHANGED, location);
                startService(lastLocationIntent);
            }

            // Request new location
            LocationRequest mLocationRequest = new LocationRequest()
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                    .setInterval(120 * 1000);


            FusedLocationApi.requestLocationUpdates(
                    googleApiClient, mLocationRequest,
                    PendingIntent.getService(this, 0, locationUpdatedIntent, 0));
        } else {
            Log.e(TAG, String.format(Constants.GOOGLE_API_CLIENT_ERROR_MSG,
                    connectionResult.getErrorCode()));
        }
    }

    /**
     * Called when the location has been updated
     */
    private void locationUpdated(Intent intent) {
        Log.v(TAG, ACTION_LOCATION_UPDATED);

        // Extra new location
        Location location =
                intent.getParcelableExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED);

        if (location != null) {
            mLastLocation = location;
            if (googleApiClient != null) {
                googleApiClient.disconnect();
                instance = null;
            }

            // EventBus.getDefault().post(new EventLocationChanged(
            //        new LatLng(location.getLatitude(), location.getLongitude())));

            Intent batteryIntent = getApplicationContext().registerReceiver(new BroadcastReceiver() {
                                                                                @Override
                                                                                public void onReceive(Context context, Intent intent) {

                                                                                }
                                                                            },
                    new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            int rawLevel;
            if (batteryIntent != null) {
                rawLevel = batteryIntent.getIntExtra("level", -1);
                double scale = batteryIntent.getIntExtra("scale", -1);
                double level = -1;
                if (rawLevel >= 0 && scale > 0) {
                    level = rawLevel / scale;
                }
            } else {
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        instance = null;
    }
}
