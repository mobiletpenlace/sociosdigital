package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class NoCoverageResponse {
    @SerializedName("Result")
    public String result;
    @SerializedName("IdResult")
    public String idResult;
    @SerializedName("Description")
    public String description;
}
