package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseRequest;

/**
 * Created by imacbookpro on 10/09/18.
 * SocioDigital
 */

public class IndicatorRecomendRequest extends WSBaseRequest {
    @SerializedName("UserId")
    public String userId;
    @SerializedName("FechaInicio")
    public String dateStart;
    @SerializedName("FechaFin")
    public String dateEnd;
    @SerializedName("Tipo")
    public String type;


    @SerializedName("login")
    public LoginRequest loginRequest = new LoginRequest();


    public IndicatorRecomendRequest(String userId, String dateStart, String dateEnd, String type) {
        this.userId = userId;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.type =  type;
    }
}
