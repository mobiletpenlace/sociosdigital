package com.totalplay.sociodigital.background.ws.querymaps;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.background.ws.querymaps.time.AddressComponents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class Results {
    @SerializedName("formatted_address")
    private String formattedAddress;
    @SerializedName("geometry")
    private Geometry geometry;
    @SerializedName("place_id")
    private String placeId;
    @SerializedName("address_components")
    private List<AddressComponents> addressComponents;
    @SerializedName("types")
    private List<String> types;

    public Results(String formattedAddress, Geometry geometry, String placeId, List<AddressComponents> addressComponents, List<String> types) {
        this.formattedAddress = formattedAddress;
        this.geometry = geometry;
        this.placeId = placeId;
        this.addressComponents = addressComponents;
        this.types = types;
    }

    public Results() {
        this.formattedAddress = "";
        this.geometry = new Geometry();
        this.placeId = "";
        this.addressComponents = new ArrayList<>();
        this.types = new ArrayList<>();
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public List<AddressComponents> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponents> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
