package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.entities.DireccionBean;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ValidateCoverageResponse extends BaseResponse {

    @SerializedName("CalculaFactibilidad")
    public Feasibility feasibility;
    public DireccionBean direccionBean;

    public class Feasibility {
        @SerializedName("factible")
        public String isfeasible;
        @SerializedName("factibilidad")
        public String factibility;
        @SerializedName("direccion")
        public String address;
        @SerializedName("porcentaje")
        public String porcent;
        @SerializedName("criterios")
        public String judgment;
        @SerializedName("comentarios")
        public String comments;
        @SerializedName("IdRegion")
        public String regionId;
        @SerializedName("Region")
        public String region;
        @SerializedName("Cuidad")
        public String city;
        @SerializedName("distrito")
        public String district;
        @SerializedName("zona")
        public String zone;
        @SerializedName("nombre_cluster")
        public String cluster;
        @SerializedName("CategoryService")
        public String categoryService;

        @SerializedName("Detalle_Respuesta")
        public ResponseDetail mResponseDetail;
    }

    public class ResponseDetail{
        @SerializedName("FechaRespuesta")
        public String dateResp;
        @SerializedName("CodigoRespuesta")
        public String codeResp;
        @SerializedName("CodigoError")
        public String codeError;
        @SerializedName("DescripcionError")
        public String descriptionError;
        @SerializedName("MensajeError")
        public String msgError;
    }

}
