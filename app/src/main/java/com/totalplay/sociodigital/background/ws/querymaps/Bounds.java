package com.totalplay.sociodigital.background.ws.querymaps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class Bounds {

    /**
     * northeast : {"lat":19.3172488,"lng":-99.16147219999999}
     * southwest : {"lat":19.3157652,"lng":-99.16277769999999}
     */


    @SerializedName("northeast")
    private Northeast northeast;
    @SerializedName("southwest")
    private Southwest southwest;

    public Bounds() {
        this.northeast = new Northeast();
        this.southwest = new Southwest();
    }

    public Northeast getNortheast() {
        return northeast;
    }

    public void setNortheast(Northeast northeast) {
        this.northeast = northeast;
    }

    public Southwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }

    public static class Northeast {


        @SerializedName("lat")
        private double lat;
        @SerializedName("lng")
        private double lng;

        public Northeast() {
            this.lat = 0.0d;
            this.lng = 0.0d;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public static class Southwest {
        @SerializedName("lat")
        private double lat;
        @SerializedName("lng")
        private double lng;

        /**
         * lat : 19.3157652
         * lng : -99.16277769999999
         */

        public Southwest() {
            this.lat = 0.0d;
            this.lng = 0.0d;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }
}
