package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class ConsultaCPResponse extends WSBaseResponse{

    @SerializedName("Result")
    public Result mResult;
    @SerializedName("ArrColonias")
    public ArrayList<Colony> mColonies;


    public class Result{
        @SerializedName("Result")
        public String result;
        @SerializedName("IdResult")
        public String idResult;
        @SerializedName("Description")
        public String description;
    }

    public class Colony{
        @SerializedName("Id")
        public String id;
        @SerializedName("Nombre")
        public String name;
        @SerializedName("Colonia")
        public String colony;
        @SerializedName("Ciudad")
        public String city;
        @SerializedName("Clave")
        public String qey;
        @SerializedName("Cluster")
        public String cluster;
        @SerializedName("DelegacionMunicipio")
        public String delegation;
        @SerializedName("Estado")
        public String state;
        @SerializedName("IdEstadoEN")
        public String idState;
        @SerializedName("IdEstadoTP")
        public String isStateTp;
        @SerializedName("IdMunicipioEN")
        public String idMuni;
        @SerializedName("IdMunicipioTP")
        public String idMuniTp;
        @SerializedName("idOrigen")
        public String idOrigin;
        @SerializedName("IdPoblacionEN")
        public String idPoblation;
        @SerializedName("IdPoblacionTP")
        public String idPoblationTp;
        @SerializedName("IdRegionEN")
        public String idRegion;
        @SerializedName("IdRegionTP")
        public String idRegionTp;
        @SerializedName("Limite")
        public String limit;
        @SerializedName("Limite1")
        public String limit1;
        @SerializedName("AplicaEstimuloFiscal")
        public String appEstimFiscal;
    }
}
