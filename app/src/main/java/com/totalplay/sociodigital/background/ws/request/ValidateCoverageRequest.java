package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.entities.ConverageInput;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ValidateCoverageRequest extends BaseRequest {
    @SerializedName("Coordenadas")
    public ConverageInput mConverageInput;

    public ValidateCoverageRequest(ConverageInput converageInput) {
        mConverageInput = converageInput;
    }
}
