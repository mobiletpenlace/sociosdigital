package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class FamilyPackagesRequest extends BaseRequest {
    @SerializedName("Planes")
    public ArrayList<IdPlans> idPlans;

    public static class IdPlans{
        @SerializedName("IdPlan")
        public String idPlan;

        public IdPlans(){
            this.idPlan = "";
        }
    }
}
