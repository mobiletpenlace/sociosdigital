package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;

public class RegisterReferredResponse extends BaseResponse {
    @SerializedName("result")
    public String mres;

    @SerializedName("resultDescription")
    public String mResultDescription;

}
