package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;
import com.totalplay.sociodigital.model.entities.InfoAddons;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class ConsultAddonsResponse extends WSBaseResponse {

    @SerializedName("Result")
    public Result mResult;

    @SerializedName("infoAddons")
    public InfoAddons mInfoAddons;

    public class Result{
        @SerializedName("IdResult")
        public String idResult;
        @SerializedName("Result")
        public String result;
        @SerializedName("ResultDescription")
        public String resultDescription;
    }


}
