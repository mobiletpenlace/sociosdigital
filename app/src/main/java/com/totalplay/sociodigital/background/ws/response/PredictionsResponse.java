package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class PredictionsResponse {
    @SerializedName("status")
    public String status;
    @SerializedName("predictions")
    public List<PredictionsEntity> predictions;

    public static class PredictionsEntity {
        @SerializedName("description")
        public String description;
        @SerializedName("id")
        public String id;
        @SerializedName("place_id")
        public String placeId;
        @SerializedName("reference")
        public String reference;
        @SerializedName("matched_substrings")
        public List<MatchedSubstringsEntity> matchedSubstrings;
        @SerializedName("terms")
        public List<TermsEntity> terms;
        @SerializedName("types")
        public List<String> types;

        public static class MatchedSubstringsEntity {
            @SerializedName("length")
            public int length;
            @SerializedName("offset")
            public int offset;
        }

        public static class TermsEntity {
            @SerializedName("offset")
            public int offset;
            @SerializedName("value")
            public String value;
        }
    }
}
