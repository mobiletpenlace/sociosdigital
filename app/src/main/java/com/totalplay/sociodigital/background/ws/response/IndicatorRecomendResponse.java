package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;
import com.totalplay.sociodigital.model.entities.DataSaleMonth;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 11/09/18.
 * SocioDigital
 */

public class IndicatorRecomendResponse extends WSBaseResponse {
    @SerializedName("Result")
    public IndicatorSaleResponse.Result mResult;
    @SerializedName("Datos")
    public ArrayList<DataSaleMonthRecomend> mDataSaleMonths = new ArrayList<>();

    public class Result{
        @SerializedName("result")
        public String result;
        @SerializedName("resultId")
        public String resultId;
        @SerializedName("resultDescription")
        public String resultDescription;
    }
}
