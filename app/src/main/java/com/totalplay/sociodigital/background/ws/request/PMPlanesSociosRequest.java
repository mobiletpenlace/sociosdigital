package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.entities.ConsultPlain;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class PMPlanesSociosRequest extends BaseRequest {
    @SerializedName("ConsultaPlanes")
    public ConsultPlain mConsultPlain;

    public PMPlanesSociosRequest(ConsultPlain consultPlain) {
        mConsultPlain = consultPlain;
    }
}
