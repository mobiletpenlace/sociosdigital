package com.totalplay.sociodigital.background.ws.querymaps;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ResultMaps {
    @SerializedName("status")
    private String status;
    @SerializedName("results")
    private List<Results> results;

    public ResultMaps(String status, List<Results> results) {
        this.status = status;
        this.results = results;
    }

    public ResultMaps() {
        this.status = "";
        this.results = new ArrayList<>();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }
}
