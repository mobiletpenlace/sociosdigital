package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class PMPlanesSocioResponse extends WSBaseResponse {


    @SerializedName("result")
    public Result mResult;
    @SerializedName("planesTotalPlay")
    public ArrayList<PlanTotalPlay>planTP = new ArrayList<>();


    public class Result{
        @SerializedName("result")
        public String resultID;
        @SerializedName("idResult")
        public String idResultCode;
        @SerializedName("Operacion Exitosa")
        public String resultDescription;
    }
}
