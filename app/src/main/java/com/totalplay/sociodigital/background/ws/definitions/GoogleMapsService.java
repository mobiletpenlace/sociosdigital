package com.totalplay.sociodigital.background.ws.definitions;

import com.totalplay.sociodigital.background.ws.querymaps.ResultMaps;
import com.totalplay.sociodigital.background.ws.querymaps.time.StimulatedTimeMaps;
import com.totalplay.sociodigital.background.ws.response.PredictionsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by imacbookpro on 28/08/18.
 * SocioDigital
 */

public interface GoogleMapsService {
    @GET("/maps/api/geocode/json")
    Call<ResultMaps> fetchLocation(@Query("latlng") String latlng, @Query("sensor") String sensor);

    @GET("/maps/api/place/autocomplete/json")
    Call<PredictionsResponse> autoComplete(@Query("input") String input,
                                           @Query("location") String location,
                                           @Query("radius") String radius,
                                           @Query("language") String language,
                                           @Query("key") String key);

    @GET("/maps/api/geocode/json")
    Call<ResultMaps> fetchDirectionFromText(@Query("address") String address);

    @GET("/maps/api/directions/json")
    Call<StimulatedTimeMaps> fetchDirection(@Query("origin") String origin, @Query("destination") String destination, @Query("sensor") String sensor);

}
