package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;
import com.totalplay.sociodigital.library.pojos.Data;

public class IndicatorsResponse extends WSBaseResponse {

    @SerializedName("Result")
    public Result mResult;
    @SerializedName("Datos")
    public Data mData;

    public class Result{
        @SerializedName("resultId")
        public String resultId;
        @SerializedName("result")
        public String result;
        @SerializedName("resultDescription")
        public String resultDescription;
    }
}
