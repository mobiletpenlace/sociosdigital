package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

public class LoginSocioRequest extends BaseRequest{
    @SerializedName("NoEmpleado")
    public String mNoEmpleado;

    @SerializedName("Password")
    public String mPassword;

    @SerializedName("IdSistema")
    public String mIdSistema;

    public LoginSocioRequest() {

    }

    public LoginSocioRequest(String mNoEmpleado, String mPassword, String mIdSistema) {
        this.mNoEmpleado = mNoEmpleado;
        this.mPassword = mPassword;
        this.mIdSistema = mIdSistema;
    }
}
