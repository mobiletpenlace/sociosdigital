package com.totalplay.sociodigital.background.ws.definitions;

import com.totalplay.sociodigital.background.ws.request.ReferredRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface WebServicesDefinitionsReferred {
    @POST("/FFMTpe/CrearLeadSF")
    Call<ResponseBody> registerReferred(@Body ReferredRequest referredRequest);
}
