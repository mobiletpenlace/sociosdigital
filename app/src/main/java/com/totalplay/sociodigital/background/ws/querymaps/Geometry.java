package com.totalplay.sociodigital.background.ws.querymaps;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.background.ws.querymaps.time.Location;
import com.totalplay.sociodigital.background.ws.querymaps.time.Viewport;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class Geometry {

    /**
     * bounds : {"northeast":{"lat":19.3172488,"lng":-99.16147219999999},"southwest":{"lat":19.3157652,"lng":-99.16277769999999}}
     * location : {"lat":19.3162171,"lng":-99.162404}
     * location_type : RANGE_INTERPOLATED
     * viewport : {"northeast":{"lat":19.3178559802915,"lng":-99.1607759697085},"southwest":{"lat":19.3151580197085,"lng":-99.1634739302915}}
     */

    @SerializedName("bounds")
    private Bounds bounds;
    @SerializedName("location")
    private Location location;
    @SerializedName("location_type")
    private String locationType;
    @SerializedName("viewport")
    private Viewport viewport;

    public Geometry() {
        this.bounds = new Bounds();
        this.location = new Location();
        this.locationType = "";
        this.viewport = new Viewport();
    }

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }
}
