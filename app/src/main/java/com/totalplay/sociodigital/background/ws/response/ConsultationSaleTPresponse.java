package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class ConsultationSaleTPresponse extends WSBaseResponse {

    @SerializedName("Result")
    public Result mResult;


    public class Result{
        @SerializedName("IdResult")
        public String idResult;
        @SerializedName("Result")
        public String result;
        @SerializedName("ResultDescription")
        public String resultDescription;
    }
}
