package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

public class ReferredRequest extends BaseRequest {
    @SerializedName("canalReferido")
    public String mChannelReferred;

    @SerializedName("subCanalReferido")
    public String subCanalReferido;

    @SerializedName("estatus")
    public String mStatus;

    @SerializedName("nombre")
    public String mNombre;

    @SerializedName("origen")
    public String mOrigin;

    @SerializedName("telefono")
    public String mPhone;

    @SerializedName("email")
    public String mMail;

    @SerializedName("codigoPostal")
    public String codigoPostal;

    @SerializedName("cuentaReferente")
    public String cuentaReferente;

    @SerializedName("IdTecnico")
    public String IdTecnico;

    @SerializedName("IdAuxiliar")
    public String IdAuxiliar;

    @SerializedName("OTFFM")
    public String OTFFM;

    @SerializedName("nombreTecnico")
    public String nombreTecnico;

    @SerializedName("numEmpleadoTecnico")
    public String numEmpleadoTecnico;

    public ReferredRequest(String mChannelReferred, String subCanalReferido, String mStatus, String mNombre, String mOrigin, String mPhone, String mMail, String codigoPostal, String cuentaReferente, String idTecnico, String idAuxiliar, String OTFFM, String nombreTecnico, String numEmpleadoTecnico) {
        this.mChannelReferred = mChannelReferred;
        this.subCanalReferido = subCanalReferido;
        this.mStatus = mStatus;
        this.mNombre = mNombre;
        this.mOrigin = mOrigin;
        this.mPhone = mPhone;
        this.mMail = mMail;
        this.codigoPostal = codigoPostal;
        this.cuentaReferente = cuentaReferente;
        IdTecnico = idTecnico;
        IdAuxiliar = idAuxiliar;
        this.OTFFM = OTFFM;
        this.nombreTecnico = nombreTecnico;
        this.numEmpleadoTecnico = numEmpleadoTecnico;
    }

    public ReferredRequest() {
    }
}

