package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class BaseResponse extends WSBaseResponse {
    @SerializedName("RestFaultElement")
    public RestFaultElementBean restFaultElement;

    public class RestFaultElementBean {
        public String summary;
        public String code;
        public String detail;
    }
}
