package com.totalplay.sociodigital.background.ws.querymaps.time;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class Location {
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;

    public Location() {
        this.lat = 0.0d;
        this.lng = 0.0d;
    }

    @Override
    public String toString() {
        return lat + "," + lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
