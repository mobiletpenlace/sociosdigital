package com.totalplay.sociodigital.background.ws.definitions;

import com.totalplay.sociodigital.background.ws.request.ACDRequest;
import com.totalplay.sociodigital.background.ws.request.ConsultAddonsTpRequest;
import com.totalplay.sociodigital.background.ws.request.ConsultaCPRequest;
import com.totalplay.sociodigital.background.ws.request.ConsultationSaleTPRequest;
import com.totalplay.sociodigital.background.ws.request.FamilyPackagesRequest;
import com.totalplay.sociodigital.background.ws.request.IndicatorRecomendRequest;
import com.totalplay.sociodigital.background.ws.request.IndicatorSaleRequest;
import com.totalplay.sociodigital.background.ws.request.LoginSocioRequest;
import com.totalplay.sociodigital.background.ws.request.NoCoverageRequest;
import com.totalplay.sociodigital.background.ws.request.PlanDetailRequest;
import com.totalplay.sociodigital.background.ws.request.PMPlanesSociosRequest;
import com.totalplay.sociodigital.background.ws.request.ProductMasterPlainsRequest;
import com.totalplay.sociodigital.background.ws.request.RecoverPassRequest;
import com.totalplay.sociodigital.background.ws.request.ReferredRequest;
import com.totalplay.sociodigital.background.ws.request.RegisterSellerRequest;
import com.totalplay.sociodigital.background.ws.request.ValidateCoverageRequest;
import com.totalplay.sociodigital.background.ws.request.ValidateSocioRequest;
import com.totalplay.sociodigital.background.ws.request.document.DocumentFilesRequest;
import com.totalplay.sociodigital.background.ws.response.NoCoverageResponse;
import com.totalplay.sociodigital.background.ws.response.VideosResponse;
import com.totalplay.sociodigital.model.entities.FormalityEntity;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

/**
 * Created by imacbookpro on 28/08/18.
 * SocioDigital
 */

public interface WebServicesDefinition {
    @POST("/Estrategia/PM_Planes")
    Call<ResponseBody> productMasterPlains(@Body ProductMasterPlainsRequest productMasterPlainsRequest);

    @POST("/Estrategia/PlainDetail")
    Call<ResponseBody> planDetail(@Body PlanDetailRequest planDetailRequest);

    @POST("/Estrategia/FamilyPackages")
    Call<ResponseBody> familyPackages(@Body FamilyPackagesRequest familyPackagesRequest);

    @POST("/ventasmovil/Cobertura")
    Call<ResponseBody> validateCoverage(@Body ValidateCoverageRequest validateCoverageRequest);

    @POST("/Estrategia/NoCoverage")
    Call<NoCoverageResponse> noCoverage(@Body NoCoverageRequest noCoverageRequest);

    @POST("/ventasmovil/AgregarOportunidad")
    Call<ResponseBody> sendOpportunity(@Body FormalityEntity formalityEntity);

    @POST("/ventasmovil/AgregarDocumento")
    Call<ResponseBody> queryUploadDocument(@Body DocumentFilesRequest request);

    @POST("/ventasmovil/EnviaACD")
    Call<ResponseBody> acd(@Body ACDRequest acdRequest);

    @POST("/ventasmovil/ConsultaCP")
    Call<ResponseBody> consultaCP(@Body ConsultaCPRequest consultaCPRequest);

    @POST("/SocioDigital/PMPlanesSocio")
    Call<ResponseBody> pmPlanesSocios(@Body PMPlanesSociosRequest pmPlanesSociosRequest);

    @POST("/SocioDigital/ConsultaAddonsTP")
    Call<ResponseBody> consultAddonsTp(@Body ConsultAddonsTpRequest consultAddonsTpRequest);

    @POST("/SocioDigital/CreacionVentaTP")
    Call<ResponseBody> createSale(@Body ConsultationSaleTPRequest consultationSaleTPRequest);

   @POST("/SocioDigital/RegistroSocio")
   Call<ResponseBody> registerSeller(@Body RegisterSellerRequest registerSellerRequest);

   @POST("/SocioDigital/LoginSocioTP")
    Call<ResponseBody> login(@Body LoginSocioRequest loginRequest);


    @POST("/SocioDigital/IndicadorVentas")
    Call<ResponseBody> indicatorSale(@Body IndicatorSaleRequest indicatorSaleRequest);
    
    @POST("/SocioDigital/IndicadorRecomendado")
    Call<ResponseBody> indicatorRecomend(@Body IndicatorRecomendRequest indicatorRecomendRequest);

    @POST("/SocioDigital/RecuperaPassword")
    Call<ResponseBody> recoverPass(@Body RecoverPassRequest recoverPassRequest);

    @POST("/SocioDigital/ValidaSocio")
    Call<ResponseBody> validateSocio(@Body ValidateSocioRequest validateSocioRequest);
}
