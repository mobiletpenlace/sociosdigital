package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseRequest;

/**
 * Created by imacbookpro on 10/09/18.
 * SocioDigital
 */

public class IndicatorSaleRequest extends WSBaseRequest {
    @SerializedName("UserId")
    public String userId;
    @SerializedName("FechaInicio")
    public String dateStart;
    @SerializedName("FechaFin")
    public String dateEnd;

    @SerializedName("login")
    public LoginRequest loginRequest = new LoginRequest();

    public IndicatorSaleRequest(String userId, String dateStart, String dateEnd) {
        this.userId = userId;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }
}
