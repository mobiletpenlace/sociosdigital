package com.totalplay.sociodigital.background.ws.definitions;

import com.totalplay.sociodigital.background.ws.response.CPResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebServiceFree {
    @GET("freeCP")
    Call<CPResponse> getData();

}
