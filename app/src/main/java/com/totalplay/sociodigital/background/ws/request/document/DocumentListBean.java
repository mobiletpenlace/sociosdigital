package com.totalplay.sociodigital.background.ws.request.document;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class DocumentListBean extends RealmObject {
    @SerializedName("OportunidadId")
    public String idOpportunity;
    @SerializedName("CuentaBRM")
    public String brmAccount;
    @SerializedName("Documento")
    public RealmList<DocumentBean> document;
}
