package com.totalplay.sociodigital.background.ws.response;

public class CPResponse {

    private String cp;
    private String municipio;
    private String estado;
    private String[] colonias;


    CPResponse(String cp, String municipio, String estado, String[] colonias){
        this.cp = cp;
        this.municipio = municipio;
        this.estado = estado;
        this.colonias = colonias;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String[] getColonias() {
        return colonias;
    }

    public void setColonias(String[] colonias) {
        this.colonias = colonias;
    }
}
