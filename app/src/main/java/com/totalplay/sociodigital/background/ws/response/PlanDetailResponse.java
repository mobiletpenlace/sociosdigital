package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.pojos.ArrServiciosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrCostoInstalacionBean;
import com.totalplay.sociodigital.model.pojos.ArrProductosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosIncluidosBean;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class PlanDetailResponse extends BaseResponse {
    public ResultBean Result;
    @SerializedName("ArrServiciosIncluidos")
    public ArrayList<ArrServiciosIncluidosBean> ArrServiciosIncluidos;
    @SerializedName("ArrServiciosAdicionales")
    public ArrayList<ArrServiciosAdicionalesBean> ArrServiciosAdicionales;
    @SerializedName("ArrProductosAdicionales")
    public ArrayList<ArrProductosAdicionalesBean> ArrProductosAdicionales;
    @SerializedName("ArrCostoInstalacion")
    public ArrayList<ArrCostoInstalacionBean> ArrCostoInstalacion;
   /* @SerializedName("Cot_PlanServicioSegundo")
    public ArrayList<Cot_PlanServicioSegundo> CotPlanServicioSegundo;*/

    public static class ResultBean {
        /**
         * Result : 0
         * IdResult : 0
         * Description : Successful operation
         */
        public String Result;
        public String IdResult;
        public String Description;
    }
    public interface AdicionalesBean {
        int SERVICE = 1;
        int PRODUCT = 2;

        int getAdicionalType();
    }

}
