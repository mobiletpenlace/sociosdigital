package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imacbookpro on 28/08/18.
 * SocioDigital
 */

public class PlanDetailRequest extends BaseRequest {
    @SerializedName("IdPlanSalesForce")
    public String idPlan;
    @SerializedName("Plaza__c")
    public String city;
    @SerializedName("Colonia__c")
    public String colony;

    public PlanDetailRequest(String idPlan, String city, String colony){
        this.idPlan = idPlan;
        this.city = city;
        this.colony = colony;
    }

    /*@SerializedName("Info")
    public Information information;

    public static class Information{
        public Information(String idPlan){
            this.idPlan = idPlan;
        }

        @SerializedName("IdPlan")
        public String idPlan;
    }*/
}
