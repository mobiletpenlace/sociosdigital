package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ProductMasterPlanesResponse extends BaseResponse implements Serializable{
    @SerializedName("Result")
    public Result result;
    @SerializedName("ArrFamilia")
    public ArrayList<ArrFamily> arrFamily = new ArrayList<>();

    public static class Result {
        @SerializedName("Result")
        public String result;
        @SerializedName("IdResult")
        public String idResult;
        @SerializedName("Description")
        public String description;
    }

    public static class ArrFamily implements Serializable {
        @SerializedName("IdFamilia")
        public String idFamily;
        @SerializedName("NombreFamilia")
        public String nameFamily;
        @SerializedName("ArrPlan")
        public ArrayList<ArrPlan> arrPlan = new ArrayList<>();
    }

    public static class ArrPlan implements Serializable {
        @SerializedName("IdPlan")
        public String planId;
        @SerializedName("NombrePlan")
        public String planName;
    }
}
