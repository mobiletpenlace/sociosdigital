package com.totalplay.sociodigital.background.ws.request.document;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.library.utils.SyncStatus;

import io.realm.RealmObject;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigigtal
 */

public class DocumentBean extends RealmObject {
    @SerializedName("OportunidadId")
    public String idOpportunity;
    @SerializedName("CuentaBRM")
    public String brmAccount;
    @SerializedName("Nombre")
    public String name;
    @SerializedName("Tipo")
    public String type;
    @SerializedName("Body")
    public String body;
    @SerializedName("ContentType")
    public String contentType = SocioDigitalKeys.CONTENT_TYPE;

    @Expose(deserialize = false, serialize = false)
    public int syncStatus = SyncStatus.NOT_SYNCHRONIZED;
    @Expose(deserialize = false, serialize = false)
    public String documentId;

    @Expose(deserialize = false, serialize = false)
    public String thumbnail;

    public DocumentBean() {
        documentId = java.util.UUID.randomUUID().toString();
    }
}
