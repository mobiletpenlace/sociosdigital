package com.totalplay.sociodigital.background.ws.querymaps.time;

import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class StimulatedTimeMaps {
    private String status;

    private List<GeocodedWaypointsEntity> geocoded_waypoints;

    private List<RoutesEntity> routes;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GeocodedWaypointsEntity> getGeocoded_waypoints() {
        return geocoded_waypoints;
    }

    public void setGeocoded_waypoints(List<GeocodedWaypointsEntity> geocoded_waypoints) {
        this.geocoded_waypoints = geocoded_waypoints;
    }

    public List<RoutesEntity> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RoutesEntity> routes) {
        this.routes = routes;
    }
}
