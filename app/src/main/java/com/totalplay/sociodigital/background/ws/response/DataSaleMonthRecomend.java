package com.totalplay.sociodigital.background.ws.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by imacbookpro on 11/09/18.
 * SocioDigital
 */

public class DataSaleMonthRecomend implements Serializable {
    @SerializedName("mes")
    public String month;
    @SerializedName("porContactar")
    public String toContact;
    @SerializedName("porInstalar")
    public String toInstall;
    @SerializedName("NoExitoso")
    public String noSuccessful;
    @SerializedName("ilocalizable")
    public String notLocatable;
    @SerializedName("instalados")
    public String instalated;
}
