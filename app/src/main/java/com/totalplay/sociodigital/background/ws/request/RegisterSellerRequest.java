package com.totalplay.sociodigital.background.ws.request;

import com.google.gson.annotations.SerializedName;
import com.totalplay.sociodigital.model.entities.UserDateBean;

import io.realm.RealmObject;

public class RegisterSellerRequest extends BaseRequest{
   @SerializedName("UserDate")
    public UserDateBean mUserDateBean;
}
