package com.totalplay.sociodigital.background.ws.definitions;

import com.totalplay.sociodigital.background.ws.response.VideosResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface WebServiceDefinitionVideos {
    @GET("videos/apps/?app_code=ventasSocialDigital")
    Call<VideosResponse> videos();

    @GET("/media/videos_apps/{videoUrl}")
    @Streaming
    Call<ResponseBody> downloadVideo(@Path("videoUrl") String video);
}
