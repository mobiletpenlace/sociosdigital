package com.totalplay.sociodigital.presenter.callbacks;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public interface DocumentRegistrationCallback {
    void onSavedDocumentSucces();
}
