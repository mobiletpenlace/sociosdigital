package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.response.IndicatorSaleResponse;

/**
 * Created by imacbookpro on 10/09/18.
 * SocioDigital
 */

public interface IndicatorSaleCallback extends CommonCallback {
    void onSuccessGetSales(IndicatorSaleResponse indicatorSaleResponse);
    void onErrorGetSales();
    void onLoadGetSales();
}
