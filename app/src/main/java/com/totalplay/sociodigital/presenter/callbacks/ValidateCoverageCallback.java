package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.AddressGenerator;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public interface ValidateCoverageCallback {
    void onLoadAddress(AddressGenerator generator);
    void onSuccessChargeDirection(String city);
}
