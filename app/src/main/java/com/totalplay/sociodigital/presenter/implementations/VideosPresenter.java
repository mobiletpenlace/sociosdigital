package com.totalplay.sociodigital.presenter.implementations;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.WebServices;
import com.totalplay.sociodigital.background.ws.response.VideosResponse;
import com.totalplay.sociodigital.model.pojos.Video;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideosPresenter extends WSPresenter {
    private VideosCallback videosCallback;
    public String DFLABS_API = "https://www.appstotalplay.com/";

    public interface VideosCallback{
        void onSuccessDownloadedVideos(List<Video> videos);

        void onErrorDownloadingVideos();

        void onDownloadingVideo();

        void onSuccessDownloadedVideo();

        void onErrorDownloadVideo();
    }



    public VideosPresenter(AppCompatActivity appCompatActivity, VideosCallback videosCallback) {
        super(appCompatActivity);
        this.videosCallback = videosCallback;
    }

    public void getListVideos(){
        MessageUtils.progress(mAppCompatActivity,"Cargando información");
        WebServices.servicesVideos().videos().enqueue(new Callback<VideosResponse>() {
            @Override
            public void onResponse(Call<VideosResponse> call, Response<VideosResponse> response) {
                if (response.isSuccessful()) {
                    VideosResponse videosResponse = response.body();
                    List<Video> videos = videosResponse.videos;
                    for (Video video : videos) {
                        File path = Environment.getExternalStorageDirectory();
                        File file = new File(path, "SocioDigital/" +video.videoUrl);
                        if (file.exists()) {
                            video.downloaded = true;
                        }
                    }
                    MessageUtils.stopProgress();
                    videosCallback.onSuccessDownloadedVideos(videos);
                } else {
                    MessageUtils.stopProgress();
                    videosCallback.onErrorDownloadingVideos();
                }
            }

            @Override
            public void onFailure(Call<VideosResponse> call, Throwable t) {
                t.printStackTrace();
                MessageUtils.stopProgress();
                videosCallback.onErrorDownloadingVideos();
            }
        });
    }

    public void downloadVideo(final Video video) {
        videosCallback.onDownloadingVideo();
        String url = "";
        try {
            URI uri = new URI(DFLABS_API + video.videoUrl);
            String path = uri.getPath();
            url = path.substring(path.lastIndexOf('/') + 1);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        WebServices.servicesVideos().downloadVideo(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        //you can now get your file in the InputStream
                        InputStream is = response.body().byteStream();
                        File path = Environment.getExternalStorageDirectory();
                        File filePath = new File(path, "TotalPlayVideos");
                        if (!filePath.exists())
                            filePath.mkdirs();
                        File file = new File(filePath, video.videoUrl);
                        try (OutputStream output = new FileOutputStream(file)) {
                            byte[] buffer = new byte[4 * 1024]; // or other buffer size
                            int read;

                            while ((read = is.read(buffer)) != -1) {
                                output.write(buffer, 0, read);
                            }
                            output.flush();
                        }
                        videosCallback.onSuccessDownloadedVideo();
                    } catch (IOException e) {
                        e.printStackTrace();
                        videosCallback.onErrorDownloadVideo();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                videosCallback.onErrorDownloadVideo();
            }
        });


    }

}
