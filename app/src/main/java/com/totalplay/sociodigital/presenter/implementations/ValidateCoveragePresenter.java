package com.totalplay.sociodigital.presenter.implementations;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.totalplay.sociodigital.background.AddressGenerator;
import com.totalplay.sociodigital.background.async.WSCoverageValidation;
import com.totalplay.sociodigital.background.services.LocationService;
import com.totalplay.sociodigital.model.entities.DireccionBean;
import com.totalplay.sociodigital.model.entities.FormalityEntity;
import com.totalplay.sociodigital.model.entities.ProspectoBean;
import com.totalplay.sociodigital.model.entities.SignatureEntity;
import com.totalplay.sociodigital.presenter.callbacks.ValidateCoverageCallback;

import java.io.IOException;
import java.util.List;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ValidateCoveragePresenter extends RealmPresenter implements OnMapReadyCallback, GoogleMap.OnMapClickListener{
    private final ValidateCoverageCallback validateCoverageCallback;
    private AddressGenerator mAddressGenerator = new AddressGenerator();
    private GoogleMap mMap;

    public ValidateCoveragePresenter(Context context, ValidateCoverageCallback validateCoverageCallback) {
        super(context);
        this.validateCoverageCallback = validateCoverageCallback;

    }

    public void validateCoverage(WSCoverageValidation.CoverageValidateListener listener, AddressGenerator generator) {
        WSCoverageValidation wsCoverageValidation = new WSCoverageValidation(mContext, generator, listener);
        wsCoverageValidation.execute();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        if (latLng != null) {
            Geocoder geocoder = new Geocoder(mContext);
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 10);
                mAddressGenerator = new AddressGenerator();
                mAddressGenerator.lat = (String.valueOf(latLng.latitude));
                mAddressGenerator.lng = (String.valueOf(latLng.longitude));
                mAddressGenerator.initAddressFromList(addresses);
                mMap.addMarker(new MarkerOptions()
                        .title("Calle: " + mAddressGenerator.street + ", CP:" + mAddressGenerator.zipCode)
                        .position(latLng));

                validateCoverageCallback.onLoadAddress(mAddressGenerator);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        Location mLocation = LocationService.getLastLocation();
        if (mLocation != null) {
            onMapClick(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()));
        }
    }


    public void saveCoverageFormality(final DireccionBean direccionBean) {
        mDataManager.deleteAll();

        mDataManager.tx(tx -> {
            FormalityEntity entity = new FormalityEntity();
            entity.prospectoBean = new ProspectoBean();
            entity.prospectoBean.direccionBean = direccionBean;
            entity.prospectoBean.signature = new SignatureEntity();
            tx.save(entity);
        });
        validateCoverageCallback.onSuccessChargeDirection(direccionBean.city);

    }
}
