package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.request.ConsultAddonsTpRequest;
import com.totalplay.sociodigital.background.ws.response.ConsultAddonsResponse;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public interface ConsultAddonsTpCallback extends CommonCallback {
    void onSucessGetAddons(ConsultAddonsResponse consultAddonsResponse);
    void onErrorGetAddons();
    void onLoadAddons();
}
