package com.totalplay.sociodigital.presenter.implementations;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.sociodigital.library.pojos.DataManager;

import io.realm.Realm;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class RealmPresenter extends BasePresenter {

    public Realm mRealm;
    public DataManager mDataManager;

    RealmPresenter(Context context) {
        super((AppCompatActivity) context);
    }

    @Override
    public void onResume() {
        super.onResume();
        mDataManager = DataManager.validateConnection(mDataManager);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mRealm = Realm.getDefaultInstance();
        mDataManager = DataManager.validateConnection(mDataManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRealm != null && !mRealm.isClosed()) mRealm.close();
        if (mDataManager != null) mDataManager.close();
    }
}
