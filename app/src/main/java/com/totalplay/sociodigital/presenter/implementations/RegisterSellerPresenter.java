package com.totalplay.sociodigital.presenter.implementations;

import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.background.ws.WebServices;
import com.totalplay.sociodigital.background.ws.definitions.WebServiceFree;
import com.totalplay.sociodigital.background.ws.request.RegisterSellerRequest;
import com.totalplay.sociodigital.background.ws.response.CPResponse;
import com.totalplay.sociodigital.background.ws.response.RegisterSellerResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.library.quoter.utils.Constans;
import com.totalplay.sociodigital.library.utils.ResponseUtils;
import com.totalplay.sociodigital.model.entities.CardPartnerBean;
import com.totalplay.sociodigital.model.entities.DirectionBuildingBusinnessBean;
import com.totalplay.sociodigital.model.entities.DirectionPartnerBean;
import com.totalplay.sociodigital.model.entities.UserDateBean;
import com.totalplay.sociodigital.model.pojos.FileRequest;
import com.totalplay.sociodigital.model.pojos.PhotoFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
@SuppressWarnings("all")
public class RegisterSellerPresenter extends WSPresenter {

    private RegisterSellerCallback mRegisterSellerCallback;
    private String nameSelfie = "";
    private String mAccount = "";
    private ArrayList<String> pathArray =  new ArrayList<>();
    private ArrayList<String> typeArray =  new ArrayList<>();
    private List<PhotoFile> saveDoucuments = new ArrayList<>();
    private boolean isAllDocumentsUpload =  true;

    public interface RegisterSellerCallback {
        void OnSuccesRegister(boolean isAllDocumentUpload);
        void onErrorRegister(boolean isAllDocumentUpload);
        void getCP(CPResponse response);
        void startProgress(int imageId, boolean isSync);
    }

    public RegisterSellerPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    public RegisterSellerPresenter(AppCompatActivity appCompatActivity, RegisterSellerCallback mRegisterSellerCallback) {
        super(appCompatActivity);
        this.mRegisterSellerCallback = mRegisterSellerCallback;
    }

    public void registerSeller() {
        RegisterSellerRequest mUserDateBeanRequest = new RegisterSellerRequest();
        mUserDateBeanRequest.mUserDateBean = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mUserDateBeanRequest.mUserDateBean.idSystem = "824";
        //mUserDateBeanRequest.mUserDateBean.idSystem = "723";
        //mUserDateBeanRequest.mUserDateBean.idRoll = "1501";
        mUserDateBeanRequest.mUserDateBean.mPathSelfie = CryptoUtils.crypt(mUserDateBeanRequest.mUserDateBean.mPathSelfie);
        mWSManager.requestWs(RegisterSellerResponse.class, WSManager.WS.REGISTER_SELLER, mUserDateBeanRequest);
    }

    public void sendSelfieToFFMServer(boolean isRetry) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mAccount = userDateEntity.mAccountNumber;
        retrievePathIdentification(isRetry);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.REGISTER_SELLER))
            OnSuccessRegister(requestUrl, (RegisterSellerResponse) baseResponse);
    }

    private void OnSuccessRegister(String requestURL, RegisterSellerResponse registerSellerResponse) {
        MessageUtils.stopProgress();
        if (registerSellerResponse.mIdPartner != null) {
            if (!registerSellerResponse.mIdPartner.equals(""))
                mRegisterSellerCallback.OnSuccesRegister(isAllDocumentsUpload);
        } else {
            mRegisterSellerCallback.onErrorRegister(isAllDocumentsUpload);
            MessageUtils.toast(mAppCompatActivity, "No se ha podido realizar el registro, intente nuevamente");
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.REGISTER_SELLER)) {
            mRegisterSellerCallback.onErrorRegister(isAllDocumentsUpload);
            MessageUtils.toast(mAppCompatActivity, "No se ha podido realizar el registro, intente nuevamente");
        }
    }

    public void saveTypeSeller(String mSubCanal, String typeSeller) {
        UserDateBean userDateEntity = new UserDateBean();
        deleteAll();
        mDataManager.tx(tx -> {
            userDateEntity.mSubCanal = CryptoUtils.crypt(mSubCanal);
            userDateEntity.idRoll = typeSeller;
            tx.save(userDateEntity);
        });
        saveNameTypeSeller("");
    }

    public void saveAccountNumber(String mAccount) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mAccountNumber = CryptoUtils.crypt(mAccount);
            userDateEntity.mNoParter = CryptoUtils.crypt(mAccount);
            tx.save(userDateEntity);
        });
    }

    public UserDateBean getInfoSeller(){
        return mDataManager.queryWhere(UserDateBean.class).findFirst();
    }

    public void saveNameTypeSeller(String mNameTypeSeller) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mNameTypeSeller = CryptoUtils.crypt(mNameTypeSeller);
            tx.save(userDateEntity);
        });
    }

    public void saveNameHaveBuilding(String mNameTypeSeller, String mNumberCoach) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mNameTypeSeller = CryptoUtils.crypt(mNameTypeSeller);
            userDateEntity.mNumberCoach = CryptoUtils.crypt(mNumberCoach);
            tx.save(userDateEntity);
        });
    }

    public void saveDirectionBuilding(DirectionBuildingBusinnessBean buildingBusinnessBean) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mDirectionBuildingBusinnessBean = buildingBusinnessBean;
            tx.save(userDateEntity);
        });
    }

    public void saveDirectionPartner(DirectionPartnerBean directionPartnerBean) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mDirectionPartnerBean = directionPartnerBean;
            tx.save(userDateEntity);
        });
    }

    public void saveDataSellerSectionOne(String mName, String mLastName, String mSecondLastName) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mName = CryptoUtils.crypt(mName);
            userDateEntity.mLastName = CryptoUtils.crypt(mLastName);
            userDateEntity.mSecondLastName = CryptoUtils.crypt(mSecondLastName);
            tx.save(userDateEntity);
        });
    }

    public void saveDataSellerSectionTwo(String mRFC, String mEmail, String mPhone, String mAccounNumber) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        if (mAccounNumber.equals("")) {
            mDataManager.tx(tx -> {
                userDateEntity.mRFC = CryptoUtils.crypt(mRFC);
                userDateEntity.mEmail = CryptoUtils.crypt(mEmail);
                userDateEntity.mPhone = CryptoUtils.crypt(mPhone);
                userDateEntity.mAccountNumber = CryptoUtils.crypt(mPhone);
                userDateEntity.mNoParter = CryptoUtils.crypt(mPhone);
                tx.save(userDateEntity);
            });
        } else {
            mDataManager.tx(tx -> {
                userDateEntity.mRFC = CryptoUtils.crypt(mRFC);
                userDateEntity.mEmail = CryptoUtils.crypt(mEmail);
                userDateEntity.mPhone = CryptoUtils.crypt(mPhone);
                userDateEntity.mAccountNumber = CryptoUtils.crypt(mAccounNumber);
                userDateEntity.mNoParter = CryptoUtils.crypt(mAccounNumber);
                tx.save(userDateEntity);
            });
        }
    }

    public void saveSelfie(String mSelfie, String mNameSelfie) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mSelfie = mSelfie;
            userDateEntity.mNameSelfie = mNameSelfie;
            tx.save(userDateEntity);
        });
    }

    public void saveDataSellerSectionThree(String mIdentificationFront, String mIdentificationBack, String mVoucherAddress, CardPartnerBean cardPartnerBean) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mIdentificationFront = mIdentificationFront;
            userDateEntity.mIdentificationBack = mIdentificationBack;
            userDateEntity.mVoucher = mVoucherAddress;
            userDateEntity.mCardPartnerBean = cardPartnerBean;
            tx.save(userDateEntity);
        });
    }

    public void saveSignature(String mSignature) {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        mDataManager.tx(tx -> {
            userDateEntity.mDigitalSignature = mSignature;
            tx.save(userDateEntity);
        });
    }

    public String getTypeSeller() {
        UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
        return userDateEntity.mSubCanal;
    }

    private void deleteAll() {
        mDataManager.deleteAll();
    }

    private void retrievePathIdentification(boolean isRetry) {
        if(!isRetry){
            List<PhotoFile> listDoucuments = new ArrayList<>();
            UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
            listDoucuments.add(new PhotoFile("IdentificacionFrente", userDateEntity.mIdentificationFront,false));
            listDoucuments.add(new PhotoFile("IdentificacionReverso", userDateEntity.mIdentificationBack, false));
            listDoucuments.add(new PhotoFile("Comprobante", userDateEntity.mVoucher, false));
            listDoucuments.add(new PhotoFile("Firma", userDateEntity.mDigitalSignature, false));
            listDoucuments.add(new PhotoFile("Selfie", userDateEntity.mNameSelfie + ".jpg",false));
            nameSelfie = userDateEntity.mNameSelfie + ".jpg";
            saveDoucuments =  listDoucuments;

            File file = new File(Environment.getExternalStorageDirectory(), "");
            FileRequest fileRequest = new FileRequest();
            fileRequest.path = (String.format("/%s/%s", pathFile(), listDoucuments.get(0)));
            pathArray.clear();
            typeArray.clear();
            new UploadImageFiles().execute(new ParamsAsyntask(file,fileRequest, listDoucuments));
        }else{
            File file = new File(Environment.getExternalStorageDirectory(), "");
            FileRequest fileRequest = new FileRequest();
            fileRequest.path = (String.format("/%s/%s", pathFile(), saveDoucuments.get(0)));
            pathArray.clear();
            typeArray.clear();
            new UploadImageFiles().execute(new ParamsAsyntask(file,fileRequest, saveDoucuments));
        }
    }

    class UploadImageFiles extends AsyncTask<ParamsAsyntask, Void, Void> {
        boolean sendRegisterSeller = false;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(ParamsAsyntask... paramsAsyntasks) {
            for (PhotoFile mFile : paramsAsyntasks[0].mImagesToUpload){
                if (!mFile.isSync)
                    mFile.isSync = syncImage(mFile);
            }

            if ( paramsAsyntasks[0].mImagesToUpload.get(0).isSync &&
                    paramsAsyntasks[0].mImagesToUpload.get(1).isSync &&
                    paramsAsyntasks[0].mImagesToUpload.get(2).isSync &&
                    paramsAsyntasks[0].mImagesToUpload.get(3).isSync &&
                    paramsAsyntasks[0].mImagesToUpload.get(4).isSync){
                for (int i = 0; i < pathArray.size() ; i++){
                    savePathInRealm(pathArray.get(i),typeArray.get(i));
                }
                sendRegisterSeller = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (PhotoFile photo: saveDoucuments){
                isAllDocumentsUpload = isAllDocumentsUpload && photo.isSync;
            }
            if (sendRegisterSeller)
                registerSeller();
            else {
                MessageUtils.toast(mAppCompatActivity, "No se ha podido subir la información");
                MessageUtils.stopProgress();
            }
        }

        void savePathInRealm(String path, String type) {
            mAppCompatActivity.runOnUiThread(() -> {
                UserDateBean userDateEntity = mDataManager.queryWhere(UserDateBean.class).findFirst();
                if (nameSelfie.equals(type)) {
                    mDataManager.tx(tx -> {
                        userDateEntity.mPathSelfie = path;
                        tx.save(userDateEntity);
                    });
                }

                switch (type) {
                    case "IdentificacionFrente.jpg":
                        mDataManager.tx(tx -> {
                            userDateEntity.mIdentificationFront = path;
                            tx.save(userDateEntity);
                        });
                        break;
                    case "IdentificacionReverso.jpg":
                        mDataManager.tx(tx -> {
                            userDateEntity.mIdentificationBack = path;
                            tx.save(userDateEntity);
                        });
                        break;
                    case "Comprobante.jpg":
                        mDataManager.tx(tx -> {
                            userDateEntity.mVoucher = path;
                            tx.save(userDateEntity);
                        });
                        break;
                    case "Firma.jpg":
                        mDataManager.tx(tx -> {
                            userDateEntity.mDigitalSignature = path;
                            tx.save(userDateEntity);
                        });
                        break;
                }
            });
        }
    }

    private String pathFile() {
        final Calendar c = Calendar.getInstance();
        String mAnio = String.valueOf(c.get(Calendar.YEAR));
        String mMont = String.valueOf(c.get(Calendar.MONTH));
        String filesBaseURL = "SociosTotalplay";
        return filesBaseURL + "/" + mAnio + "/" + mMont + "/" + CryptoUtils.desEncrypt(mAccount);
    }

    private boolean syncImage(PhotoFile mImageToUpload){
        String urlResponse;
        boolean imageSuccessSync =  false;
        //4 posibles ciclos
        for (int i = 0; i < 4; i++) {
            if (!mImageToUpload.nameFromRealm.equals("")) {
                File file = new File(Environment.getExternalStorageDirectory(), mImageToUpload.nameFromRealm);
                FileRequest fileRequest = new FileRequest();
                fileRequest.path = CryptoUtils.crypt(String.format("/%s/%s", pathFile(),mImageToUpload.nameImage));
                MediaType TYPE_MULTIPART = MediaType.parse("multipart/form-data");
                RequestBody path = RequestBody.create(TYPE_MULTIPART, fileRequest.path);
                RequestBody requestFile = RequestBody.create(TYPE_MULTIPART, file);
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);
                Call<ResponseBody> mFileCall = WebServices.servicesImages().writeFile(filePart, path);
                try {
                    Response<ResponseBody> response = mFileCall.execute();
                    if (response.isSuccessful()) {
                        urlResponse = CryptoUtils.desEncrypt(ResponseUtils.getServerURL(response));
                        pathArray.add(urlResponse);
                        typeArray.add(mImageToUpload.nameFromRealm);
                        Log.e("URL", urlResponse);
                        imageSuccessSync = true;
                        setProgress(mImageToUpload.nameImage, true);
                        break;
                    } else {
                        if (i == 3)
                            setProgress(mImageToUpload.nameImage, false);
                        continue;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (i == 3)
                        setProgress(mImageToUpload.nameImage, false);
                    continue;
                }
            }
        }
        return  imageSuccessSync;
    }

    private void setProgress(String nameImage, boolean isSync){
        switch (nameImage){
            case "IdentificacionFrente":
                mRegisterSellerCallback.startProgress(0, isSync);
                break;
            case "IdentificacionReverso":
                mRegisterSellerCallback.startProgress(1, isSync);
                break;
            case "Comprobante":
                mRegisterSellerCallback.startProgress(2, isSync);
                break;
            case "Firma":
                mRegisterSellerCallback.startProgress(3, isSync);
                break;
            case "Selfie":
                mRegisterSellerCallback.startProgress(4, isSync);
                break;
        }
    }

    public void loadCP(String cp){
        MessageUtils.progress(mAppCompatActivity, "Cargando información");
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.URL_CP+cp+"/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        WebServiceFree restClient = retrofit.create(WebServiceFree.class);
        Call<CPResponse> call = restClient.getData();
        call.enqueue(new Callback<CPResponse>() {
            @Override
            public void onResponse(Call<CPResponse> call, Response<CPResponse> response) {
                switch (response.code()) {
                    case 200:
                        MessageUtils.stopProgress();
                        CPResponse data = response.body();
                        data.setEstado(response.body().getEstado());
                        data.setMunicipio(response.body().getMunicipio());
                        data.setColonias(response.body().getColonias());
                        System.out.print(data.getEstado());
                        mRegisterSellerCallback.getCP(data);
                        android.util.Log.e("estadoes:",""+data.getEstado());
                        break;
                    case 401:
                        MessageUtils.toast(mContext, "No se encontró información");
                        MessageUtils.stopProgress();
                        break;
                    default:
                        MessageUtils.toast(mContext, "No se encontró información");
                        MessageUtils.stopProgress();
                        break;
                }
            }
            @Override
            public void onFailure(Call<CPResponse> call, Throwable t) {

            }
        });
    }
}

class ParamsAsyntask {
    List<PhotoFile> mImagesToUpload;
    ParamsAsyntask(File mFile, FileRequest mFileRequest, List<PhotoFile> imagesToUpload) {
        this.mImagesToUpload =  imagesToUpload;
    }
}

