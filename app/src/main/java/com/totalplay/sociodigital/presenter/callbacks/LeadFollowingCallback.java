package com.totalplay.sociodigital.presenter.callbacks;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public interface LeadFollowingCallback {
    void onSuccessSavedProposal();
}
