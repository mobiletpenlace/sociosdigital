package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.response.IndicatorRecomendResponse;
import com.totalplay.sociodigital.background.ws.response.IndicatorSaleResponse;
import com.totalplay.sociodigital.background.ws.response.IndicatorsResponse;

/**
 * Created by imacbookpro on 11/09/18.
 * SocioDigital
 */

public interface IndicatorRecomendCallback {
    void onSuccessGetRecomend(IndicatorsResponse indicatorsResponse, String type);
    void onErrorGetRecomend();
    void onLoadGetRecomend();
}