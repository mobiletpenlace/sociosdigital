package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public interface AddonSelectorCallback extends CommonCallback {
    void onLoadAddons();
    void onErrorAddons();
    void onSuccessAddons(PlanDetailResponse planDetailResponse);
}
