package com.totalplay.sociodigital.presenter.implementations;

import android.content.Context;

import com.totalplay.sociodigital.model.entities.FormalityEntity;
import com.totalplay.sociodigital.model.entities.PropuestaBean;
import com.totalplay.sociodigital.model.pojos.ArrProductosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosAdicionalesBean;
import com.totalplay.sociodigital.presenter.callbacks.LeadFollowingCallback;

import java.util.List;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class AdditionalServicesPresenter extends RealmPresenter implements LeadFollowingCallback{
    private LeadFollowingCallback mLeadFollowingCallback;

    public AdditionalServicesPresenter(Context context, LeadFollowingCallback leadFollowingCallback) {
        super(context);
        this.mLeadFollowingCallback = leadFollowingCallback;
    }


    public void savePackage(List<ArrServiciosAdicionalesBean> addonServices, List<ArrProductosAdicionalesBean> arrAddonProd, PropuestaBean propuestaBean, Double listPrice, Double priceSoonPaymen) {
        final FormalityEntity entity = mDataManager.queryWhere(FormalityEntity.class).findFirst();
        mDataManager.tx(tx -> {
            entity.prospectoBean.proposal = propuestaBean;
            entity.listPrice = listPrice;
            entity.priceSoonPaymen = priceSoonPaymen;
            entity.addonServices.clear();
            entity.addonProducts.clear();
            entity.addonServices.addAll(addonServices);
            entity.addonProducts.addAll(arrAddonProd);
            tx.save(propuestaBean);
            tx.save(entity);
        });
        mLeadFollowingCallback.onSuccessSavedProposal();
    }

    @Override
    public void onSuccessSavedProposal() {

    }
}
