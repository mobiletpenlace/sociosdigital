package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.response.FamilyPackagesResponse;

/**
 * Created by imacbookpro on 31/08/18.
 */

public interface FamilyPackagesCallback extends CommonCallback {
    void onLoaFamilyPackages();
    void onSuccessFamilyPackages(FamilyPackagesResponse familyPackageResponse);
    void onErrorPackagesFamily();
}
