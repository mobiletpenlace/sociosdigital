package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.response.ConsultaCPResponse;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public interface ConsultaCpCallback extends CommonCallback{
    void onSuccessConsultaCp(ConsultaCPResponse mConsultaCPResponse);
    void onErrorConsultaCp();
}
