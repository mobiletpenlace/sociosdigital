package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.totalplay.sociodigital.background.ws.request.FamilyPackagesRequest;
import com.totalplay.sociodigital.background.ws.response.FamilyPackagesResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.presenter.callbacks.FamilyPackagesCallback;

import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by imacbookpro on 31/08/18.
 * SocioDigital
 */

public class FamilyPackagesPresenter extends WSPresenter {
    private FamilyPackagesCallback familyPackagesCallback;
    private Call<FamilyPackagesResponse> familyPackagesCall;

    public FamilyPackagesPresenter(AppCompatActivity appCompatActivity, FamilyPackagesCallback familyPackagesCallback) {
        super(appCompatActivity);
        this.familyPackagesCallback = familyPackagesCallback;
    }

    public void loadFamilyPackages(ArrayList<FamilyPackagesRequest.IdPlans> idPlansArrayList) {
        FamilyPackagesRequest familyPackagesRequest = new FamilyPackagesRequest();
        familyPackagesRequest.idPlans = idPlansArrayList;
        mWSManager.requestWs(FamilyPackagesResponse.class, WSManager.WS.FAMILY_PACKAGES, familyPackagesRequest);
    }

    private void onSuccessLoadFamilyPackages(FamilyPackagesResponse familyPackagesResponse) {
        if (familyPackagesResponse.result.equals("0")) {
            familyPackagesCallback.onSuccessFamilyPackages(familyPackagesResponse);
        } else {
            familyPackagesCallback.onErrorPackagesFamily();
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.FAMILY_PACKAGES)) {
            familyPackagesCallback.onLoaFamilyPackages();
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.FAMILY_PACKAGES)) {
            onSuccessLoadFamilyPackages((FamilyPackagesResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        if (requestUrl.equals(WSManager.WS.FAMILY_PACKAGES)) {
            familyPackagesCallback.onErrorPackagesFamily();
        }
    }

    @Override
    public void onErrorConnection() {
        familyPackagesCallback.onErrorConnection();
    }
}
