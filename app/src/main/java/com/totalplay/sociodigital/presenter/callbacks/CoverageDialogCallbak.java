package com.totalplay.sociodigital.presenter.callbacks;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public interface CoverageDialogCallbak {
    void closeDialog();
}
