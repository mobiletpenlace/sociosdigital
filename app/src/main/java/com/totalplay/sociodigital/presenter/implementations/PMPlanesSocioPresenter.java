package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.totalplay.sociodigital.background.ws.request.ProductMasterPlainsRequest;
import com.totalplay.sociodigital.background.ws.response.PMPlanesSocioResponse;
import com.totalplay.sociodigital.background.ws.response.ProductMasterPlanesResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.presenter.callbacks.PMPlanesCallback;

import retrofit2.Call;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public class PMPlanesSocioPresenter  {
    /*
    private Call<PMPlanesSocioResponse> mPmPlanesResponseCall;
    private PMPlanesCallback pmPlanesCallback;
    private String mYypePackage;

    public PMPlanesSocioPresenter(AppCompatActivity appCompatActivity, PMPlanesCallback pmPlanesCallback) {
        super(appCompatActivity);
        this.pmPlanesCallback = pmPlanesCallback;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPmPlanesResponseCall != null && mPmPlanesResponseCall.isExecuted())
            mPmPlanesResponseCall.cancel();
    }

    public void loadPMPlanes(final String typePackage, String city) {
        mYypePackage = typePackage;
        ProductMasterPlainsRequest productMasterPlainsRequest = new ProductMasterPlainsRequest();
        productMasterPlainsRequest.information = new ProductMasterPlainsRequest.Information(typePackage, city);
        mWSManager.requestWs(ProductMasterPlanesResponse.class, WSManager.WS.PM_PLANES, productMasterPlainsRequest);
    }

    private void onSuccessLoadPMPlanes(ProductMasterPlanesResponse productMasterPlanesResponse) {
        if (productMasterPlanesResponse.result.idResult != null && productMasterPlanesResponse.result.result.equals("0")) {
            pmPlanesCallback.onSuccessPMPlanes(productMasterPlanesResponse, mYypePackage);
        } else {
            pmPlanesCallback.onErrorPMPlanes();
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.PM_PLANES)) {
            pmPlanesCallback.onLoadPMPlanes();
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.PM_PLANES)) {
            onSuccessLoadPMPlanes((ProductMasterPlanesResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        if (requestUrl.equals(WSManager.WS.PM_PLANES)) {
            pmPlanesCallback.onErrorPMPlanes();
        }
    }

    @Override
    public void onErrorConnection() {
        pmPlanesCallback.onErrorConnection();
    }
    */
}
