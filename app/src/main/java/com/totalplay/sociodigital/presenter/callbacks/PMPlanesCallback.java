package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.response.PMPlanesSocioResponse;
import com.totalplay.sociodigital.background.ws.response.ProductMasterPlanesResponse;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public interface PMPlanesCallback extends CommonCallback {
    void onLoadPMPlanes();

    void onSuccessPMPlanes(PMPlanesSocioResponse pmPlanesSocioResponse);

    void onErrorPMPlanes();
}
