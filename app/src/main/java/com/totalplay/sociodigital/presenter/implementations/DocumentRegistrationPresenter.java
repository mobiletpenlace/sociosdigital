package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.sociodigital.background.ws.request.document.DocumentFilesRequest;
import com.totalplay.sociodigital.model.entities.FormalityEntity;
import com.totalplay.sociodigital.presenter.callbacks.DocumentRegistrationCallback;

/**
 * Created by imacbookpro on 07/09/18.
 * SocioDigital
 */

public class DocumentRegistrationPresenter extends WSPresenter {
    private final DocumentRegistrationCallback documentRegistrationCallback;

    public DocumentRegistrationPresenter(AppCompatActivity appCompatActivity, DocumentRegistrationCallback documentRegistrationCallback) {
        super(appCompatActivity);
        this.documentRegistrationCallback = documentRegistrationCallback;
    }

    public void saveDocuments(final DocumentFilesRequest mFileDocumentRequest) {
        mDataManager.tx(tx -> {
            mDataManager.queryWhere(DocumentFilesRequest.class).delete();
            FormalityEntity entity = mDataManager.queryWhere(FormalityEntity.class).findFirst();
            mFileDocumentRequest.idLocalStorage = entity.uuid;
            tx.save(mFileDocumentRequest);
        });
        documentRegistrationCallback.onSavedDocumentSucces();
    }
}
