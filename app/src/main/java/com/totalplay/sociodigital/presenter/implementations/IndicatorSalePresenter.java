package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.background.ws.request.ConsultaCPRequest;
import com.totalplay.sociodigital.background.ws.request.IndicatorSaleRequest;
import com.totalplay.sociodigital.background.ws.response.ConsultaCPResponse;
import com.totalplay.sociodigital.background.ws.response.IndicatorSaleResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.presenter.callbacks.ConsultaCpCallback;
import com.totalplay.sociodigital.presenter.callbacks.IndicatorSaleCallback;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class IndicatorSalePresenter extends WSPresenter {
    IndicatorSaleCallback mIndicatorSaleCallback;

    public IndicatorSalePresenter(AppCompatActivity appCompatActivity, IndicatorSaleCallback indicatorSaleCallback) {
        super(appCompatActivity);
        this.mIndicatorSaleCallback = indicatorSaleCallback;
    }

    public void getSales(String userId , String dateStart, String dateEnd){
        IndicatorSaleRequest indicatorSaleRequest =  new IndicatorSaleRequest(userId,dateStart,dateEnd);
        mWSManager.requestWs(IndicatorSaleResponse.class, WSManager.WS.INDICATOR_SALE,indicatorSaleRequest);
    }

    private void successGetSales(String requestUrl,IndicatorSaleResponse response){
        if (response.mResult.result != null && response.mResult.result.equals("1")){
            mIndicatorSaleCallback.onSuccessGetSales(response);
        }
        else {
           onErrorLoadResponse(requestUrl,response.mResult.resultDescription);
        }
    }


    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        successGetSales(requestUrl, (IndicatorSaleResponse) baseResponse);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        super.onRequestWS(requestUrl);
        mIndicatorSaleCallback.onLoadGetSales();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        MessageUtils.stopProgress();
        mIndicatorSaleCallback.onErrorGetSales();
    }
}
