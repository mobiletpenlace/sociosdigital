package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.totalplay.sociodigital.background.ws.request.ConsultAddonsTpRequest;
import com.totalplay.sociodigital.background.ws.response.ConsultAddonsResponse;
import com.totalplay.sociodigital.background.ws.response.PMPlanesSocioResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.library.pojos.DataManager;
import com.totalplay.sociodigital.model.entities.Consult;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.callbacks.ConsultAddonsTpCallback;

import io.realm.Realm;
import retrofit2.Call;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class ConsultAddonsTpPresenter extends WSPresenter {
    private Call<PMPlanesSocioResponse> mPmPlanesResponseCall;
    private ConsultAddonsTpCallback mConsultAddonsTpCallback;
    private String mYypePackage;

    public ConsultAddonsTpPresenter(AppCompatActivity appCompatActivity, ConsultAddonsTpCallback consultAddonsTpCallback) {
        super(appCompatActivity);
        this.mConsultAddonsTpCallback = consultAddonsTpCallback;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPmPlanesResponseCall != null && mPmPlanesResponseCall.isExecuted())
            mPmPlanesResponseCall.cancel();
    }

    public void loadAddons(Consult mConsult) {
        ConsultAddonsTpRequest consultAddonsTpRequest = new ConsultAddonsTpRequest(mConsult);
        mWSManager.requestWs(ConsultAddonsResponse.class, WSManager.WS.CONSULT_ADDONS_TP, consultAddonsTpRequest);
    }

    private void onSuccessLoadPMPlanes(ConsultAddonsResponse response) {
        if (response.mResult.result != null && response.mResult.result.equals("0")) {
            mConsultAddonsTpCallback.onSucessGetAddons(response);
        } else {
            mConsultAddonsTpCallback.onErrorGetAddons();
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.CONSULT_ADDONS_TP)) {
            mConsultAddonsTpCallback.onLoadAddons();
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.CONSULT_ADDONS_TP)) {
            onSuccessLoadPMPlanes((ConsultAddonsResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        if (requestUrl.equals(WSManager.WS.PM_PLANES)) {
            mConsultAddonsTpCallback.onErrorGetAddons();
        }
    }

    @Override
    public void onErrorConnection() {
        mConsultAddonsTpCallback.onErrorConnection();
    }

}
