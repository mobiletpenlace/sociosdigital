package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.background.ws.request.IndicatorSaleRequest;
import com.totalplay.sociodigital.background.ws.request.RecoverPassRequest;
import com.totalplay.sociodigital.background.ws.response.IndicatorSaleResponse;
import com.totalplay.sociodigital.background.ws.response.RecoverPassResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.presenter.callbacks.IndicatorSaleCallback;
import com.totalplay.sociodigital.presenter.callbacks.RecoverPassCallbak;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class RecoverPassPresenter extends WSPresenter {
    RecoverPassCallbak mRecoverPassCallbak;

    public RecoverPassPresenter(AppCompatActivity appCompatActivity, RecoverPassCallbak recoverPassCallbak) {
        super(appCompatActivity);
        this.mRecoverPassCallbak =  recoverPassCallbak;
    }

    public void getRecoverPass(String noSocio){
       RecoverPassRequest recoverPassRequest =  new RecoverPassRequest(noSocio);
       mWSManager.requestWs(RecoverPassResponse.class, WSManager.WS.RECOVER_PASS,recoverPassRequest);
    }

    private void successRecover(String requestUrl,RecoverPassResponse response){
        if (response.mResult.result != null && response.mResult.idResult.equals("0")){
            mRecoverPassCallbak.successRecoverPass();
        }
        else {
           onErrorLoadResponse(requestUrl,response.mResult.description);
        }
    }


    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        successRecover(requestUrl, (RecoverPassResponse) baseResponse);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        super.onRequestWS(requestUrl);
        mRecoverPassCallbak.onLoadRecoverPass();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        MessageUtils.stopProgress();
        mRecoverPassCallbak.onErrorRecoverPass();
    }
}
