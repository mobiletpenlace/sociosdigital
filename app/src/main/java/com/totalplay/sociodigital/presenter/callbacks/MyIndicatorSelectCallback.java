package com.totalplay.sociodigital.presenter.callbacks;
import com.totalplay.sociodigital.library.pojos.Data;

public interface MyIndicatorSelectCallback {
    void OnSelectIndicatorMonth(Data dataRecomend, String nameMonth, String numberDay);

    void OnSelectIndicatorReferred(Data dataSale, String nameMonth, String numberDay);
}
