package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.background.ws.WebServices;
import com.totalplay.sociodigital.background.ws.request.ReferredRequest;
import com.totalplay.sociodigital.background.ws.response.RegisterReferredResponse;
import com.totalplay.sociodigital.background.ws.response.RegisterSellerResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.library.utils.PersistenData;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;

public class ReferredPresenter extends WSPresenter  {

    private RegisterReferredCallback mRegisterReferredCallback;
    private PersistenData pref;

    public interface RegisterReferredCallback{
        void OnSuccessRegister();
    }

    public ReferredPresenter(AppCompatActivity appCompatActivity, RegisterReferredCallback registerSellerCallback) {
        super(appCompatActivity);
        pref = new PersistenData(mAppCompatActivity);
        this.mRegisterReferredCallback = registerSellerCallback;
    }

    public void capturedReferred(String mChannelReferred, String mName, String mLastName, String mSecondLastName, String mPhone, String mMail){
        String mNameComplete = mName + " " + mLastName + " " + mSecondLastName;
        InfoSocioBean infoSocioBean = retrieveInfoSocio();
        ReferredRequest mReferredRequest = new ReferredRequest(infoSocioBean.mSubCanal,"Socio Referido","Candidato",mNameComplete,"Call Center BTL",mPhone,mMail,"","", "","","", infoSocioBean.mNombre,infoSocioBean.idOportunity);
        mWSManager.requestWs(RegisterReferredResponse.class, WSManager.WS.REGISTER_REFERRED, mReferredRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        if(requestUrl.equals(WSManager.WS.REGISTER_REFERRED)){
            OnSucessRegisterReferred(requestUrl,(RegisterReferredResponse) baseResponse);
        }
    }

    public void OnSucessRegisterReferred(String requestUrl, RegisterReferredResponse registerReferredResponse){
        if (registerReferredResponse != null) {
            if(registerReferredResponse.mres.equals("0")){
                MessageUtils.stopProgress();
                mRegisterReferredCallback.OnSuccessRegister();
            }else{
                onErrorLoadResponse(requestUrl,registerReferredResponse.mResultDescription);
            }
        }
    }


    public InfoSocioBean retrieveInfoSocio(){
        Gson gson = new Gson();
        String json = pref.getData("infoSocio");
        InfoSocioBean obj = gson.fromJson(json, InfoSocioBean.class);
        return obj;
    }
}