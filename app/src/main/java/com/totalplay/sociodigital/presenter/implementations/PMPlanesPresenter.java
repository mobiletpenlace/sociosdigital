package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.CryptoUtils;
import com.totalplay.sociodigital.background.ws.request.PMPlanesSociosRequest;
import com.totalplay.sociodigital.background.ws.request.ProductMasterPlainsRequest;
import com.totalplay.sociodigital.background.ws.response.PMPlanesSocioResponse;
import com.totalplay.sociodigital.background.ws.response.ProductMasterPlanesResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.model.entities.ConsultPlain;
import com.totalplay.sociodigital.presenter.callbacks.ConsultAddonsTpCallback;
import com.totalplay.sociodigital.presenter.callbacks.PMPlanesCallback;

import retrofit2.Call;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public class PMPlanesPresenter extends WSPresenter {
    private Call<PMPlanesSocioResponse> mPmPlanesResponseCall;
    private PMPlanesCallback mPMPlanesCallback;

    public PMPlanesPresenter(AppCompatActivity appCompatActivity, PMPlanesCallback pmPlanesCallback) {
        super(appCompatActivity);
        this.mPMPlanesCallback = pmPlanesCallback;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPmPlanesResponseCall != null && mPmPlanesResponseCall.isExecuted())
            mPmPlanesResponseCall.cancel();
    }

    public void loadPMPlanes(final String channelFront, String city) {
        ConsultPlain mConsultPlain = new ConsultPlain(CryptoUtils.desEncrypt(city),CryptoUtils.desEncrypt(channelFront));
        PMPlanesSociosRequest pmPlanesSociosRequest = new PMPlanesSociosRequest(mConsultPlain);
        mWSManager.requestWs(PMPlanesSocioResponse.class, WSManager.WS.PM_PLANES_SOCIOS, pmPlanesSociosRequest);
    }

    private void onSuccessLoadPMPlanes(PMPlanesSocioResponse response) {
        if (response.mResult.resultID != null && response.mResult.resultID.equals("0")) {
           mPMPlanesCallback.onSuccessPMPlanes(response);
        } else {
           mPMPlanesCallback.onErrorPMPlanes();
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.PM_PLANES_SOCIOS)) {
           mPMPlanesCallback.onLoadPMPlanes();
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.PM_PLANES_SOCIOS)) {
            onSuccessLoadPMPlanes((PMPlanesSocioResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        if (requestUrl.equals(WSManager.WS.PM_PLANES_SOCIOS)) {
            mPMPlanesCallback.onErrorPMPlanes();
        }
    }

    @Override
    public void onErrorConnection() {
        mPMPlanesCallback.onErrorConnection();
    }
}
