package com.totalplay.sociodigital.presenter.callbacks;

import com.totalplay.sociodigital.background.ws.response.ConsultationSaleTPresponse;

/**
 * Created by imacbookpro on 08/09/18.
 */

public interface ConsultationSaleCallback extends CommonCallback {
    void onLoadConsultationSale();
    void onSuccesConsultationSale(ConsultationSaleTPresponse consultationSaleTPresponse);
    void onErrorConsultationSale();
}
