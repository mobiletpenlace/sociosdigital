package com.totalplay.sociodigital.presenter.callbacks;

/**
 * Created by imacbookpro on 13/09/18.
 * socioDigital
 */

public interface RecoverPassCallbak extends CommonCallback {
    void successRecoverPass();
    void onErrorRecoverPass();
    void onLoadRecoverPass();
}
