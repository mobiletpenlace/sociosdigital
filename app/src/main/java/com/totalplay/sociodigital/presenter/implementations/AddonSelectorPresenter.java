package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.background.ws.request.PlanDetailRequest;
import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.presenter.callbacks.AddonSelectorCallback;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class AddonSelectorPresenter extends WSPresenter {
    private final AddonSelectorCallback addonSelectorCallback;

    public AddonSelectorPresenter(AppCompatActivity appCompatActivity, AddonSelectorCallback addonSelectorCallback) {
        super(appCompatActivity);
        this.addonSelectorCallback = addonSelectorCallback;
    }

    public void loadAddons(String selectedPackage) {
        PlanDetailRequest planDetailRequest = new PlanDetailRequest(selectedPackage, Prefs.instance().string(SocioDigitalKeys.CITY), Prefs.instance().string(SocioDigitalKeys.COLONY));
        mWSManager.requestWs(PlanDetailResponse.class, WSManager.WS.PLAIN_DETAIL, planDetailRequest);
    }

    private void onSuccessLoadPlanDetail(PlanDetailResponse planDetailResponse) {
        if (planDetailResponse.Result.IdResult != null && planDetailResponse.Result.Result.equals("0")) {
            addonSelectorCallback.onSuccessAddons(planDetailResponse);
        } else {
            addonSelectorCallback.onErrorAddons();
        }
    }
    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.PLAIN_DETAIL)) {
            addonSelectorCallback.onLoadAddons();
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.PLAIN_DETAIL)) {
            onSuccessLoadPlanDetail((PlanDetailResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        if (requestUrl.equals(WSManager.WS.PLAIN_DETAIL)) {
            addonSelectorCallback.onErrorAddons();
        }
    }

    @Override
    public void onErrorConnection() {
        addonSelectorCallback.onErrorConnection();
    }
}
