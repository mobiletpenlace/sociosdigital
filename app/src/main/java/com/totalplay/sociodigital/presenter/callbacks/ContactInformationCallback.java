package com.totalplay.sociodigital.presenter.callbacks;

/**
 * Created by imacbookpro on 06/09/18.
 * SocioDigital
 */

public interface ContactInformationCallback extends CommonCallback {
    void onSaveSuccess();
}
