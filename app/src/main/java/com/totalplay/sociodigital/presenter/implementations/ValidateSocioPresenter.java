package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.background.ws.request.ValidateSocioRequest;
import com.totalplay.sociodigital.background.ws.response.ConsultAddonsResponse;
import com.totalplay.sociodigital.background.ws.response.ValidateSocioResponse;
import com.totalplay.sociodigital.library.background.WSManager;

/**
 * Created by imacbookpro on 25/09/18.
 * SocioDigital
 */

public class ValidateSocioPresenter extends WSPresenter {

    public interface ValidateSocioCallback{
        void isvalidSocio(ValidateSocioResponse validateSocioResponse);
    }

    private ValidateSocioCallback mValidateSocioCallback;

    public ValidateSocioPresenter(AppCompatActivity appCompatActivity, ValidateSocioCallback validateSocioCallback) {
        super(appCompatActivity);
        this.mValidateSocioCallback = validateSocioCallback;
    }

    public void validateSocio(String idSocio){
        ValidateSocioRequest validateSocioRequest =  new ValidateSocioRequest(idSocio);
        mWSManager.requestWs(ValidateSocioResponse.class, WSManager.WS.VALIDATE_SOCIO, validateSocioRequest);
    }

    public void onSuccessValidateSocio(String requestUrl, ValidateSocioResponse response){
        if (response.mResult.result != null && response.mResult.result.equals("0"))
            mValidateSocioCallback.isvalidSocio(response);
        else
            onErrorLoadResponse(requestUrl,response.mResult.resultDescription);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        super.onRequestWS(requestUrl);
        MessageUtils.progress(mAppCompatActivity, "Consultando información");
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        MessageUtils.stopProgress();
        onSuccessValidateSocio(requestUrl, (ValidateSocioResponse) baseResponse);
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        MessageUtils.stopProgress();
        MessageUtils.toast(mAppCompatActivity, "No se ha podido consultar la información");
    }
}
