package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.totalplay.sociodigital.background.ws.request.ConsultationSaleTPRequest;
import com.totalplay.sociodigital.background.ws.response.ConsultationSaleTPresponse;
import com.totalplay.sociodigital.background.ws.response.PMPlanesSocioResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.model.ConsutationSale.SaleTP;
import com.totalplay.sociodigital.presenter.callbacks.ConsultationSaleCallback;

import retrofit2.Call;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class ConsultationSalePresenter extends WSPresenter {
    private Call<ConsultationSaleTPresponse> mConsultationSaleTPresponseCall;
    private ConsultationSaleCallback mConsultationSaleCallback;

    public ConsultationSalePresenter(AppCompatActivity appCompatActivity, ConsultationSaleCallback consultationSaleCallback) {
        super(appCompatActivity);
        this.mConsultationSaleCallback =  consultationSaleCallback;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mConsultationSaleTPresponseCall != null && mConsultationSaleTPresponseCall.isExecuted())
            mConsultationSaleTPresponseCall.cancel();
    }

    public void createSale(SaleTP saleTP) {
        ConsultationSaleTPRequest consultationSaleTPRequest = new ConsultationSaleTPRequest(saleTP);
        mWSManager.requestWs(ConsultationSaleTPresponse.class, WSManager.WS.CREATE_SALE, consultationSaleTPRequest);
    }

    private void onSuccessCreateSale(String resquestUrl,ConsultationSaleTPresponse response) {
        if (response.mResult.result != null && response.mResult.result.equals("0"))
            mConsultationSaleCallback.onSuccesConsultationSale(response);
        else
            onErrorLoadResponse(resquestUrl,response.mResult.resultDescription);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.CREATE_SALE)) {
            mConsultationSaleCallback.onLoadConsultationSale();
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.CREATE_SALE)) {
            onSuccessCreateSale(requestUrl,(ConsultationSaleTPresponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        if (requestUrl.equals(WSManager.WS.CREATE_SALE)) {
            mConsultationSaleCallback.onErrorConsultationSale();
        }
    }

    @Override
    public void onErrorConnection() {
        mConsultationSaleCallback.onErrorConnection();
    }
}
