package com.totalplay.sociodigital.presenter.callbacks;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public interface CommonCallback {
    void onErrorConnection();
}
