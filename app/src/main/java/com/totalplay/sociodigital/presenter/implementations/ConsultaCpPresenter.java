package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.background.ws.request.ConsultaCPRequest;
import com.totalplay.sociodigital.background.ws.response.ConsultaCPResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.presenter.callbacks.ConsultaCpCallback;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class ConsultaCpPresenter extends WSPresenter {
    ConsultaCpCallback mConsultaCpCallback;

    public ConsultaCpPresenter(AppCompatActivity appCompatActivity,ConsultaCpCallback consultaCpCallback) {
        super(appCompatActivity);
        this.mConsultaCpCallback = consultaCpCallback;
    }

    public void consultaCp(String cp){
        ConsultaCPRequest consultaCPRequest =  new ConsultaCPRequest(cp);
        mWSManager.requestWs(ConsultaCPResponse.class, WSManager.WS.CONSULTA_CP,consultaCPRequest);
    }

    private void successConsultaCP(String requestUrl, ConsultaCPResponse response){
        if (response.mResult.idResult != null && response.mResult.idResult.equals("0")){
            mConsultaCpCallback.onSuccessConsultaCp(response);
        }
        else {
            mConsultaCpCallback.onErrorConsultaCp();
            onErrorLoadResponse(requestUrl, response.mResult.description);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        successConsultaCP(requestUrl, (ConsultaCPResponse) baseResponse);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        super.onRequestWS(requestUrl);
        MessageUtils.stopProgress();
    }
}
