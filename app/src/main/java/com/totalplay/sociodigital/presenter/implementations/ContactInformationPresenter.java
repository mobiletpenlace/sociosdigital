package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.sociodigital.model.entities.AdditionalInfoEntity;
import com.totalplay.sociodigital.model.entities.ContactEntity;
import com.totalplay.sociodigital.model.entities.FormalityEntity;
import com.totalplay.sociodigital.model.entities.SellerEntity;
import com.totalplay.sociodigital.presenter.callbacks.ContactInformationCallback;

/**
 * Created by imacbookpro on 06/09/18.
 * SocioDigital
 */

public class ContactInformationPresenter extends WSPresenter {
    private final ContactInformationCallback mContactInformationCallback;

    public ContactInformationPresenter(AppCompatActivity appCompatActivity, ContactInformationCallback mContactInformationCallback) {
        super(appCompatActivity);
        this.mContactInformationCallback = mContactInformationCallback;
    }

    public void saveContactInformation(ContactEntity vContact, AdditionalInfoEntity vAdditionalInfo, SellerEntity vSeller) {
        FormalityEntity entity = mDataManager.queryWhere(FormalityEntity.class).findFirst();
        mDataManager.tx(tx -> {
            if (entity != null) {
                tx.save(vContact);
                tx.save(vAdditionalInfo);
                tx.save(vSeller);
                entity.prospectoBean.contact = vContact;
                entity.prospectoBean.signature.additionalInfo = vAdditionalInfo;
                entity.prospectoBean.seller = vSeller;
                tx.save(entity);
                mContactInformationCallback.onSaveSuccess();
            } else {
                mContactInformationCallback.onErrorConnection();
            }
        });
    }
}
