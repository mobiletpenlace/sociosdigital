package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.background.ws.request.IndicatorRecomendRequest;
import com.totalplay.sociodigital.background.ws.request.IndicatorSaleRequest;
import com.totalplay.sociodigital.background.ws.response.IndicatorRecomendResponse;
import com.totalplay.sociodigital.background.ws.response.IndicatorSaleResponse;
import com.totalplay.sociodigital.background.ws.response.IndicatorsResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.presenter.callbacks.IndicatorRecomendCallback;
import com.totalplay.sociodigital.presenter.callbacks.IndicatorSaleCallback;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class IndicatorRecomendPresenter extends WSPresenter {
    IndicatorRecomendCallback mIndicatorRecomendCallback;
    private String typeSel;

    public IndicatorRecomendPresenter(AppCompatActivity appCompatActivity, IndicatorRecomendCallback indicatorRecomendCallback) {
        super(appCompatActivity);
        this.mIndicatorRecomendCallback = indicatorRecomendCallback;
    }

    public void getRecomend(String userId , String dateStart, String dateEnd, String type){
        typeSel = type;
        IndicatorRecomendRequest indicatorRecomendRequest =  new IndicatorRecomendRequest(userId,dateStart,dateEnd, type);
        mWSManager.requestWs(IndicatorsResponse.class, WSManager.WS.INDICATOR_RECOMEND, indicatorRecomendRequest);
    }

    private void successGetRecomend(String requestUrl,IndicatorsResponse response){
        if (response.mResult.result != null && response.mResult.result.equals("1")){
            mIndicatorRecomendCallback.onSuccessGetRecomend(response, typeSel);
        }
        else {
            onErrorLoadResponse(requestUrl,response.mResult.resultDescription);
        }
    }


    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        successGetRecomend(requestUrl, (IndicatorsResponse) baseResponse);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        super.onRequestWS(requestUrl);
        mIndicatorRecomendCallback.onLoadGetRecomend();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        MessageUtils.stopProgress();
        mIndicatorRecomendCallback.onErrorGetRecomend();
    }
}
