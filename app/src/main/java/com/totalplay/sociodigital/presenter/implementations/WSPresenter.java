package com.totalplay.sociodigital.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.background.WSCallback;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.library.background.BaseWSManager;
import com.totalplay.sociodigital.library.background.WSManager;

/**
 * Created by jorgehdezvilla on 14/10/17.
 * Estrategia
 */

public class WSPresenter extends RealmPresenter implements WSCallback {

    public AppCompatActivity mAppCompatActivity;
    public BaseWSManager mWSManager;

    WSPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
        mAppCompatActivity = appCompatActivity;
        mWSManager = WSManager.init(mContext).settings(mContext, this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWSManager != null) mWSManager.onDestroy();
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, "Cargando información");
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (messageError.equals("")) {
            MessageUtils.toast(mContext, "Ha ocurrido un error. Intente nuevamente");
        } else {
            MessageUtils.toast(mContext, messageError);
        }
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, "Error de conexión");
    }
}
