package com.totalplay.sociodigital.presenter.implementations;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.resources.background.WSBaseResponseInterface;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.background.ws.request.LoginSocioRequest;
import com.totalplay.sociodigital.background.ws.response.LoginSocioResponse;
import com.totalplay.sociodigital.library.background.WSManager;
import com.totalplay.sociodigital.library.quoter.utils.Constans;
import com.totalplay.sociodigital.library.utils.PersistenData;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.view.activities.HomeActivity;

import java.util.List;

public class LoginPresenter extends WSPresenter {
    PersistenData mPrefs;
    boolean flagCheckBox;
    private String pass;

    public LoginPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
        mPrefs = new PersistenData(appCompatActivity);
    }

    public void login(String mUser, String mPassword, boolean flag){
        flagCheckBox = flag;
        pass = mPassword;
        LoginSocioRequest mLoginSocioRequest = new LoginSocioRequest(CryptoUtils.crypt(mUser),CryptoUtils.crypt(mPassword),"");
        mWSManager.requestWs(LoginSocioResponse.class, WSManager.WS.LOGIN, mLoginSocioRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        if(WSManager.WS.LOGIN.equals(requestUrl)){
            OnSuccessLogin(requestUrl, (LoginSocioResponse)baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        switch (requestUrl){
            case WSManager.WS.LOGIN:
                if(messageError.isEmpty()){
                    MessageUtils.toast(mAppCompatActivity, "Ocurrió un error. Intente más tarde");
                }
//                MessageUtils.toast(mAppCompatActivity, messageError);
            break;
        }
    }

    public void OnSuccessLogin(String requestUrl, LoginSocioResponse mLoginSocioResponse){
        if(mLoginSocioResponse.mInfoSocioBean != null && mLoginSocioResponse.mResponse != null && mLoginSocioResponse.mResponse.mResultId.equals("0")){
            mLoginSocioResponse.mInfoSocioBean.mAccessKey = pass;
            //TEST//
            mLoginSocioResponse.mInfoSocioBean.mIsAnyCardRegister = "1";
            //////
            saveDataInfoSocio(mLoginSocioResponse.mInfoSocioBean);
            Prefs.instance().putObject(Constans.KEY_SAVE_LOGIN,mLoginSocioResponse.mInfoSocioBean,InfoSocioBean.class);
            Intent newIntent = new Intent(mAppCompatActivity,HomeActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mAppCompatActivity.startActivity(newIntent);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mAppCompatActivity);
            prefs.edit().putBoolean(Constans.KEY_CHECKED, flagCheckBox).commit();
        }else{
            onErrorLoadResponse(requestUrl, mLoginSocioResponse.mResponse.mDescription);
        }
    }


    public void saveDataInfoSocio(InfoSocioBean infoSocioBean){
        Gson gson = new Gson();
        String json = gson.toJson(infoSocioBean);
        mPrefs.saveData(Constans.KEY_INFO_SOCIO,json);
    }


    public InfoSocioBean retrieveInfoSocio(){
        Gson gson = new Gson();
        String json = mPrefs.getData(Constans.KEY_INFO_SOCIO);
        InfoSocioBean obj = gson.fromJson(json, InfoSocioBean.class);
        return obj;
    }
}
