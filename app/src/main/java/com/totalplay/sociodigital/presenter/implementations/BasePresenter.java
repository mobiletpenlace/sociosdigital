package com.totalplay.sociodigital.presenter.implementations;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class BasePresenter {
    protected AppCompatActivity mAppCompatActivity;
    protected final Context mContext;

    public BasePresenter(AppCompatActivity appCompatActivity) {
        mAppCompatActivity = appCompatActivity;
        mContext = appCompatActivity;
    }

    public void onCreate() {

    }

    public void onStart() {

    }

    public void onResume() {

    }

    public void onPause() {

    }

    public void onStop() {

    }

    public void onDestroy() {

    }
}
