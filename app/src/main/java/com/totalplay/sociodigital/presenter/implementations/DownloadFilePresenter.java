package com.totalplay.sociodigital.presenter.implementations;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import com.totalplay.sociodigital.BuildConfig;
import com.totalplay.sociodigital.background.ws.WebServices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by imacbookpro on 19/09/18.
 * SocioDigital
 */

public class DownloadFilePresenter extends BasePresenter {

    public interface DownloadImageCallback{
        void postImage(File file);
        void onErrorNotSuccessPostImageResponse();
    }

    private DownloadImageCallback mDownloadImageCallback;

    public DownloadFilePresenter(AppCompatActivity appCompatActivity, DownloadImageCallback downloadImageCallback) {
        super(appCompatActivity);
        this.mDownloadImageCallback =  downloadImageCallback;
    }

    public void fullImage(String url, String nameFile) {
        Call<ResponseBody> imageCall;
        imageCall = WebServices.servicesImages().downloadFile(Base64.encodeToString(url.getBytes(), Base64.NO_WRAP));
        imageCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    InputStream is = null;
                    try {
                        //you can now get your file in the InputStream
                        is = response.body().byteStream();
                        File path = Environment.getExternalStorageDirectory();
                        File filePath = new File(path, ".FFM");
                        if (!filePath.exists())
                            filePath.mkdirs();
                        File file = new File(filePath, nameFile + ".jpg");
                        try (OutputStream output = new FileOutputStream(file)) {
                            byte[] buffer = new byte[4 * 1024]; // or other buffer size
                            int read;

                            while ((read = is.read(buffer)) != -1) {
                                output.write(buffer, 0, read);
                            }
                            output.flush();
                        }
                        mDownloadImageCallback.postImage(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }finally {
                        if (is != null){
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } else {
                    mDownloadImageCallback.onErrorNotSuccessPostImageResponse();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
