package com.totalplay.sociodigital.view.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.ProductMasterPlanesResponse;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.view.activities.PackagesActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public class PackagesFamilyRecyclerAdapter extends RecyclerView.Adapter<PackagesFamilyRecyclerAdapter.RecyclerViewSimpleTextViewHolder> {

    private ArrayList<PlanTotalPlay> plans = new ArrayList<>();
    private ArrayList<String> idPlanses = new ArrayList<>();
    private Activity activity;
    private String nameFamily;
    ArrayList<PlanTotalPlay> plansDoblePlay =  new ArrayList<>();
    ArrayList<PlanTotalPlay> plansDoblePlayTV =  new ArrayList<>();
    ArrayList<PlanTotalPlay> plansTriplePlay =  new ArrayList<>();


    public PackagesFamilyRecyclerAdapter(Activity activity, ArrayList<PlanTotalPlay> plans) {
        this.activity = activity;
        this.plans = plans;
        plansDoblePlay.clear();
        plansDoblePlayTV.clear();
        plansTriplePlay.clear();
        origanicePackages();
    }

    @Override
    public PackagesFamilyRecyclerAdapter.RecyclerViewSimpleTextViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_plan, viewGroup, false);
        return new PackagesFamilyRecyclerAdapter.RecyclerViewSimpleTextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PackagesFamilyRecyclerAdapter.RecyclerViewSimpleTextViewHolder holder, int position) {

        if (position == 0){
            if (plansDoblePlay.size() > 0)
                holder.act.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_dobleplay_1));
            else
                holder.act.setVisibility(View.GONE);
        }

        if(position == 1){
            if (plansDoblePlayTV.size() > 0)
                holder.act.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_dobleplay_2));
            else
                holder.act.setVisibility(View.GONE);
        }

        if (position == 2){
            if (plansTriplePlay.size() > 0)
                holder.act.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_tp_int_tel_tv));
            else
                holder.act.setVisibility(View.GONE);
        }

            holder.itemView.setOnClickListener(v -> {
                if (holder.act.getDrawable().getConstantState().equals(activity.getResources().getDrawable(R.drawable.ic_dobleplay_1).getConstantState())){
                    activity.startActivity(PackagesActivity.launch(activity,plansDoblePlay, "Paquetes Dobleplay"));
                }
                if (holder.act.getDrawable().getConstantState().equals(activity.getResources().getDrawable(R.drawable.ic_dobleplay_2).getConstantState())){
                    activity.startActivity(PackagesActivity.launch(activity,plansDoblePlayTV, "Paquetes Dobleplay TV"));
                }

                if (holder.act.getDrawable().getConstantState().equals(activity.getResources().getDrawable(R.drawable.ic_tp_int_tel_tv).getConstantState())){
                    activity.startActivity(PackagesActivity.launch(activity,plansTriplePlay,"Paquetes Tripleplay"));
                }
            });
    }

    public class RecyclerViewSimpleTextViewHolder extends RecyclerView.ViewHolder {
        ImageView act;
        public RecyclerViewSimpleTextViewHolder(View itemView) {
            super(itemView);
            act= itemView.findViewById(R.id.item_plan_image);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    private void origanicePackages(){
        for (PlanTotalPlay plan:plans){
            switch (plan.typeOffert){
                case "2P":
                    // inter y tel
                    plansDoblePlay.add(plan);
                    break;
                case "3P":
                    //todos
                    plansTriplePlay.add(plan);
                    break;
                case "2P tv":
                    //inter tv
                    plansDoblePlayTV.add(plan);
                    break;
            }

        }
    }
}
