package com.totalplay.sociodigital.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.totalplay.sociodigital.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
    }
}
