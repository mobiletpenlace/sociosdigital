package com.totalplay.sociodigital.view.adapters;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;
import com.totalplay.sociodigital.model.pojos.ArrProductosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosAdicionalesBean;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class AdditionalsServicesAdapter extends PagerAdapter {


    private Activity activity;
    private ArrayList<PlanDetailResponse.AdicionalesBean> additionals = new ArrayList<>();
    public ArrayList<PlanDetailResponse.AdicionalesBean> itemsSelected = new ArrayList<>();
    private View.OnClickListener listener;
    StatusButton statusButton;

    public AdditionalsServicesAdapter(Activity activity, ArrayList<PlanDetailResponse.AdicionalesBean> additionals, View.OnClickListener listener, StatusButton statusButton) {
        this.activity = activity;
        this.additionals = additionals;
        this.listener = listener;
        this.statusButton = statusButton;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        view = LayoutInflater.from(activity).inflate(R.layout.item_aditional, null);

        ImageView imgAddon = view.findViewById(R.id.item_aditional_image);
        ImageView imageView3 = view.findViewById(R.id.item_additional_select_image);

        ArrServiciosAdicionalesBean arrServiciosAdicionalesBean;
        ArrProductosAdicionalesBean arrProductosAdicionalesBean;
        PlanDetailResponse.AdicionalesBean adicionalesBean = additionals.get(position);

        if (adicionalesBean.getAdicionalType() == PlanDetailResponse.AdicionalesBean.PRODUCT) {
            arrProductosAdicionalesBean = (ArrProductosAdicionalesBean) adicionalesBean;
            Glide.with(activity).load(arrProductosAdicionalesBean.URLApp__c).into(imgAddon);
        } else {
            arrServiciosAdicionalesBean = (ArrServiciosAdicionalesBean) adicionalesBean;
            Glide.with(activity).load(arrServiciosAdicionalesBean.ImgIconoApp).into(imgAddon);
        }


        imgAddon.setOnClickListener(view1 -> {
            PlanDetailResponse.AdicionalesBean plan = additionals.get(position);

            if (itemsSelected.contains(plan)) {
                itemsSelected.remove(plan);
            } else {
                itemsSelected.add(plan);
            }
            listener.onClick(view1);
            if (itemsSelected.size() > 0)
                statusButton.onChangeButton(true);
            else
                statusButton.onChangeButton(false);

        });

        view.setTag(position);

        if (itemsSelected.contains(adicionalesBean)) {
            imageView3.setVisibility(View.VISIBLE);
        } else {
            imageView3.setVisibility(View.INVISIBLE);
        }

        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return additionals.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }

    public interface StatusButton{
        void onChangeButton(boolean status);
    }

}
