package com.totalplay.sociodigital.view.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.AddressGenerator;
import com.totalplay.sociodigital.background.async.WSCoverageValidation;
import com.totalplay.sociodigital.background.ws.WebServices;
import com.totalplay.sociodigital.background.ws.querymaps.Geometry;
import com.totalplay.sociodigital.background.ws.querymaps.ResultMaps;
import com.totalplay.sociodigital.background.ws.response.ConsultaCPResponse;
import com.totalplay.sociodigital.background.ws.response.PredictionsResponse;
import com.totalplay.sociodigital.background.ws.response.ValidateCoverageResponse;
import com.totalplay.sociodigital.library.enums.DataAddress;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.presenter.callbacks.ConsultaCpCallback;
import com.totalplay.sociodigital.presenter.callbacks.ValidateCoverageCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.ConsultaCpPresenter;
import com.totalplay.sociodigital.presenter.implementations.ValidateCoveragePresenter;
import com.totalplay.sociodigital.view.adapters.AutoCompleteAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TypeSellerMap extends BaseActivity implements ValidateCoverageCallback, ConsultaCpCallback {
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;
    private Button acceptButton;
    float zoom = 17.489744f;
    private EditText addressText;
    private ListView mAutoCompleteList;
    PredictionsResponse predictions;
    Call<ResultMaps> resultMapsCall;
    private GoogleMap mMap;
    private Marker mMarker;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    private double latitude = 0.0f;
    private double longitude = 0.0f;
    private ValidateCoveragePresenter validateCoveragePresenter;
    private LocationManager mLocationManager;
    private ImageView mSetLocateUserImageView;
    private TextView detailTextView;
    private Button cancelButton;
    private ImageView searchImageView;
    private CardView searchContent;
    private ViewGroup contentDialogCoverage;
    private TextView titleTextView;
    private ImageView imageGif;
    private TextView subtitleTextView;
    private ViewGroup contentCoverageDialogSuccess;
    private TextView successCoverageTitle;
    private ImageView successCoverageImage;
    private TextView successCoveragePlaceTitle;
    private TextView successCoveragePlace;
    private TextView successCoverageZoneTitle;
    private TextView successCoverageZone;
    private TextView successCoverageClusterTitle;
    private TextView successCoverageCluster;
    private TextView successCoverageDistrictTitle;
    private TextView successCoverageDistrict;
    private TextView successCoverageAcceptButton;
    private AddressGenerator mGenerator;
    private ValidateCoverageResponse mEntity;
    private ConsultaCpPresenter mConsultaCpPresenter;
    private DataAddress mDataAddress;
    //private ImageView deleteText;

    private TextView mCancelDialogTitleTextView;
    private TextView mCancelDialogDetailTextView;
    private ImageView mCancelDialogImageView;
    private Button mCancelDialogButton;
    private Button mCancelDialogAccpetButton;
    private ViewGroup mCancelDialogContent;
    private ImageView mBackToolbar;

    private TextView mNoCoverageTitleTextView;
    private TextView mNoCoverageDetailTextView;
    private Button mNoCoverageAcceptButton;
    private ViewGroup mNoCoverageContent;
    private boolean flagSearchDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.flush_type_seller_map);
        bindResources();
        setFont();

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //deleteText.setOnClickListener(view -> {
        //    addressText.setText("");
        //});
        //deleteText.setVisibility(View.INVISIBLE);
        reload();
        setOnClicks();
        mConsultaCpPresenter = new ConsultaCpPresenter(this, this);
        flagSearchDelete = true;
    }

    private void bindResources() {
        acceptButton = findViewById(R.id.act_map_save);
        addressText = findViewById(R.id.adressText);
        mAutoCompleteList = findViewById(R.id.searchResultLV);
        mSetLocateUserImageView = findViewById(R.id.act_map_validate_coverage_set_position_user);
        detailTextView = findViewById(R.id.act_validate_map_coverage_detail_text);
        cancelButton = findViewById(R.id.act_validate_map_coverage_cancel_btn);
        searchImageView = findViewById(R.id.act_validate_map_coverage_btn_search);
        searchContent = findViewById(R.id.linear);
        contentDialogCoverage = findViewById(R.id.include_content_dialog_coverage);
        titleTextView = findViewById(R.id.act_validate_coverage_dialog_title);
        imageGif = findViewById(R.id.act_validate_coverage_dialog_image);
        subtitleTextView = findViewById(R.id.act_validate_coverage_dialog_detail);
        contentCoverageDialogSuccess = findViewById(R.id.content_dialog_success_coverage);
        successCoverageTitle = findViewById(R.id.coverage_success_dialog_title);
        successCoverageImage = findViewById(R.id.coverage_success_dialog_image);
        successCoveragePlaceTitle = findViewById(R.id.coverage_success_dialog_title_place_title);
        successCoveragePlace = findViewById(R.id.coverage_success_dialog_place);
        successCoverageZoneTitle = findViewById(R.id.coverage_success_dialog_zone_title);
        successCoverageZone = findViewById(R.id.coverage_success_dialog_zone);
        successCoverageClusterTitle = findViewById(R.id.coverage_success_dialog_cluster_title);
        successCoverageCluster = findViewById(R.id.coverage_success_dialog_cluster);
        successCoverageDistrictTitle = findViewById(R.id.coverage_success_dialog_district_title);
        successCoverageDistrict = findViewById(R.id.coverage_success_dialog_district);
        successCoverageAcceptButton = findViewById(R.id.coverage_success_dialog_accept_button);
        Glide.with(this).load(R.drawable.ic_location_animation).into(imageGif);
        mDataAddress = new DataAddress("", "", "", "");

        mCancelDialogTitleTextView = findViewById(R.id.dialog_cancel_operation_title);
        mCancelDialogDetailTextView = findViewById(R.id.dialog_cancel_operation_detail);
        mCancelDialogImageView = findViewById(R.id.dialog_cancel_operation_image);
        mCancelDialogButton = findViewById(R.id.dialog_cancel_operation_cancel_button);
        mCancelDialogAccpetButton = findViewById(R.id.dialog_cancel_operation_aceppt_button);
        mCancelDialogContent = findViewById(R.id.dialog_cancel_operation_content);
        mBackToolbar = findViewById(R.id.mBackImageView);

        mNoCoverageTitleTextView = findViewById(R.id.dialog_no_coverage_title);
        mNoCoverageDetailTextView = findViewById(R.id.dialog_no_coverage_detail);
        mNoCoverageAcceptButton = findViewById(R.id.dialog_no_coverage_aceppt_button);
        mNoCoverageContent = findViewById(R.id.dialog_no_coverage_content);


    }

    private void setOnClicks() {
        mSetLocateUserImageView.setOnClickListener(v -> {
            getCurrentLocation();
        });
        searchImageView.setOnClickListener(v -> {
            //searchContent.animate().scaleX(5f).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(3000);
            addressText.setText("");
            searchImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_coverage));

        });

        cancelButton.setOnClickListener(v -> {
            showDialogCancelOperation();
            mCancelDialogContent.setVisibility(View.VISIBLE);
        });

        acceptButton.setOnClickListener(v -> {
            LatLng latLng = mMap.getCameraPosition().target;
            queryAddress(latLng, true, null);
        });

        successCoverageAcceptButton.setOnClickListener(v -> {
            startActivity(ValidateFormCoverageActivity.launch(getApplicationContext(), mGenerator, mEntity.direccionBean, mDataAddress,null));
            contentCoverageDialogSuccess.setVisibility(View.GONE);
        });

        mCancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCancelDialogContent.setVisibility(View.GONE);
            }
        });

        mCancelDialogAccpetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CancelRegisterDialog.class));
            }
        });

        mBackToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //showDialogCancelOperation();
                //mCancelDialogContent.setVisibility(View.VISIBLE);
                hiddenKeyBoard();
            }
        });

        mNoCoverageAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNoCoverageContent.setVisibility(View.GONE);
            }
        });
    }

    private void setFont() {
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        addressText.setTypeface(monserratRegular);
        detailTextView.setTypeface(monserratRegular);
        cancelButton.setTypeface(monserratMedium);
        acceptButton.setTypeface(monserratMedium);
        titleTextView.setTypeface(monserratRegular);
        subtitleTextView.setTypeface(monserratRegular);
        successCoverageTitle.setTypeface(monserratRegular);
        successCoveragePlaceTitle.setTypeface(monserratRegular);
        successCoveragePlace.setTypeface(monserratBold);
        successCoverageZoneTitle.setTypeface(monserratRegular);
        successCoverageZone.setTypeface(monserratBold);
        successCoverageClusterTitle.setTypeface(monserratRegular);
        successCoverageCluster.setTypeface(monserratBold);
        successCoverageDistrictTitle.setTypeface(monserratRegular);
        successCoverageDistrict.setTypeface(monserratBold);
        successCoverageAcceptButton.setTypeface(monserratMedium);
        mCancelDialogTitleTextView.setTypeface(monserratRegular);
        mCancelDialogButton.setTypeface(monserratMedium);
        mCancelDialogAccpetButton.setTypeface(monserratMedium);
        mCancelDialogDetailTextView.setTypeface(monserratRegular);
    }

    @Override
    protected BasePresenter getPresenter() {
        return validateCoveragePresenter = new ValidateCoveragePresenter(this, this);
    }

    @Override
    public void onLoadAddress(AddressGenerator generator) {

    }

    @Override
    public void onSuccessChargeDirection(String city) {

    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                //mLocationManager.removeUpdates(mLocationListener);
            } else {
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    public void reload() {
        setTitle("");
        setUpMapIfNeeded();

        addressText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //if (addressText.length() > 0)
                //deleteText.setVisibility(View.VISIBLE);
                //else
                //deleteText.setVisibility(View.INVISIBLE);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                if (addressText.getText().length() > 0)
                    searchImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_delete));

                if (addressText.getText().length() > 3) {
                    mAutoCompleteList.setVisibility(View.VISIBLE);
                    WebServices.mapsServices().autoComplete(addressText.getText().toString(), latitude + "," + longitude, "500", "en", "AIzaSyCgcbDXiHOSFLAGDGS_b3uwQRP6RfGLQtg").enqueue(new Callback<PredictionsResponse>() {
                        @Override
                        public void onResponse(Call<PredictionsResponse> call, Response<PredictionsResponse> response) {

                            if (response.isSuccessful()) {
                                predictions = response.body();
                                if (mAutoCompleteAdapter == null) {
                                    mAutoCompleteAdapter = new AutoCompleteAdapter(TypeSellerMap.this, predictions.predictions, TypeSellerMap.this);
                                    mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
                                } else {
                                    mAutoCompleteAdapter.clear();
                                    mAutoCompleteAdapter.addAll(predictions.predictions);
                                    mAutoCompleteAdapter.notifyDataSetChanged();
                                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                                    mAutoCompleteList.invalidate();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<PredictionsResponse> call, Throwable t) {

                        }
                    });

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        addressText.setSelection(addressText.getText().length());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mAutoCompleteList.setOnItemClickListener((parent, view, position, id) -> {
            searchMapCoordinate(predictions.predictions.get(position).description);
            mAutoCompleteAdapter.clear();
            mAutoCompleteAdapter.notifyDataSetChanged();
            mAutoCompleteList.invalidate();
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(addressText.getWindowToken(), 0);
            addressText.setText(predictions.predictions.get(position).description);
            mAutoCompleteList.setVisibility(View.GONE);
        });
    }

    private void setUpMapIfNeeded() {
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.act_coverage_map))
                .getMapAsync(googleMap -> {
                    if (googleMap != null) {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        googleMap.setMyLocationEnabled(false);
                        setUpMap(googleMap);
                    }
                });

    }

    private void setUpMap(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        try {
            boolean success = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
            if (!success) {
                Log.e(SocioDigitalKeys.DEBUG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(SocioDigitalKeys.DEBUG, "Can't find style. Error: ", e);
        }
        getCurrentLocation();
    }

    private void searchMapCoordinate(String query) {
        Geocoder geocoder = new Geocoder(TypeSellerMap.this, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(query + " Mexico", 30);
            if (addresses != null && !addresses.isEmpty() && addresses.size() > 0) {
                LatLng direction = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
                if (mMap != null) {
                    mMap.clear();
                    mMap.moveCamera(CameraUpdateFactory
                            .newCameraPosition(CameraPosition
                                    .fromLatLngZoom(direction, 16.0f)));
                    queryAddress(direction, false, null);
                }
            }
        } catch (IOException e) {
            searchByWebService(query);
            e.printStackTrace();
        }

    }

    private void queryAddress(LatLng latLng, boolean isSuccess, ResultMaps resultMaps) {
        List<Address> addresses = new ArrayList<>();
        if (latLng != null) {
            Geocoder geocoder = new Geocoder(this);
            AddressGenerator generator = new AddressGenerator();
            generator.lat = (String.valueOf(latLng.latitude));
            generator.lng = (String.valueOf(latLng.longitude));
            if (resultMaps != null) {
                generator.initAddress(resultMaps);
            } else {
                try {
                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 25);
                    generator.initAddressFromList(addresses);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 25);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(this, ConfirmAddressActivity.class);
            try {
                intent.putExtra("Latitude", String.valueOf(latLng.latitude));
                intent.putExtra("Longitude", String.valueOf(latLng.longitude));
                intent.putExtra("zipCode", addresses.get(0).getPostalCode() == null ? "" : addresses.get(0).getPostalCode());
                intent.putExtra("colony", addresses.get(5).getLocality() == null ? "" : addresses.get(0).getFeatureName());
                intent.putExtra("delegation", addresses.get(5).getLocality() == null ? "" : addresses.get(0).getAdminArea());
                intent.putExtra("city", addresses.get(5).getFeatureName() == null ? "" : addresses.get(0).getLocality());
                intent.putExtra("state", addresses.get(0).getCountryName() == null ? "" : addresses.get(0).getCountryName());
                intent.putExtra("street", addresses.get(0).getThoroughfare() == null ? "" : addresses.get(0).getSubAdminArea());
            }catch (IndexOutOfBoundsException e){
                e.printStackTrace();
            }
            startActivity(intent);

            /*if (isSuccess) {

                contentDialogCoverage.setVisibility(View.VISIBLE);
                mGenerator = generator;
                validateCoveragePresenter.validateCoverage(new WSCoverageValidation.CoverageValidateListener() {
                    @Override
                    public void onSuccessCoverage(ValidateCoverageResponse entity, String feasibility) {
                        if (entity.idResult.equals("1") && entity.direccionBean != null) {
                            mEntity = entity;
                            mConsultaCpPresenter.consultaCp(generator.zipCode);
                            MessageUtils.toast(getApplicationContext(), "CP: " + generator.zipCode);
                            //ConfirmLocationActivity.launch(CoverageActivity.this, generator, entity.direccionBean);
                            //mGenerator =  generator;
                            MessageUtils.toast(getApplicationContext(), "Coverage");
                        } else {
                            contentDialogCoverage.setVisibility(View.GONE);
                            //NoCoverageActivity.launch(CoverageActivity.this, generator, entity.direccionBean);
                            mNoCoverageContent.setVisibility(View.VISIBLE);
                            MessageUtils.toast(getApplicationContext(), "No Coverage");
                        }
                    }

                    @Override
                    public void onErrorCoverage(ValidateCoverageResponse entity, String feasibility) {
                        Toast.makeText(getApplicationContext(), "Error, intente nuevamente", Toast.LENGTH_LONG).show();
                        mNoCoverageContent.setVisibility(View.VISIBLE);
                    }
                }, mGenerator);
            }*/
        }
    }

    private void searchByWebService(String query) {
        resultMapsCall = WebServices.mapsServices().fetchDirectionFromText(query);
        resultMapsCall.enqueue(new Callback<ResultMaps>() {
            @Override
            public void onResponse(Call<ResultMaps> call, Response<ResultMaps> response) {
                if (response.isSuccessful()) {
                    ResultMaps result = response.body();
                    if (result != null) {
                        if (result.getResults() != null && !result.getResults().isEmpty()) {
                            if (result.getResults().get(0) != null) {
                                if (result.getResults().get(0).getGeometry() != null) {
                                    Geometry geometry = result.getResults().get(0).getGeometry();
                                    if (geometry.getLocation() != null) {
                                        LatLng direction = new LatLng(
                                                geometry.getLocation().getLat(),
                                                geometry.getLocation().getLng());

                                        if (mMap != null) {
                                            mMap.clear();
                                            mMap.moveCamera(CameraUpdateFactory
                                                    .newCameraPosition(CameraPosition
                                                            .fromLatLngZoom(direction, 16.0f)));
                                            queryAddress(direction, false, result);
                                        }

                                    }
                                }
                            }
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<ResultMaps> call, Throwable t) {
                MessageUtils.toast(getBaseContext(), "No se encontraron coincidencias");

            }
        });
    }

    public void tedPermission() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                reload();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                finish();
            }
        };

        new TedPermission(TypeSellerMap.this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Favor de aceptar todos los permisos para continuar")
                .setPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();
    }

    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Location location = null;

        if (!(isGPSEnabled || isNetworkEnabled))
            MessageUtils.toast(this, "Error en la localización");
        else {

            if (isGPSEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        0, 0, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            }

            if (isNetworkEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        0, 0, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            }

        }
        if (location != null) {
            LatLng gps = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(gps, 17));
        }


    }

    private void getMarkerCenterPosition() {
        //mMap.getCameraPosition().target;

    }

    @Override
    public void onErrorConnection() {
        contentDialogCoverage.setVisibility(View.GONE);
        mNoCoverageContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccessConsultaCp(ConsultaCPResponse mConsultaCPResponse) {
        Prefs.instance().putString("savePlace", mConsultaCPResponse.mColonies.get(0).city);
        successCoveragePlace.setText(mEntity.feasibility.city);
        successCoverageZone.setText(mEntity.feasibility.zone);
        successCoverageCluster.setText(mEntity.feasibility.cluster);
        successCoverageDistrict.setText(mEntity.feasibility.district);
        contentDialogCoverage.setVisibility(View.GONE);
        contentCoverageDialogSuccess.setVisibility(View.VISIBLE);
        mDataAddress = new DataAddress(mConsultaCPResponse.mColonies.get(0).city, mConsultaCPResponse.mColonies.get(0).colony, mConsultaCPResponse.mColonies.get(0).delegation, mConsultaCPResponse.mColonies.get(0).state);
    }

    @Override
    public void onErrorConsultaCp() {
        contentDialogCoverage.setVisibility(View.GONE);
        MessageUtils.toast(this, "No se ha podido consultar la información del código postal");
        mNoCoverageContent.setVisibility(View.VISIBLE);
    }

    private void showDialogCancelOperation() {
        mCancelDialogTitleTextView.setText("Cancelar operación");
        mCancelDialogDetailTextView.setText("¿Estás seguro de que deseas cancelar?");
        mCancelDialogButton.setText("Cancelar");
        mCancelDialogAccpetButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hiddenKeyBoard();
    }

    private void hiddenKeyBoard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}