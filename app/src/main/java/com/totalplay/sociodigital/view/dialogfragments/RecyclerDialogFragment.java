package com.totalplay.sociodigital.view.dialogfragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.totalplay.sociodigital.R;

import java.util.ArrayList;

@SuppressWarnings("unchecked")
public class RecyclerDialogFragment extends DialogFragment {

    RecyclerView pickerView;
    ComplexRecyclerViewAdapter adapter;
    private OnDialogOptionSelectedListener selectedListener;
    private ArrayList<String> arrayList = new ArrayList<>();

    public static RecyclerDialogFragment newInstance() {
        return new RecyclerDialogFragment();
    }

    public void setSelectedListener(OnDialogOptionSelectedListener selectedListener) {
        this.selectedListener = selectedListener;
    }

    public void setArrayList(ArrayList<String> arrayList) {
        if (arrayList != null) {
            this.arrayList = arrayList;
        }
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_custom_recycler, null);
        pickerView= view.findViewById(R.id.pickerView);
        adapter = new ComplexRecyclerViewAdapter(arrayList, (index, object) -> {
            selectedListener.onOptionSelected(index, object);
            dismiss();
        });

        pickerView.setLayoutManager(new LinearLayoutManager(getContext()));
        pickerView.setAdapter(adapter);
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics mMetrics = new DisplayMetrics();
        display.getMetrics(mMetrics);
        mBuilder.setView(view);
        mBuilder.setCancelable(false);
        return mBuilder.create();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }


    public interface OnDialogOptionSelectedListener<T> {

        void onOptionSelected(int index, T object);
    }

    class ComplexRecyclerViewAdapter extends RecyclerView.Adapter<ComplexRecyclerViewAdapter.RecyclerViewSimpleTextViewHolder> {

        private ArrayList<String> items;
        private OnDialogOptionSelectedListener listener;
        ComplexRecyclerViewAdapter(ArrayList<String> items, OnDialogOptionSelectedListener listener) {
            this.items = items;
            this.listener = listener;
        }


        @Override
        public int getItemCount() {
            return this.items.size();
        }


        @Override
        public RecyclerViewSimpleTextViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View v = inflater.inflate(R.layout.simple_list_item_1, viewGroup, false);
            return new RecyclerViewSimpleTextViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final RecyclerViewSimpleTextViewHolder holder, int position) {
            holder.text1.setText(items.get(position));
            holder.text1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onOptionSelected(holder.getAdapterPosition(), items.get(holder.getAdapterPosition()));
                }
            });
        }


        class RecyclerViewSimpleTextViewHolder extends RecyclerView.ViewHolder {

            TextView text1;
            RecyclerViewSimpleTextViewHolder(View view) {
                super(view);
                text1= view.findViewById(R.id.text1);
            }
        }
    }
}