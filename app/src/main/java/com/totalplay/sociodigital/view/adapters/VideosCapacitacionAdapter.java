package com.totalplay.sociodigital.view.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.pojos.Video;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VideosCapacitacionAdapter extends RecyclerView.Adapter<VideosCapacitacionAdapter.VideosHolder> {
    private List<Video> mListVideos = new ArrayList<>();
    private View.OnClickListener mOnClickListener;
    private View.OnClickListener mDownloadClickListener;

    public VideosCapacitacionAdapter(List<Video> mListVideos, View.OnClickListener clickListener, View.OnClickListener downloadClickListener) {
        this.mListVideos = mListVideos;
        this.mOnClickListener = clickListener;
        this.mDownloadClickListener = downloadClickListener;

    }

    @NonNull
    @Override
    public VideosHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_video,viewGroup , false);
        VideosHolder vHolder = new VideosHolder(layoutView);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VideosHolder videosHolder, int i) {
        Video video = this.mListVideos.get(i);
        videosHolder.itemView.setTag(video);
        videosHolder.mDownloadVideoImageVideo.setTag(video);
        videosHolder.mDescriptionVideo.setText(video.description);
    }

    public void update(List<Video> mListVideos){
        this.mListVideos = mListVideos;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mListVideos.size();
    }

    class VideosHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView mVidePreviewImageView;
        TextView mTitleVideo;
        TextView mDescriptionVideo;
        ImageView mDownloadVideoImageVideo;
        public VideosHolder(@NonNull View itemView) {
            super(itemView);
            mVidePreviewImageView = itemView.findViewById(R.id.mThumbnailVideoImageView);
            mTitleVideo = itemView.findViewById(R.id.mTitleVideo);
            mDescriptionVideo = itemView.findViewById(R.id.mDecriptionVideo);
            mDownloadVideoImageVideo = itemView.findViewById(R.id.mDownloadVideoImageVideo);
            mDownloadVideoImageVideo.setOnClickListener(mDownloadClickListener);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mOnClickListener.onClick(view);
        }
    }


}
