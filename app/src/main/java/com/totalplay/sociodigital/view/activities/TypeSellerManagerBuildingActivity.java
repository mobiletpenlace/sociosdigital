package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.widget.EditText;

import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TypeSellerManagerBuildingActivity extends BaseActivity{
    @BindView(R.id.mNameBuildingEditText)
    public EditText mNameBuildingEditText;

    @BindView(R.id.mNumberCoachEditText)
    public EditText mNumberCoachEditText;

    public FormValidator mFormValidator;
    private RegisterSellerPresenter mRegisterSellerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.flush_type_seller_building_manager_activity);
        ButterKnife.bind(this);
        addValidators();
        mNameBuildingEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mNumberCoachEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mNameBuildingEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty));
    }

    @OnClick(R.id.mNextManagerBuldingButton)
    public void OnClickNextManager(){
        if (mFormValidator.isValid()) {
            mRegisterSellerPresenter.saveNameHaveBuilding(mNameBuildingEditText.getText().toString(),mNumberCoachEditText.getText().toString());
            startActivity(new Intent(this, TypeSellerMap.class));
        }
    }

    @OnClick(R.id.mCancelManagerBuildingButton)
    public void OnCancelManagerBuilding() {
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mBackButton)
    public void OnBackButton(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
