package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.totalplay.sociodigital.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CancelRegisterDialog extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.cancel_register_dialog);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mCancelRegisterButton)
    public void OnCancelRegisterButton(){
        this.finish();
    }

    @OnClick(R.id.mAcceptCancelRegisterButton)
    public void OnAcceptCancelRegister(){
        Intent newIntent = new Intent(this,LoginActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
    }

}
