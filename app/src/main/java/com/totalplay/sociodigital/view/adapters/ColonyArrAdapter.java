package com.totalplay.sociodigital.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.entities.ColonyCP;

import java.util.ArrayList;

public class ColonyArrAdapter extends RecyclerView.Adapter<ColonyArrAdapter.SimplerecyclerViewHolder>{

    private ArrayList<ColonyCP> colonyCPArrayList;
    private Context context;
    private updateColonyName updateColonyName;

    public ColonyArrAdapter(ArrayList<ColonyCP> colonyCPArrayList, Context context, ColonyArrAdapter.updateColonyName updateColonyName) {
        this.colonyCPArrayList = colonyCPArrayList;
        this.context = context;
        this.updateColonyName = updateColonyName;
    }

    @NonNull
    @Override
    public SimplerecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_colony, viewGroup, false);
        return new ColonyArrAdapter.SimplerecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SimplerecyclerViewHolder viewHolder, int i) {
        viewHolder.nameColony.setText(colonyCPArrayList.get(i).nameColony);
    }

    @Override
    public int getItemCount() {
        return colonyCPArrayList.size();
    }

    class SimplerecyclerViewHolder extends RecyclerView.ViewHolder{

        private TextView nameColony;

        SimplerecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            nameColony =  itemView.findViewById(R.id.item_colony_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Prefs.instance().putString("AplicaEstimuloFiscal", colonyCPArrayList.get(getAdapterPosition()).appFiscalEstim);
                    updateColonyName.updateColony(colonyCPArrayList.get(getAdapterPosition()).nameColony);
                }
            });
        }
    }

    public interface updateColonyName{
        void updateColony(String colony);
    }
}

