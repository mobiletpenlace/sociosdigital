package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.entities.Additional;
import com.totalplay.sociodigital.model.entities.Promotion;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by imacbookpro on 05/09/18.
 * SocioDigital
 */

public class PayMethodActivity extends BaseActivity {

    public static Intent launch(Context context, PlanTotalPlay planSelected, ArrayList<Additional> additonals, ArrayList<Promotion> mPromotions, String priceList){
        Intent intent = new Intent(context,PayMethodActivity.class);
        intent.putExtra("planSelected", (Serializable) planSelected);
        intent.putExtra("additionals", (Serializable) additonals);
        intent.putExtra("promotions", (Serializable) mPromotions);
        intent.putExtra("priceList", priceList);
        return  intent;
    }

    private TextView mTitleTextView;
    private TextView mSubtitleTextView;
    private  TextView mCashTextView;
    private TextView mCardTextView;
    private ViewGroup mCashContent;
    private ViewGroup mCardContent;
    private ViewGroup mBackToolbar;

    private PlanTotalPlay mPlanTotalPlay;
    private ArrayList<Additional> mAdditonals;
    private ArrayList<Promotion> mPromotions;
    private String priceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.pay_method_acitivy);
        mPlanTotalPlay = (PlanTotalPlay) getIntent().getSerializableExtra("planSelected");
        mAdditonals = (ArrayList<Additional>) getIntent().getSerializableExtra("additionals");
        mPromotions = (ArrayList<Promotion>) getIntent().getSerializableExtra("promotions");
        priceList = getIntent().getStringExtra("priceList");
        bindResources();
        setFonts();
        setonClicks();
    }

    private void bindResources(){
        mTitleTextView =  findViewById(R.id.act_pay_method_title);
        mSubtitleTextView =  findViewById(R.id.act_pay_method_subtitle);
        mCashTextView =  findViewById(R.id.act_pay_method_cash_text);
        mCardTextView =  findViewById(R.id.act_pay_method_cash_text);
        mCashContent =  findViewById(R.id.act_pay_method_cash_payment);
        mCardContent = findViewById(R.id.act_pay_method_card_payment);
        mBackToolbar =  findViewById(R.id.mBackButton);
    }

    private void setFonts(){
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        mTitleTextView.setTypeface(monserratRegular);
        mSubtitleTextView.setTypeface(monserratRegular);
        mCardTextView.setTypeface(monserratRegular);
        mCashTextView.setTypeface(monserratRegular);
    }

    private void setonClicks(){
        mCardContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(PayWithCardActvity.launch(getApplicationContext(),mPlanTotalPlay,mAdditonals,mPromotions, priceList));
            }
        });

        mCashContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(PayWithCashActivity.launch(getApplicationContext(),mPlanTotalPlay,mAdditonals,mPromotions, priceList));
            }
        });

        mBackToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
