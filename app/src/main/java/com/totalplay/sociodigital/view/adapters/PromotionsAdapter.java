package com.totalplay.sociodigital.view.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.entities.Promotion;
import com.totalplay.sociodigital.view.activities.PackagesActivity;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 08/09/18.
 * SocioDigital
 */

public class PromotionsAdapter extends RecyclerView.Adapter<PromotionsAdapter.RecyclerViewSimpleTextViewHolder> {

    private ArrayList<Promotion> mPromotions;
    private Activity activity;
    Typeface monserratRegular;


    public PromotionsAdapter(Activity activity, ArrayList<Promotion>promotions) {
        this.activity = activity;
        this.mPromotions = promotions;
        monserratRegular = Typeface.createFromAsset(activity.getAssets(), "fonts/Montserrat-Regular.ttf");
    }

    @Override
    public PromotionsAdapter.RecyclerViewSimpleTextViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_promotional, viewGroup, false);
        return new PromotionsAdapter.RecyclerViewSimpleTextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewSimpleTextViewHolder holder, int i) {
        holder.title.setTypeface(monserratRegular);
        holder.title.setText(mPromotions.get(i).namePromotion);
    }


    public class RecyclerViewSimpleTextViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView subtitle;
        public RecyclerViewSimpleTextViewHolder(View itemView) {
            super(itemView);
            title= itemView.findViewById(R.id.item_promotional_title);
            subtitle= itemView.findViewById(R.id.item_promotional_detail);
        }
    }

    @Override
    public int getItemCount() {
        return mPromotions.size();
    }


}

