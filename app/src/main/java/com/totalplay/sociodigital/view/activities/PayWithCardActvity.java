package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.icu.text.IDNA;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.resources.utils.validators.TextViewValidator;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.request.document.DocumentBean;
import com.totalplay.sociodigital.background.ws.response.ConsultationSaleTPresponse;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.library.utils.StringUtils;
import com.totalplay.sociodigital.model.ConsutationSale.AdditionalData;
import com.totalplay.sociodigital.model.ConsutationSale.AdditionalsInformation;
import com.totalplay.sociodigital.model.ConsutationSale.BillingFact;
import com.totalplay.sociodigital.model.ConsutationSale.DocumentEvidence;
import com.totalplay.sociodigital.model.ConsutationSale.Documents;
import com.totalplay.sociodigital.model.ConsutationSale.PayMethod;
import com.totalplay.sociodigital.model.ConsutationSale.ProductDetail;
import com.totalplay.sociodigital.model.ConsutationSale.Promotional;
import com.totalplay.sociodigital.model.ConsutationSale.PromotionalsAdd;
import com.totalplay.sociodigital.model.ConsutationSale.ProspectInfo;
import com.totalplay.sociodigital.model.ConsutationSale.SaleTP;
import com.totalplay.sociodigital.model.ConsutationSale.SiteInfo;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.entities.Additional;
import com.totalplay.sociodigital.model.entities.PayMethodEntity;
import com.totalplay.sociodigital.model.entities.Promotion;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.callbacks.ConsultationSaleCallback;
import com.totalplay.sociodigital.presenter.implementations.ConsultationSalePresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

import static android.view.View.GONE;

/**
 * Created by imacbookpro on 05/09/18.
 * SocioDigital
 */

public class PayWithCardActvity extends BaseActivity implements ConsultationSaleCallback {

    public static Intent launch(Context context, PlanTotalPlay planSelected, ArrayList<Additional> additonals, ArrayList<Promotion> mPromotions, String priceList) {
        Intent intent = new Intent(context, PayWithCardActvity.class);
        intent.putExtra("planSelected", planSelected);
        intent.putExtra("additionals", additonals);
        intent.putExtra("promotions", mPromotions);
        intent.putExtra("priceList", priceList);
        return intent;
    }

    private static int MY_SCAN_REQUEST_CODE = 8;
    private PayMethodEntity payMethod = new PayMethodEntity();
    ConsultationSalePresenter mConsultationSalePresenter;
    private LoginPresenter mLoginPresenter;

    private TextView mTitleTextView;
    private TextView mAmountToPayTextView;
    private TextView mAmountTextView;
    private TextView mNamePackageFamilyTextView;
    private TextView mBankAccountTextView;
    private TextView mCardNameTextView;
    private EditText mCardNameEditext;
    private TextView mCardNumberTextView;
    private EditText mCardNumberEditext;
    private Button mRegisterCard;
    private TextView mScanCardTextView;
    private ViewGroup mScanCardContent;
    private TextView mExpirateDateTextView;
    private EditText mExpirateDateEditext;
    private CheckBox mDomiciliedCard;
    private TextView mDomiciliedTextView;
    private ViewGroup mBackToolbar;

    //Dialog Success oportunity
    private TextView mSuccesOportunityTitleTextview;
    private TextView mSuccessOportunityDetailTextview;
    private TextView mSuccessOportunitySolicitudeNumber;
    private TextView mSuccessOportunityNumber;
    private Button mSuccessOportunityAccept;
    private ViewGroup mSuceessOportunityContent;

    //Dialog Failed oportunity
    private TextView mFailedOportunityTitleTextView;
    private TextView mFailedOportunityDetailTextView;
    private Button mFailedOportunityRetryButton;
    private ViewGroup mFailedOportunityContent;

    //Dialog Syncronized Documents
    private TextView mSyncDocumentsTitleTextView;
    private TextView mSyncDocumentsDetailTextview;
    private ViewGroup mSyncDicumentsContent;

    private FormValidator mFormValidator;

    private PlanTotalPlay mPlanTotalPlay;
    private ArrayList<Additional> mAdditonals;
    private ArrayList<Promotion> mPromotions;

    private ViewGroup contentCredtiCard;
    private ViewGroup contentDebitCard;
    private ImageView typeCreditCardImageView;
    private ImageView typeDebitCardImageView;
    private boolean debitCard = false;
    private boolean creditCard = false;

    private TextWatcher alternateTextWatcher;
    private String tempNumber = "";
    private int progressStatus1;
    private Handler handler = new Handler();
    private ProgressBar progress;
    private ImageView imgResult;
    private String priceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.pay_with_card_activity);
        mPlanTotalPlay = (PlanTotalPlay) getIntent().getSerializableExtra("planSelected");
        mAdditonals = (ArrayList<Additional>) getIntent().getSerializableExtra("additionals");
        mPromotions = (ArrayList<Promotion>) getIntent().getSerializableExtra("promotions");
        priceList =  getIntent().getStringExtra("priceList");
        bindResources();
        setFonts();
        setOnClicks();
        createValidator();
        alternateTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = mCardNumberEditext.getText().toString();
                if (text.length() > 0) {
                    if (!(tempNumber.length() > text.length())) {
                        if (text.length() == 4 || text.length() == 9 || text.length() == 14){
                            text =  text + " ";
                            tempNumber = tempNumber + text.substring(text.length() - 2);
                        }else{
                            tempNumber = tempNumber + text.substring(text.length() - 1);
                        }
                    } else {
                        tempNumber = tempNumber.substring(0, tempNumber.length() - 1);
                    }

                    if (text.length() > 0 && text.length() < 16)
                        text = text.replaceAll("[0-9]", "•");

                    setNumber(text);
                }else {
                    tempNumber = "";
                }

            }
        };
//        textWatcher();
        mCardNumberEditext.addTextChangedListener(alternateTextWatcher);
        mConsultationSalePresenter = new ConsultationSalePresenter(this, this);
        putData();
        mLoginPresenter = new LoginPresenter(this);
        Glide.with(this).load(R.drawable.ic_charging).into(imgResult);
    }
/*
    private void textWatcher() {

    }
*/
    private void setNumber(String text) {
        mCardNumberEditext.removeTextChangedListener(alternateTextWatcher);
        mCardNumberEditext.setText(text);
        mCardNumberEditext.addTextChangedListener(alternateTextWatcher);
        if(text.length() < 19)
            mCardNumberEditext.setSelection(text.length());

    }

    private void bindResources() {
        mTitleTextView = findViewById(R.id.act_pay_whith_card_title);
        mAmountTextView = findViewById(R.id.act_pay_whith_card_amount);
        mAmountToPayTextView = findViewById(R.id.act_pay_whith_card_amount_to_pay);
        mNamePackageFamilyTextView = findViewById(R.id.act_pay_whith_card_name_package);
        mBankAccountTextView = findViewById(R.id.act_pay_whith_card_bank_account);
        mCardNameTextView = findViewById(R.id.act_pay_whith_card_name_card);
        mCardNameEditext = findViewById(R.id.act_pay_whith_card_name_card_text);
        mCardNumberTextView = findViewById(R.id.act_pay_whith_card_card_number);
        mCardNumberEditext = findViewById(R.id.act_pay_whith_card_card_number_text);
        mRegisterCard = findViewById(R.id.act_pay_whith_card_register_card);
        mScanCardContent = findViewById(R.id.act_pay_whith_card_scan_card_content);
        mScanCardTextView = findViewById(R.id.act_pay_whith_card_scan_card_text);
        mDomiciliedCard = findViewById(R.id.act_pay_whith_card_domicilied_card);
        mDomiciliedTextView = findViewById(R.id.act_pay_whith_card_domicilied_card_text);
        mExpirateDateTextView = findViewById(R.id.act_pay_whith_card_expired_date);
        mExpirateDateEditext = findViewById(R.id.act_pay_whith_card_expired_date_text);
        mBackToolbar = findViewById(R.id.mBackButton);
        //Dialog SuccessOportunity
        mSuccesOportunityTitleTextview = findViewById(R.id.dialog_success_oportunity_title);
        mSuccessOportunityDetailTextview = findViewById(R.id.dialog_success_oportunity_detail);
        mSuccessOportunitySolicitudeNumber = findViewById(R.id.dialog_success_oportunity_number_solicitude);
        mSuccessOportunityNumber = findViewById(R.id.dialog_success_oportunity_number);
        mSuccessOportunityAccept = findViewById(R.id.dialog_success_oportunity_accept);
        mSuceessOportunityContent = findViewById(R.id.dialog_success_oportunity_content);
        //Dialog FailedOportunity
        mFailedOportunityTitleTextView = findViewById(R.id.dialog_failed_oportunity_title);
        mFailedOportunityDetailTextView = findViewById(R.id.dialog_failed_oportunity_detail);
        mFailedOportunityRetryButton = findViewById(R.id.dialog_failed_oportunity_retry);
        mFailedOportunityContent = findViewById(R.id.dialog_failed_oportunity_content);
        //Dialog SyncDocuments
        mSyncDocumentsTitleTextView = findViewById(R.id.dialog_syncronized_documents_title);
        mSyncDocumentsDetailTextview = findViewById(R.id.dialog_syncronized_documents_detail);
        mSyncDicumentsContent = findViewById(R.id.dialog_syncronized_documents_content);
        //Content typeCard
        contentCredtiCard = findViewById(R.id.typeCreditCard);
        contentDebitCard = findViewById(R.id.typeDebitCard);
        typeCreditCardImageView = findViewById(R.id.typeCreditCardImageView);
        typeDebitCardImageView = findViewById(R.id.typeDebitCardImageView);

        progress =  findViewById(R.id.dialog_synchronized_documents_progress);
        imgResult =  findViewById(R.id.dialog_synchronized_documents_result_img);
    }

    private void setFonts() {
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");

        mTitleTextView.setTypeface(monserratRegular);
        mAmountToPayTextView.setTypeface(monserratBold);
        mAmountTextView.setTypeface(monserratBold);
        mNamePackageFamilyTextView.setTypeface(monserratBold);
        mBankAccountTextView.setTypeface(monserratBold);
        mCardNameTextView.setTypeface(monserratBold);
        mCardNameEditext.setTypeface(monserratBold);
        mCardNumberTextView.setTypeface(monserratBold);
        mCardNumberEditext.setTypeface(monserratBold);
        mScanCardTextView.setTypeface(monserratBold);
        mDomiciliedTextView.setTypeface(monserratBold);
        mExpirateDateTextView.setTypeface(monserratBold);
        mExpirateDateEditext.setTypeface(monserratBold);
        mRegisterCard.setTypeface(monserratMedium);
        //Dialog Success Oportunity
        mSuccesOportunityTitleTextview.setTypeface(monserratRegular);
        mSuccessOportunityDetailTextview.setTypeface(monserratRegular);
        mSuccessOportunitySolicitudeNumber.setTypeface(monserratBold);
        mSuccessOportunityNumber.setTypeface(monserratBold);
        mSuccessOportunityAccept.setTypeface(monserratMedium);
        //Dialog FailedOportunity
        mFailedOportunityTitleTextView.setTypeface(monserratRegular);
        mFailedOportunityDetailTextView.setTypeface(monserratRegular);
        mFailedOportunityRetryButton.setTypeface(monserratMedium);
        //Dialog SyncDocuments
        mSyncDocumentsTitleTextView.setTypeface(monserratRegular);
        mSyncDocumentsDetailTextview.setTypeface(monserratBold);
    }

    private void setOnClicks() {
        mRegisterCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        mScanCardContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scanIntent = new Intent(getApplicationContext(), CardIOActivity.class);

                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

                startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
            }
        });

        mBackToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mSuccessOportunityAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSuceessOportunityContent.setVisibility(GONE);
                Intent newIntent = new Intent(getApplicationContext(), HomeActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(newIntent);
            }
        });

        mFailedOportunityRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(getApplicationContext()).load(R.drawable.ic_charging).into(imgResult);
                progress.setProgress(0);
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
                mFailedOportunityContent.setVisibility(GONE);
            }
        });

        mSyncDicumentsContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        mFailedOportunityContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mSuceessOportunityContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        contentCredtiCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeCreditCardImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
                typeDebitCardImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
                creditCard = true;
                debitCard = false;
            }
        });

        contentDebitCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeCreditCardImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
                typeDebitCardImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
                creditCard = true;
                debitCard = false;
            }
        });

        SimpleMaskFormatter smf = new SimpleMaskFormatter("NN/NN");
        MaskTextWatcher mtw = new MaskTextWatcher(mExpirateDateEditext, smf);
        mExpirateDateEditext.addTextChangedListener(mtw);
    }

    public void createValidator() {
        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mCardNameEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCardNameEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCardNameEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                //new EditTextValidator(idCardTypeEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new TextViewValidator(mCardNumberEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new TextViewValidator(mExpirateDateEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    private void putData() {
        mNamePackageFamilyTextView.setText(mPlanTotalPlay.namePackage);
        Double instaConst = Double.parseDouble(Prefs.instance().string(SocioDigitalKeys.INSTALATION_COST));
        Double listPriceDouble = Double.valueOf(priceList);
        Double totalPay = (Math.ceil(instaConst) + Math.ceil(listPriceDouble));
        mAmountTextView.setText(String.format("$ %s", String.format("%.2f", totalPay)));
    }
    public boolean validateMonth(int month){

        if(month > 12 || month ==0){
            return false;
        }else{
            return true;
        }
    }


    private void getData() {
        if (debitCard || creditCard) {
            if (mFormValidator.isValid()) {
                String[] result = mExpirateDateEditext.getText().toString().split("/");
                if (validateMonth(Integer.parseInt(result[0]) )  ) {
                    if(validateYear( Integer.parseInt(result[1]) + 2000)){
                        if (validateCarNumber()) {
                            String cardNumberString = mCardNumberEditext.getText().toString().replace(" ", "");
                            payMethod.cardNumber = (CryptoUtils.crypt(cardNumberString.trim()));
                            payMethod.nameOwner = (CryptoUtils.crypt(mCardNameEditext.getText().toString()));
                            payMethod.lastNameFatherOwner = (CryptoUtils.crypt(mCardNameEditext.getText().toString()));
                            payMethod.lastNameMotherOwner = (CryptoUtils.crypt(mCardNameEditext.getText().toString()));
                            String[] resul2 = mExpirateDateEditext.getText().toString().split("/");
                            payMethod.expiryMonth = (CryptoUtils.crypt(result[0]));
                            payMethod.expiryYear = (CryptoUtils.crypt(result[1]));
                            payMethod.typeCard = CryptoUtils.crypt(StringUtils.getTypeCard(mCardNumberEditext.getText().toString().trim()));
                            if (debitCard) {
                                payMethod.method = (CryptoUtils.crypt("TDD"));
                            } else {
                                payMethod.method = (CryptoUtils.crypt("TDC"));
                            }
                            createProspect();
                        }else{
                            MessageUtils.toast(this,"Ingrese un número de tarjeta válido");
                        }
                    }else{
                        MessageUtils.toast(this, "El año no es válido");
                    }
                } else {
                    MessageUtils.toast(this, "El mes es incorrecto");
                }


            } else
                MessageUtils.toast(this, "Falta información por ingresar");
        } else {
            MessageUtils.toast(this, "Seleccione un tipo de tarjeta para continuar");
        }

    }

    private boolean validateYear(int current) {
        final Calendar c = Calendar.getInstance();
        int mAnio = c.get(Calendar.YEAR);

        int maxAnio = mAnio + 10;
        if(mAnio < current && current < maxAnio){
            return true;
        }else{
            return false;
        }
    }
/*
    private void createProspect(){
        AdditionalData additionalData = new AdditionalData(mFormalityEntity.prospectoBean.signature.additionalInfo.typeID,
                mFormalityEntity.prospectoBean.contact.medioContacto,
                mFormalityEntity.prospectoBean.signature.additionalInfo.infoID);
        BillingFact billingFact =  new BillingFact(CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.street),
                mFormalityEntity.prospectoBean.direccionBean.place,
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.zipCode),
                mFormalityEntity.prospectoBean.direccionBean.colony,
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.delegation),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.state),
                "true",
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.noExt),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.noInt));
        ArrayList<DocumentEvidence> mDocumentsEvidence = new ArrayList<>();
        List<DocumentBean> documents = mDocumentFilesRequest.documentAdd.document;
        if (documents.size() < 4){
            DocumentBean idProofs = new DocumentBean();
            idProofs.idOpportunity = "";
            idProofs.idOpportunity = "";
            idProofs.brmAccount = "";
            idProofs.name = "Comprobante.jpeg";
            idProofs.type = "Comprobante de domicilio";
            idProofs.body = documents.get(0).body;
            idProofs.thumbnail = documents.get(0).thumbnail;
            documents.add(idProofs);
        }
        for (DocumentBean document: documents){
            mDocumentsEvidence.add(new DocumentEvidence("BOOOOODYYYYY",document.name,document.type));
        }
        Documents mDocument = new Documents(mDocumentsEvidence);
        PayMethod payMethodService = new PayMethod(CryptoUtils.desEncrypt(payMethod.expiryYear),
                CryptoUtils.desEncrypt(payMethod.lastNameMotherOwner),
                CryptoUtils.desEncrypt(payMethod.lastNameFatherOwner),
                CryptoUtils.desEncrypt(payMethod.cardNumber),
                CryptoUtils.desEncrypt(payMethod.expiryMonth),
                CryptoUtils.desEncrypt(payMethod.method),
                CryptoUtils.desEncrypt(payMethod.nameOwner),
                CryptoUtils.desEncrypt(payMethod.typeCard));
        ProspectInfo prospectInfo = new ProspectInfo(mFormalityEntity.prospectoBean.direccionBean.street,
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.cellphone),
                mFormalityEntity.prospectoBean.direccionBean.place,
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.zipCode),
                mFormalityEntity.prospectoBean.direccionBean.colony,
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.socialReason),//Si es moral poner aqui el nombre?
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.email),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.delegation),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.state),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.birthDay),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.name),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.fatherLastName),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.motherLastName),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.noExt),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.noInt),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.otherEmail),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.otherPhone),
                mFormalityEntity.prospectoBean.direccionBean.betweenStreets,
                mFormalityEntity.prospectoBean.direccionBean.urbanReference,
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.rfc),
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.contact.phone),
                mFormalityEntity.prospectoBean.contact.typePerson,
                "a0hQ0000004C8QnIAK",
                "Socios TP",
                "ClienteTP");

        SiteInfo siteInfo =  new SiteInfo(Prefs.instance().string("categoryService"),//poner aqui lo de la fibra?
                mFormalityEntity.prospectoBean.direccionBean.cluster,
                mFormalityEntity.prospectoBean.direccionBean.district,
                mFormalityEntity.prospectoBean.direccionBean.feasibility,
                mFormalityEntity.prospectoBean.direccionBean.latitude,
                mFormalityEntity.prospectoBean.direccionBean.longitude,
                mFormalityEntity.prospectoBean.direccionBean.place,
                mFormalityEntity.prospectoBean.direccionBean.region,
                mFormalityEntity.prospectoBean.direccionBean.idRegion,
                mFormalityEntity.prospectoBean.direccionBean.typeCoverage,
                mFormalityEntity.prospectoBean.direccionBean.zone);
        ArrayList<Promotional> promotionals =  new ArrayList<>();
        for (Promotion promotion: mPromotions){
            promotionals.add(new Promotional(promotion.idPromotion,promotion.namePromotion));
        }
        PromotionalsAdd promotionalsAdd = new PromotionalsAdd(promotionals);

        com.totalplay.sociodigital.model.ConsutationSale.ProductsAdd productsAdd =  new com.totalplay.sociodigital.model.ConsutationSale.ProductsAdd(new ArrayList<ProductDetail>());
        //AdditionalsInformation additionalsInformation =  new AdditionalsInformation(productsAdd, promotionalsAdd);
        AdditionalsInformation additionalsInformation =  new AdditionalsInformation();
        SaleTP saleTP = new SaleTP(additionalData,billingFact,mDocument,payMethodService,mPlanTotalPlay.id,prospectInfo,siteInfo,additionalsInformation);
        mConsultationSalePresenter.createSale(saleTP);
    }

*/

    private void createProspect() {
        InfoSocioBean infoSocioBean = mLoginPresenter.retrieveInfoSocio();
        AdditionalData additionalData = new AdditionalData(CryptoUtils.crypt(mFormalityEntity.prospectoBean.signature.additionalInfo.typeID),
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.contact.medioContacto),
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.signature.additionalInfo.infoID));
        BillingFact billingFact = new BillingFact(mFormalityEntity.prospectoBean.direccionBean.street,
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.direccionBean.place),
                mFormalityEntity.prospectoBean.direccionBean.zipCode,
                mFormalityEntity.prospectoBean.direccionBean.colony,
                mFormalityEntity.prospectoBean.direccionBean.delegation,
                mFormalityEntity.prospectoBean.direccionBean.state,
                CryptoUtils.crypt("true"),
                mFormalityEntity.prospectoBean.direccionBean.noExt,
                mFormalityEntity.prospectoBean.direccionBean.noInt);
        ArrayList<DocumentEvidence> mDocumentsEvidence = new ArrayList<>();
        List<DocumentBean> documents = mDocumentFilesRequest.documentAdd.document;
        if (documents.size() < 4) {
            DocumentBean idProofs = new DocumentBean();
            idProofs.idOpportunity = "";
            idProofs.idOpportunity = "";
            idProofs.brmAccount = "";
            idProofs.name = "Comprobante.jpeg";
            idProofs.type = "Comprobante de domicilio";
            idProofs.body = documents.get(0).body;
            idProofs.thumbnail = documents.get(0).thumbnail;
            documents.add(idProofs);
        }
        for (DocumentBean document : documents) {
            mDocumentsEvidence.add(new DocumentEvidence(document.body, document.name, document.type));
        }
        Documents mDocument = new Documents(mDocumentsEvidence);
        PayMethod payMethodService = new PayMethod(payMethod.expiryYear,
                payMethod.lastNameMotherOwner,
                payMethod.lastNameFatherOwner,
                payMethod.cardNumber,
                payMethod.expiryMonth,
                payMethod.method,
                payMethod.nameOwner,
                payMethod.typeCard);
        String estimFiscal = Prefs.instance().string("AplicaEstimuloFiscal");
        ProspectInfo prospectInfo = new ProspectInfo(mFormalityEntity.prospectoBean.direccionBean.street,
                mFormalityEntity.prospectoBean.contact.cellphone,
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.direccionBean.place),
                mFormalityEntity.prospectoBean.direccionBean.zipCode,
                mFormalityEntity.prospectoBean.direccionBean.colony,
                mFormalityEntity.prospectoBean.contact.socialReason,//Si es moral poner aqui el nombre?
                mFormalityEntity.prospectoBean.contact.email,
                mFormalityEntity.prospectoBean.direccionBean.delegation,
                mFormalityEntity.prospectoBean.direccionBean.state,
                mFormalityEntity.prospectoBean.contact.birthDay,
                mFormalityEntity.prospectoBean.contact.name,
                mFormalityEntity.prospectoBean.contact.motherLastName,
                mFormalityEntity.prospectoBean.contact.fatherLastName,
                mFormalityEntity.prospectoBean.direccionBean.noExt,
                mFormalityEntity.prospectoBean.direccionBean.noInt,
                mFormalityEntity.prospectoBean.contact.otherEmail,
                mFormalityEntity.prospectoBean.contact.otherPhone,
                mFormalityEntity.prospectoBean.direccionBean.betweenStreets,
                mFormalityEntity.prospectoBean.direccionBean.urbanReference,
                mFormalityEntity.prospectoBean.contact.rfc,
                mFormalityEntity.prospectoBean.contact.phone,
                mFormalityEntity.prospectoBean.contact.typePerson,
                CryptoUtils.crypt(infoSocioBean.idOportunity),
                CryptoUtils.crypt(infoSocioBean.mCanal),
                CryptoUtils.crypt(infoSocioBean.mSubCanal),
                estimFiscal);

        SiteInfo siteInfo = new SiteInfo(Prefs.instance().string("categoryService"),//poner aqui lo de la fibra?
                mFormalityEntity.prospectoBean.direccionBean.cluster,
                mFormalityEntity.prospectoBean.direccionBean.district,
                mFormalityEntity.prospectoBean.direccionBean.feasibility,
                mFormalityEntity.prospectoBean.direccionBean.latitude,
                mFormalityEntity.prospectoBean.direccionBean.longitude,
                mFormalityEntity.prospectoBean.direccionBean.place,
                mFormalityEntity.prospectoBean.direccionBean.region,
                mFormalityEntity.prospectoBean.direccionBean.idRegion,
                "Sólo fibra",
                mFormalityEntity.prospectoBean.direccionBean.zone);
        ArrayList<Promotional> promotionals = new ArrayList<>();
        for (Promotion promotion : mPromotions) {
            promotionals.add(new Promotional(promotion.idPromotion, promotion.namePromotion));
        }
        PromotionalsAdd promotionalsAdd = new PromotionalsAdd(promotionals);

        com.totalplay.sociodigital.model.ConsutationSale.ProductsAdd productsAdd = new com.totalplay.sociodigital.model.ConsutationSale.ProductsAdd(new ArrayList<ProductDetail>());
        //AdditionalsInformation additionalsInformation =  new AdditionalsInformation(productsAdd, promotionalsAdd);
        AdditionalsInformation additionalsInformation = new AdditionalsInformation();
        SaleTP saleTP = new SaleTP(additionalData, billingFact, mDocument, payMethodService, mPlanTotalPlay.id, prospectInfo, siteInfo, additionalsInformation);
        mConsultationSalePresenter.createSale(saleTP);
    }


    @Override
    public void onErrorConnection() {
        //MessageUtils.stopProgress();
        startProgress1(progress,false);
    }

    @Override
    public void onLoadConsultationSale() {
        //MessageUtils.progress(this,"Creando venta");
        mSyncDicumentsContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccesConsultationSale(ConsultationSaleTPresponse response) {
        //MessageUtils.stopProgress();
        //MessageUtils.toast(this,"Venta creada con exito");
        String[] result = response.mResult.resultDescription.split(":");
        String noSolicitude = null;
        if (result.length > 1) {
            noSolicitude = result[1].trim();
            mSuccessOportunityNumber.setText(noSolicitude);
        }
        startProgress1(progress,true);
    }

    @Override
    public void onErrorConsultationSale() {
        //MessageUtils.stopProgress();
        startProgress1(progress, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                mCardNumberEditext.removeTextChangedListener(alternateTextWatcher);
                tempNumber = scanResult.cardNumber.substring(0, 4) +  " " + scanResult.cardNumber.substring(4,8) + " " + scanResult.cardNumber.substring(8,12) + " " + scanResult.cardNumber.substring(12,16);
                String numPart1 = scanResult.cardNumber.substring(0,4) +  " " + scanResult.cardNumber.substring(4,8) + " " + scanResult.cardNumber.substring(8,12) + " ";
                numPart1 = numPart1.replaceAll("[0-9]", "•");
                mCardNumberEditext.setText(numPart1 + scanResult.cardNumber.substring(12 , 16));
                mCardNumberEditext.addTextChangedListener(alternateTextWatcher);
//                mCardNumberEditext.setText(scanResult.cardNumber);//

                if (scanResult.isExpiryValid()) {
                    String month = String.valueOf(scanResult.expiryMonth);
                    if (scanResult.expiryMonth < 10)

                        month = "0" + month;
                    String year = String.valueOf(scanResult.expiryYear).substring(2);
                    mExpirateDateEditext.setText(month + "/" + year);
                }
            }
        }
    }
    public String setTypeCard(String mNumberCad){
        return com.totalplay.sociodigital.library.utils.StringUtils.getIdTypeCard(mNumberCad);
    }


    private boolean validateCarNumber(){
        boolean isValidNumber =  false;
        String result = setTypeCard(tempNumber.trim().replace(" ", ""));
        if (result.equals("1") || result.equals("2")  || result.equals("3")){
            isValidNumber =  true;
        }


/*        boolean isValidNumber =  false;
        String result = StringUtils.getTypeCard(mCardNumberEditext.getText().toString().replace(" ", ""));
        if (result.equals("Visa") || result.equals("Master Card")  || result.equals("American Express")){
            isValidNumber =  true;
        }
*/        return isValidNumber;
    }

    private void startProgress1(ProgressBar progress, boolean isSuccessRegister){
        progressStatus1 = 0;
        runOnUiThread(() -> {
            progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
        });
        new Thread(() -> {
            progressStatus1 = 0;
            boolean enable = true;
            while(progressStatus1 < 100){
                progressStatus1 +=1;
                if (progressStatus1 > 95 && enable){
                    runOnUiThread(() -> {
                        //launch image
                        if (isSuccessRegister){
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_green).into(imgResult);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mSyncDicumentsContent.setVisibility(GONE);
                                    mSuceessOportunityContent.setVisibility(View.VISIBLE);
                                }
                            }, 1200);
                        }else {
                            progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_red));
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_red).into(imgResult);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mSyncDicumentsContent.setVisibility(GONE);
                                    mFailedOportunityContent.setVisibility(View.VISIBLE);
                                    MessageUtils.toast(getApplicationContext(), "Error al general la venta");
                                }
                            }, 1200);
                        }
                    });
                    enable =  false;
                }
                try{
                    Thread.sleep(20);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(() -> progress.setProgress(progressStatus1));
            }
        }).start();
    }
}
