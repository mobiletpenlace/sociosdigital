package com.totalplay.sociodigital.view.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.FamilyPackagesResponse;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.presenter.callbacks.DetailPacakagesCallback;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 31/08/18.
 * SocioDigital
 */

public class PackageAdapter extends PagerAdapter {

    private Activity activity;
    private ArrayList<PlanTotalPlay> plans = new ArrayList<>();
    private View.OnClickListener mItemClickListener;
    private DetailPacakagesCallback mDetailPacakagesCallback;

    public PackageAdapter(Activity activity, ArrayList<PlanTotalPlay> plans, DetailPacakagesCallback detailPacakagesCallback) {
        this.plans = plans;
        this.activity = activity;
        this.mDetailPacakagesCallback =  detailPacakagesCallback;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        view = LayoutInflater.from(activity).inflate(R.layout.item_package, null);
        ImageView imgPackage = view.findViewById(R.id.item_package_image);
        ImageView imgPromo = view.findViewById(R.id.item_package_image);
        //TextView detailTextView = view.findViewById(R.id.item_packege_detail);
        TextView nameFamilyTextView = view.findViewById(R.id.item_package_name_family);
        //Typeface monserratMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(activity.getAssets(), "fonts/Montserrat-Regular.ttf");
        Typeface monserratBold = Typeface.createFromAsset(activity.getAssets(), "fonts/Montserrat-Bold.ttf");
        //detailTextView.setTypeface(monserratRegular);
        nameFamilyTextView.setTypeface(monserratBold);
        //nameFamilyTextView.setText(plans.get(position).familyPackage);
        //TextView infoPackage = (TextView) view.findViewById(R.id.act_packages_detail);
        if (plans.get(position).packageImage != null){
            Glide.with(activity).load(plans.get(position).packageImage).into(imgPackage);
            // Glide.with(activity).load(packageList.get(position).imgPromo).into(imgPromo);
            //infoPackage.setText(packageList.get(position).planDetail);
        }

        //linearLayout.setTag(packageList.get(position));
        //linearLayout.setOnClickListener(mItemClickListener);

        //detailTextView.setText(plans.get(position).planDetail.replace("*br*", "\n \n"));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDetailPacakagesCallback.setDetailPacakage(plans.get(position).planDetail.replace("*br*", "\n \n"));
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return plans.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    private void setFont(){


    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
