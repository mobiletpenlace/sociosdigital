package com.totalplay.sociodigital.view.activities;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.resources.utils.validators.TextViewValidator;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.handlers.FingerprintHandler;
import com.totalplay.sociodigital.library.quoter.utils.Constans;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity{

    @BindView(R.id.mUserEditText)
    public EditText mUserEditText;
    @BindView(R.id.mPasswordEditText)
    public EditText mPasswordEditText;
    @BindView(R.id.act_login_recover_pass)
    public  TextView recoverPAssTextView;
    @BindView(R.id.mFingerprintCheckBox)
    public CheckBox mFingerprintCheckBox;
    public boolean flagLogin = false;
    public LoginPresenter mLoginPresenter;
    public FormValidator mFormValidator;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        requestPermission();
        addValidator();
        checkBoxValidate();

        SpannableString content = new SpannableString(getResources().getString(R.string.login_recover_password));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        recoverPAssTextView.setText(content);
        mPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 2){
                    mFingerprintCheckBox.setVisibility(View.INVISIBLE);
                }else {
                    mFingerprintCheckBox.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    protected BasePresenter getPresenter() {
        mLoginPresenter = new LoginPresenter(this);
        return mLoginPresenter;
    }

    private void addValidator(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mUserEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPasswordEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    private void requestPermission(){
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                finish();
            }
        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getResources().getString(R.string.login_accept_permissions))
                .setPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                        //Manifest.permission.USE_FINGERPRINT
                )
                .check();

    }

    @OnClick(R.id.mRegisterButton)
    public void OnRegisterButton(){
        startActivity(new Intent(this, WelcomeActivity.class ));
    }

    @OnClick(R.id.mEnterButton)
    public void OnEnterButton(){
        if (mFingerprintCheckBox.isChecked()){
            flagLogin = true;
        }
        if (mFormValidator.isValid()) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mPasswordEditText.getWindowToken(), 0);
            mLoginPresenter.login(mUserEditText.getText().toString().trim(), mPasswordEditText.getText().toString().trim(), flagLogin);
        }

    }

    @OnClick(R.id.act_login_recover_pass)
    public void onClick(){
        startActivity(RecoverPassActivity.launch(this));
    }

    public void checkBoxValidate(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean flag = sharedPreferences.getBoolean(Constans.KEY_CHECKED, false);

        if (flag){
            alertDialog();
        }
    }

    public void alertDialog(){
        startActivity(new Intent(this, LoginFingerActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
