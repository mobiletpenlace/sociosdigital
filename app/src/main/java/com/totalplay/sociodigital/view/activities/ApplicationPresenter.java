package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import com.totalplay.sociodigital.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationPresenter {
    private Context context;

    public ApplicationPresenter(Context context) {
        this.context = context;
    }

    public void setup(){
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

}
