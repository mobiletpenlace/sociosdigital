package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.PMPlanesSocioResponse;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.callbacks.PMPlanesCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;
import com.totalplay.sociodigital.presenter.implementations.PMPlanesPresenter;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public class TypeProfileActivity extends BaseActivity implements PMPlanesCallback {

    public static Intent launch(Context context, String city){
        Intent intent =  new Intent(context, TypeProfileActivity.class);
        intent.putExtra("getCity", city);
        return  intent;
    }

    private TextView mTitleTextView;
    private TextView mSubtititleTextView;
    private ViewGroup mContentHome;
    private ViewGroup mContentBusiness;
    private TextView mHomeTextView;
    private TextView mBusinessTextView;
    private Button mCancelButton;
    private ViewGroup mBackToolbar;

    PMPlanesPresenter mPMPlanesPresenter;
    private ArrayList<String> idPlanses = new ArrayList<>();
    private String city;
    ArrayList<PlanTotalPlay>planTP = new ArrayList<>();
    ArrayList<PlanTotalPlay>planesMicronegocio = new ArrayList<>();
    ArrayList<PlanTotalPlay>planesResidencial = new ArrayList<>();
    LoginPresenter mLoginPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.type_profile_activity);
        city = getIntent().getStringExtra("getCity");
        bindResources();
        setFonts();
        setOnClicks();
        mPMPlanesPresenter = new PMPlanesPresenter(this,this);
        mLoginPresenter =  new LoginPresenter(this);
        InfoSocioBean infoSocioBean = mLoginPresenter.retrieveInfoSocio();
        if (infoSocioBean.mSubCanal != null){
            if (infoSocioBean.mSubCanal.length() > 0)
                mPMPlanesPresenter.loadPMPlanes(CryptoUtils.crypt(infoSocioBean.mSubCanal),city);
            else
                mPMPlanesPresenter.loadPMPlanes(CryptoUtils.crypt("Socio Edificios"), city);
        }else {
            mPMPlanesPresenter.loadPMPlanes(CryptoUtils.crypt("Socio Edificios"), city);
        }

    }



    private void bindResources(){
        mTitleTextView = findViewById(R.id.act_type_profile_title);
        mSubtititleTextView =  findViewById(R.id.act_type_profile_subtitle);
        mContentHome =  findViewById(R.id.act_type_profile_content_home);
        mContentBusiness =  findViewById(R.id.act_type_profile_content_business);
        mHomeTextView =  findViewById(R.id.act_type_profile_text_home);
        mBusinessTextView =  findViewById(R.id.act_type_profile_text_business);
        mCancelButton =  findViewById(R.id.act_type_profile_cancel_button);
        mBackToolbar =  findViewById(R.id.mBackButton);
    }

    private void setFonts(){
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        mTitleTextView.setTypeface(monserratRegular);
        mSubtititleTextView.setTypeface(monserratRegular);
        mHomeTextView.setTypeface(monserratRegular);
        mBusinessTextView.setTypeface(monserratRegular);
        mCancelButton.setTypeface(monserratMedium);
    }

    private void setOnClicks(){
        mContentHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(PlansActivity.launch(getApplicationContext(),planesResidencial));
            }
        });

        mContentBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(PlansActivity.launch(getApplicationContext(),planesMicronegocio));
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CancelSaleDialog.class));
            }
        });

        mBackToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
    }

    @Override
    public void onLoadPMPlanes() {
        MessageUtils.progress(this,"Cargando información");
    }

    @Override
    public void onSuccessPMPlanes(PMPlanesSocioResponse response) {
        MessageUtils.stopProgress();
        planTP = response.planTP;
        agroupFamily();
    }

    @Override
    public void onErrorPMPlanes() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error al cargar la inforamción");
    }

    private void agroupFamily(){
        planesMicronegocio.clear();
        planesResidencial.clear();
        for (PlanTotalPlay plan : planTP){
            if (plan.typePlan.equals("Residencial")){
                planesResidencial.add(plan);
            }else if(plan.typePlan.equals("Micronegocios")){
                planesMicronegocio.add(plan);
            }
        }
    }
}
