package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.totalplay.sociodigital.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by imacbookpro on 20/09/18.
 * SocioDigital
 */

public class CancelSaleDialog extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.cancel_sale_dialog);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.dialog_cancel_operation_cancel_button)
    public void OnCancelRegisterButton(){
        this.finish();
    }

    @OnClick(R.id.dialog_cancel_operation_aceppt_button)
    public void OnAcceptCancelRegister(){
        Intent newIntent = new Intent(this,HomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
    }
}
