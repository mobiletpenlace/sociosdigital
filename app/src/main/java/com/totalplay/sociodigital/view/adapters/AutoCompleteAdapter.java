package com.totalplay.sociodigital.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.PredictionsResponse;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class AutoCompleteAdapter extends ArrayAdapter<PredictionsResponse.PredictionsEntity> {
    public AutoCompleteAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    ViewHolder holder;
    Context context;
    List<PredictionsResponse.PredictionsEntity> mPlaces;
    private Activity mActivity;

    public AutoCompleteAdapter(Context context, List<PredictionsResponse.PredictionsEntity> modelsArrayList, Activity activity) {
        super(context, R.layout.autocomplete_row, modelsArrayList);
        this.context = context;
        this.mPlaces = modelsArrayList;
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.autocomplete_row, parent, false);
            holder = new ViewHolder();
            holder.name = rowView.findViewById(R.id.place_name);
            holder.location = rowView.findViewById(R.id.place_detail);
            rowView.setTag(holder);

        } else
            holder = (ViewHolder) rowView.getTag();
        /***** Get each Model object from ArrayList ********/
        holder.place = mPlaces.get(position);
        StringTokenizer st = new StringTokenizer(holder.place.description, ",");
        /************  Set Model values in Holder elements ***********/

        holder.name.setText(st.nextToken());
        String desc_detail = "";
        for (int i = 1; i < st.countTokens(); i++) {
            if (i == st.countTokens() - 1) {
                desc_detail = desc_detail + st.nextToken();
            } else {
                desc_detail = desc_detail + st.nextToken() + ",";
            }
        }
        holder.location.setText(desc_detail);
        return rowView;
    }

    @Override
    public int getCount() {
        return mPlaces.size();
    }

    class ViewHolder {
        PredictionsResponse.PredictionsEntity place;
        TextView name, location;
    }
}
