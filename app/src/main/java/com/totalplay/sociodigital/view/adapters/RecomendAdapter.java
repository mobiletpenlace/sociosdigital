package com.totalplay.sociodigital.view.adapters;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.DataSaleMonthRecomend;
import com.totalplay.sociodigital.library.pojos.Data;
import com.totalplay.sociodigital.library.pojos.InfoDetail;
import com.totalplay.sociodigital.model.entities.DataSaleMonth;
import com.totalplay.sociodigital.model.pojos.MonthSalesBean;
import com.totalplay.sociodigital.model.pojos.ReferredSalesBean;
import com.totalplay.sociodigital.presenter.callbacks.MyIndicatorSelectCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class RecomendAdapter extends PagerAdapter{
    private Activity activity;
    private Data mRecomendData;
    private MyIndicatorSelectCallback mMyIndicatorSelectCallback;
    private String auxMonthCurrent;
    private String auxMonthBack1;
    private String auxMonthBack2;
    private String auxMonthBack3;

    public RecomendAdapter(Activity activity,
                           Data recomendData,
                           MyIndicatorSelectCallback mMyIndicatorSelectCallback,
                           String auxMonthCurrent,
                           String auxMonthBack1,
                           String auxMonthBack2,
                           String auxMonthBack3){
        this.activity=activity;
        this.mRecomendData =  recomendData;
        this.mMyIndicatorSelectCallback = mMyIndicatorSelectCallback;
        this.auxMonthCurrent =  auxMonthCurrent;
        this.auxMonthBack1 =  auxMonthBack1;
        this.auxMonthBack2 =  auxMonthBack2;
        this.auxMonthBack3 = auxMonthBack3;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = null;
        ViewGroup mContentIndicatorSales = null;
        TextView monthTextView = null;
        ImageView mImageView = null;
        ViewGroup mContentSubtitle = null;
        TextView mSubtitle = null;
        TextView dateDatTextView = null;

        if (view == null) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_card_referred, container, false);
            monthTextView = view.findViewById(R.id.item_card_referred_month);
            mContentIndicatorSales = view.findViewById(R.id.mContentIndicatorSales);
            mImageView = view.findViewById(R.id.item_card_referred_image);
            mContentSubtitle =  view.findViewById(R.id.item_card_referred_subtitle_content);
            mSubtitle =  view.findViewById(R.id.item_card_referred_subtitle);
            dateDatTextView =  view.findViewById(R.id.item_date_day);

            mSubtitle.setText("Mis  Recomendados del mes");
        }

        if (position == 3){
            mImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_calendar_blue));
            monthTextView.setTextColor(activity.getResources().getColor(R.color.blue_color));
            dateDatTextView.setTextColor(activity.getResources().getColor(R.color.blue_color));
            monthTextView.setText(getMonth(auxMonthCurrent));
            String[] dateArr = auxMonthCurrent.split("-");
            dateDatTextView.setText(dateArr[2]);
        }

        if (position == 2){
            mImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_calendar_pink_card));
            monthTextView.setTextColor(activity.getResources().getColor(R.color.pink_card));
            dateDatTextView.setTextColor(activity.getResources().getColor(R.color.pink_card));
            mContentSubtitle.setBackground(activity.getResources().getDrawable(R.drawable.shape_two_corner_bottom_pink_card));
            monthTextView.setText(getMonth(auxMonthBack1));
            dateDatTextView.setText(getNumberDays(auxMonthBack1));
        }

        if (position == 1){
            mImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_calendar_purple_card));
            monthTextView.setTextColor(activity.getResources().getColor(R.color.purple_card));
            dateDatTextView.setTextColor(activity.getResources().getColor(R.color.purple_card));
            mContentSubtitle.setBackground(activity.getResources().getDrawable(R.drawable.shape_two_corner_bottom_purple_card));
            monthTextView.setText(getMonth(auxMonthBack2));
            dateDatTextView.setText(getNumberDays(auxMonthBack2));
        }

        if (position == 0){
            mImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_calendar_orange));
            monthTextView.setTextColor(activity.getResources().getColor(R.color.orange_card));
            dateDatTextView.setTextColor(activity.getResources().getColor(R.color.orange_card));
            mContentSubtitle.setBackground(activity.getResources().getDrawable(R.drawable.shape_two_corner_bottom_orange_card));
            monthTextView.setText(getMonth(auxMonthBack3));
            dateDatTextView.setText(getNumberDays(auxMonthBack3));
        }
        mContentIndicatorSales.setOnClickListener(v -> {
            switch (position){
                case 3:
                    String[] dateArr = auxMonthCurrent.split("-");

                    mMyIndicatorSelectCallback.OnSelectIndicatorMonth(cleanDataForMonth(getAuxMonth(auxMonthCurrent)),auxMonthCurrent, dateArr[2]);
                    break;
                case 2:

                    mMyIndicatorSelectCallback.OnSelectIndicatorMonth(cleanDataForMonth(getAuxMonth(auxMonthBack1)),auxMonthBack1, getNumberDays(auxMonthBack1));
                    break;
                case 1:
                    mMyIndicatorSelectCallback.OnSelectIndicatorMonth(cleanDataForMonth(getAuxMonth(auxMonthBack2)),auxMonthBack2, getNumberDays(auxMonthBack2));
                    break;
                case 0:
                    mMyIndicatorSelectCallback.OnSelectIndicatorMonth(cleanDataForMonth(getAuxMonth(auxMonthBack3)),auxMonthBack3, getNumberDays(auxMonthBack3));
                    break;
            }

        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private String getMonth(String numberMonth){
        String month = "";
        numberMonth = getAuxMonth(numberMonth);
        switch (numberMonth){
            case "01":
                month =  "Enero";
                break;
            case "02":
                month =  "Febrero";
                break;
            case "03":
                month =  "Marzo";
                break;
            case "04":
                month =  "Abril";
                break;
            case "05":
                month =  "Mayo";
                break;
            case "06":
                month =  "Junio";
                break;
            case "07":
                month =  "Julio";
                break;
            case "08":
                month =  "Agosto";
                break;
            case "09":
                month =  "Septiembre";
                break;
            case "10":
                month =  "Octubre";
                break;
            case "11":
                month =  "Noviembre";
                break;
            case "12":
                month =  "Diciembre";
                break;
        }
        return month;
    }


    private String getAuxMonth(String dateIn) {
        String[] dateArr = dateIn.split("-");
        return  dateArr[1];
    }

    private String getNumberDays(String date){
        String[] dateArr = date.split("-");
        int iYear = 1999;
        int iMonth = (Integer.parseInt(dateArr[1])-1); // 1 (months begin with 0)
        int iDay = 1;
        Calendar mycal = new GregorianCalendar(iYear, iMonth, iDay);
        int days = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return String.valueOf(days);
    }

    private Data cleanDataForMonth(String month){
        //2018-10-18 18:27:40
        Data filterData =  new Data();
        for(InfoDetail infoDetail : mRecomendData.installedArray){
            if (Integer.parseInt(infoDetail.sendMonth) == Integer.parseInt(month))
                filterData.installedArray.add(infoDetail);
        }
        for(InfoDetail infoDetail : mRecomendData.forContactArray){
            if (Integer.parseInt(infoDetail.sendMonth) == Integer.parseInt(month))
                filterData.forContactArray.add(infoDetail);
        }
        for(InfoDetail infoDetail : mRecomendData.forInstallArray){
            if (Integer.parseInt(infoDetail.sendMonth) == Integer.parseInt(month))
                filterData.installedArray.add(infoDetail);
        }
        for(InfoDetail infoDetail : mRecomendData.failArray){
            if (Integer.parseInt(infoDetail.sendMonth) == Integer.parseInt(month))
                filterData.failArray.add(infoDetail);
        }

        return filterData;
    }
}
