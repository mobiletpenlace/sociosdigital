package com.totalplay.sociodigital.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.library.utils.BusinessFiles;
import com.totalplay.sociodigital.library.utils.CapturePhotoActivity;
import com.totalplay.sociodigital.library.utils.FileUtils;
import com.totalplay.sociodigital.library.utils.PersistenData;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StepOneSelfieActivity extends BaseActivity{
    private File mRequestedFile;

    @BindView(R.id.mSelfieImageView)
    ImageView mSelfieImageView;

    @BindView(R.id.mContentSelfieCapturedLinearLayout)
    LinearLayout mContentSelfieCapturedLinearLayout;

    @BindView(R.id.mContentTakeSelfieLinearLayout)
    LinearLayout mContentTakeSelfieLinearLayout;

    @BindView(R.id.mDescriptionSelfieTextView)
    TextView mDescriptionSelfieTextView;

    private String mPathFile = "";
    private String mSelfieName = "";

    private RegisterSellerPresenter mRegisterSellerPresenter;


    private static String pathTemp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.step_one_selfie_activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mCancelSelfieButton)
    public void OnCancelSelfieButton(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    @OnClick(R.id.mContinueSelfieButton)
    public void OnContinueSelfieButton(){
        if(mSelfieImageView.getDrawable() != null) {
            Log.e("Selfie:->>",mPathFile + "   "+mSelfieName);
            mRegisterSellerPresenter.saveSelfie(mPathFile,mSelfieName);
            startActivity(new Intent(this, StepThreeDataUserActivity.class));
        }else{
            MessageUtils.toast(this, "Es necesario que te tomes una foto para generar tu credencial");
        }
    }

    public File createDocument(String name){
        try {
            String rootPath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            File root = new File(rootPath);
            if (!root.exists()) {
                root.mkdirs();
            }
            mRequestedFile = new File(rootPath +"/"+ name +".jpg");
            if (mRequestedFile.exists()) {
                mRequestedFile.delete();
            }
            mRequestedFile.createNewFile();

            FileOutputStream out = new FileOutputStream(mRequestedFile);

            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mRequestedFile;
    }

    @OnClick(R.id.mCameraSelfieImageView)
    public void OnClickCameraSelfie(){
        takeSelfie();
    }

    @OnClick(R.id.mTakeAgainSelfieButton)
    public void OnClickTakeAgainSelfie(){
        takeSelfie();
    }

    public void takeSelfie(){
        mSelfieName = String.valueOf(createUniqueId());
        Log.d("#########---->",mSelfieName);
        mRequestedFile = createDocument(mSelfieName);//BusinessFiles.newDocument(mSelfieName);
        pathTemp = "/storage/emulated/0/"+mSelfieName+".jpg";
        try {
            CapturePhotoActivity.launch(this, mRequestedFile.getAbsolutePath(), 33);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private String createUniqueId() {
        Calendar cal = Calendar.getInstance();

        String mAnio = String.valueOf(cal.get(Calendar.YEAR));
        String mMont = String.valueOf(cal.get(Calendar.MONTH));
        String mDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

        int millisecond = cal.get(Calendar.MILLISECOND);
        int second = cal.get(Calendar.SECOND);
        int minute = cal.get(Calendar.MINUTE);
        //12 hour format
        int hour = cal.get(Calendar.HOUR);
        //24 hour format
        int hourofday = cal.get(Calendar.HOUR_OF_DAY);

        return mAnio+"-"+mMont+"-"+mDay+"-"+hourofday+":"+minute+":"+second+":"+millisecond;
    }

    @OnClick(R.id.mBackButton)
    public void OnBackButton(){
      onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void hideCapturedTake(){
        mContentTakeSelfieLinearLayout.setVisibility(View.GONE);
        mDescriptionSelfieTextView.setVisibility(View.GONE);
    }

    public void showCaptured(){
        mContentSelfieCapturedLinearLayout.setVisibility(View.VISIBLE) ;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            try {
                if (mRequestedFile != null) {
                    mPathFile = pathTemp;
                } else {
                    mPathFile = mRequestedFile.getAbsolutePath();
                }
            }catch (NullPointerException e){
                mPathFile = pathTemp;
            }

            Glide.with(this)
                    .load(mPathFile)
                    .into(mSelfieImageView);
            hideCapturedTake();
            showCaptured();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI,
                null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
