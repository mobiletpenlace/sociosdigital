package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.widget.EditText;

import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TypeSellerHaveEnterpriseActivity extends BaseActivity {
    @BindView(R.id.mNameBusinessEditText)
    EditText mNameBusinessEditText;
    public FormValidator mFormValidator;
    public RegisterSellerPresenter mRegisterSellerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.flush_type_seller_have_enterprise);
        ButterKnife.bind(this);
        addValidators();

        mNameBusinessEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mNameBusinessEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    @OnClick(R.id.mNextHaceEnterpriseButton)
    public void OnClickNext(){
        if (mFormValidator.isValid()) {
            mRegisterSellerPresenter.saveNameTypeSeller(mNameBusinessEditText.getText().toString());
            startActivity(new Intent(this, TypeSellerMap.class));
        }
    }

    @OnClick(R.id.mCancelHaveEnterpriseButton)
    public void mCancelHaveEnterprise(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mBackButton)
    public void OnBackButton(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
