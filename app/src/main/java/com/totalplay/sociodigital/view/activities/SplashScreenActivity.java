package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;

public class SplashScreenActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.splash_screen_activity);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    InfoSocioBean infoSocioBean = (InfoSocioBean) Prefs.instance().object("saveLogin", InfoSocioBean.class);
                    if (infoSocioBean != null){
                        if (infoSocioBean.mNoEmpleado.length() > 0){
                            Intent newIntent = new Intent(getApplicationContext(),HomeActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplication().startActivity(newIntent);
                        }
                        else {
                            startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                        }
                    }else {
                        startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
