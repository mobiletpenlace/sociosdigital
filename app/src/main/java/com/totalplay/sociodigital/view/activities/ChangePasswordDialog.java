package com.totalplay.sociodigital.view.activities;

import android.os.Bundle;
import android.view.Window;

import com.totalplay.sociodigital.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordDialog extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.change_password_dialog);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mConfirmChangePasswordButton)
    public void OnClickConfirmChangePassword(){
        this.finish();
    }
}
