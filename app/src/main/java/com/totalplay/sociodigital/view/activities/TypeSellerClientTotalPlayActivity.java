package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TypeSellerClientTotalPlayActivity extends BaseActivity {
    @BindView(R.id.mNumberAccountEditText)
    EditText mNumberAccountEditText;

    public FormValidator mFormValidator;
    private RegisterSellerPresenter mRegisterSellerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.flush_type_seller_client_totalplay);
        ButterKnife.bind(this);
        addValidators();
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mNumberAccountEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    @OnClick(R.id.mNextClientTotalPlayButton)
    public void OnClickNextClinet(){
        if (mFormValidator.isValid()) {
            mRegisterSellerPresenter.saveAccountNumber(mNumberAccountEditText.getText().toString());
            startActivity(new Intent(this, StepOneSelfieActivity.class));
        }
    }


    @OnClick(R.id.mCancelClientTotalPlayButton)
    public void OnCancelClientTotalPlay(){
        startActivity(new Intent(this,CancelRegisterDialog.class));
    }

    @OnClick(R.id.mBackButton)
    public void OnBackButton(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
