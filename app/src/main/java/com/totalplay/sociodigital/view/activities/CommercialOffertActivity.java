package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.resources.icc.viewflow.CoverFlow;
import com.resources.icc.viewflow.core.PagerContainer;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.PMPlanesSocioResponse;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.callbacks.DetailPacakagesCallback;
import com.totalplay.sociodigital.presenter.callbacks.PMPlanesCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;
import com.totalplay.sociodigital.presenter.implementations.PMPlanesPresenter;
import com.totalplay.sociodigital.view.adapters.PackageAdapter;
import com.totalplay.sociodigital.view.adapters.PromotionsAdapter;
import com.totalplay.sociodigital.view.adapters.SelectPackageAdapter;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 31/08/18.
 * SocioDigital
 */

public class CommercialOffertActivity extends BaseActivity implements DetailPacakagesCallback, PMPlanesCallback {

    public static Intent launch(Context context){
        Intent intent = new Intent(context, CommercialOffertActivity.class);
        return  intent;
    }

    private PMPlanesPresenter mPMPlanesPresenter;
    private ArrayList<PlanTotalPlay> plans;
    private TextView mTitleTextView;
    private PagerContainer mPackagscontainer;
    private RecyclerView mItemSelectorRecyclerView;
    private Button mButtonCancel;
    private Button mButtonAccept;

    private LinearLayoutManager HorizontalLayout;
    private ViewPager pager;
    private SelectPackageAdapter mSelectPackageAdapter;
    private PackageAdapter mPackageAdapter;
    private int positionPackageSelect;
    private ViewGroup mBackToolbar;
    private TextView mToolbarTitle;

    //PromotionalRecycler
    private PromotionsAdapter mPromotionsAdapter;
    private  RecyclerView mPromotinalsRecycler;

    private Double installationCost= 0.0;
    private String mTitlePackage;

    //DilogPAckageDetail
    private ViewGroup detailPacakageContent;
    private TextView detailPacakageTextView;
    private Button acceptButtonTetView;
    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.commercial_offert_activity);
        bindResources();
        setFonts();
        setOnClicks();
        mLoginPresenter =  new LoginPresenter(this);
        InfoSocioBean infoSocioBean = mLoginPresenter.retrieveInfoSocio();

        if (infoSocioBean.mSubCanal != null){
            if (infoSocioBean.mSubCanal.length() > 0)
                mPMPlanesPresenter.loadPMPlanes(CryptoUtils.crypt(infoSocioBean.mSubCanal),CryptoUtils.crypt("CIUDAD DE MEXICO"));
            else
                mPMPlanesPresenter.loadPMPlanes(CryptoUtils.crypt("Socio Edificios"), CryptoUtils.crypt("CIUDAD DE MEXICO"));
        }else {
            mPMPlanesPresenter.loadPMPlanes(CryptoUtils.crypt("Socio Edificios"), CryptoUtils.crypt("CIUDAD DE MEXICO"));
        }
    }

    @Override
    protected BasePresenter getPresenter() {
       return mPMPlanesPresenter = new PMPlanesPresenter(this,this);
    }
    private void bindResources(){
        mTitleTextView = findViewById(R.id.act_packages_title);
        mPackagscontainer =  findViewById(R.id.pager_container_packages);
        mItemSelectorRecyclerView =  findViewById(R.id.act_packages_selector);
        mButtonCancel =  findViewById(R.id.act_packages_cancel);
        mButtonAccept =  findViewById(R.id.act_packages_continue);
        mBackToolbar = findViewById(R.id.mBackButton);
        mToolbarTitle =  findViewById(R.id.toolbar_title);
        //Dialog PackageDetails
        detailPacakageContent =  findViewById(R.id.dialog_detail_package_content);
        detailPacakageTextView =  findViewById(R.id.dialog_detail_package_detalle);
        acceptButtonTetView = findViewById(R.id.dialog_detail_package_accept_button);
    }

    private void setFonts(){
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        mTitleTextView.setTypeface(monserratRegular);
        mButtonCancel.setTypeface(monserratMedium);
        mButtonAccept.setTypeface(monserratRegular);
    }

    private void setOnClicks(){
        mButtonAccept.setOnClickListener(v -> {

        });

        mButtonCancel.setOnClickListener(v -> finish());

        mBackToolbar.setOnClickListener(v -> finish());

        acceptButtonTetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailPacakageContent.setVisibility(View.GONE);
            }
        });

        detailPacakageContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });
    }

    private void setPager(){
        HorizontalLayout = new LinearLayoutManager(CommercialOffertActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mItemSelectorRecyclerView.setLayoutManager(HorizontalLayout);

        //Pager
        PagerContainer container = findViewById(R.id.pager_container_packages);
        assert container != null;

        pager = container.getViewPager();
        pager.setClipChildren(false);
        pager.setOffscreenPageLimit(15);

        new CoverFlow.Builder()
                .with(pager)
                .scale(0f)
                .pagerMargin(getResources().getDimensionPixelSize(R.dimen.pager_margin_card))
                .spaceSize(3f)
                .build();

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                positionPackageSelect = position;
                mSelectPackageAdapter.setPageActivated(position);
                mSelectPackageAdapter.notifyDataSetChanged();
                setPackageTitle(plans.get(position).typeOffert);
                mToolbarTitle.setText(mTitlePackage);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mSelectPackageAdapter = new SelectPackageAdapter(plans,this);
        mItemSelectorRecyclerView.setAdapter(mSelectPackageAdapter);
        mPackageAdapter = new PackageAdapter(this, plans,this);
        pager.setAdapter(mPackageAdapter);
        setPackageTitle(plans.get(0).typeOffert);
        mToolbarTitle.setText(mTitlePackage);
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error de conexión");
    }

    @Override
    public void setDetailPacakage(String detailPacakage) {
        detailPacakageTextView.setText(detailPacakage);
        detailPacakageContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadPMPlanes() {
        MessageUtils.progress(this,"Cargando información");
    }

    @Override
    public void onSuccessPMPlanes(PMPlanesSocioResponse pmPlanesSocioResponse) {
        MessageUtils.stopProgress();
        plans = pmPlanesSocioResponse.planTP;
        setPager();
    }

    @Override
    public void onErrorPMPlanes() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error al consultar la información");
    }


    private void setPackageTitle(String packageSelect){

            switch (packageSelect){
                case "2P":
                    // inter y tel
                    mTitlePackage = "Paquete Dobleplay";
                    break;
                case "3P":
                    //todos
                    mTitlePackage = "Paquete Tripleplay";
                    break;
                case "2P tv":
                    //inter tv
                    mTitlePackage = "Paquete Dobleplay TV";
                    break;
            }

    }

}