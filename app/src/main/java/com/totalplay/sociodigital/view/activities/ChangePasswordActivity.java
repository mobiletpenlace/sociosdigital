package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity {
    @BindView(R.id.mChangePasswordCurrentPassEditText)
    EditText mChangePasswordCurrentPassEditText;

    @BindView(R.id.mChangePasswordNewPassEditText)
    EditText mChangePasswordNewPassEditText;

    @BindView(R.id.mChangePasswordRepeatPassEditText)
    EditText mChangePasswordRepeatPassEditText;

    public FormValidator mFormValidator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.change_password_activity);
        ButterKnife.bind(this);

        addValidators();
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mChangePasswordCurrentPassEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mChangePasswordNewPassEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mChangePasswordRepeatPassEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    @OnClick(R.id.mChangePasswordButton)
    public void OnClickChangePassword(){
        if(mFormValidator.isValid()) {
            startActivity(new Intent(this, ChangePasswordDialog.class));
        }
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
