package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.ConsultAddonsResponse;
import com.totalplay.sociodigital.background.ws.response.FamilyPackagesResponse;
import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;
import com.totalplay.sociodigital.library.quoter.Producto.CotServicioProducto;
import com.totalplay.sociodigital.library.quoter.Servicio.CotPlanServicio;
import com.totalplay.sociodigital.library.quoter.plan.Cot_SitioPlan;
import com.totalplay.sociodigital.library.quoter.utils.Constans;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.entities.Additional;
import com.totalplay.sociodigital.model.entities.Consult;
import com.totalplay.sociodigital.model.entities.FormalityEntity;
import com.totalplay.sociodigital.model.entities.InfoPlan;
import com.totalplay.sociodigital.model.entities.ProductsAdd;
import com.totalplay.sociodigital.model.entities.Promotion;
import com.totalplay.sociodigital.model.entities.PropuestaBean;
import com.totalplay.sociodigital.model.entities.QuotationPlainService;
import com.totalplay.sociodigital.model.entities.QuotationPlainServiceProduct;
import com.totalplay.sociodigital.model.entities.QuotationPromotionPlain;
import com.totalplay.sociodigital.model.entities.QuotationSitePlainEntity;
import com.totalplay.sociodigital.model.pojos.ArrCostoInstalacionBean;
import com.totalplay.sociodigital.model.pojos.ArrProductosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrProductosIncluidosBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosIncluidosBean;
import com.totalplay.sociodigital.presenter.callbacks.AddonSelectorCallback;
import com.totalplay.sociodigital.presenter.callbacks.ConsultAddonsTpCallback;
import com.totalplay.sociodigital.presenter.callbacks.LeadFollowingCallback;
import com.totalplay.sociodigital.presenter.implementations.AdditionalServicesPresenter;
import com.totalplay.sociodigital.presenter.implementations.AddonSelectorPresenter;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.ConsultAddonsTpPresenter;
import com.totalplay.sociodigital.presenter.implementations.ContactInformationPresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class QuotationPackageActivity extends BaseActivity {
    private TextView mTitleTextView;
    private TextView mPricePackageTextView;
    private TextView mPricePackageAmountTextView;
    private TextView mPriceEarlyTextView;
    private TextView mPriceEarlyAmount;
    private TextView mAdditionalsServicesTextView;
    private TextView mAdditionalsSerVicesAmountTextView;
    private TextView mTotalTextView;
    private TextView mTotalAmountTextView;
    private TextView mTotalEarlyTextView;
    private TextView mTotalEarlyAmountTextView;
    private TextView mInstallationCostTextView;
    private TextView mInstallationCostAmountTextView;
    private Button mCancelButton;
    private Button mContinueButton;
    private ViewGroup mBackToolbar;
    private ArrayList<Additional> mAdditonals;
    private ArrayList<Promotion> mPromotions;
    private PlanTotalPlay mPlanTotalPlay;
    private Double installationCost = 0.0;
    private String priceList;
    private String priceEarly;

    public static Intent launch(Context context, PlanTotalPlay planSelected,Double installationCost,ArrayList<Additional> additionals,ArrayList<Promotion> mPromotions, String priceList, String priceEarly ){
    Intent intent = new Intent(context, QuotationPackageActivity.class);
        intent.putExtra("planSelected", planSelected);
        intent.putExtra("installationCost",installationCost );
        intent.putExtra("additionals",additionals);
        intent.putExtra("promotions", mPromotions);
        intent.putExtra("priceList", priceList);
        intent.putExtra("priceEarly", priceEarly);
        return  intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.quotation_package_activity);
        mAdditonals = (ArrayList<Additional>) getIntent().getSerializableExtra("additionals");
        mPromotions = (ArrayList<Promotion>) getIntent().getSerializableExtra("promotions");
        mPlanTotalPlay = (PlanTotalPlay) getIntent().getSerializableExtra("planSelected");
        installationCost = getIntent().getDoubleExtra("installationCost",0.0);
        priceList =  getIntent().getStringExtra("priceList");
        priceEarly = getIntent().getStringExtra("priceEarly");
        bindResources();
        setFont();
        setOnClick();
        setData();
    }


    private void bindResources(){
        mTitleTextView = findViewById(R.id.act_quotation_package_title);
        mPricePackageTextView = findViewById(R.id.act_quotation_package_price);
        mPricePackageAmountTextView =  findViewById(R.id.act_quotation_package_price_amount);
        mPriceEarlyTextView =  findViewById(R.id.act_quotation_package_price_early);
        mPriceEarlyAmount =  findViewById(R.id.act_quotation_package_price_early_amount);
        mAdditionalsServicesTextView = findViewById(R.id.act_quotation_package_additional_services);
        mAdditionalsSerVicesAmountTextView = findViewById(R.id.act_quotation_package_additional_services_amount);
        mTotalTextView = findViewById(R.id.act_quotation_package_total);
        mTotalAmountTextView = findViewById(R.id.act_quotation_package_total_amount);
        mTotalEarlyTextView =  findViewById(R.id.act_quotation_package_total_early);
        mTotalEarlyAmountTextView = findViewById(R.id.act_quotation_package_total_early_amount);
        mCancelButton = findViewById(R.id.act_quotation_package_cancel_button);
        mContinueButton =  findViewById(R.id.act_quotation_package_continue_button);
        mInstallationCostTextView =  findViewById(R.id.act_quotation_package_installation_cost_text);
        mInstallationCostAmountTextView = findViewById(R.id.act_quotation_package_installation_cost_amount);
        mBackToolbar =  findViewById(R.id.mBackButton);
    }

    private void setFont(){
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");

        mTitleTextView.setTypeface(monserratRegular);
        mPricePackageTextView.setTypeface(monserratBold);
        mPricePackageAmountTextView.setTypeface(monserratRegular);
        mPriceEarlyTextView.setTypeface(monserratBold);
        mPriceEarlyAmount.setTypeface(monserratRegular);
        mAdditionalsServicesTextView.setTypeface(monserratBold);
        mAdditionalsSerVicesAmountTextView.setTypeface(monserratRegular);
        mTotalTextView.setTypeface(monserratBold);
        mTotalAmountTextView.setTypeface(monserratBold);
        mTotalEarlyTextView.setTypeface(monserratBold);
        mTotalEarlyAmountTextView.setTypeface(monserratBold);
        mCancelButton.setTypeface(monserratMedium);
        mContinueButton.setTypeface(monserratMedium);
        mInstallationCostTextView.setTypeface(monserratBold);
        mInstallationCostAmountTextView.setTypeface(monserratRegular);
    }

    private void setOnClick(){
        mContinueButton.setOnClickListener(v -> startActivity(LoadDocumentsActivity.launch(getApplicationContext(),mPlanTotalPlay, mAdditonals, mPromotions, priceList)));

        mCancelButton.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), CancelSaleDialog.class));
        });

        mBackToolbar.setOnClickListener(v -> finish());
    }

    private void setData(){
        mPricePackageAmountTextView.setText( String.format("$ %s", Math.ceil(Float.parseFloat(priceList))));
        mPriceEarlyAmount.setText(String.format("$ %s", Math.ceil(Float.parseFloat(priceEarly))));
        mTotalAmountTextView.setText(String.format("$ %s", Math.ceil(Float.parseFloat(priceList))));
        mTotalEarlyAmountTextView.setText(String.format("$ %s", Math.ceil(Float.parseFloat(priceEarly))));
        mInstallationCostAmountTextView .setText(String.format("$ %s", installationCost));
    }
}