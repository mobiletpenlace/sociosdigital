package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;

import com.totalplay.sociodigital.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setHomeAsUpEnabled(false);
        setContentView(R.layout.welcome_activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mContinueWelcomeButton)
    public void mContinueWelcomeButton(){
        startActivity(new Intent(this, TypeSellerActivity.class));
    }


    @OnClick(R.id.mCancelWelcomeButton)
    public void OnClickCacnelWelcome(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

}
