package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;

import com.totalplay.sociodigital.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class StepTwoFingerPrintActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setToolbarEnabled(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_two_fingerprint_activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mContinueFingerPrintButton)
    public void OnClickContinueFingerPrint(){
        startActivity(new Intent(this, StepThreeDataUserActivity.class));
    }

    @OnClick(R.id.mBackButton)
    public void OnBackButton(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
