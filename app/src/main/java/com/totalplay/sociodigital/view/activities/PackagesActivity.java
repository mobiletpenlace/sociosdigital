package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.resources.icc.viewflow.CoverFlow;
import com.resources.icc.viewflow.core.PagerContainer;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.ConsultAddonsResponse;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.entities.Additional;
import com.totalplay.sociodigital.model.entities.Consult;
import com.totalplay.sociodigital.model.entities.InfoPlan;
import com.totalplay.sociodigital.model.entities.Promotion;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.callbacks.ConsultAddonsTpCallback;
import com.totalplay.sociodigital.presenter.callbacks.DetailPacakagesCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.ConsultAddonsTpPresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;
import com.totalplay.sociodigital.view.adapters.PackageAdapter;
import com.totalplay.sociodigital.view.adapters.PromotionsAdapter;
import com.totalplay.sociodigital.view.adapters.SelectPackageAdapter;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 31/08/18.
 * SocioDigital
 */

public class PackagesActivity extends BaseActivity implements ConsultAddonsTpCallback, DetailPacakagesCallback{

    public static Intent launch(Context context, ArrayList<PlanTotalPlay> plans, String titlePackage){
        Intent intent = new Intent(context, PackagesActivity.class);
        intent.putExtra("plans", plans);
        intent.putExtra("titlePackage",titlePackage);
        return  intent;
    }

    private ArrayList<PlanTotalPlay> plans;
    private TextView mTitleTextView;
    private PagerContainer mPackagscontainer;
    private RecyclerView mItemSelectorRecyclerView;
    private Button mButtonCancel;
    private Button mButtonAccept;

    private LinearLayoutManager HorizontalLayout;
    private ViewPager pager;
    private SelectPackageAdapter mSelectPackageAdapter;
    private PackageAdapter mPackageAdapter;
    private int positionPackageSelect;
    private ViewGroup mBackToolbar;
    private TextView mToolbarTitle;
    //promotionals dialog
    private TextView mPromotionalsTitleTextView;
    private TextView mPromotionalsSubtitleTextview;
    private Button mPromotionlaButton;
    private ViewGroup mPromotionalsContent;


    private ConsultAddonsTpPresenter mConsultAddonsTpPresenter;
    private ArrayList<Additional> mAdditonals;
    private ArrayList<Promotion> mPromotions;

    //PromotionalRecycler
    private PromotionsAdapter mPromotionsAdapter;
    private  RecyclerView mPromotinalsRecycler;

    private Double installationCost= 0.0;
    private String mTitlePackage;

    //DilogPAckageDetail
    private ViewGroup detailPacakageContent;
    private TextView detailPacakageTextView;
    private Button acceptButtonTetView;

    private LoginPresenter mLoginPresenter;
    private String priceList;
    private String priceEarly;

    private TextView msgAppEstimFiscal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.package_activity);
        plans = new ArrayList<>();
        plans = (ArrayList<PlanTotalPlay>) getIntent().getSerializableExtra("plans");
        mTitlePackage =  getIntent().getStringExtra("titlePackage");
        mLoginPresenter =  new LoginPresenter(this);
        String estimFiscal = Prefs.instance().string("AplicaEstimuloFiscal");
        bindResources();
        setFonts();
        setOnClicks();
        if (estimFiscal.equals("true"))
            msgAppEstimFiscal.setVisibility(View.VISIBLE);
        else
            msgAppEstimFiscal.setVisibility(View.GONE);
        setPager();
        mToolbarTitle.setText(mTitlePackage);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mConsultAddonsTpPresenter = new ConsultAddonsTpPresenter(this, this);
    }
    private void bindResources(){
        mTitleTextView = findViewById(R.id.act_packages_title);
        mPackagscontainer =  findViewById(R.id.pager_container_packages);
        mItemSelectorRecyclerView =  findViewById(R.id.act_packages_selector);
        mButtonCancel =  findViewById(R.id.act_packages_cancel);
        mButtonAccept =  findViewById(R.id.act_packages_continue);
        mBackToolbar = findViewById(R.id.mBackButton);
        mToolbarTitle =  findViewById(R.id.toolbar_title);
        //Dialog PRomotionals
        mPromotionalsTitleTextView =  findViewById(R.id.dialog_promotionals_title);
        mPromotionalsSubtitleTextview = findViewById(R.id.dialog_promotionals_subtitle);
        mPromotionlaButton =  findViewById(R.id.dialog_promotionals_accept);
        mPromotionalsContent = findViewById(R.id.dialog_promotionals_content);
        mPromotinalsRecycler =  findViewById(R.id.promotionals_dialog_recycler);
        //Dialog PackageDetails
        detailPacakageContent =  findViewById(R.id.dialog_detail_package_content);
        detailPacakageTextView =  findViewById(R.id.dialog_detail_package_detalle);
        acceptButtonTetView = findViewById(R.id.dialog_detail_package_accept_button);
        msgAppEstimFiscal =  findViewById(R.id.act_package_description_iva);
    }

    private void setFonts(){
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        mTitleTextView.setTypeface(monserratRegular);
        mButtonCancel.setTypeface(monserratMedium);
        mButtonAccept.setTypeface(monserratRegular);
        //DialogPromotionals
        mPromotionalsTitleTextView.setTypeface(monserratRegular);
        mPromotionalsSubtitleTextview.setTypeface(monserratRegular);
        mPromotionlaButton.setTypeface(monserratMedium);
    }

    private void setOnClicks(){
        mButtonAccept.setOnClickListener(v -> {
            loadInfo(plans.get(positionPackageSelect).id);
        });

        mButtonCancel.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), CancelSaleDialog.class));
        });

        mBackToolbar.setOnClickListener(v -> finish());

        mPromotionlaButton.setOnClickListener(v -> {
            startActivity(QuotationPackageActivity.launch(getApplicationContext(),plans.get(positionPackageSelect),installationCost,mAdditonals,mPromotions,priceList,priceEarly));
            mPromotionalsContent.setVisibility(View.GONE);

        });

        mPromotionalsContent.setOnClickListener(v -> {
            //
        });

        acceptButtonTetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailPacakageContent.setVisibility(View.GONE);
            }
        });

        detailPacakageContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });
    }

    private void setPager(){
        HorizontalLayout = new LinearLayoutManager(PackagesActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mItemSelectorRecyclerView.setLayoutManager(HorizontalLayout);

        //Pager
        PagerContainer container = findViewById(R.id.pager_container_packages);
        assert container != null;

        pager = container.getViewPager();
        pager.setClipChildren(false);
        pager.setOffscreenPageLimit(15);

        new CoverFlow.Builder()
                .with(pager)
                .scale(0f)
                .pagerMargin(getResources().getDimensionPixelSize(R.dimen.pager_margin_card))
                .spaceSize(3f)
                .build();

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                positionPackageSelect = position;
                mSelectPackageAdapter.setPageActivated(position);
                mSelectPackageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mSelectPackageAdapter = new SelectPackageAdapter(plans,this);
        mItemSelectorRecyclerView.setAdapter(mSelectPackageAdapter);
        mPackageAdapter = new PackageAdapter(this, plans,this);
        pager.setAdapter(mPackageAdapter);
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error de conexión");
    }

    @Override
    public void onSucessGetAddons(ConsultAddonsResponse consultAddonsResponse) {
        MessageUtils.stopProgress();
        priceList = consultAddonsResponse.mInfoAddons.priceListPackage;
        priceEarly =  consultAddonsResponse.mInfoAddons.priceEarlyPackage;
        Double installCost = 0.0;
        Double garantCost = 0.0;
        Double aditionalCost = 0.0;
        mAdditonals =  new ArrayList<>();
        mPromotions =  new ArrayList<>();
        mAdditonals = consultAddonsResponse.mInfoAddons.mProductsAdd.mAdditionals;
        if (consultAddonsResponse.mInfoAddons.mPromocionesAdd != null)
            mPromotions =  consultAddonsResponse.mInfoAddons.mPromocionesAdd.mPromotions;
        for (Additional additional:mAdditonals){
            /*if (additional.name.contains())
            if (additional.name.contains("Cargo de acti") || additional.name.contains("Cargo por acti"))
                installCost = Double.valueOf(additional.price);
            if (additional.name.contains("Cargo de garan")||additional.name.contains("Cargo por garan"))
                garantCost = Double.valueOf(additional.price);
                */
            aditionalCost = (aditionalCost + Double.valueOf(additional.price));
        }

        installationCost = Double.valueOf(Math.ceil(aditionalCost));
        Prefs.instance().putString(SocioDigitalKeys.INSTALATION_COST, String.valueOf(aditionalCost));

        //mInstallationCostAmountTextView.setText(String.format("%s.00", String.format("$ %s", Math.round(mPlanTotalPlay.))));
        populatePromotions(mPromotions);
    }

    @Override
    public void onErrorGetAddons() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error al consultar la información");
    }

    @Override
    public void onLoadAddons() {
        MessageUtils.progress(this,"Cargando información");
    }

    private void loadInfo(String packageSelectedID){
        InfoSocioBean infoSocioBean = mLoginPresenter.retrieveInfoSocio();
        InfoPlan infoPlan = new InfoPlan("true",
                "true",
                "false",
                mFormalityEntity.prospectoBean.direccionBean.cluster,
                CryptoUtils.desEncrypt(mFormalityEntity.prospectoBean.direccionBean.zipCode),
                mFormalityEntity.prospectoBean.direccionBean.colony,
                mFormalityEntity.prospectoBean.direccionBean.district,
                "",
                mFormalityEntity.prospectoBean.direccionBean.place,
                infoSocioBean.mSubCanal);
        String estimFiscal =  Prefs.instance().string("AplicaEstimuloFiscal");
        Consult consult =  new Consult(packageSelectedID,infoPlan, estimFiscal);
        mConsultAddonsTpPresenter.loadAddons(consult);
    }

    private void populatePromotions(ArrayList<Promotion> promotions){
        mPromotionsAdapter =  new PromotionsAdapter(this,promotions);
        mPromotinalsRecycler.setLayoutManager(new LinearLayoutManager(this));
        mPromotinalsRecycler.setAdapter(mPromotionsAdapter);
        mPromotionalsContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPromotionalsContent.setVisibility(View.GONE);
    }

    @Override
    public void setDetailPacakage(String detailPacakage) {
        detailPacakageTextView.setText(detailPacakage);
        detailPacakageContent.setVisibility(View.VISIBLE);
    }
}
