package com.totalplay.sociodigital.view.activities;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.handlers.FingerprintHandler;
import com.totalplay.sociodigital.library.utils.StringUtils;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ivancortes on 10/31/18.
 */

public class LoginFingerActivity extends BaseActivity implements FingerprintHandler.Callback {

    private static final String KEY_NAME = "yourKey";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private FingerprintHandler fingerprintHandler;
    private LoginPresenter mLoginPresenter;
    private InfoSocioBean mInfoSocioBean;
    private String name;
    private String pass;
    @BindView(R.id.dialog_login_finger_cancel)
    TextView mBtnCancel;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setFinishOnTouchOutside(false);
        setContentView(R.layout.dialog_login_finger);
        ButterKnife.bind(this);
        fingerprintHandler =  new FingerprintHandler(this,this, findViewById(R.id.check_txt_finger));
        mLoginPresenter = new LoginPresenter(this);
        retreiveInfoSocio();

        StringUtils.unlineTextView("Cancelar", mBtnCancel);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {


            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!fingerprintManager.isHardwareDetected()) {

                }
            }

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!fingerprintManager.hasEnrolledFingerprints()) {

                }
            }

            if (!keyguardManager.isKeyguardSecure()) {

                } else {
                try {
                    generateKey();
                } catch (LoginFingerActivity.FingerprintException e) {
                    e.printStackTrace();
                }
                if (initCipher()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        cryptoObject = new FingerprintManager.CryptoObject(cipher);
                    }
                    //fingerprintHandler.startAuth(fingerprintManager, cryptoObject);
                }
            }

        }
    }

    @OnClick(R.id.dialog_login_finger_cancel)
    public void OnCancelRegisterButton() {
        this.finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() throws FingerprintException {
        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");


            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @Override
    public void onAuthenticated() {
        mLoginPresenter.login(getName(),getPass(),true);
    }

    @Override
    public void onError() {
        finish();
    }

    private class FingerprintException extends Exception {

        public FingerprintException(Exception e) {
            super(e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onPause() {
        super.onPause();
        fingerprintHandler.stopListening();
        //finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        cryptoObject = new FingerprintManager.CryptoObject(cipher);
        fingerprintHandler.startAuth(fingerprintManager,cryptoObject);
    }

    public void retreiveInfoSocio(){
        InfoSocioBean infoSocioBean = (InfoSocioBean) Prefs.instance().object("saveLogin", InfoSocioBean.class);
        if (infoSocioBean != null){
            if (infoSocioBean.mNoEmpleado.length() > 0){
                putDataSocio(infoSocioBean);
            }
            else {
                mInfoSocioBean = mLoginPresenter.retrieveInfoSocio();
                putDataSocio(mInfoSocioBean);
            }
        }else {
            mInfoSocioBean = mLoginPresenter.retrieveInfoSocio();
            putDataSocio(mInfoSocioBean);
        }
    }

    private void putDataSocio(InfoSocioBean infoSocioBean) {
        setName(infoSocioBean.mNoEmpleado);
        setPass(infoSocioBean.mAccessKey);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
