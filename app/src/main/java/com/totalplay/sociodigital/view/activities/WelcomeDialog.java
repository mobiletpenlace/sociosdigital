package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.totalplay.sociodigital.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeDialog extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.welcome_to_totalplay_dialog);
        ButterKnife.bind(this);
        this.setFinishOnTouchOutside(false);
    }

    @OnClick(R.id.mSuccessWelcomeButton)
    public void OnClickSuccessWelcome(){
        Intent newIntent = new Intent(this,LoginActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
    }
}
