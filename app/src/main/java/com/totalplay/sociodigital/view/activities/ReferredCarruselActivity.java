package com.totalplay.sociodigital.view.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.DataSaleMonthRecomend;
import com.totalplay.sociodigital.background.ws.response.IndicatorSaleResponse;
import com.totalplay.sociodigital.background.ws.response.IndicatorsResponse;
import com.totalplay.sociodigital.library.pojos.Data;
import com.totalplay.sociodigital.model.entities.DataSaleMonth;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.model.pojos.MonthSalesBean;
import com.totalplay.sociodigital.model.pojos.ReferredSalesBean;
import com.totalplay.sociodigital.presenter.callbacks.IndicatorRecomendCallback;
import com.totalplay.sociodigital.presenter.callbacks.IndicatorSaleCallback;
import com.totalplay.sociodigital.presenter.callbacks.MyIndicatorSelectCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.IndicatorRecomendPresenter;
import com.totalplay.sociodigital.presenter.implementations.IndicatorSalePresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;
import com.totalplay.sociodigital.view.adapters.RecomendAdapter;
import com.totalplay.sociodigital.view.adapters.ReferredSalesAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.crosswall.lib.coverflow.CoverFlow;
import me.crosswall.lib.coverflow.core.PagerContainer;

public class ReferredCarruselActivity extends BaseActivity implements MyIndicatorSelectCallback, IndicatorRecomendCallback{
    //Changes
    @BindView(R.id.mMonthPagerContainer)
    PagerContainer mMonthPagerContainer;
    @BindView(R.id.mMonthPager)
    ViewPager mMonthPager;

    @BindView(R.id.mReferredPagerContainer)
    PagerContainer mReferredPagerContainer;
    @BindView(R.id.mReferredPager)
    ViewPager mReferredPager;

    @BindView(R.id.mContentIndicatorMonthSales)
    ViewPagerIndicator mContentIndicatorMonthSales;

    @BindView(R.id.mContentIndicatorReferred)
    ViewPagerIndicator mContentIndicatorReferred;

    @BindView(R.id.act_referred_carrusel_content_sales)
    LinearLayout contentSalesCarrusel;
    private IndicatorRecomendPresenter mIndicatorRecomendPresenter;

    private RecomendAdapter mRecomendAdapter;
    private ReferredSalesAdapter mReferredSalesAdapter;
    //Sales
    String auxMonthCurrent;
    String auxMonthBack1;
    String auxMonthBack2;
    String auxMonthBack3;
    //Recomend
    private String auxMonthCurrentRecomend;
    private String auxMonthBack1Recomend;
    private String auxMonthBack2Recomend;
    private String auxMonthBack3Recomend;

    private LoginPresenter mLoginPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.referred_carrusel_activity);

        ButterKnife.bind(this);

        configurePagerMonthSales();
        configurePagerReferredSales();
        mIndicatorRecomendPresenter =  new IndicatorRecomendPresenter(this,this);
        mLoginPresenter =  new LoginPresenter(this);
        if(mLoginPresenter.retrieveInfoSocio().mSubCanal.equals("Cliente TP")){
            contentSalesCarrusel.setVisibility(View.INVISIBLE);
        }
        getRecomend();
    }

    public void configurePagerMonthSales(){
        mMonthPager = mMonthPagerContainer.getViewPager();
        mMonthPager.setClipChildren(true);
    }

    public void configurePagerReferredSales(){
        mReferredPager = mReferredPagerContainer.getViewPager();
        mReferredPager.setClipChildren(true);
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void OnSelectIndicatorMonth(Data mRecomendData, String monthName, String numberDay) {
        startActivity(MySalesActivity.launch(this,mRecomendData, monthName,"recomend", numberDay));
    }

    @Override
    public void OnSelectIndicatorReferred(Data mSaleData, String monthName, String numberDay) {
        startActivity(MySalesActivity.launch(this,mSaleData, monthName,"sale", numberDay));
    }


    @Override
    public void onSuccessGetRecomend(IndicatorsResponse response, String type) {
        if (type.equals("recomendado")){
            putDataRecomend(response.mData);
            getSales();

        }
        if (type.equals("venta")) {
            putDataSale(response.mData);
            MessageUtils.stopProgress();
        }
    }

    @Override
    public void onErrorGetRecomend() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error al consultar la información");
        finish();
    }

    @Override
    public void onLoadGetRecomend() {
        MessageUtils.progress(this,"Cargando información");
    }

    private void getRecomend(){
        InfoSocioBean infoSocioBean = mLoginPresenter.retrieveInfoSocio();
        Date date = new Date();
        String dateStart = restDate(date,-3);
        String dateEnd = new SimpleDateFormat("yyyy-MM-dd").format(date);
        auxMonthCurrentRecomend = dateEnd;
        auxMonthBack1Recomend = restDate(date,-1);
        auxMonthBack2Recomend = restDate(date,-2);
        auxMonthBack3Recomend = dateStart;
        mIndicatorRecomendPresenter.getRecomend(infoSocioBean.idOportunity,dateStart,dateEnd,"recomendado");
    }

    private void getSales(){
        InfoSocioBean infoSocioBean = mLoginPresenter.retrieveInfoSocio();
        Date date = new Date();
        String dateStart = restDate(date,-3);
        String dateEnd = new SimpleDateFormat("yyyy-MM-dd").format(date);
        auxMonthCurrent = dateEnd;
        auxMonthBack1 = restDate(date,-1);
        auxMonthBack2 = restDate(date,-2);
        auxMonthBack3 = dateStart;
        mIndicatorRecomendPresenter.getRecomend(infoSocioBean.idOportunity,dateStart,dateEnd,"venta");
    }

    private void putDataRecomend(Data mDataRecomend){
        if (mDataRecomend == null){
            mDataRecomend = new Data();
        }
        mRecomendAdapter = new RecomendAdapter(this,mDataRecomend,this,auxMonthCurrentRecomend, auxMonthBack1Recomend, auxMonthBack2Recomend, auxMonthBack3Recomend);
        mMonthPager.setAdapter(mRecomendAdapter);
        mMonthPager.setOffscreenPageLimit(mRecomendAdapter.getCount());
        mContentIndicatorMonthSales.setupWithViewPager(mMonthPager);

        new CoverFlow.Builder()
                .with(mMonthPager)
                .scale(0f)
                .pagerMargin(30f)
                .spaceSize(0f)
                .rotationY(0f)
                .build();

        mMonthPager.setCurrentItem(3);
    }

    private void putDataSale(Data mDataSale){
        if (mDataSale == null){
            mDataSale = new Data();
        }
        mReferredSalesAdapter = new ReferredSalesAdapter(this,mDataSale,this, auxMonthCurrent,auxMonthBack1,auxMonthBack2,auxMonthBack3);
        mReferredPager.setAdapter(mReferredSalesAdapter);
        mReferredPager.setOffscreenPageLimit(mReferredSalesAdapter.getCount());
        mContentIndicatorReferred.setupWithViewPager(mReferredPager);
        new CoverFlow.Builder()
                .with(mReferredPager)
                .scale(0f)
                .pagerMargin(30f)
                .spaceSize(0f)
                .rotationY(0f)
                .build();

        mReferredPager.setCurrentItem(3);
    }



    public String restDate(Date fecha, int months){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.MONTH, months);
        Date resultDate = calendar.getTime();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateStart = df.format(resultDate);
        return  dateStart;
    }

}
