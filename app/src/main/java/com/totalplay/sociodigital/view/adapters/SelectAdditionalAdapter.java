package com.totalplay.sociodigital.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 31/08/18.
 * SocioDigital
 */

public class SelectAdditionalAdapter extends RecyclerView.Adapter<SelectAdditionalAdapter.RecyclerViewSimpleTextViewHolder> {

    private ArrayList<PlanDetailResponse.AdicionalesBean> addtionalList = new ArrayList<>();
    private int positionActiva = 0;
    private Context mContext;

    public SelectAdditionalAdapter(ArrayList<PlanDetailResponse.AdicionalesBean> packageList, Context context) {
        this.addtionalList = packageList;
        this.mContext = context;
    }

    public void setPageActivated(int positionActiva){
        this.positionActiva = positionActiva;
    }

    @Override
    public SelectAdditionalAdapter.RecyclerViewSimpleTextViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_activated_pager_addons, viewGroup, false);
        return new SelectAdditionalAdapter.RecyclerViewSimpleTextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SelectAdditionalAdapter.RecyclerViewSimpleTextViewHolder holder, int position) {
        if(position==positionActiva){
            holder.act.setVisibility(View.VISIBLE);
            int newHeight = 25;
            int newWidth = 25;
            holder.act.requestLayout();
            holder.act.getLayoutParams().height = newHeight;
            holder.act.getLayoutParams().width = newWidth;
            holder.act.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.act.setImageDrawable(mContext.getResources().getDrawable(R.drawable.background_circle_blue));
        }else{
            int newHeight = 15;
            int newWidth = 15;
            holder.act.requestLayout();
            holder.act.getLayoutParams().height = newHeight;
            holder.act.getLayoutParams().width = newWidth;
            holder.act.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.act.setImageDrawable(mContext.getResources().getDrawable(R.drawable.background_circle_gray));
        }
    }

    @Override
    public int getItemCount() {
        return this.addtionalList.size();
    }

    public class RecyclerViewSimpleTextViewHolder extends RecyclerView.ViewHolder {
        ImageView act;
        public RecyclerViewSimpleTextViewHolder(View itemView) {
            super(itemView);
            act= itemView.findViewById(R.id.item_activated_addons_control);
        }
    }

}
