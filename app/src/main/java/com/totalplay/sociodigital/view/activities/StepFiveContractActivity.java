package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.CPResponse;
import com.totalplay.sociodigital.background.ws.response.ValidateSocioResponse;
import com.totalplay.sociodigital.model.entities.UserDateBean;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;
import com.totalplay.sociodigital.presenter.implementations.ValidateSocioPresenter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class StepFiveContractActivity extends BaseActivity implements RegisterSellerPresenter.RegisterSellerCallback, ValidateSocioPresenter.ValidateSocioCallback{
    @BindView(R.id.mContentSignatureEletronicLinearLayout)
    LinearLayout mContentSignatureEletronicLinearLayout;

    @BindView(R.id.mSignatureImageView)
    ImageView mSignatureImageView;

    @BindView(R.id.mAcceptContractCheckButton)
    CheckBox mAcceptContractCheckButton;

    @BindView(R.id.dialog_upload_progress_doc1)
    public ProgressBar progress1;
    @BindView(R.id.dialog_upload_progress_doc2)
    public ProgressBar progress2;
    @BindView(R.id.dialog_upload_progress_doc3)
    public ProgressBar progress3;
    @BindView(R.id.dialog_upload_progress_doc4)
    public ProgressBar progress4;
    @BindView(R.id.dialog_upload_progress_doc5)
    public ProgressBar progress5;
    @BindView(R.id.dialog_upload_documentation)
    public ViewGroup uploadDocumentationContent;
    @BindView(R.id.dialog_upload_img1)
    public ImageView uploadingImage1;
    @BindView(R.id.dialog_upload_img2)
    public ImageView uploadingImage2;
    @BindView(R.id.dialog_upload_img3)
    public ImageView uploadingImage3;
    @BindView(R.id.dialog_upload_img4)
    public ImageView uploadingImage4;
    @BindView(R.id.dialog_upload_img5)
    public ImageView uploadingImage5;
    @BindView(R.id.dialog_upload_retry)
    public Button retryButton;
    @BindView(R.id.dialog_upload_msg_text)
    public TextView msgTextView;

    private RegisterSellerPresenter mRegisterSellerPresenter;
    private ValidateSocioPresenter mValidateSocioPresenter;

    private int progressStatus1;
    private int progressStatus2;
    private int progressStatus3;
    private int progressStatus4;
    private int progressStatus5;
    private boolean isAcceptContract = false;
    private Handler handler = new Handler();
    private boolean isAllDocumentsUpload =  false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setToolbarEnabled(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_five_contract_activity);
        ButterKnife.bind(this);
        uploadDocumentationContent.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);
        msgTextView.setVisibility(View.GONE);
        mValidateSocioPresenter = new ValidateSocioPresenter(this,this);
        Glide.with(this).load(R.drawable.ic_charging).into(uploadingImage1);
        Glide.with(this).load(R.drawable.ic_charging).into(uploadingImage2);
        Glide.with(this).load(R.drawable.ic_charging).into(uploadingImage3);
        Glide.with(this).load(R.drawable.ic_charging).into(uploadingImage4);
        Glide.with(this).load(R.drawable.ic_charging).into(uploadingImage5);
    }

    @OnClick(R.id.dialog_upload_retry)
    public void onClickRetryButton(){
        mRegisterSellerPresenter.sendSelfieToFFMServer(true);
    }


    private void startProgress1(ProgressBar progress, boolean isSync, ImageView imageView){
        progressStatus1 = 0;

        if (isSync) {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
            });

        }else {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_red));
            });
        }
        new Thread(() -> {
            progressStatus1 = 0;
            boolean enable = true;
            while(progressStatus1 < 100){
                progressStatus1 +=1;
                if (progressStatus1 > 95 && enable){
                    if (isSync) {
                        runOnUiThread(() -> {
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_green).into(imageView);
                        });

                    }else {
                        runOnUiThread(() -> {
                            msgTextView.setVisibility(View.VISIBLE);
                            retryButton.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_red).into(imageView);
                        });
                    }
                    enable =  false;
                }
                try{
                    Thread.sleep(20);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(() -> progress.setProgress(progressStatus1));
            }
        }).start();
    }

    private void startProgress2(ProgressBar progress, boolean isSync, ImageView imageView){


        if (isSync) {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
            });

        }else {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_red));
            });
        }
        new Thread(() -> {
            progressStatus2 = 0;
            boolean enable = true;
            while(progressStatus2 < 100){
                progressStatus2 +=1;
                if (progressStatus2 > 95 && enable){
                    if (isSync) {
                        runOnUiThread(() -> {
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_green).into(imageView);
                        });

                    }else {
                        runOnUiThread(() -> {
                            msgTextView.setVisibility(View.VISIBLE);
                            retryButton.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_red).into(imageView);
                        });
                    }
                    enable =  false;
                }
                try{
                    Thread.sleep(20);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(() -> progress.setProgress(progressStatus2));
            }
        }).start();
    }

    private void startProgress3(ProgressBar progress, boolean isSync, ImageView imageView){


        if (isSync) {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
            });

        }else {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_red));
            });
        }
        new Thread(() -> {
            progressStatus3 = 0;
            boolean enable = true;
            while(progressStatus3 < 100){
                progressStatus3 +=1;
                if (progressStatus3 > 95 && enable){
                    if (isSync) {
                        runOnUiThread(() -> {
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_green).into(imageView);
                        });

                    }else {
                        runOnUiThread(() -> {
                            msgTextView.setVisibility(View.VISIBLE);
                            retryButton.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_red).into(imageView);
                        });
                    }
                    enable =  false;
                }
                try{
                    Thread.sleep(20);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(() -> progress.setProgress(progressStatus3));
            }
        }).start();
    }

    private void startProgress4(ProgressBar progress, boolean isSync, ImageView imageView){


        if (isSync) {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
            });

        }else {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_red));
            });
        }
        new Thread(() -> {
            progressStatus4 = 0;
            boolean enable = true;
            while(progressStatus4 < 100){
                progressStatus4 +=1;
                if (progressStatus4 > 95 && enable){
                    if (isSync) {
                        runOnUiThread(() -> {
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_green).into(imageView);
                        });

                    }else {
                        runOnUiThread(() -> {
                            msgTextView.setVisibility(View.VISIBLE);
                            retryButton.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_red).into(imageView);
                        });
                    }
                    enable =  false;
                }
                try{
                    Thread.sleep(20);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(() -> progress.setProgress(progressStatus4));
            }
        }).start();
    }

    private void startProgress5(ProgressBar progress, boolean isSync, ImageView imageView){


        if (isSync) {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
            });

        }else {
            runOnUiThread(() -> {
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_red));
            });
        }
        new Thread(() -> {
            progressStatus5 = 0;
            boolean enable = true;
            while(progressStatus5 < 100){
                progressStatus5 +=1;
                if (progressStatus5 > 95 && enable){
                    if (isSync) {
                        runOnUiThread(() -> {
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_green).into(imageView);
                        });

                    }else {
                        runOnUiThread(() -> {
                            msgTextView.setVisibility(View.VISIBLE);
                            retryButton.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_red).into(imageView);
                        });
                    }
                    enable =  false;
                }
                try{
                    Thread.sleep(20);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(() -> progress.setProgress(progressStatus5));
            }
        }).start();
    }


    @OnClick(R.id.mContentSignatureEletronicLinearLayout)
    public void OnClickSignature(){
        startActivityForResult(new Intent(this, SignatureElectronicDialog.class),1);
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this, this);
        return mRegisterSellerPresenter;
    }

    @OnClick(R.id.mFinishButton)
    public void OnFinishContract(){

        if (mSignatureImageView.getDrawable() != null) {
            UserDateBean userDateBean = mRegisterSellerPresenter.getInfoSeller();
            mValidateSocioPresenter.validateSocio(userDateBean.mAccountNumber);
            //mRegisterSellerPresenter.sendSelfieToFFMServer();
        } else {
            MessageUtils.toast(this, "Es necesario ingresar la firma electrónica y aceptar el aviso de privacidad");
        }

    }

    @OnClick(R.id.mCancelContractButton)
    public void OnCancelContract(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            byte[] byteArray = data.getByteArrayExtra("signature");
            Bitmap current_bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            saveInternaSing(current_bmp);
            mSignatureImageView.setImageBitmap(current_bmp);
            if(mSignatureImageView.getDrawable() != null){
                mRegisterSellerPresenter.saveSignature("Firma.jpg");
            }
        }
    }

    @OnCheckedChanged(R.id.mAcceptContractCheckButton)
    public void OnClickOnContractButton(boolean b){
        if(b){
            isAcceptContract = true;
        }else{
            isAcceptContract = false;
        }
    }

    @Override
    public void OnSuccesRegister(boolean isDocumentUpload) {
        isAllDocumentsUpload = isDocumentUpload;
        MessageUtils.stopProgress();
        uploadDocumentationContent.setVisibility(View.GONE);
        startActivity(new Intent(this, WelcomeDialog.class));
    }

    @Override
    public void onErrorRegister(boolean isDocumentUpload) {
        isAllDocumentsUpload =  isDocumentUpload;
        uploadDocumentationContent.setVisibility(View.GONE);
    }

    @Override
    public void getCP(CPResponse response) {

    }

    @Override
    public void startProgress(int imageId, boolean isSync) {
        switch (imageId){
            case 0:
                startProgress1(progress1, isSync, uploadingImage1);
                break;
            case 1:
                startProgress2(progress2, isSync, uploadingImage2);
                break;
            case 2:
                startProgress3(progress3, isSync, uploadingImage3);
                break;
            case 3:
                startProgress4(progress4, isSync, uploadingImage4);
                break;
            case 4:
                startProgress5(progress5, isSync, uploadingImage5);
                break;
        }
    }


    public void saveInternaSing(Bitmap capturedBitmap){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root);
        myDir.mkdirs();


        String fname = "Firma"+".jpg";

        File file = new File(myDir, "/"+fname);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            capturedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("###############","Error");
        }
    }

    @Override
    public void isvalidSocio(ValidateSocioResponse response) {
        if (response.id.equals("0")){
            if (isAllDocumentsUpload){
                mRegisterSellerPresenter.registerSeller();
            }else{
                mRegisterSellerPresenter.sendSelfieToFFMServer(false);
                uploadDocumentationContent.setVisibility(View.VISIBLE);
            }

        }else{
            MessageUtils.toast(this, "El número telefónico que utilizaste ya se encuentra registrado a nombre de: " + response.nameSocio);
        }
    }
}
