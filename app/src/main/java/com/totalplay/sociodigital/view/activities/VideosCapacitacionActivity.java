package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.library.utils.Keys;
import com.totalplay.sociodigital.model.pojos.Video;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.VideosPresenter;
import com.totalplay.sociodigital.view.adapters.VideosCapacitacionAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideosCapacitacionActivity extends BaseActivity implements VideosPresenter.VideosCallback{
    @BindView(R.id.mListVideosRecyclerView)
    RecyclerView mListVideosRecyclerView;

    private VideosCapacitacionAdapter mVideosCapacitacionAdapter;
    private VideosPresenter videosPresenter;
    private List<Video> mListVideos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.videos_capacitacion_activity);
        ButterKnife.bind(this);
        connfigureRecycler();
        videosPresenter.getListVideos();
    }

    private View.OnClickListener mOnDownloadClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Video video = (Video) view.getTag();
            videosPresenter.downloadVideo(video);
        }
    };

    private View.OnClickListener mOnVideoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Video video = (Video) view.getTag();
            Intent intent = new Intent(VideosCapacitacionActivity.this, PlayVideoActivity.class);
            intent.putExtra(Keys.EXTRA_VIDEO, video);
            startActivity(intent);
        }
    };

    @Override
    protected BasePresenter getPresenter() {
        videosPresenter = new VideosPresenter(this, this);
        return videosPresenter;
    }

    public void connfigureRecycler(){
        mListVideosRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mVideosCapacitacionAdapter = new VideosCapacitacionAdapter(mListVideos,mOnVideoClickListener,mOnDownloadClickListener);
        mListVideosRecyclerView.setAdapter(mVideosCapacitacionAdapter);
    }

    @Override
    public void onSuccessDownloadedVideos(List<Video> videos) {
       mVideosCapacitacionAdapter.update(videos);
    }

    @Override
    public void onErrorDownloadingVideos() {
        MessageUtils.stopProgress();
        Toast.makeText(this, "Error al descargar los videos", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onDownloadingVideo() {
        MessageUtils.progress(this, "Decargando video...");
    }

    @Override
    public void onSuccessDownloadedVideo() {
        MessageUtils.stopProgress();
        videosPresenter.getListVideos();
    }

    @Override
    public void onErrorDownloadVideo() {
        MessageUtils.stopProgress();
        Toast.makeText(this, "Error al descargar el video", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
