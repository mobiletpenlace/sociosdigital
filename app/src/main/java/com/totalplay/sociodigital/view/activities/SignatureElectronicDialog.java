package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignatureElectronicDialog extends BaseActivity {
    @BindView(R.id.mContentSignatureDraw)
    SignaturePad mContentSignatureDraw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.signature_electronic_dialog);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mClearSignatureButton)
    public void OnClickClearSignature(){
        mContentSignatureDraw.clear();
    }

    @OnClick(R.id.mSaveSignatureButton)
    public void OnClickSaveSignature(){
        Bitmap signatureBitmap = mContentSignatureDraw.getSignatureBitmap();
        boolean isWhiteImage = true;
        for(int i = 0; i < signatureBitmap.getWidth(); i++){
            for(int j = 0; j < signatureBitmap.getHeight(); j++){
                int colour = signatureBitmap.getPixel(i,j);
                int red = Color.red(colour);
                int blue = Color.blue(colour);
                int green = Color.green(colour);
                if (!(red == 255 && blue == 255 && green == 255)){
                    isWhiteImage = false;
                }
            }
        }

        if(isWhiteImage){
            MessageUtils.toast(this,"La firma no puede ir en blanco");
        }else{
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            Intent intent=new Intent();
            intent.putExtra("signature",byteArray);
            setResult(RESULT_OK, intent);

            finish();
        }

    }
}
