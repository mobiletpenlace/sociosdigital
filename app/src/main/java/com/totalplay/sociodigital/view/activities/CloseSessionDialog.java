package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CloseSessionDialog extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.close_session_dialog);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mCancelCloseSessionButton)
    public void OnClickCancelCloseSession(){
        this.finish();
    }

    @OnClick(R.id.mCloseSessionButton)
    public void OnClickCloseSessionButton(){
        InfoSocioBean infoSocioBean =  new InfoSocioBean();
        Prefs.instance().putObject("saveLogin",infoSocioBean,InfoSocioBean.class);
        Intent newIntent = new Intent(this,LoginActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
    }


}
