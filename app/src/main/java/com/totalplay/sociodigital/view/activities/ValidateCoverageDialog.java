package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.presenter.callbacks.CoverageDialogCallbak;
import com.totalplay.sociodigital.presenter.callbacks.ValidateCoverageCallback;

import java.io.Serializable;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public class ValidateCoverageDialog extends BaseActivity{

    public static Intent launch(Context context){
        Intent intent = new Intent(context,ValidateCoverageDialog.class);
            return intent;
    }

    private TextView titleTextView;
    private ImageView imageGif;
    private TextView detailTextView;
    private CoverageDialogCallbak mCoverageDialogCallbak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.validate_coverage_dialog);
        bindResources();
        setFonts();
        Glide.with(this).load(R.drawable.ic_location_animation).into(imageGif);
    }

    private void bindResources(){
        titleTextView =  findViewById(R.id.act_validate_coverage_dialog_title);
        imageGif =  findViewById(R.id.act_validate_coverage_dialog_image);
        detailTextView =  findViewById(R.id.act_validate_coverage_dialog_detail);
    }

    private void setFonts(){
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        titleTextView.setTypeface(monserratRegular);
        detailTextView.setTypeface(monserratRegular);
    }

    public static void close(){

    }

}
