package com.totalplay.sociodigital.view.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.library.utils.Keys;
import com.totalplay.sociodigital.model.pojos.Video;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayVideoActivity extends AppCompatActivity{
    @BindView(R.id.mPlayContentVideView)
    public VideoView mPlayContentVideView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video_activity);

        ButterKnife.bind(this);
        Video video = (Video) getIntent().getSerializableExtra(Keys.EXTRA_VIDEO);

        File path = Environment.getExternalStorageDirectory();
        File file = new File(path, "SocioDigital/" + video.id + ".mp4");
        Uri vidUri;
        /*if (file.exists()) {
            vidUri = Uri.fromFile(file);
        } else {*/
            String URL = getResources().getString(R.string.apps_totalplay_url);
            vidUri = Uri.parse(URL + video.videoUrl);
        //}

        mPlayContentVideView.setVideoURI(vidUri);

        MediaController vidControl = new MediaController(this);
        mPlayContentVideView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                Toast.makeText(PlayVideoActivity.this, "No se puede reproducir el video", Toast.LENGTH_LONG).show();
                finish();
                return false;
            }
        });

        vidControl.setAnchorView(mPlayContentVideView);
        mPlayContentVideView.setMediaController(vidControl);

        mPlayContentVideView.start();
    }

    @OnClick(R.id.mCloseVideoPlayImageView)
    public void OnClickClose(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
