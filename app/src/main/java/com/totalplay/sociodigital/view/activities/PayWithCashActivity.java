package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.request.document.DocumentBean;
import com.totalplay.sociodigital.background.ws.response.ConsultationSaleTPresponse;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.model.ConsutationSale.AdditionalData;
import com.totalplay.sociodigital.model.ConsutationSale.AdditionalsInformation;
import com.totalplay.sociodigital.model.ConsutationSale.BillingFact;
import com.totalplay.sociodigital.model.ConsutationSale.DocumentEvidence;
import com.totalplay.sociodigital.model.ConsutationSale.Documents;
import com.totalplay.sociodigital.model.ConsutationSale.PayMethod;
import com.totalplay.sociodigital.model.ConsutationSale.ProductDetail;
import com.totalplay.sociodigital.model.ConsutationSale.Promotional;
import com.totalplay.sociodigital.model.ConsutationSale.PromotionalsAdd;
import com.totalplay.sociodigital.model.ConsutationSale.ProspectInfo;
import com.totalplay.sociodigital.model.ConsutationSale.SaleTP;
import com.totalplay.sociodigital.model.ConsutationSale.SiteInfo;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.entities.Additional;
import com.totalplay.sociodigital.model.entities.PayMethodEntity;
import com.totalplay.sociodigital.model.entities.ProductsAdd;
import com.totalplay.sociodigital.model.entities.PromocionesAdd;
import com.totalplay.sociodigital.model.entities.Promotion;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.callbacks.ConsultationSaleCallback;
import com.totalplay.sociodigital.presenter.implementations.ConsultationSalePresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.resolveSize;

/**
 * Created by imacbookpro on 06/09/18.
 * SocioDigital
 */

public class PayWithCashActivity extends BaseActivity implements ConsultationSaleCallback {

    public static Intent launch(Context context, PlanTotalPlay planSelected, ArrayList<Additional> additonals, ArrayList<Promotion> mPromotions, String priceList) {
        Intent intent = new Intent(context, PayWithCashActivity.class);
        intent.putExtra("planSelected", (Serializable) planSelected);
        intent.putExtra("additionals", (Serializable) additonals);
        intent.putExtra("promotions", (Serializable) mPromotions);
        intent.putExtra("priceList", priceList);
        return intent;
    }

    ConsultationSalePresenter mConsultationSalePresenter;
    PayMethodEntity payMethod = new PayMethodEntity();
    private LoginPresenter mLoginPresenter;

    private TextView mTitleTextView;
    private TextView mAmountToPayTextView;
    private TextView mAmountTextView;
    private TextView mNamePackageTextView;
    private Button mContinueButton;
    private ViewGroup mBackToolbar;

    //Dialog Success oportunity
    private TextView mSuccesOportunityTitleTextview;
    private TextView mSuccessOportunityDetailTextview;
    private TextView mSuccessOportunitySolicitudeNumber;
    private TextView mSuccessOportunityNumber;
    private Button mSuccessOportunityAccept;
    private ViewGroup mSuceessOportunityContent;

    //Dialog Failed oportunity
    private TextView mFailedOportunityTitleTextView;
    private TextView mFailedOportunityDetailTextView;
    private Button mFailedOportunityRetryButton;
    private ViewGroup mFailedOportunityContent;

    //Dialog Syncronized Documents
    private TextView mSyncDocumentsTitleTextView;
    private TextView mSyncDocumentsDetailTextview;
    private ViewGroup mSyncDicumentsContent;

    private PlanTotalPlay mPlanTotalPlay;
    private ArrayList<Additional> mAdditonals;
    private ArrayList<Promotion> mPromotions;

    private int progressStatus1;
    private Handler handler = new Handler();
    private ProgressBar progress;
    private ImageView imgResult;
    private String priceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.pay_with_cash_activity);
        mPlanTotalPlay = (PlanTotalPlay) getIntent().getSerializableExtra("planSelected");
        mAdditonals = (ArrayList<Additional>) getIntent().getSerializableExtra("additionals");
        mPromotions = (ArrayList<Promotion>) getIntent().getSerializableExtra("promotions");
        priceList = getIntent().getStringExtra("priceList");
        bindResources();
        setFont();
        setOnClicks();
        putDataData();
        mConsultationSalePresenter = new ConsultationSalePresenter(this, this);
        mLoginPresenter = new LoginPresenter(this);
        Glide.with(this).load(R.drawable.ic_charging).into(imgResult);
    }

    private void bindResources() {
        mTitleTextView = findViewById(R.id.act_pay_whith_cash_title);
        mAmountToPayTextView = findViewById(R.id.act_pay_whith_cash_amount_to_pay);
        mAmountTextView = findViewById(R.id.act_pay_whith_cash_amount);
        mNamePackageTextView = findViewById(R.id.act_pay_whith_cash_name_package);
        mContinueButton = findViewById(R.id.act_pay_whith_cash_cotinue);
        mBackToolbar = findViewById(R.id.mBackButton);
        //Dialog SuccessOportunity
        mSuccesOportunityTitleTextview = findViewById(R.id.dialog_success_oportunity_title);
        mSuccessOportunityDetailTextview = findViewById(R.id.dialog_success_oportunity_detail);
        mSuccessOportunitySolicitudeNumber = findViewById(R.id.dialog_success_oportunity_number_solicitude);
        mSuccessOportunityNumber = findViewById(R.id.dialog_success_oportunity_number);
        mSuccessOportunityAccept = findViewById(R.id.dialog_success_oportunity_accept);
        mSuceessOportunityContent = findViewById(R.id.dialog_success_oportunity_content);
        //Dialog FailedOportunity
        mFailedOportunityTitleTextView = findViewById(R.id.dialog_failed_oportunity_title);
        mFailedOportunityDetailTextView = findViewById(R.id.dialog_failed_oportunity_detail);
        mFailedOportunityRetryButton = findViewById(R.id.dialog_failed_oportunity_retry);
        mFailedOportunityContent = findViewById(R.id.dialog_failed_oportunity_content);
        //Dialog SyncDocuments
        mSyncDocumentsTitleTextView = findViewById(R.id.dialog_syncronized_documents_title);
        mSyncDocumentsDetailTextview = findViewById(R.id.dialog_syncronized_documents_detail);
        mSyncDicumentsContent = findViewById(R.id.dialog_syncronized_documents_content);

        progress = findViewById(R.id.dialog_synchronized_documents_progress);
        imgResult = findViewById(R.id.dialog_synchronized_documents_result_img);
    }

    private void setFont() {
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        mTitleTextView.setTypeface(monserratRegular);
        mAmountToPayTextView.setTypeface(monserratBold);
        mAmountTextView.setTypeface(monserratBold);
        mNamePackageTextView.setTypeface(monserratBold);
        mContinueButton.setTypeface(monserratMedium);
        //Dialog Success Oportunity
        mSuccesOportunityTitleTextview.setTypeface(monserratRegular);
        mSuccessOportunityDetailTextview.setTypeface(monserratRegular);
        mSuccessOportunitySolicitudeNumber.setTypeface(monserratBold);
        mSuccessOportunityNumber.setTypeface(monserratBold);
        mSuccessOportunityAccept.setTypeface(monserratMedium);
        //Dialog FailedOportunity
        mFailedOportunityTitleTextView.setTypeface(monserratRegular);
        mFailedOportunityDetailTextView.setTypeface(monserratRegular);
        mFailedOportunityRetryButton.setTypeface(monserratMedium);
        //Dialog SyncDocuments
        mSyncDocumentsTitleTextView.setTypeface(monserratRegular);
        mSyncDocumentsDetailTextview.setTypeface(monserratBold);
    }

    private void setOnClicks() {
        mContinueButton.setOnClickListener(v -> saveData());

        mSuccessOportunityAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSuceessOportunityContent.setVisibility(GONE);
                Intent newIntent = new Intent(getApplicationContext(), HomeActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(newIntent);
            }
        });

        mFailedOportunityRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(getApplicationContext()).load(R.drawable.ic_charging).into(imgResult);
                progress.setProgress(0);
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
                mFailedOportunityContent.setVisibility(GONE);
            }
        });

        mSyncDicumentsContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        mFailedOportunityContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mSuceessOportunityContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mBackToolbar.setOnClickListener(v -> finish());
    }

    private void putDataData() {
        mNamePackageTextView.setText(mPlanTotalPlay.namePackage);
        Double instaConst = Double.parseDouble(Prefs.instance().string(SocioDigitalKeys.INSTALATION_COST));
        Double listPriceDouble = Double.valueOf(priceList);
        Double totalPay = (Math.ceil(instaConst) + Math.ceil(listPriceDouble));
        mAmountTextView.setText(String.format("$ %s", String.format("%.2f", totalPay)));
    }

    private void saveData() {
        payMethod.nameOwner = (CryptoUtils.crypt(Prefs.instance().string("ownerName")));
        payMethod.lastNameFatherOwner = (CryptoUtils.crypt(Prefs.instance().string("ownerLastName")));
        payMethod.lastNameMotherOwner = (CryptoUtils.crypt(Prefs.instance().string("ownerSecondLastName")));
        payMethod.method = (CryptoUtils.crypt("Efectivo"));
        payMethod.expiryMonth = CryptoUtils.crypt("");
        payMethod.expiryYear = CryptoUtils.crypt("");
        payMethod.cardNumber = CryptoUtils.crypt("");
        payMethod.typeCard = CryptoUtils.crypt("");
        createProspect();
    }

    private void createProspect() {
        InfoSocioBean infoSocioBean = mLoginPresenter.retrieveInfoSocio();
        AdditionalData additionalData = new AdditionalData(CryptoUtils.crypt(mFormalityEntity.prospectoBean.signature.additionalInfo.typeID),
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.contact.medioContacto),
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.signature.additionalInfo.infoID));
        BillingFact billingFact = new BillingFact(mFormalityEntity.prospectoBean.direccionBean.street,
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.direccionBean.place),
                mFormalityEntity.prospectoBean.direccionBean.zipCode,
                mFormalityEntity.prospectoBean.direccionBean.colony,
                mFormalityEntity.prospectoBean.direccionBean.delegation,
                mFormalityEntity.prospectoBean.direccionBean.state,
                CryptoUtils.crypt("true"),
                mFormalityEntity.prospectoBean.direccionBean.noExt,
                mFormalityEntity.prospectoBean.direccionBean.noInt);
        ArrayList<DocumentEvidence> mDocumentsEvidence = new ArrayList<>();
        List<DocumentBean> documents = mDocumentFilesRequest.documentAdd.document;
        if (documents.size() < 4) {
            DocumentBean idProofs = new DocumentBean();
            idProofs.idOpportunity = "";
            idProofs.idOpportunity = "";
            idProofs.brmAccount = "";
            idProofs.name = "Comprobante.jpeg";
            idProofs.type = "Comprobante de domicilio";
            idProofs.body = documents.get(0).body;
            idProofs.thumbnail = documents.get(0).thumbnail;
            documents.add(idProofs);
        }
        for (DocumentBean document : documents) {
            mDocumentsEvidence.add(new DocumentEvidence(document.body, document.name, document.type));
        }
        Documents mDocument = new Documents(mDocumentsEvidence);
        PayMethod payMethodService = new PayMethod(payMethod.expiryYear,
                payMethod.lastNameMotherOwner,
                payMethod.lastNameFatherOwner,
                payMethod.cardNumber,
                payMethod.expiryMonth,
                payMethod.method,
                payMethod.nameOwner,
                payMethod.typeCard);
        String estimFiscal = Prefs.instance().string("AplicaEstimuloFiscal");
        ProspectInfo prospectInfo = new ProspectInfo(mFormalityEntity.prospectoBean.direccionBean.street,
                mFormalityEntity.prospectoBean.contact.cellphone,
                CryptoUtils.crypt(mFormalityEntity.prospectoBean.direccionBean.place),
                mFormalityEntity.prospectoBean.direccionBean.zipCode,
                mFormalityEntity.prospectoBean.direccionBean.colony,
                mFormalityEntity.prospectoBean.contact.socialReason,//Si es moral poner aqui el nombre?
                mFormalityEntity.prospectoBean.contact.email,
                mFormalityEntity.prospectoBean.direccionBean.delegation,
                mFormalityEntity.prospectoBean.direccionBean.state,
                mFormalityEntity.prospectoBean.contact.birthDay,
                mFormalityEntity.prospectoBean.contact.name,
                mFormalityEntity.prospectoBean.contact.motherLastName,
                mFormalityEntity.prospectoBean.contact.fatherLastName,
                mFormalityEntity.prospectoBean.direccionBean.noExt,
                mFormalityEntity.prospectoBean.direccionBean.noInt,
                mFormalityEntity.prospectoBean.contact.otherEmail,
                mFormalityEntity.prospectoBean.contact.otherPhone,
                mFormalityEntity.prospectoBean.direccionBean.betweenStreets,
                mFormalityEntity.prospectoBean.direccionBean.urbanReference,
                mFormalityEntity.prospectoBean.contact.rfc,
                mFormalityEntity.prospectoBean.contact.phone,
                mFormalityEntity.prospectoBean.contact.typePerson,
                CryptoUtils.crypt(infoSocioBean.idOportunity),
                CryptoUtils.crypt(infoSocioBean.mCanal),
                CryptoUtils.crypt(infoSocioBean.mSubCanal),
                estimFiscal);

        SiteInfo siteInfo = new SiteInfo(Prefs.instance().string("categoryService"),//poner aqui lo de la fibra?
                mFormalityEntity.prospectoBean.direccionBean.cluster,
                mFormalityEntity.prospectoBean.direccionBean.district,
                mFormalityEntity.prospectoBean.direccionBean.feasibility,
                mFormalityEntity.prospectoBean.direccionBean.latitude,
                mFormalityEntity.prospectoBean.direccionBean.longitude,
                mFormalityEntity.prospectoBean.direccionBean.place,
                mFormalityEntity.prospectoBean.direccionBean.region,
                mFormalityEntity.prospectoBean.direccionBean.idRegion,
                "Sólo fibra",
                mFormalityEntity.prospectoBean.direccionBean.zone);
        ArrayList<Promotional> promotionals = new ArrayList<>();
        for (Promotion promotion : mPromotions) {
            promotionals.add(new Promotional(promotion.idPromotion, promotion.namePromotion));
        }
        PromotionalsAdd promotionalsAdd = new PromotionalsAdd(promotionals);

        com.totalplay.sociodigital.model.ConsutationSale.ProductsAdd productsAdd = new com.totalplay.sociodigital.model.ConsutationSale.ProductsAdd(new ArrayList<ProductDetail>());
        //AdditionalsInformation additionalsInformation =  new AdditionalsInformation(productsAdd, promotionalsAdd);
        AdditionalsInformation additionalsInformation = new AdditionalsInformation();
        SaleTP saleTP = new SaleTP(additionalData, billingFact, mDocument, payMethodService, mPlanTotalPlay.id, prospectInfo, siteInfo, additionalsInformation);
        mConsultationSalePresenter.createSale(saleTP);
    }

    @Override
    public void onErrorConnection() {
        //MessageUtils.stopProgress();
        startProgress1(progress, false);
    }

    @Override
    public void onLoadConsultationSale() {
        //MessageUtils.progress(this,"Creando venta");
        mSyncDicumentsContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccesConsultationSale(ConsultationSaleTPresponse response) {
        MessageUtils.stopProgress();
        //MessageUtils.toast(this,"Venta creada con exito");
        String[] result = response.mResult.resultDescription.split(":");
        String noSolicitude = null;
        if (result.length > 1) {
            noSolicitude = result[1].trim();
            mSuccessOportunityNumber.setText(noSolicitude);
        }
        startProgress1(progress, true);
    }

    @Override
    public void onErrorConsultationSale() {
        //MessageUtils.stopProgress();
        startProgress1(progress, false);
    }

    private void startProgress1(ProgressBar progress, boolean isSuccessRegister) {
        progressStatus1 = 0;
        runOnUiThread(() -> {
            progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
        });
        new Thread(() -> {
            progressStatus1 = 0;
            boolean enable = true;
            while (progressStatus1 < 100) {
                progressStatus1 += 1;
                if (progressStatus1 > 95 && enable) {
                    runOnUiThread(() -> {
                        //launch image
                        if (isSuccessRegister) {
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_green).into(imgResult);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mSyncDicumentsContent.setVisibility(GONE);
                                    mSuceessOportunityContent.setVisibility(View.VISIBLE);
                                }
                            }, 1200);
                        } else {
                            progress.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_red));
                            Glide.with(getApplicationContext()).load(R.drawable.ic_check_red).into(imgResult);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mSyncDicumentsContent.setVisibility(GONE);
                                    mFailedOportunityContent.setVisibility(View.VISIBLE);
                                    MessageUtils.toast(getApplicationContext(), "Error al general la venta");
                                }
                            }, 1200);
                        }
                    });
                    enable = false;
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(() -> progress.setProgress(progressStatus1));
            }
        }).start();
    }
}
