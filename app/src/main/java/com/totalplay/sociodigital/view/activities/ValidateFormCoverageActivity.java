package com.totalplay.sociodigital.view.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.AddressGenerator;
import com.totalplay.sociodigital.library.enums.DataAddress;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.model.entities.ColonyCP;
import com.totalplay.sociodigital.model.entities.DireccionBean;
import com.totalplay.sociodigital.presenter.callbacks.ValidateCoverageCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.ValidateCoveragePresenter;
import com.totalplay.sociodigital.view.adapters.ColonyArrAdapter;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public class ValidateFormCoverageActivity extends BaseActivity implements ValidateCoverageCallback, ColonyArrAdapter.updateColonyName {
    public static Intent launch(Context context, AddressGenerator generator, DireccionBean direccionBean, DataAddress dataAddress, ArrayList<ColonyCP> coloniesCP){
        Intent intent =  new Intent(context, ValidateFormCoverageActivity.class);
        intent.putExtra(SocioDigitalKeys.ADDRESS, generator);
        intent.putExtra(SocioDigitalKeys.EXTRA_DIRECCION_BEAN, direccionBean);
        intent.putExtra("dataAddress", dataAddress);
        Bundle extra = new Bundle();
        extra.putSerializable("objects", coloniesCP);
        intent.putExtra("ArrColonies", extra);
        return intent;
    }

    private AddressGenerator generator;
    private DireccionBean mDireccionBean;
    private ValidateCoveragePresenter validateCoveragePresenter;

    private TextView mTitle;
    private TextView mCPTitle;
    private EditText mCPEditext;
    private TextView mColonyTitle;
    private EditText mColonyEditext;
    private TextView mDelegateTitle;
    private EditText mDelegateEditext;
    private TextView mCityTitle;
    private EditText mCityEditext;
    private TextView mStateTitle;
    private EditText mStateEditext;
    private TextView mStreetTitle;
    private EditText mStreetEditext;
    private TextView mExternalNumberTitle;
    private EditText mExternalNumberEditext;
    private TextView mInternalNumberTitle;
    private EditText mInternalNumberEditext;
    private TextView mBetweenStreetTitle;
    private EditText mBetweenStreetEditext;
    private TextView mDetailsTitle;
    private EditText mDetailsEditext;
    private Button mSendButton;
    private FormValidator mFormValidor;
    private DataAddress mDataAddress;
    private ViewGroup mBackToolbar;
    private  ArrayList<ColonyCP> coloniesCP;
    private ColonyArrAdapter colonyArrAdapter;
    private ViewGroup contentColonySelector;
    private RecyclerView recyclerViewColonySelector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.validate_form_coverage_activity);
        generator = (AddressGenerator) getIntent().getSerializableExtra(SocioDigitalKeys.ADDRESS);
        mDireccionBean = (DireccionBean) getIntent().getSerializableExtra(SocioDigitalKeys.EXTRA_DIRECCION_BEAN);
        mDataAddress = (DataAddress) getIntent().getSerializableExtra("dataAddress");
        Bundle extra = getIntent().getBundleExtra("ArrColonies");
        coloniesCP = (ArrayList<ColonyCP>) extra.getSerializable("objects");
        colonyArrAdapter =  new ColonyArrAdapter(coloniesCP,this, this);
        bindResources();
        setFont();
        setFormValidator();
        setOnClicks();
        putDataForm();

        //mColonyEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mDelegateEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mCityEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mStateEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mStreetEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mBetweenStreetEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mDetailsEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @Override
    protected BasePresenter getPresenter() {
        return validateCoveragePresenter = new ValidateCoveragePresenter(this, this);
    }


    private void bindResources(){
        mTitle = findViewById(R.id.act_validate_form_coverage_title);
        mCPTitle = findViewById(R.id.act_validate_form_coverage_cp_title);
        mCPEditext = findViewById(R.id.act_validate_form_coverage_cp_editext);
        mColonyTitle = findViewById(R.id.act_validate_form_coverage_colony_title);
        mColonyEditext = findViewById(R.id.act_validate_form_coverage_colony_editext);
        mDelegateTitle = findViewById(R.id.act_validate_form_coverage_delegate_title);
        mDelegateEditext = findViewById(R.id.act_validate_form_coverage_delegate_editext);
        mCityTitle = findViewById(R.id.act_validate_form_coverage_city_title);
        mCityEditext = findViewById(R.id.act_validate_form_coverage_city_editext);
        mStateTitle = findViewById(R.id.act_validate_form_coverage_state_title);
        mStateEditext = findViewById(R.id.act_validate_form_coverage_state_editext);
        mStreetTitle = findViewById(R.id.act_validate_form_coverage_street_title);
        mStreetEditext = findViewById(R.id.act_validate_form_coverage_street_editext);
        mExternalNumberTitle = findViewById(R.id.act_validate_form_coverage_external_number_title);
        mExternalNumberEditext = findViewById(R.id.act_validate_form_coverage_external_number_editext);
        mInternalNumberTitle = findViewById(R.id.act_validate_form_coverage_internal_number_title);
        mInternalNumberEditext = findViewById(R.id.act_validate_form_coverage_internal_number_editext);
        mBetweenStreetTitle = findViewById(R.id.act_validate_form_coverage_between_street_title);
        mBetweenStreetEditext = findViewById(R.id.act_validate_form_coverage_between_street_editext);
        mDetailsTitle = findViewById(R.id.act_validate_form_coverage_details_title);
        mDetailsEditext = findViewById(R.id.act_validate_form_coverage_details_editext);
        mSendButton = findViewById(R.id.act_validate_form_coverage_send_button);
        mBackToolbar =  findViewById(R.id.mBackButton);
        mCPEditext.setEnabled(false);
        mCityEditext.setEnabled(false);
        recyclerViewColonySelector =  findViewById(R.id.custom_selector_colony_recycler);
        contentColonySelector =  findViewById(R.id.custom_selector_colony_content);
    }

    private void setFont(){
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");

        mTitle.setTypeface(monserratRegular);
        mCPTitle.setTypeface(monserratBold);
        mColonyTitle.setTypeface(monserratBold);
        mDelegateTitle.setTypeface(monserratBold);
        mCityTitle.setTypeface(monserratBold);
        mStateTitle.setTypeface(monserratBold);
        mStreetTitle.setTypeface(monserratBold);
        mExternalNumberTitle.setTypeface(monserratBold);
        mInternalNumberTitle.setTypeface(monserratBold);
        mBetweenStreetTitle.setTypeface(monserratBold);
        mDetailsTitle.setTypeface(monserratBold);
        mCPEditext.setTypeface(monserratRegular);
        mColonyEditext.setTypeface(monserratRegular);
        mDelegateEditext.setTypeface(monserratRegular);
        mCityEditext.setTypeface(monserratRegular);
        mStateEditext.setTypeface(monserratRegular);
        mStreetEditext.setTypeface(monserratRegular);
        mExternalNumberEditext.setTypeface(monserratRegular);
        mInternalNumberEditext.setTypeface(monserratRegular);
        mBetweenStreetEditext.setTypeface(monserratRegular);
        mDetailsEditext.setTypeface(monserratRegular);
        mSendButton.setTypeface(monserratMedium);
    }

    private void setOnClicks(){
        mSendButton.setOnClickListener(v -> getDataForm());
        mBackToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mColonyEditext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contentColonySelector.setVisibility(View.VISIBLE);
            }
        });
    }

    private void putDataForm(){
        recyclerViewColonySelector.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewColonySelector.setAdapter(colonyArrAdapter);
        if (generator != null && generator.zipCode != null && !generator.zipCode.isEmpty()) {
            mCPEditext.setText(generator.zipCode);

            /*if (mDataAddress.colony != null)
                mColonyEditext.setText(mDataAddress.colony.toUpperCase());
            else if (generator.colony != null)
                mColonyEditext.setText(generator.colony.toUpperCase());
*/
            if (mDataAddress.delegation != null)
                mDelegateEditext.setText(mDataAddress.delegation.toUpperCase());
            else if (generator.delegation != null)
                mDelegateEditext.setText(generator.delegation);

            String place = Prefs.instance().string("savePlace");
            mCityEditext.setText(place);

            if (mDataAddress.state != null)
                mStateEditext.setText(mDataAddress.state.toUpperCase());
            else if(generator.state != null)
                mStateEditext.setText(generator.state.toUpperCase());

            if (generator.street != null)
                mStreetEditext.setText(generator.street.toUpperCase());
        }
    }
    private void setFormValidator(){
        mFormValidor = new FormValidator(this);
        mFormValidor.addValidators(
                new EditTextValidator(mCPEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mColonyEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mDelegateEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCityEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStateEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mColonyEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStreetEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mExternalNumberEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mBetweenStreetEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mDetailsEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty)

        );
    }

    private void getDataForm(){
        if (mFormValidor.isValid()) {
            mDireccionBean.street = mStreetEditext.getText().toString().trim();
            mDireccionBean.noExt = CryptoUtils.crypt(mExternalNumberEditext.getText().toString().trim());
            if (mInternalNumberEditext.length() > 0)
                mDireccionBean.noInt = CryptoUtils.crypt(mInternalNumberEditext.getText().toString().trim());
            else
                mDireccionBean.noInt = CryptoUtils.crypt("SN");
            mDireccionBean.colony = mColonyEditext.getText().toString().trim();
            mDireccionBean.city = CryptoUtils.crypt(mCityEditext.getText().toString().trim());
            Prefs.instance().putString(SocioDigitalKeys.CITY, mCityEditext.getText().toString().trim());
            Prefs.instance().putString(SocioDigitalKeys.COLONY, mColonyEditext.getText().toString().trim());
            mDireccionBean.zipCode = CryptoUtils.crypt(mCPEditext.getText().toString().trim());
            mDireccionBean.delegation = CryptoUtils.crypt(mDelegateEditext.getText().toString().trim());
            mDireccionBean.state = CryptoUtils.crypt(mStateEditext.getText().toString().trim());
            mDireccionBean.betweenStreets = mBetweenStreetEditext.getText().toString().trim();
            mDireccionBean.urbanReference = mDetailsEditext.getText().toString().trim();
            validateCoveragePresenter.saveCoverageFormality(mDireccionBean);
            finish();
         }
    }

    @Override
    public void onLoadAddress(AddressGenerator generator) {

    }

    @Override
    public void onSuccessChargeDirection(String city) {
        //startActivity(TypeProfileActivity.launch(this,city));
        startActivity(ContactInformationActivity.launch(this,city));
        Log.e("Datos", "Almacenados");
    }

    @Override
    public void updateColony(String nameColony) {
        contentColonySelector.setVisibility(View.GONE);
        mColonyEditext.setText(nameColony);
    }
}
