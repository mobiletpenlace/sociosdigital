package com.totalplay.sociodigital.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.library.pojos.InfoDetail;

import java.util.ArrayList;

public class SearchNameAdapter extends RecyclerView.Adapter<SearchNameAdapter.RecyclerViewSimpleTextViewHolder>{
    //Changes2
    private ArrayList<InfoDetail> listItems;
    private Context mContext;

    public SearchNameAdapter(Context context, ArrayList<InfoDetail> items){
        this.mContext =  context;
        this.listItems =  items;
    }

    @NonNull
    @Override
    public RecyclerViewSimpleTextViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_search_names, viewGroup, false);
        return new SearchNameAdapter.RecyclerViewSimpleTextViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewSimpleTextViewHolder holder, int i) {
        holder.name.setText(listItems.get(i).name);
        holder.code.setText(listItems.get(i).codeSale);
        holder.date.setText(getDate(listItems.get(i).sendDate));
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class RecyclerViewSimpleTextViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView code;
        TextView date;

        public RecyclerViewSimpleTextViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_search_name);
            code = itemView.findViewById(R.id.item_search_code);
            date = itemView.findViewById(R.id.item_search_date);
        }
    }

    public void updateListItems(ArrayList<InfoDetail> listItems){
        this.listItems = listItems;
        notifyDataSetChanged();
    }

    private String getDate(String rawDate){
        String[] date = rawDate.split(" ");
        if(date.length == 2){
            String resultDate =  date[0];
            String[] filterDate = resultDate.split("-");
            return filterDate[2] + "/" + filterDate[1] + "/" + filterDate[0];
        }else
            return "-";
    }
}
