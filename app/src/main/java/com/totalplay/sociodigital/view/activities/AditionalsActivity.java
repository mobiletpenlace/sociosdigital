package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.resources.icc.viewflow.CoverFlow;
import com.resources.icc.viewflow.core.PagerContainer;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.FamilyPackagesResponse;
import com.totalplay.sociodigital.background.ws.response.PlanDetailResponse;
import com.totalplay.sociodigital.library.quoter.Producto.CotServicioProducto;
import com.totalplay.sociodigital.library.quoter.Servicio.CotPlanServicio;
import com.totalplay.sociodigital.library.quoter.plan.Cot_SitioPlan;
import com.totalplay.sociodigital.library.quoter.utils.Constans;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.model.entities.PropuestaBean;
import com.totalplay.sociodigital.model.entities.QuotationPlainService;
import com.totalplay.sociodigital.model.entities.QuotationPlainServiceProduct;
import com.totalplay.sociodigital.model.entities.QuotationPromotionPlain;
import com.totalplay.sociodigital.model.entities.QuotationSitePlainEntity;
import com.totalplay.sociodigital.model.pojos.ArrCostoInstalacionBean;
import com.totalplay.sociodigital.model.pojos.ArrProductosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrProductosIncluidosBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosAdicionalesBean;
import com.totalplay.sociodigital.model.pojos.ArrServiciosIncluidosBean;
import com.totalplay.sociodigital.presenter.callbacks.AddonSelectorCallback;
import com.totalplay.sociodigital.presenter.callbacks.LeadFollowingCallback;
import com.totalplay.sociodigital.presenter.implementations.AdditionalServicesPresenter;
import com.totalplay.sociodigital.presenter.implementations.AddonSelectorPresenter;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.view.adapters.AdditionalsServicesAdapter;
import com.totalplay.sociodigital.view.adapters.SelectAdditionalAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class AditionalsActivity  extends BaseActivity implements AddonSelectorCallback, LeadFollowingCallback, AdditionalsServicesAdapter.StatusButton{

    public static Intent launch(Context context,FamilyPackagesResponse.Plans plans){
        Intent intent =  new Intent(context,AditionalsActivity.class);
        intent.putExtra("IdPlan", plans.planId);
        intent.putExtra("NamePlan", plans.planName);
        intent.putExtra("PlanPrice", plans.ammountL);
        return intent;
    }

    private AdditionalServicesPresenter mAdditionalServicesPresenter;
    private LinearLayoutManager HorizontalLayout;
    private PagerContainer mAddtionalsContainer;
    private RecyclerView mItemSelectorRecyclerView;
    private SelectAdditionalAdapter mSelectAdditionalAdapter;
    private ViewPager pager;
    private AddonSelectorPresenter mAddonSelectorPresenter;
    private PlanDetailResponse mPlanDetailResponse;
    private double installationCostAmount;
    private String group;
    public AdditionalsServicesAdapter adapterService;
    private double planPrice;
    private double listPrice;
    private double priceSoonPaymen;
    private double planSoonPrice = 0.0f;
    private Button mAcceptButton;
    private Button mCancelButton;
    private PropuestaBean proposal = new PropuestaBean();
    private double priceList;
    private double priceEarly;
    private double total;
    private double totalEarly;
    private boolean flagOnce;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.aditionals_activity);
        planPrice = Double.parseDouble(getIntent().getStringExtra("PlanPrice"));
        bindResources();
        setFont();
        setOnClicks();
        setPager();
        flagOnce =  true;
    }

    private void bindResources(){
        mItemSelectorRecyclerView = findViewById(R.id.act_aditionals_selector);
        mAddtionalsContainer =  findViewById(R.id.pager_container_aditionals);
        mCancelButton = findViewById(R.id.act_aditionals_cancel);
        mAcceptButton = findViewById(R.id.act_aditionals_continue);
    }

    private void setFont() {
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        //Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");

        mCancelButton.setTypeface(monserratMedium);
        mAcceptButton.setTypeface(monserratMedium);
    }

    private void setOnClicks(){
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ArrServiciosAdicionalesBean> arrServiciosAdicionalesBean = new ArrayList<>();
                List<ArrProductosAdicionalesBean> arrProductosAdicionalesBean = new ArrayList<>();
                List<ArrCostoInstalacionBean> arrCostoInstalacionBeans = new ArrayList<>();

                for (PlanDetailResponse.AdicionalesBean adicionalesBean : adapterService.itemsSelected) {
                    if (adicionalesBean.getAdicionalType() == PlanDetailResponse.AdicionalesBean.SERVICE) {
                        arrServiciosAdicionalesBean.add((ArrServiciosAdicionalesBean) adicionalesBean);
                    } else {
                        arrProductosAdicionalesBean.add((ArrProductosAdicionalesBean) adicionalesBean);
                    }
                }

                storagePlain(mPlanDetailResponse.ArrServiciosIncluidos, arrServiciosAdicionalesBean, arrProductosAdicionalesBean, mPlanDetailResponse.ArrCostoInstalacion);
            }
        });
    }

    public void storagePlain(ArrayList<ArrServiciosIncluidosBean> includeService, List<ArrServiciosAdicionalesBean> addonServices, List<ArrProductosAdicionalesBean> arrAddonProd, List<ArrCostoInstalacionBean> arrCostoInstalacionBeans) {

        proposal.quotationSitePlain = new QuotationSitePlainEntity();
        proposal.quotationSitePlain.plainName = getIntent().getStringExtra("NamePlan");
        proposal.quotationSitePlain.plain = getIntent().getStringExtra("IdPlan");
        proposal.offer = Prefs.instance().string(SocioDigitalKeys.OFFERT);

        QuotationPromotionPlain cot_planPromocion = new QuotationPromotionPlain();
        cot_planPromocion.detailPlainPromotion = (new RealmList<>());
        proposal.quotationPromotionPlain = (cot_planPromocion);
        proposal.quotationPlainService = new RealmList<>();

        //SERVICIOS INCLUIDOS
        if (includeService != null) {
            for (int i = 0; i < includeService.size(); i++) {
                QuotationPlainService Cot_planServicio = new QuotationPlainService();
                Cot_planServicio.detailPlainService = includeService.get(i).Id;
                Cot_planServicio.isAdditionalService = "false";
                Cot_planServicio.nameService = includeService.get(i).Nombre;
                Cot_planServicio.type = "Incluido";
                Cot_planServicio.detailPromotionPlain = "";
                RealmList<QuotationPlainServiceProduct> arrCotServicioProductoBeen = new RealmList<>();


                //PRODUCTOS INCLUIDOS
                if (includeService.get(i).ArrProductosIncluidos != null)
                    for (int j = 0; j < includeService.get(i).ArrProductosIncluidos.size(); j++) {
                        ArrProductosIncluidosBean productoIncluido = includeService.get(i).ArrProductosIncluidos.get(j);
                        if (productoIncluido.Id != null && !productoIncluido.Id.isEmpty()) {
                            QuotationPlainServiceProduct product = new QuotationPlainServiceProduct();
                            product.quantity = (productoIncluido.Cantidad);
                            product.discount = ("0.0");
                            product.detailPromotionProductService = ("");
                            product.detailProductService = (productoIncluido.Id);
                            product.isUniqueCharge = (productoIncluido.EsCargoUnico);
                            product.isDiscount = ("false");
                            product.isAdditionalProduct = ("false");
                            product.isSoonPay = (productoIncluido.EsProntoPago);
                            product.tax2 = (productoIncluido.IEPS);
                            product.tax1 = (productoIncluido.IVA);
                            product.nameProduct = (productoIncluido.NameProducto);
                            product.unitaryPrice = (productoIncluido.PrecioBase);
                            product.unitarySoonPrice = (productoIncluido.PrecioProntoPago);
                            product.baseUnitaryPrice = (productoIncluido.PrecioBase);
                            product.fatherProduct = (productoIncluido.ProductoPadre);
                            product.typeProduct = (productoIncluido.TipoProducto);
                            product.detailPromotionPlain = ("");
                            product.mesInicio = "0";
                            arrCotServicioProductoBeen.add(product);
                        }
                    }

                //PRODUCTOS ADICIONALES
                if (arrAddonProd != null)
                    for (int j = 0; j < arrAddonProd.size(); j++) {
                        ArrProductosAdicionalesBean productoAdicional = arrAddonProd.get(j);
                        if (productoAdicional.Id != null && !productoAdicional.Id.isEmpty() && !productoAdicional.Cantidad.isEmpty()) {
                            //TELEFONIA E INTERNET
                            if (productoAdicional.Nombre.contains(Cot_planServicio.nameService)) {
                                QuotationPlainServiceProduct product = new QuotationPlainServiceProduct();
                                product.quantity = (productoAdicional.Cantidad);
                                product.discount = ("0.0");
                                product.detailPromotionProductService = ("");
                                product.detailProductService = (productoAdicional.Id);
                                product.isUniqueCharge = (productoAdicional.EsCargoUnico);
                                product.isDiscount = ("false");
                                product.isAdditionalProduct = ("true");
                                product.isSoonPay = (productoAdicional.EsProntoPago);
                                product.tax2 = (productoAdicional.IEPS);
                                product.tax1 = (productoAdicional.IVA);
                                product.nameProduct = (productoAdicional.NameProducto);
                                product.unitaryPrice = (productoAdicional.PrecioBase);
                                product.unitarySoonPrice = (productoAdicional.PrecioProntoPago);
                                product.baseUnitaryPrice = (productoAdicional.PrecioBase);
                                product.fatherProduct = (productoAdicional.ProductoPadre);
                                product.typeProduct = (productoAdicional.TipoProducto);
                                product.detailPromotionPlain = ("");
                                product.mesInicio = "0";
                                arrCotServicioProductoBeen.add(product);
                            }
                            //TV
                            else if (productoAdicional.Nombre.contains("TV TP") && Cot_planServicio.nameService.contains("Television TP")) {
                                QuotationPlainServiceProduct product = new QuotationPlainServiceProduct();
                                product.quantity = (productoAdicional.Cantidad);
                                product.discount = ("0.0");
                                product.detailPromotionProductService = ("");
                                product.detailProductService = (productoAdicional.Id);
                                product.isUniqueCharge = (productoAdicional.EsCargoUnico);
                                product.isDiscount = ("false");
                                product.isAdditionalProduct = ("true");
                                product.isSoonPay = (productoAdicional.EsProntoPago);
                                product.tax2 = (productoAdicional.IEPS);
                                product.tax1 = (productoAdicional.IVA);
                                product.nameProduct = (productoAdicional.NameProducto);
                                product.unitaryPrice = (productoAdicional.PrecioBase);
                                product.unitarySoonPrice = (productoAdicional.PrecioProntoPago);
                                product.baseUnitaryPrice = (productoAdicional.PrecioBase);
                                product.fatherProduct = (productoAdicional.ProductoPadre);
                                product.typeProduct = (productoAdicional.TipoProducto);
                                product.detailPromotionPlain = ("");
                                product.mesInicio = "0";
                                arrCotServicioProductoBeen.add(product);
                            }
                        }
                    }

                //COSTO DE INSTALACION
                if (arrCostoInstalacionBeans != null) {
                    if (i == 0) {
                        for (int j = 0; j < arrCostoInstalacionBeans.size(); j++) {
                            ArrCostoInstalacionBean costoInstalacion = arrCostoInstalacionBeans.get(j);
                            QuotationPlainServiceProduct product = new QuotationPlainServiceProduct();
                            product.quantity = (costoInstalacion.Cantidad);
                            product.discount = ("0.0");
                            product.detailPromotionProductService = ("");
                            product.detailProductService = (costoInstalacion.Id);
                            product.isUniqueCharge = (costoInstalacion.EsCargoUnico);
                            product.isDiscount = ("false");
                            product.isAdditionalProduct = ("true");
                            product.isSoonPay = (costoInstalacion.EsProntoPago);
                            product.tax2 = (costoInstalacion.IEPS);
                            product.tax1 = (costoInstalacion.IVA);
                            product.nameProduct = (costoInstalacion.NameProducto);
                            product.unitaryPrice = (costoInstalacion.PrecioBase);
                            product.unitarySoonPrice = (costoInstalacion.PrecioProntoPago);
                            product.baseUnitaryPrice = (costoInstalacion.PrecioBase);
                            product.fatherProduct = (costoInstalacion.ProductoPadre);
                            product.typeProduct = (costoInstalacion.TipoProducto);
                            product.detailPromotionPlain = ("");
                            product.mesInicio = (costoInstalacion.MesInicio);
                            arrCotServicioProductoBeen.add(product);
                        }
                    }
                }

                Cot_planServicio.quotationPlainServiceProducts = (arrCotServicioProductoBeen);
                proposal.quotationPlainService.add(Cot_planServicio);
            }

            //////////////////SERVICIOS ADICIONLES//////////////////////////////////////////////
            if (addonServices != null)
                for (int i = 0; i < addonServices.size(); i++) {
                    QuotationPlainService quotationPlainService = new QuotationPlainService();
                    quotationPlainService.namePlain = (addonServices.get(i).Id);
                    quotationPlainService.isAdditionalService = "true";
                    quotationPlainService.nameService = addonServices.get(i).Nombre;
                    quotationPlainService.type = "Adicional";
                    quotationPlainService.detailPromotionPlain = "";
                    quotationPlainService.detailPlainService = (addonServices.get(i).Id);
                    RealmList<QuotationPlainServiceProduct> arrCotServicioProductoBeen = new RealmList<>();

                    //PRODUCTOS INCLUIDOS
                    for (int j = 0; j < addonServices.get(i).ArrProductosIncluidos.size(); j++) {
                        ArrProductosIncluidosBean productoIncluido = addonServices.get(i).ArrProductosIncluidos.get(j);
                        if (productoIncluido.Id != null && !productoIncluido.Id.isEmpty()) {
                            QuotationPlainServiceProduct product = new QuotationPlainServiceProduct();
                            product.quantity = productoIncluido.Cantidad;
                            product.discount = "0.0";
                            product.detailPromotionProductService = "";
                            product.detailProductService = productoIncluido.Id;
                            product.isUniqueCharge = productoIncluido.EsCargoUnico;
                            product.isDiscount = "false";
                            product.isAdditionalProduct = "false";
                            product.isSoonPay = productoIncluido.EsProntoPago;
                            product.tax2 = productoIncluido.IEPS;
                            product.tax1 = productoIncluido.IVA;
                            product.nameProduct = productoIncluido.NameProducto;
                            product.unitaryPrice = productoIncluido.PrecioBase;
                            product.unitarySoonPrice = productoIncluido.PrecioProntoPago;
                            product.baseUnitaryPrice = productoIncluido.PrecioBase;
                            product.fatherProduct = productoIncluido.ProductoPadre;
                            product.typeProduct = productoIncluido.TipoProducto;
                            product.detailPromotionPlain = "";
                            product.mesInicio = "0";
                            arrCotServicioProductoBeen.add(product);
                        }
                    }

                    quotationPlainService.quotationPlainServiceProducts = arrCotServicioProductoBeen;
                    proposal.quotationPlainService.add(quotationPlainService);
                }
        }
        mAdditionalServicesPresenter.savePackage(addonServices, arrAddonProd, proposal, listPrice, priceSoonPaymen);
    }


    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mAddonSelectorPresenter = new AddonSelectorPresenter(this, this),
                mAdditionalServicesPresenter = new AdditionalServicesPresenter(this, this)
        };
    }

    private void setPager(){
        HorizontalLayout = new LinearLayoutManager(AditionalsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mItemSelectorRecyclerView.setLayoutManager(HorizontalLayout);

        //Pager
        PagerContainer container = (PagerContainer) findViewById(R.id.pager_container_aditionals);
        assert container != null;

        pager = container.getViewPager();
        pager.setClipChildren(false);
        pager.setOffscreenPageLimit(15);

        new CoverFlow.Builder()
                .with(pager)
                .scale(0f)
                .pagerMargin(getResources().getDimensionPixelSize(R.dimen.pager_margin_card))
                .spaceSize(3f)
                .build();

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                //mSelectPackageAdapter.setPageActivated(position);
                //mSelectPackageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        mAddonSelectorPresenter.loadAddons(getIntent().getStringExtra("IdPlan"));
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error de conexión");
    }

    @Override
    public void onSuccessSavedProposal() {
        //startActivity(QuotationPackageActivity.launch(getApplicationContext(),priceList,priceEarly,total,totalEarly));
    }

    @Override
    public void onLoadAddons() {
        MessageUtils.progress(this, "Cargando adicionales");
    }

    @Override
    public void onErrorAddons() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this, "Error al cargar los adicionales");
        finish();
    }

    @Override
    public void onSuccessAddons(PlanDetailResponse planDetailResponse) {
        mPlanDetailResponse = planDetailResponse;
        MessageUtils.stopProgress();
        ArrayList<PlanDetailResponse.AdicionalesBean> listaAux = new ArrayList<>();
        if (planDetailResponse.ArrProductosAdicionales != null && !planDetailResponse.ArrProductosAdicionales.isEmpty()) {
            for (ArrProductosAdicionalesBean producto : planDetailResponse.ArrProductosAdicionales) {
                if (producto.URLApp__c != null) {
                    listaAux.add(producto);
                }
            }

            if (planDetailResponse.ArrServiciosAdicionales != null && !planDetailResponse.ArrServiciosAdicionales.isEmpty()) {
                for (ArrServiciosAdicionalesBean servicio : planDetailResponse.ArrServiciosAdicionales) {
                    if (servicio.ImgIconoApp != null) {
                        listaAux.add(servicio);
                    }
                }
            }
        }
        //if (planDetailResponse.ArrCostoInstalacion != null)
        //    installationCostAmount = (Double.valueOf(planDetailResponse.ArrCostoInstalacion.get(0).PrecioBase) * 1.16);
        //else{
        //    installationCostAmount = 0.0;
        //    mCostoInstalacion.setVisibility(View.GONE);
        //}
        installationCostAmount = 0;
        if (planDetailResponse.ArrCostoInstalacion != null && planDetailResponse.ArrCostoInstalacion.size() > 0) {
            for (ArrCostoInstalacionBean installationCost : planDetailResponse.ArrCostoInstalacion) {
                installationCostAmount += (Double.valueOf(installationCost.PrecioBase) * (((Double.parseDouble(installationCost.IVA)) * (0.01)) + 1));
                if (Boolean.parseBoolean(installationCost.EsVisible)) {
                    group = installationCost.AgrupacionAddon + ": ";
                    if (group.length() == 0 || group.isEmpty() || group.equals(""))
                        group = "Cargo de activación y fianza: ";
                    Prefs.instance().putString(SocioDigitalKeys.CONCEP_INSTALLATION, group);
                }
            }
        }

        if (installationCostAmount > 0) {
           // mCostoInstalacion.setText(String.format("%s$%s.00", group, String.valueOf(Math.round(installationCostAmount))));
            Prefs.instance().putString(SocioDigitalKeys.INSTALATION_COST, String.valueOf(installationCostAmount));
        } else {
            //mCostoInstalacion.setVisibility(View.GONE);
        }

        //mCostoInstalacion.setText(String.valueOf(installationCostAmount));

        mSelectAdditionalAdapter = new SelectAdditionalAdapter(listaAux, getApplicationContext());
        adapterService = new AdditionalsServicesAdapter(this, listaAux, view -> {
            adapterService.notifyDataSetChanged();
            paintDetails();
        },this);
        pager.setAdapter(adapterService);
        mItemSelectorRecyclerView.setAdapter(mSelectAdditionalAdapter);
        paintDetails();
    }


    private void paintDetails() {
        StringBuilder additonals = new StringBuilder();
        ArrServiciosAdicionalesBean arrServiciosAdicionalesBean;
        ArrProductosAdicionalesBean arrProductosAdicionalesBean;
        ArrayList<ArrProductosAdicionalesBean> arrAddonProd = new ArrayList<>();
        ArrayList<ArrServiciosAdicionalesBean> addonServices = new ArrayList<>();

        for (PlanDetailResponse.AdicionalesBean adicionalesBean : adapterService.itemsSelected) {
            if (adicionalesBean.getAdicionalType() == PlanDetailResponse.AdicionalesBean.SERVICE) {
                arrServiciosAdicionalesBean = (ArrServiciosAdicionalesBean) adicionalesBean;
                additonals.append(arrServiciosAdicionalesBean.Nombre).append(" + ");
                addonServices.add(arrServiciosAdicionalesBean);

            } else {
                arrProductosAdicionalesBean = (ArrProductosAdicionalesBean) adicionalesBean;
                additonals.append(arrProductosAdicionalesBean.NameProducto).append(" + ");
                arrAddonProd.add(arrProductosAdicionalesBean);
            }
        }

        //CSER3324034

        CotServicioProducto cot_ServicioProducto = null;
        CotPlanServicio cot_PlanServicio = new CotPlanServicio();
        cot_PlanServicio.name = getIntent().getStringExtra("NamePlan");
        Cot_SitioPlan cot_SitioPlan = new Cot_SitioPlan();

        if (mPlanDetailResponse.ArrServiciosIncluidos != null && mPlanDetailResponse.ArrServiciosIncluidos.size() > 0) {
            for (int i = 0; i < mPlanDetailResponse.ArrServiciosIncluidos.size(); i++) {
                //PRODUCTOS INCLUIDOS
                if (!mPlanDetailResponse.ArrServiciosIncluidos.isEmpty() && !mPlanDetailResponse.ArrServiciosIncluidos.get(i).ArrProductosIncluidos.isEmpty())
                    for (int j = 0; j < mPlanDetailResponse.ArrServiciosIncluidos.get(i).ArrProductosIncluidos.size(); j++) {
                        ArrProductosIncluidosBean productoIncluido = mPlanDetailResponse.ArrServiciosIncluidos.get(i).ArrProductosIncluidos.get(j);
                        if (productoIncluido.Id != null && !productoIncluido.Id.isEmpty()) {
                            //LIB JC CPR19154592
                            cot_ServicioProducto = new CotServicioProducto();
                            cot_ServicioProducto.setPrecioUnitario__c(productoIncluido.PrecioBase != null ? Double.parseDouble(productoIncluido.PrecioBase.trim()) : 0.0d);
                            cot_ServicioProducto.setPrecioUnitarioBase__c(productoIncluido.PrecioBase != null ? Double.parseDouble(productoIncluido.PrecioBase.trim()) : 0.0d);
                            cot_ServicioProducto.setPrecioUnitario_ProntoPago__c(productoIncluido.PrecioProntoPago != null ? Double.parseDouble(productoIncluido.PrecioProntoPago.trim()) : 0.0d);
                            cot_ServicioProducto.setCantidad__c((int) Double.parseDouble(productoIncluido.Cantidad));
                            cot_ServicioProducto.setImpuesto1__c(productoIncluido.IVA != null ? Double.parseDouble(productoIncluido.IVA.trim()) : 0.0d);
                            cot_ServicioProducto.setImpuesto2__c(productoIncluido.IEPS != null ? Double.parseDouble(productoIncluido.IEPS.trim()) : 0.0d);
                            cot_ServicioProducto.setTipoProducto(Constans.PRODUCTO_INCLUIDO);
                            cot_PlanServicio.getList_cot_ServicioProducto().add(cot_ServicioProducto);
                        }
                    }
            }

            //PRODUCTOS ADICIONALES
            if (!arrAddonProd.isEmpty())
                for (int j = 0; j < arrAddonProd.size(); j++) {
                    ArrProductosAdicionalesBean productoAdicional = arrAddonProd.get(j);
                    if (productoAdicional.Id != null && !productoAdicional.Id.isEmpty() && !productoAdicional.Cantidad.isEmpty()) {
                        if (productoAdicional.Nombre.length() > cot_PlanServicio.name.length()) {
                            String name = productoAdicional.Nombre.substring(0, cot_PlanServicio.name.length());
                            if (cot_PlanServicio.name.equals(name)) {
                                //LIB JC CPR19154592
                                cot_ServicioProducto = new CotServicioProducto();
                                cot_ServicioProducto.setPrecioUnitario__c(productoAdicional.PrecioBase != null ? Double.parseDouble(productoAdicional.PrecioBase.trim()) : 0.0d);
                                cot_ServicioProducto.setPrecioUnitarioBase__c(productoAdicional.PrecioBase != null ? Double.parseDouble(productoAdicional.PrecioBase.trim()) : 0.0d);
                                cot_ServicioProducto.setPrecioUnitario_ProntoPago__c(productoAdicional.PrecioProntoPago != null ? Double.parseDouble(productoAdicional.PrecioProntoPago.trim()) : 0.0d);
                                cot_ServicioProducto.setCantidad__c((int) Double.parseDouble(productoAdicional.Cantidad));
                                cot_ServicioProducto.setImpuesto1__c(productoAdicional.IVA != null ? Double.parseDouble(productoAdicional.IVA.trim()) : 0.0d);
                                cot_ServicioProducto.setImpuesto2__c(productoAdicional.IEPS != null ? Double.parseDouble(productoAdicional.IEPS.trim()) : 0.0d);
                                cot_ServicioProducto.setTipoProducto(Constans.PRODUCTO_ADICIONAL);
                                if (!cot_PlanServicio.getList_cot_ServicioProducto().contains(cot_ServicioProducto)) {
                                    cot_PlanServicio.getList_cot_ServicioProducto().add(cot_ServicioProducto);
                                }
                            }
                        }

                    }
                }

            //////////////////SERVICIOS ADICIONLES//////////////////////////////////////////////
            if (!addonServices.isEmpty())
                for (int i = 0; i < addonServices.size(); i++) {
                    if (addonServices.get(i).ArrProductosIncluidos != null) {
                        for (int j = 0; j < addonServices.get(i).ArrProductosIncluidos.size(); j++) {
                            ArrProductosIncluidosBean productoIncluido = addonServices.get(i).ArrProductosIncluidos.get(j);
                            assert productoIncluido != null;
                            if (productoIncluido.Id != null && !productoIncluido.Id.isEmpty()) {
                                //LIB JC CPR19154592
                                cot_ServicioProducto = new CotServicioProducto();
                                cot_ServicioProducto.setPrecioUnitario__c(productoIncluido.PrecioBase != null ? Double.parseDouble(productoIncluido.PrecioBase.trim()) : 0.0d);
                                cot_ServicioProducto.setPrecioUnitarioBase__c(productoIncluido.PrecioBase != null ? Double.parseDouble(productoIncluido.PrecioBase.trim()) : 0.0d);
                                cot_ServicioProducto.setPrecioUnitario_ProntoPago__c(productoIncluido.PrecioProntoPago != null ? Double.parseDouble(productoIncluido.PrecioProntoPago.trim()) : 0.0d);
                                cot_ServicioProducto.setCantidad__c((int) Double.parseDouble(productoIncluido.Cantidad));
                                cot_ServicioProducto.setImpuesto1__c(productoIncluido.IVA != null ? Double.parseDouble(productoIncluido.IVA.trim()) : 0.0d);
                                cot_ServicioProducto.setImpuesto2__c(productoIncluido.IEPS != null ? Double.parseDouble(productoIncluido.IEPS.trim()) : 0.0d);
                                cot_ServicioProducto.setTipoProducto(Constans.PRODUCTO_ADICIONAL);
                                cot_PlanServicio.getList_cot_ServicioProducto().add(cot_ServicioProducto);
                            }
                        }
                    }
                }
        }
        cot_SitioPlan.getListCot_PlanServicio().add(cot_PlanServicio);

        if (planSoonPrice == 0.0d) {
            planSoonPrice = cot_SitioPlan.precio_Pronto_Pago__c();
            listPrice = planPrice;
        } else {
            listPrice = planPrice + (cot_SitioPlan.precio_Pronto_Pago__c() - planSoonPrice);
        }
        /*Gson gson = new Gson();
        String json = gson.toJson(cot_PlanServicio);

        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Error", json);
        clipboard.setPrimaryClip(clip);
*/
        //amountPListTextView.setText(String.format("%s.00", String.format("Precio de lista: $ %s", Math.round(listPrice))));
        //amountPPTextView.setText(String.format("%s.00", String.format("Precio de pronto pago: $ %s", Math.round(cot_SitioPlan.precio_Pronto_Pago__c()))));
        priceSoonPaymen = cot_SitioPlan.precio_Pronto_Pago__c();
        //nameAddonTextView.setText(additonals.toString());
        if (flagOnce) {
            priceList = listPrice;
            priceEarly = priceSoonPaymen;
            flagOnce =  false;
        }
        total = listPrice;
        totalEarly = priceSoonPaymen;
    }

    @Override
    public void onChangeButton(boolean status) {

    }

}
