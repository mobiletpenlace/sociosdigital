package com.totalplay.sociodigital.view.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.joooonho.SelectableRoundedImageView;
import com.resources.utils.ImageUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.theartofdev.edmodo.cropper.CropImage;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.request.LoginRequest;
import com.totalplay.sociodigital.background.ws.request.document.DocumentBean;
import com.totalplay.sociodigital.background.ws.request.document.DocumentFilesRequest;
import com.totalplay.sociodigital.background.ws.request.document.DocumentListBean;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.model.entities.Additional;
import com.totalplay.sociodigital.model.entities.Promotion;
import com.totalplay.sociodigital.presenter.callbacks.DocumentRegistrationCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.DocumentRegistrationPresenter;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */
public class LoadDocumentsActivity extends BaseActivity implements DocumentRegistrationCallback {

    public static Intent launch(Context context,PlanTotalPlay planSelected, ArrayList<Additional> additonals, ArrayList<Promotion> mPromotions, String priceList){
        Intent intent =  new Intent(context,LoadDocumentsActivity.class);
        intent.putExtra("planSelected", (Serializable) planSelected);
        intent.putExtra("additionals", (Serializable) additonals);
        intent.putExtra("promotions", (Serializable) mPromotions);
        intent.putExtra("priceList", priceList);
        return intent;
    }
    private final int CAMERA_FRONT_REQUEST = 100;
    private final int CAMERA_FRONT_REQUEST_CROP = 102;

    private final int CAMERA_BACK_REQUEST = 150;
    private final int CAMERA_BACK_REQUEST_CROP = 152;

    private final int SIGNATURE_DIALOG_REQUEST = 1;

    private final int CAMERA_PROOF_REQUEST = 250;
    private final int CAMERA_PROOF_REQUEST_CROP = 252;

    private DocumentBean idCard1;
    private DocumentBean idCard2;
    private DocumentBean idProofs;
    private DocumentBean idSignature;

    private int camaraRequest = 0;
    private int CAMARA = 10;
    private int CROP = 20;

    private static final int THUMB_SIZE = 200;
    private TextView mTitleTextView;
    private TextView mSubtitleTextView;
    private ViewGroup mIneFrontContent;
    private SelectableRoundedImageView mIneFrontImageView;
    private ViewGroup mIneBackContent;
    private SelectableRoundedImageView mIneBackImageView;
    private TextView mIneFrontTextView;
    private TextView mIneBackTextView;
    private CheckBox mCheckBox;
    private TextView mDigitalSignature;
    private Button mCancelButton;
    private Button mContinueButton;
    private ViewGroup mContentSignature;
    private ImageView mSignatureImgView;

    private File destination;
    private Bitmap picture;
    private boolean img1 = false;
    private boolean img2 = false;
    private boolean img3 = false;
    private boolean img4 = false;
    DocumentRegistrationPresenter mDocumentRegistrationPresenter;
    private DocumentFilesRequest mFileDocumentRequest = new DocumentFilesRequest();
    private ViewGroup mBackToolbar;

    private ViewGroup mAddressContent;
    private SelectableRoundedImageView mAddressImageView;
    private TextView mAddressTextView;
    private PlanTotalPlay mPlanTotalPlay;
    private ArrayList<Additional> mAdditonals;
    private ArrayList<Promotion> mPromotions;

    private ImageView mTerms1ImageView;
    private ImageView mTerms2ImageView;
    private ImageView mTerms3ImageView;
    private TextView mTerms1TextView;
    private TextView mTerms2TextView;
    private TextView mTerms3TextView;
    private ViewGroup mTerms1Content;
    private ViewGroup mTerms2Content;
    private ViewGroup mTerms3Content;
    private ViewGroup mShowTerms1;
    private ViewGroup mShowTerms2;
    private ViewGroup mShowTerms3;

    private boolean mTerm1Status;
    private boolean mTerm2Status;
    private boolean mTerm3Status;
    private String priceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.load_documents_activity);
        mPlanTotalPlay = (PlanTotalPlay) getIntent().getSerializableExtra("planSelected");
        mAdditonals = (ArrayList<Additional>) getIntent().getSerializableExtra("additionals");
        mPromotions = (ArrayList<Promotion>) getIntent().getSerializableExtra("promotions");
        priceList = getIntent().getStringExtra("priceList");
        bindResources();
        setFonts();
        generateDocuments();
        setOnClicks();
        tedPermission();
        mFileDocumentRequest.documentAdd = new DocumentListBean();
        mFileDocumentRequest.documentAdd.document = new RealmList<>();
        mTerm1Status =  false;
        mTerm2Status  = false;
        mTerm3Status =  false;
    }

    @Override
    protected BasePresenter getPresenter() {
        return mDocumentRegistrationPresenter = new DocumentRegistrationPresenter(this, this);
    }

    private void bindResources(){
        mTitleTextView =  findViewById(R.id.act_load_documents_title);
        mSubtitleTextView =  findViewById(R.id.act_load_documents_subtitle);
        mIneFrontContent =  findViewById(R.id.act_load_documents_ine_front_content);
        mIneFrontImageView =  findViewById(R.id.act_load_documents_ine_front_image);
        mIneFrontTextView = findViewById(R.id.act_load_documents_ine_front_text);
        mIneBackContent =  findViewById(R.id.act_load_documents_ine_back_content);
        mIneBackTextView =  findViewById(R.id.act_load_documents_ine_back_text);
        mIneBackImageView =  findViewById(R.id.act_load_documents_ine_back_image);
        mCheckBox = findViewById(R.id.act_load_documents_checkbox);
        mDigitalSignature =  findViewById(R.id.act_load_documents_digital_signature_text);
        mSignatureImgView =  findViewById(R.id.act_load_documents_signature_img);
        mCancelButton =  findViewById(R.id.act_load_documents_cancel_button);
        mContinueButton =  findViewById(R.id.act_load_documents_continue_button);
        mContentSignature =  findViewById(R.id.act_quotation_package_signature_launch);
        mBackToolbar = findViewById(R.id.mBackButton);
        mAddressContent =  findViewById(R.id.act_load_documents_address_content);
        mAddressTextView =  findViewById(R.id.act_load_documents_address_text);
        mAddressImageView = findViewById(R.id.act_load_documents_address_image);
        mTerms1TextView = findViewById(R.id.minimum_rights_chart);
        mTerms2TextView =  findViewById(R.id.terms_and_onditions_text);
        mTerms3TextView =  findViewById(R.id.notice_privacy_text);
        mTerms1Content =  findViewById(R.id.container_minimum_rights_chart);
        mTerms2Content = findViewById(R.id.container_terms_and_conditions);
        mTerms3Content = findViewById(R.id.container_notice_privacy);
        mShowTerms1 =  findViewById(R.id.act_load_documents_show_terms1);
        mShowTerms2 =  findViewById(R.id.act_load_documents_show_terms2);
        mShowTerms3 = findViewById(R.id.act_load_documents_show_terms3);
        mTerms1ImageView =  findViewById(R.id.minimum_rights_chart_checkbox);
        mTerms2ImageView =  findViewById(R.id.terms_and_conditions_checkbox);
        mTerms3ImageView = findViewById(R.id.notice_privacy_checkbox);
    }

    private void setFonts(){
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        mTitleTextView.setTypeface(monserratRegular);
        mSubtitleTextView.setTypeface(monserratBold);
        mIneFrontTextView.setTypeface(monserratRegular);
        mIneBackTextView.setTypeface(monserratRegular);
        mCheckBox.setTypeface(monserratBold);
        mDigitalSignature.setTypeface(monserratBold);
        mCancelButton.setTypeface(monserratMedium);
        mContinueButton.setTypeface(monserratMedium);
        mAddressTextView.setTypeface(monserratRegular);
        mTerms1TextView.setTypeface(monserratBold);
        mTerms2TextView.setTypeface(monserratBold);
        mTerms3TextView.setTypeface(monserratBold);
        mCheckBox.setChecked(false);

    }

    private void setOnClicks(){
        mContinueButton.setOnClickListener(v -> validatePutData());

        mCancelButton.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), CancelSaleDialog.class));
        });

        mIneFrontContent.setOnClickListener(v -> {

            camaraRequest = CAMARA;
            destination = new File(Environment.getExternalStorageDirectory(), "documentation.jpg");
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
            startActivityForResult( cameraIntent , CAMERA_FRONT_REQUEST);
        });

        mIneBackContent.setOnClickListener(v -> {
            camaraRequest = CAMARA;
            destination = new File(Environment.getExternalStorageDirectory(), "documentation.jpg");
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
            startActivityForResult( cameraIntent , CAMERA_BACK_REQUEST);
        });

        mAddressContent.setOnClickListener(v -> {
            camaraRequest = CAMARA;
            destination = new File(Environment.getExternalStorageDirectory(), "documentation.jpg");
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
            startActivityForResult( cameraIntent , CAMERA_PROOF_REQUEST);
        });

        mContentSignature.setOnClickListener(v -> {
            camaraRequest = CROP;
            startActivityForResult(new Intent(getApplicationContext(), SignatureElectronicDialog.class),SIGNATURE_DIALOG_REQUEST);
        });

        mBackToolbar.setOnClickListener(v -> finish());

        mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                mAddressContent.setVisibility(View.GONE);
            }else{
                mAddressContent.setVisibility(View.VISIBLE);
            }
        });

        mTerms1Content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mTerm1Status){
                    mTerms1ImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
                    mTerm1Status = true;
                }else {
                    mTerms1ImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
                    mTerm1Status = false;
                }
            }
        });

        mTerms2Content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mTerm2Status){
                    mTerms2ImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
                    mTerm2Status = true;
                }else {
                    mTerms2ImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
                    mTerm2Status = false;
                }
            }
        });

        mTerms3Content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mTerm3Status){
                    mTerms3ImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
                    mTerm3Status = true;
                }else {
                    mTerms3ImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
                    mTerm3Status = false;
                }
            }
        });

        mShowTerms1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri tc = Uri.parse("https://appstotalplay.com/media/files/Contrato2018_nuevo.pdf");
                Intent intent = new Intent(Intent.ACTION_VIEW, tc);
                startActivity(intent);
            }
        });

        mShowTerms2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri np = Uri.parse("https://appstotalplay.com/media/files/AVISO_SIMPLIFICADO_PARA_FIRMA_DE_CONTRATO_20032018.pdf");
                Intent intenta = new Intent(Intent.ACTION_VIEW, np);
                startActivity(intenta);
            }
        });

        mShowTerms3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri mrc = Uri.parse("https://appstotalplay.com/media/files/carta-derechos_MmaiaTh.pdf");
                Intent intenta = new Intent(Intent.ACTION_VIEW, mrc);
                startActivity(intenta);
            }
        });
    }

    /*
    if (i == R.id.terms_and_onditions_text) {
            Uri tc = Uri.parse("https://appstotalplay.com/media/files/Contrato2018_nuevo.pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW, tc);
            startActivity(intent);
        } else if (i == R.id.notice_privacy_text) {
            Uri np = Uri.parse("https://appstotalplay.com/media/files/AVISO_SIMPLIFICADO_PARA_FIRMA_DE_CONTRATO_20032018.pdf");
            Intent intenta = new Intent(Intent.ACTION_VIEW, np);
            startActivity(intenta);
        }else if (i == R.id.minimum_rights_chart) {
            Uri mrc = Uri.parse("https://appstotalplay.com/media/files/carta-derechos_MmaiaTh.pdf");
            Intent intenta = new Intent(Intent.ACTION_VIEW, mrc);
            startActivity(intenta);
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            picture = null;
            Bitmap thumbnail = null;
            if(resultCode == RESULT_OK){
                if(camaraRequest == CAMARA){
                    camaraRequest = CROP;
                    Intent cameraIntent = CropImage.activity(Uri.fromFile(destination))
                            .getIntent(getApplicationContext());
                    if(requestCode == CAMERA_FRONT_REQUEST){
                        startActivityForResult( cameraIntent , CAMERA_FRONT_REQUEST_CROP);
                    }else if(requestCode == CAMERA_BACK_REQUEST){
                        startActivityForResult( cameraIntent , CAMERA_BACK_REQUEST_CROP);
                    }else if(requestCode == CAMERA_PROOF_REQUEST){
                        startActivityForResult( cameraIntent , CAMERA_PROOF_REQUEST_CROP);
                    }
                }else if(camaraRequest == CROP){
                    if (requestCode == SIGNATURE_DIALOG_REQUEST){
                        byte[] bitmapdata = data.getByteArrayExtra("signature");
                        picture = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata.length);
                        thumbnail = ThumbnailUtils.extractThumbnail(picture, THUMB_SIZE, THUMB_SIZE);
                        mSignatureImgView.setImageBitmap(picture);
                        img4 = true;
                    }
                    else{
                        double ratio =  1.0;
                        Bitmap cutImage = null;
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);

                        CropImage.ActivityResult result = CropImage.getActivityResult(data);
                        InputStream input = this.getContentResolver().openInputStream(result.getUri());
                        cutImage = BitmapFactory.decodeStream(input, null, bitmapOptions);
                        thumbnail = ThumbnailUtils.extractThumbnail(cutImage, THUMB_SIZE, THUMB_SIZE);
                        BitmapDrawable myBackground = new BitmapDrawable(thumbnail);
                        picture = cutImage;

                        if(requestCode == CAMERA_FRONT_REQUEST_CROP){
                            img1 = setElementsView(mIneFrontImageView ,mIneFrontContent , myBackground );
                        }else if(requestCode == CAMERA_BACK_REQUEST_CROP){
                            img2 = setElementsView(mIneBackImageView ,mIneBackContent , myBackground );
                        }else if(requestCode == CAMERA_PROOF_REQUEST_CROP){
                            img3 = setElementsView(mAddressImageView ,mAddressContent , myBackground );
                        }
                    }
                    processImages(requestCode, picture, thumbnail);
                }
            }
        } catch (Exception | OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    private boolean setElementsView(SelectableRoundedImageView view , ViewGroup content , BitmapDrawable background ){
        view.setImageDrawable(background);
        view.setPadding(0,0,0,0);
        view = setRounded(view,content);
        return true;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio){
        int k = Integer.highestOneBit((int)Math.floor(ratio));
        if(k==0) return 1;
        else return k;
    }

    private DocumentBean setIdData( DocumentBean dben ,String image , String imageThumb ){
        dben.body = image;
        dben.thumbnail = imageThumb;
        return dben;
    }

    private void processImages(int requestCode, Bitmap bitmap, Bitmap thumbnail) {
        if (bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
            int newHeight = (int) (bitmap.getHeight() * (bitmap.getWidth() > 4000 ? 0.12 : 0.5));
            int newWidth = (int) (bitmap.getWidth() * (bitmap.getWidth() > 4000 ? 0.12 : 0.5));
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
            String image = ImageUtils.compressBitmapToBase64(scaledBitmap, 85);
            String imageThumb = ImageUtils.compressBitmapToBase64(thumbnail, 65);
            switch (requestCode) {
                case CAMERA_FRONT_REQUEST_CROP:
                    idCard1 = setIdData(idCard1 , image , imageThumb);
                    break;
                case CAMERA_BACK_REQUEST_CROP:
                    idCard2 = setIdData(idCard2 , image , imageThumb);
                    break;
                case CAMERA_PROOF_REQUEST_CROP:
                    idProofs = setIdData(idProofs , image , imageThumb);
                    break;
                case SIGNATURE_DIALOG_REQUEST:
                    idSignature = setIdData(idSignature , image , imageThumb);
                    break;
            }
        }
    }

    private void putDocuments() {
        mFileDocumentRequest.documentAdd.document.add(idCard1);
        mFileDocumentRequest.documentAdd.document.add(idCard2);
        mFileDocumentRequest.documentAdd.document.add(idSignature);
        if (mCheckBox.isChecked()){
            idProofs.body =idCard1.body;
            idProofs.thumbnail = idCard1.thumbnail;
            mFileDocumentRequest.documentAdd.document.add(idProofs);
        }else{
            if (idProofs.body != null)
                mFileDocumentRequest.documentAdd.document.add(idProofs);
        }

    }

    private void generateDocuments() {
        idCard1 = new DocumentBean();
        idCard1.idOpportunity = "";
        idCard1.brmAccount = "";
        idCard1.name = "Identificacion1.jpeg";
        idCard1.type = "Identificación";

        idCard2 = new DocumentBean();
        idCard2.idOpportunity = "";
        idCard2.brmAccount = "";
        idCard2.name = "Identificacion2.jpeg";
        idCard2.type = "Identificación";

        idProofs = new DocumentBean();
        idProofs.idOpportunity = "";
        idProofs.brmAccount = "";
        idProofs.name = "Comprobante.jpeg";
        idProofs.type = "Comprobante de domicilio";

        idSignature = new DocumentBean();
        idSignature.idOpportunity = "";
        idSignature.brmAccount = "";
        idSignature.name = "Firma.jpeg";
        idSignature.type = "Firma Digital";
    }

    public void tedPermission(){
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }
            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        };

        new TedPermission(LoadDocumentsActivity.this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Favor de aceptar todos los permisos para continuar")
                .setPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    public void onSavedDocumentSucces() {
        startActivity(PayMethodActivity.launch(getApplicationContext(),mPlanTotalPlay, mAdditonals,mPromotions, priceList));
        finish();
    }

    private void validatePutData(){
        if (mTerm1Status && mTerm2Status && mTerm3Status){
            if (mCheckBox.isChecked()){
                img3 = true;
            }
            if (img1 && img2 && img3 && img4){
                putDocuments();
                saveImage();
            } else
                MessageUtils.toast(this,"Faltan evidencias por capturar");
        }else{
            MessageUtils.toast(this,"Debe aceptar todos los términos y condiciones");
        }

    }

    public void saveImage() {
        if (mFileDocumentRequest != null
                && mFileDocumentRequest.documentAdd != null
                && !mFileDocumentRequest.documentAdd.document.isEmpty()
                && mFileDocumentRequest.documentAdd.document.size() > 1) {
            mFileDocumentRequest.login = new LoginRequest("25631", "Middle100$", "102.010.01.01");
            mDocumentRegistrationPresenter.saveDocuments(mFileDocumentRequest);
        } else {
            MessageUtils.toast(this, "Faltan documentos por capturar");
        }
    }

    public SelectableRoundedImageView setRounded(SelectableRoundedImageView img, ViewGroup content){
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        img.setCornerRadiiDP(15, 15, 15, 15);
        return img;
    }

}

