package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.ProductMasterPlanesResponse;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;
import com.totalplay.sociodigital.view.adapters.PackagesFamilyRecyclerAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by imacbookpro on 30/08/18.
 * SocioDigital
 */

public class PlansActivity extends BaseActivity {

    public static Intent launch(Context context, ArrayList<PlanTotalPlay> plans){
        Intent intent =  new Intent(context, PlansActivity.class);
        //intent.putExtra("packages", (Serializable) arrFamily);
        intent.putExtra("plans", plans);
        return intent;
    }


    private TextView mTitleTextView;
    private TextView mSubtitleTextView;
    private Button mCancelButton;
    private RecyclerView mRecyclerView;
    private PackagesFamilyRecyclerAdapter mPackagesFamilyRecyclerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ViewGroup mBackToolbar;
    private ArrayList<PlanTotalPlay> plans;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.plans_activity);
        plans = (ArrayList<PlanTotalPlay>) getIntent().getSerializableExtra("plans");
        bindResources();
        setFonts();
        setOnClick();
        setRecycler();
    }

    private void bindResources(){
        mTitleTextView =  findViewById(R.id.act_plans_title);
        mSubtitleTextView = findViewById(R.id.act_plans_subtitle);
        mRecyclerView =  findViewById(R.id.act_plans_recycler);
        mCancelButton =  findViewById(R.id.act_plans_cancel_button);
        mBackToolbar =  findViewById(R.id.mBackButton);
    }

    private void setFonts(){
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");

        mTitleTextView.setTypeface(monserratRegular);
        mSubtitleTextView.setTypeface(monserratRegular);
        mCancelButton.setTypeface(monserratMedium);
    }

    private void setOnClick(){
        mCancelButton.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), CancelSaleDialog.class));
        });

        mBackToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setRecycler(){
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPackagesFamilyRecyclerAdapter = new PackagesFamilyRecyclerAdapter(this,plans);
        mRecyclerView.setAdapter(mPackagesFamilyRecyclerAdapter);

    }

}
