package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.DataSaleMonthRecomend;
import com.totalplay.sociodigital.library.pojos.Data;
import com.totalplay.sociodigital.library.pojos.InfoDetail;
import com.totalplay.sociodigital.model.entities.DataSaleMonth;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;
import com.totalplay.sociodigital.view.adapters.SearchNameAdapter;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MySalesActivity extends BaseActivity {
    //changes2
    public static Intent launch(Context context, Data mRecomendData, String nameMonth, String type, String numberDay){
        Intent intent = new Intent(context, MySalesActivity.class);
        intent.putExtra("dataInfo", mRecomendData);
        intent.putExtra("nameMonth", nameMonth);
        intent.putExtra("type", type);
        intent.putExtra("numberDay", numberDay);
        return intent;
    }

    @BindView(R.id.act_sales_month)
    public TextView monthTextView;
    @BindView(R.id.act_sales_to_contact)
    public TextView toContactTextView;
    @BindView(R.id.act_sales_to_install)
    public TextView toInstallTextView;
    @BindView(R.id.act_sales_not_successful)
    public TextView notSuccessfulTextView;
    @BindView(R.id.act_sales_not_locatable)
    public TextView notLocatableTextView;
    @BindView(R.id.act_sale_install)
    public TextView installTextView;
    @BindView(R.id.act_sales_start_sale)
    public Button startSaleButton;
    @BindView(R.id.act_my_sales_date)
    public TextView dateTextView;
    @BindView(R.id.mTitleTextView)
    public TextView titleScreen;
    @BindView(R.id.act_my_sale_subtitle)
    public TextView subtitle;
    @BindView(R.id.dialog_detail_info_title)
    public TextView titleDetailDialog;
    @BindView(R.id.dialog_detail_info_recycler)
    public RecyclerView infoDetailRecycler;
    @BindView(R.id.dialog_detail_info_search)
    public EditText infoSearch;
    @BindView(R.id.dialog_info_detail_content)
    public ViewGroup dialogInfoDetailContent;
    @BindView(R.id.act_my_sales_detail1)
    public TextView detail1;
    @BindView(R.id.act_my_sales_detail2)
    public TextView detail2;
    @BindView(R.id.act_my_sales_detail3)
    public TextView detail3;
    @BindView(R.id.act_my_sales_detail4)
    public TextView detail4;
    public Data mDataInfo;
    private String nameMonth;
    private String type;
    private LoginPresenter mLoginPresenter;
    private ArrayList<InfoDetail> arraySelected = new ArrayList<>();
    private ArrayList<InfoDetail> installedArray = new ArrayList<>();
    private ArrayList<InfoDetail> forContacArray = new ArrayList<>();
    private ArrayList<InfoDetail> forInstallArray = new ArrayList<>();
    private ArrayList<InfoDetail> failArray = new ArrayList<>();
    private SearchNameAdapter searchNameAdapter;
    private String numberDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.my_sales_activity);
        ButterKnife.bind(this);
        mLoginPresenter = new LoginPresenter(this);
        mDataInfo = (Data) getIntent().getSerializableExtra("dataInfo");
        nameMonth =  getIntent().getStringExtra("nameMonth");
        type = getIntent().getStringExtra("type");
        numberDay = getIntent().getStringExtra("numberDay");
        nameMonth = getAuxMonth(nameMonth);
        dateTextView.setText(numberDay);
        setDetailAdapter();
        setData();
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        dateTextView.setTypeface(monserratBold);
        setFilter();
        unlineTextView("Ver detalle", detail1);
        unlineTextView("Ver detalle", detail2);
        unlineTextView("Ver detalle", detail3);
        unlineTextView("Ver detalle", detail4);
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @OnClick(R.id.act_sales_start_sale)
    public void onClickStartSale(){
        if (type.equals("sale"))
            startActivity(new Intent(getApplicationContext(),ValidateCoverageMapActivity.class));
        else if (type.equals("recomend")){
            startActivity(new Intent(this,CapturedReferredActivity.class));
        }
    }

    @Override
    public void onBackPressed() {
        if (dialogInfoDetailContent.getVisibility() == View.VISIBLE)
            dialogInfoDetailContent.setVisibility(View.GONE);
        else {
            super.onBackPressed();
        }

    }

    private void setDetailAdapter(){
        searchNameAdapter =  new SearchNameAdapter(this,new ArrayList<InfoDetail>());
        infoDetailRecycler.setLayoutManager(new LinearLayoutManager(this));
        infoDetailRecycler.setAdapter(searchNameAdapter);
    }

    @OnClick(R.id.act_indicator_detail_for_contact)
    public void onClickForContact(){
        if (forContacArray.size() > 0){
            arraySelected =  forContacArray;
            titleDetailDialog.setText("Por contactar");
            searchNameAdapter.updateListItems(forContacArray);
            dialogInfoDetailContent.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.act_indicator_detail_for_install)
    public void onClickForInstall(){
        if (forInstallArray.size() > 0){
            arraySelected = forInstallArray;
            titleDetailDialog.setText("Por instalar");
            searchNameAdapter.updateListItems(forInstallArray);
            dialogInfoDetailContent.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.act_indicator_detail_failed)
    public void onClickFailed(){
        if (failArray.size() > 0){
            arraySelected =  failArray;
            titleDetailDialog.setText("No exitoso");
            searchNameAdapter.updateListItems(failArray);
            dialogInfoDetailContent.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.act_indicator_detail_installed)
    public void onClickInstalled(){
        if (installedArray.size() > 0){
            arraySelected = installedArray;
            titleDetailDialog.setText("Instalados");
            searchNameAdapter.updateListItems(installedArray);
            dialogInfoDetailContent.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.dialog_detail_content_card)
    public void onClickContentInfoCard() {
        //nothing
    }

    @OnClick(R.id.dialog_info_detail_content)
    public void onClickDetailContent2(){
        //nothin
    }

    @OnClick(R.id.dialog_detail_info_close)
    public void onClickClose(){
        dialogInfoDetailContent.setVisibility(View.GONE);
    }

    private void setFilter(){
        ArrayList<InfoDetail> itemsFilter =  new ArrayList<>();
        infoSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = infoSearch.getText().toString().trim();
                itemsFilter.clear();
                for (InfoDetail item: arraySelected){
                    if (item.name.contains(text) || item.phone.contains(text)|| item.name.contains(text.toUpperCase())) {
                        itemsFilter.add(item);
                    }
                }
                searchNameAdapter.updateListItems(itemsFilter);
            }
        });
    }

    private void setData(){
        InfoSocioBean  infoSocioBean = mLoginPresenter.retrieveInfoSocio();
        monthTextView.setText(getMonth(nameMonth));
        if (type.equals("sale")){
            if (infoSocioBean.mSubCanal.equals("Cliente TP"))
                startSaleButton.setVisibility(View.GONE);
        }else if (type.equals("recomend")){
            titleScreen.setText("Mis recomendados");
            subtitle.setText("Tus recomendados del mes");
            startSaleButton.setText("Capturar recomendado");
        }
        if (mDataInfo != null){
            toContactTextView.setText(String.valueOf(validateDetail(mDataInfo.forContactArray,forContacArray)));
            toInstallTextView.setText(String.valueOf(validateDetail(mDataInfo.forInstallArray,forInstallArray)));
            notSuccessfulTextView.setText(String.valueOf(validateDetail(mDataInfo.failArray,failArray)));
            //if ((mDataSaleMonth.notLocatable) != null)
            //    notLocatableTextView.setText(mDataSaleMonth.notLocatable);
            installTextView.setText(String.valueOf(validateDetail(mDataInfo.installedArray,installedArray)));
            if (!toContactTextView.getText().toString().equals("0"))
                forContacArray = mDataInfo.forContactArray;
            if (!toInstallTextView.getText().toString().equals("0"))
                forInstallArray = mDataInfo.forInstallArray;
            if (!notSuccessfulTextView.getText().toString().equals("0"))
                failArray =  mDataInfo.failArray;
            if (!installTextView.getText().toString().equals("0"))
                installedArray = mDataInfo.installedArray;
        }
    }

    private int validateDetail(ArrayList<InfoDetail> itemsSel, ArrayList<InfoDetail> saveInfoDetail){
        saveInfoDetail.clear();
        for (InfoDetail item: itemsSel){
            if (Integer.parseInt(item.sendMonth) == Integer.parseInt(nameMonth))
                saveInfoDetail.add(item);
        }
        return saveInfoDetail.size();
    }

    private String getMonth(String numberMonth){
        String month = "";
        switch (numberMonth){
            case "01":
                month =  "Enero";
                break;
            case "02":
                month =  "Febrero";
                break;
            case "03":
                month =  "Marzo";
                break;
            case "04":
                month =  "Abril";
                break;
            case "05":
                month =  "Mayo";
                break;
            case "06":
                month =  "Junio";
                break;
            case "07":
                month =  "Julio";
                break;
            case "08":
                month =  "Agosto";
                break;
            case "09":
                month =  "Septiembre";
                break;
            case "10":
                month =  "Octubre";
                break;
            case "11":
                month =  "Noviembre";
                break;
            case "12":
                month =  "Diciembre";
                break;
        }
        return month;
    }

    private String getAuxMonth(String dateIn) {
        String[] dateArr = dateIn.split("-");
        return  dateArr[1];
    }


    public static void unlineTextView(String textInput, TextView textView){
        SpannableString text = new SpannableString(textInput);
        text.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        textView.setText(text);
    }
}
