package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.widget.EditText;
import android.widget.TextView;

import com.resources.utils.CryptoUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.entities.DirectionBuildingBusinnessBean;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmAddressActivity extends BaseActivity {
    @BindView(R.id.mConfirmDirectionZipCodeEditText)
    public EditText mConfirmDirectionZipCodeEditText;

    @BindView(R.id.mConfirmDirectionColonyEditText)
    public EditText mConfirmDirectionColonyEditText;

    @BindView(R.id.mConfirmDirectionDelegationEditText)
    public EditText mConfirmDirectionDelegationEditText;

    @BindView(R.id.mConfirmDirectionCityEditText)
    public EditText mConfirmDirectionCityEditText;

    @BindView(R.id.mConfirmDirectionStateEditText)
    public EditText mConfirmDirectionStateEditText;

    @BindView(R.id.mConfirmDirectionStreetEditText)
    public EditText mConfirmDirectionStreetEditText;

    @BindView(R.id.mConfirmDirectionNumExteriorEditText)
    public EditText mConfirmDirectionNumExteriorEditText;

    @BindView(R.id.mConfirmDirectionNumInteriorEditText)
    public EditText mConfirmDirectionNumInteriorEditText;

    @BindView(R.id.mConfirmDirectionBetweenStreetEditText)
    public EditText mConfirmDirectionBetweenStreetEditText;

    @BindView(R.id.mConfirmDirectionReferencesEditText)
    public EditText mConfirmDirectionReferencesEditText;

    @BindView(R.id.act_confirm_address_title)
    public TextView mTitle;

    public FormValidator mFormValidator;
    private RegisterSellerPresenter mRegisterSellerPresenter;

    private String mLatitude = "";
    private String mLongitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.confirm_address_activity);
        ButterKnife.bind(this);
        addValidators();
        retrieveLocation();
        mConfirmDirectionColonyEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mConfirmDirectionDelegationEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mConfirmDirectionCityEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mConfirmDirectionStateEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mConfirmDirectionStreetEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mConfirmDirectionBetweenStreetEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mConfirmDirectionReferencesEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        if (mRegisterSellerPresenter.getInfoSeller().mSubCanal.equals(CryptoUtils.crypt("Socio Edificios"))){
            mTitle.setText("Confirma la dirección del edificio");
        }else if (mRegisterSellerPresenter.getInfoSeller().mSubCanal.equals(CryptoUtils.crypt("Socio Negocios"))){
            mTitle.setText("Confirma la dirección del negocio");
        }
    }

    public void retrieveLocation(){
        mLatitude = getIntent().getStringExtra("Latitude");
        mLongitude = getIntent().getStringExtra("Longitude");

        try {
            mConfirmDirectionZipCodeEditText.setText(getIntent().getStringExtra("zipCode"));
            mConfirmDirectionColonyEditText.setText(getIntent().getStringExtra("colony"));
            mConfirmDirectionDelegationEditText.setText(getIntent().getStringExtra("delegation"));
            mConfirmDirectionCityEditText.setText(getIntent().getStringExtra("city"));
            mConfirmDirectionStateEditText.setText(getIntent().getStringExtra("state"));
            mConfirmDirectionStreetEditText.setText(getIntent().getStringExtra("street"));
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mConfirmDirectionZipCodeEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionColonyEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionDelegationEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionCityEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionStateEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionStreetEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionNumExteriorEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                //new EditTextValidator(mConfirmDirectionNumInteriorEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionBetweenStreetEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mConfirmDirectionReferencesEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    public DirectionBuildingBusinnessBean createPojo(){
        DirectionBuildingBusinnessBean mDirectionBuildingBusinnessBean = new DirectionBuildingBusinnessBean(
                CryptoUtils.crypt(mLatitude),
                CryptoUtils.crypt(mLongitude),
                CryptoUtils.crypt(mConfirmDirectionZipCodeEditText.getText().toString()),
                mConfirmDirectionColonyEditText.getText().toString(),
                CryptoUtils.crypt(mConfirmDirectionDelegationEditText.getText().toString()),
                CryptoUtils.crypt(mConfirmDirectionCityEditText.getText().toString()),
                CryptoUtils.crypt(mConfirmDirectionStateEditText.getText().toString()),
                mConfirmDirectionStreetEditText.getText().toString(),
                CryptoUtils.crypt(mConfirmDirectionNumInteriorEditText.getText().toString()),
                CryptoUtils.crypt(mConfirmDirectionNumExteriorEditText.getText().toString()),
                CryptoUtils.crypt(mConfirmDirectionBetweenStreetEditText.getText().toString()),
                CryptoUtils.crypt(mConfirmDirectionReferencesEditText.getText().toString())
        );
        return mDirectionBuildingBusinnessBean;
    }

    @OnClick(R.id.mContinueConfirmAddressButton)
    public void OnClickConfirmAddress(){
        if (mFormValidator.isValid()) {
            mRegisterSellerPresenter.saveDirectionBuilding(createPojo());
            //startActivity(new Intent(this, StepOneSelfieActivity.class));
            startActivity(new Intent(this, StepFiveContractActivity.class));
        }
    }

    @OnClick(R.id.mBackButton)
    public void OnBackButton(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
