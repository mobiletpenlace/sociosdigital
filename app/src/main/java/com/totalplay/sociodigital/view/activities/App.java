package com.totalplay.sociodigital.view.activities;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.totalplay.sociodigital.R;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class App extends Application {
    ApplicationPresenter mApplicationPresenter;
    public static String secretKey;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        ProspectModuleConfig config = new ProspectModuleConfig(this);
        config.setup();
        secretKey=getResources().getString(R.string.secret_key);
        mApplicationPresenter = new ApplicationPresenter(this);
        mApplicationPresenter.setup();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
