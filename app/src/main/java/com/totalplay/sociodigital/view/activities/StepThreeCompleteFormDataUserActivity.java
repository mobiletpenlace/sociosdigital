package com.totalplay.sociodigital.view.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.CPResponse;
import com.totalplay.sociodigital.model.entities.DirectionPartnerBean;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StepThreeCompleteFormDataUserActivity extends BaseActivity implements  RegisterSellerPresenter.RegisterSellerCallback{
    @BindView(R.id.mStepThreeCompleteFormZipCodeEditText)
    public EditText mStepThreeCompleteFormZipCodeEditText;

    @BindView(R.id.mStepThreeCompleteFormColonyEditText)
    public EditText mStepThreeCompleteFormColonyEditText;

    @BindView(R.id.mStepThreeCompleteFormDelegationEditText)
    public EditText mStepThreeCompleteFormDelegationEditText;

    @BindView(R.id.mStepThreeCompleteFormCityEditText)
    public EditText mStepThreeCompleteFormCityEditText;

    @BindView(R.id.mStepThreeCompleteFormStateEditText)
    public EditText mStepThreeCompleteFormStateEditText;

    @BindView(R.id.mStepThreeCompleteFormStreetEditText)
    public EditText mStepThreeCompleteFormStreetEditText;

    @BindView(R.id.mStepThreeCompleteFormNumberInternalEditText)
    public EditText mStepThreeCompleteFormNumberInternalEditText;

    @BindView(R.id.mStepThreeCompleteFormNumberExteriorEditText)
    public EditText mStepThreeCompleteFormNumberExteriorEditText;

    @BindView(R.id.mSearchColonyEditText)
    public EditText mSearchColonyEditText;

    public FormValidator mFormValidator;

    private RegisterSellerPresenter mRegisterSellerPresenter;

    private String[] mListColony;
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setToolbarEnabled(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_three_data_user_form_complete);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        addValidators();

        mStepThreeCompleteFormColonyEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mStepThreeCompleteFormDelegationEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mStepThreeCompleteFormCityEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mStepThreeCompleteFormStateEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mStepThreeCompleteFormStreetEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //setListenerCP();
        mStepThreeCompleteFormColonyEditText.setVisibility(View.VISIBLE);
        mSearchColonyEditText.setVisibility(View.GONE);
    }

    private void setListenerCP(){
        mStepThreeCompleteFormZipCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String cp =  mStepThreeCompleteFormZipCodeEditText.getText().toString();
                if (cp.length() == 5)
                    checkCP(cp);
            }
        });
    }

    private void checkCP(String cp){
        cleanEditText();

        mSearchColonyEditText.setCompoundDrawablesWithIntrinsicBounds(null, null,
                getResources().getDrawable(R.drawable.ic_arrow_down__, null), null);
        mRegisterSellerPresenter.loadCP(cp);
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mStepThreeCompleteFormZipCodeEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeCompleteFormColonyEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeCompleteFormDelegationEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeCompleteFormCityEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeCompleteFormStateEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeCompleteFormStreetEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeCompleteFormNumberExteriorEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this, this);
        return mRegisterSellerPresenter;
    }

    public DirectionPartnerBean createPojo(){
        DirectionPartnerBean directionPartnerBean = new DirectionPartnerBean(
                CryptoUtils.crypt(mStepThreeCompleteFormZipCodeEditText.getText().toString().trim()),
                mStepThreeCompleteFormColonyEditText.getText().toString().trim(),
                CryptoUtils.crypt(mStepThreeCompleteFormDelegationEditText.getText().toString().trim()),
                CryptoUtils.crypt(mStepThreeCompleteFormCityEditText.getText().toString().trim()),
                CryptoUtils.crypt(mStepThreeCompleteFormStateEditText.getText().toString().trim()),
                mStepThreeCompleteFormStreetEditText.getText().toString().trim(),
                CryptoUtils.crypt(mStepThreeCompleteFormNumberInternalEditText.getText().toString().trim()),
                CryptoUtils.crypt(mStepThreeCompleteFormNumberExteriorEditText.getText().toString().trim()),
                CryptoUtils.crypt(""),
                CryptoUtils.crypt("")
                );
        return directionPartnerBean;
    }

    @OnClick(R.id.mCancelDataUserCompleteButton)
    public void OnClickCancelCompleteButton(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mContinueDataUserCompleteButton)
    public void mContinueDataUserComplete(){
        if (mFormValidator.isValid()) {
            mRegisterSellerPresenter.saveDirectionPartner(createPojo());
            startActivity(new Intent(this, StepFourLoadDocumentsActivity.class));
        }
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void OnSuccesRegister(boolean isAllDocumentUpload) {
        
    }

    @Override
    public void onErrorRegister(boolean isAllDocumentUpload) {

    }

    @Override
    public void getCP(CPResponse response) {
        if(response.getColonias().length>0){
            mStepThreeCompleteFormColonyEditText.setVisibility(View.GONE);
            mSearchColonyEditText.setVisibility(View.VISIBLE);
        }else{
            mStepThreeCompleteFormColonyEditText.setVisibility(View.VISIBLE);
            mSearchColonyEditText.setVisibility(View.GONE);
        }

        System.out.println(response.getEstado());
        mStepThreeCompleteFormDelegationEditText.setText(response.getMunicipio());
        mStepThreeCompleteFormStateEditText.setText(response.getEstado());
        mListColony = response.getColonias();
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void startProgress(int imageId, boolean isSync) {

    }

    @OnClick(R.id.mSearchColonyEditText)
    public void OnclickListColony(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.item_list_colony);
        listView = dialog.findViewById(R.id.list);

        dialog.show();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mListColony);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                int itemPosition = position;
                String itemValue = (String) listView.getItemAtPosition(position);
                dialog.cancel();
                mSearchColonyEditText.setText(itemValue);
                mStepThreeCompleteFormColonyEditText.setText(itemValue);

            }

        });
    }

    public void cleanEditText(){
        mStepThreeCompleteFormColonyEditText.setText("");
        mStepThreeCompleteFormDelegationEditText.setText("");
        mStepThreeCompleteFormStateEditText.setText("");
        mSearchColonyEditText.setText("");
    }

}
