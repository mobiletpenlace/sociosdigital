package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageActivity;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.entities.CardPartnerBean;
import com.totalplay.sociodigital.model.entities.UserDateBean;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class StepFourLoadDocumentsActivity extends BaseActivity{
    static final Pattern CODE_PATTERN = Pattern.compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");
    private boolean flagDelete = false;
    private ArrayList<String> tempCardNumber = new ArrayList<>();
    private ArrayList<String>  asteriskNumber =  new ArrayList<>();

    @BindView(R.id.mContentAddressPhotosLY)
    LinearLayout mContentAddressPhotosLY;

    @BindView(R.id.mStepFourLoadDocumentsBackImageView)
    SelectableRoundedImageView mStepFourLoadDocumentsBackImageView;

    @BindView(R.id.mStepFourLoadDocumentsFrontImageView)
    SelectableRoundedImageView mStepFourLoadDocumentsFrontImageView;

    @BindView(R.id.mStepFourLoadDocumentsVoucherAddressImageView)
    SelectableRoundedImageView mStepFourLoadDocumentsVoucherAddressImageView;

    @BindView(R.id.mStepFourLoadDocumentsNameOwnerCardEditText)
    EditText mStepFourLoadDocumentsNameOwnerCardEditText;

    @BindView(R.id.mStepFourLoadDocumentsNumberCardEditText)
    EditText mCardEditext;

    @BindView(R.id.mStepFourLoadDocumentsDateExpirationAnioEditText)
    EditText mStepFourLoadDocumentsDateExpirationAnioEditText;

    @BindView(R.id.mStepFourLoadDocumentsDateExpiratioMonthEditText)
    EditText mStepFourLoadDocumentsDateExpiratioMonthEditText;

    @BindView(R.id.mStepFourLoadDocumentsScanCardLineaLayout)
    LinearLayout mStepFourLoadDocumentsScanCardLineaLayout;

    @BindView(R.id.typeCreditCardImageView)
    ImageView mCreditCardImageView;

    @BindView(R.id.typeDebitCardImageView)
    ImageView mDebitCardImageView;

    public FormValidator mFormValidator;
    private RegisterSellerPresenter mRegisterSellerPresenter;
    private File destination;
    private File copyDestination;

    private final int CAMERA_FRONT_REQUEST = 100;
    private final int CAMERA_FRONT_REQUEST_CROP = 102;

    private final int CAMERA_BACK_REQUEST = 150;
    private final int CAMERA_BACK_REQUEST_CROP = 152;

    private final int CAMERA_PROOF_ADDRESS_REQUEST = 250;
    private final int CAMERA_PROOF_REQUEST_CROP = 252;

    private int camaraRequest = 0;
    private int CAMARA = 10;
    private int CROP = 20;

    private static int MY_SCAN_REQUEST_CODE = 8;
    private int SIZE_PHOTO_WIDTH = 2500;
    private int SIZE_PHOTO_HEIGTH = 2500;

    private String pathFront = "";
    private String pathBack = "";
    private String pathVoucherAddress = "";

    private boolean isSameIdentification = false;
    private TextWatcher alternateTextWatcher;
    private String tempNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setToolbarEnabled(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_four_documents_collect_activity);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        addValidators();

        alternateTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            //1234 1234 1234 1234

            @Override
            public void afterTextChanged(Editable s) {
                String text = mCardEditext.getText().toString();
                if (text.length() > 0) {
                    if (!(tempNumber.length() > text.length())) {
                        if (text.length() == 4 || text.length() == 9 || text.length() == 14){
                            text =  text + " ";
                            tempNumber = tempNumber + text.substring(text.length() - 2);
                        }else{
                            tempNumber = tempNumber + text.substring(text.length() - 1);
                        }
                    } else {
                        tempNumber = tempNumber.substring(0, tempNumber.length() - 1);
                    }

                    if (text.length() > 0 && text.length() < 16)
                        text = text.replaceAll("[0-9]", "•");

                    setNumber(text);
                }else {
                    tempNumber = "";
                }
                //MessageUtils.toast(getApplicationContext(), tempNumber);
            }
        };

        mCardEditext.addTextChangedListener(alternateTextWatcher);

    }

    private void setNumber(String text){

        mCardEditext.removeTextChangedListener(alternateTextWatcher);
        mCardEditext.setText(text);
        mCardEditext.addTextChangedListener(alternateTextWatcher);
        if(text.length() < 19)
            mCardEditext.setSelection(text.length());

    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mStepFourLoadDocumentsNameOwnerCardEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCardEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepFourLoadDocumentsDateExpirationAnioEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepFourLoadDocumentsDateExpiratioMonthEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );


    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    @OnClick(R.id.mCancelarLoadDocumentsButton)
    public void OnCancelLoadDocument(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mContinueLoadDocumentsButton)
    public void OnContinuelLoadDocument(){
        UserDateBean userDateBean = mRegisterSellerPresenter.getInfoSeller();
        if (mFormValidator.isValid()) {
            if (validateImage()) {
                if(validateMonth()) {
                    if(validateYear()) {
                        if (validateCarNumber()) {
                            saveImages();
                            if (CryptoUtils.desEncrypt(userDateBean.mSubCanal).equals("Socio Negocios")) {
                                startActivity(new Intent(this, TypeSellerHaveEnterpriseActivity.class));
                            } else if (CryptoUtils.desEncrypt(userDateBean.mSubCanal).equals("Socio Edificios")) {
                                startActivity(new Intent(this, TypeSellerManagerBuildingActivity.class));
                            } else {
                                startActivity(new Intent(this, StepFiveContractActivity.class));
                            }
                        }else {
                            MessageUtils.toast(this,"Ingrese un número de tarjeta válido");
                        }
                    }else{
                        MessageUtils.toast(this, "El año no es válido");
                    }
                }else{
                    MessageUtils.toast(this, "El mes es incorrecto");
                }
            } else {
                MessageUtils.toast(this, "Es necesario capturar las imagenes");
            }
        }
    }

    public boolean validateMonth(){
        int month = Integer.parseInt(mStepFourLoadDocumentsDateExpiratioMonthEditText.getText().toString());

        if(month > 12 || month == 0 ){
            return false;
        }else{
            return true;
        }
    }

    public boolean validateYear() {
        final Calendar c = Calendar.getInstance();
        int mAnio = c.get(Calendar.YEAR);
        int current = Integer.parseInt(mStepFourLoadDocumentsDateExpirationAnioEditText.getText().toString()) + 2000;

        int maxAnio = mAnio + 10;
        if(mAnio < current && current < maxAnio){
            return true;
        }else{
            return false;
        }
    }

    public String setTypeCard(String mNumberCad){
        return com.totalplay.sociodigital.library.utils.StringUtils.getIdTypeCard(mNumberCad);
    }

    public boolean validateImage(){
        if(isSameIdentification){
            if(!pathFront.equals("") && !pathBack.equals("")){
                return true;
            }else{
                return false;
            }
        }else {
            if(!pathFront.equals("") && !pathBack.equals("") && !pathVoucherAddress.equals("")){
                return true;
            }else{
                return false;
            }
        }
    }

    public void saveImages(){
        CardPartnerBean mCardPartnerBean = new CardPartnerBean(
                CryptoUtils.crypt(mStepFourLoadDocumentsNameOwnerCardEditText.getText().toString().trim()),
                CryptoUtils.crypt(""),
                CryptoUtils.crypt(tempNumber.trim().replace(" ", "")),
                CryptoUtils.crypt(mStepFourLoadDocumentsDateExpiratioMonthEditText.getText().toString().trim()),
                CryptoUtils.crypt(mStepFourLoadDocumentsDateExpirationAnioEditText.getText().toString().trim()),
                CryptoUtils.crypt(setTypeCard(mCardEditext.getText().toString().trim())));
        if(isSameIdentification){
            try {
                createCopyAddress();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mRegisterSellerPresenter.saveDataSellerSectionThree(pathFront,pathBack,pathVoucherAddress,mCardPartnerBean);
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @OnClick(R.id.mStepFourLoadDocumentsFrontImageView)
    public void OnClickFront(){
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        destination = new File(Environment.getExternalStorageDirectory(), "IdentificacionFrente.jpg");
        camaraRequest = CAMARA;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        startActivityForResult( cameraIntent , CAMERA_FRONT_REQUEST);
    }

    @OnClick(R.id.mStepFourLoadDocumentsBackImageView)
    public void OnClickBack(){
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        destination = new File(Environment.getExternalStorageDirectory(), "IdentificacionReverso.jpg");
        camaraRequest = CAMARA;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        startActivityForResult( cameraIntent , CAMERA_BACK_REQUEST);
    }

    @OnClick(R.id.mStepFourLoadDocumentsVoucherAddressImageView)
    public void OnClickVoucher(){
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        destination = new File(Environment.getExternalStorageDirectory(), "Comprobante.jpg");
        camaraRequest = CAMARA;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        startActivityForResult( cameraIntent , CAMERA_PROOF_ADDRESS_REQUEST);
    }

    @OnClick(R.id.mStepFourLoadDocumentsScanCardLineaLayout)
    public void OnClickCard(){
        Intent scanIntent = new Intent(this, CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if(resultCode == RESULT_OK){
                if(camaraRequest == CAMARA){
                    camaraRequest = CROP;
                    Intent cameraIntent = CropImage.activity(Uri.fromFile(destination))
                            .setOutputUri(Uri.fromFile(destination))
                            .getIntent(getApplicationContext());
                    if (requestCode == CAMERA_FRONT_REQUEST) {
                        startActivityForResult( cameraIntent , CAMERA_FRONT_REQUEST_CROP);
                    }else if (requestCode == CAMERA_BACK_REQUEST) {
                        startActivityForResult( cameraIntent , CAMERA_BACK_REQUEST_CROP);
                    }else if (requestCode == CAMERA_PROOF_ADDRESS_REQUEST) {
                        startActivityForResult( cameraIntent , CAMERA_PROOF_REQUEST_CROP);
                    }
                }else if(camaraRequest == CROP){
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (requestCode == CAMERA_FRONT_REQUEST_CROP) {
                        pathFront = "IdentificacionFrente.jpg";
                        ((ImageView) findViewById(R.id.mStepFourLoadDocumentsFrontImageView)).setImageURI(result.getUri());
                    }else if (requestCode == CAMERA_BACK_REQUEST_CROP) {
                        ((ImageView) findViewById(R.id.mStepFourLoadDocumentsBackImageView)).setImageURI(result.getUri());
                        pathBack = "IdentificacionReverso.jpg";
                    }else if (requestCode == CAMERA_PROOF_REQUEST_CROP) {
                        ((ImageView) findViewById(R.id.mStepFourLoadDocumentsVoucherAddressImageView)).setImageURI(result.getUri());
                        pathVoucherAddress = "Comprobante.jpg";
                    }
                }
            }
            else{
                if(requestCode == MY_SCAN_REQUEST_CODE){
                    if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                        CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                        mCardEditext.removeTextChangedListener(alternateTextWatcher);

                        tempNumber = scanResult.cardNumber.substring(0,4) +  " " + scanResult.cardNumber.substring(4,8) + " " + scanResult.cardNumber.substring(8,12) + " " + scanResult.cardNumber.substring(12,16);
                        String numPart1 = scanResult.cardNumber.substring(0,4) +  " " + scanResult.cardNumber.substring(4,8) + " " + scanResult.cardNumber.substring(8,12) + " ";
                        numPart1 = numPart1.replaceAll("[0-9]", "•");
                        mCardEditext.setText(numPart1 + scanResult.cardNumber.substring(12,16));
                        mCardEditext.addTextChangedListener(alternateTextWatcher);

                        if (scanResult.isExpiryValid()) {

                            mStepFourLoadDocumentsDateExpiratioMonthEditText.setText(formatEpirationDateMonth(scanResult.expiryMonth));
                            mStepFourLoadDocumentsDateExpirationAnioEditText.setText(formatExpirationDateYear(String.valueOf(scanResult.expiryYear)));

                        }
                    }
                }
            }
        } catch (Exception | OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public SelectableRoundedImageView setRounded(SelectableRoundedImageView img){
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        img.setCornerRadiiDP(8, 8, 8, 8);
        img.setBorderWidthDP(4);
        img.setBorderColor(R.color.gray_border_bank_account);
        return img;
    }

    public String formatExpirationDateYear(String mYear){
        String mY=mYear.substring(2,mYear.length());
        return mY;
    }

    public String formatEpirationDateMonth(int mMonth){
        String month = "";
        if(mMonth<10){
            month = String.valueOf(mMonth);
            month = "0" + month;
        }else{
            month = String.valueOf(mMonth);
        }

        return month;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnCheckedChanged(R.id.mCheckSameAddressIdentficactionCheckButton)
    public void OnChangeSameIdentification(boolean b){
        if(b){
            mContentAddressPhotosLY.setVisibility(View.GONE);
            isSameIdentification = true;
        }else{
            mContentAddressPhotosLY.setVisibility(View.VISIBLE);
            isSameIdentification = false;
        }
    }

    private void createCopyAddress() throws IOException {
        destination = new File(Environment.getExternalStorageDirectory(), "IdentificacionFrente.jpg");
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        copyDestination = new File(Environment.getExternalStorageDirectory(), "Comprobante.jpg");
        copyFile(destination,copyDestination);
        pathVoucherAddress = "Comprobante.jpg";
    }


    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }

    }

    private boolean validateCarNumber(){
        boolean isValidNumber =  false;
        String result = setTypeCard(tempNumber.trim().replace(" ", ""));
        if (result.equals("1") || result.equals("2")  || result.equals("3")){
            isValidNumber =  true;
        }
        return isValidNumber;
    }

}
