package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.library.utils.StringUtils;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;
import com.totalplay.sociodigital.view.dialogfragments.RecyclerDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class StepThreeDataUserClientActivity extends BaseActivity {
    @BindView(R.id.mContentAccountNumberLinearLayout)
    LinearLayout mContentAccountNumberLinearLayout;

    @BindView(R.id.mClientCheckBoxButton)
    CheckBox mClientCheckBoxButton;

    @BindView(R.id.mStepThreeRFCEditText)
    EditText mStepThreeRFCEditText;

    @BindView(R.id.mStepThreeDataCollectEmailEditText)
    EditText mStepThreeDataCollectEmailEditText;

    @BindView(R.id.mStepThreeDataCollectNumberAccountEditText)
    EditText mStepThreeDataCollectNumberAccountEditText;

    @BindView(R.id.mStepThreeDataCollectPhoneEditText)
    EditText mStepThreeDataCollectPhoneEditText;

    @BindView(R.id.mContentCheckClickLY)
    LinearLayout mContentCheckClickLY;

    public FormValidator mFormValidator;

    private RegisterSellerPresenter mRegisterSellerPresenter;

    private boolean isWithAccount = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.step_three_data_user_collect_client_activity);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        addValidators();
        checkTypeSeller();

        mStepThreeRFCEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mStepThreeDataCollectEmailEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        OnEditEmail();
    }

    public void checkTypeSeller(){
        if(CryptoUtils.desEncrypt(mRegisterSellerPresenter.getTypeSeller()).equals("Cliente TP")){
            mContentCheckClickLY.setVisibility(View.GONE);
        }
    }

    public void OnEditEmail(){
        mStepThreeDataCollectEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                final String[] emailCompose = text.toString().split("\\@");
                if (text.toString().contains("@") && emailCompose.length != 2) {
                    RecyclerDialogFragment dialog = RecyclerDialogFragment.newInstance();
                    dialog.setArrayList(StringUtils.getEmailCommonDomain());
                    dialog.setSelectedListener((index, result) -> mStepThreeDataCollectEmailEditText.setText(String.format("%s@%s", emailCompose[0], result)));
                    dialog.show(getSupportFragmentManager(), "SelectedDomain");
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mStepThreeDataCollectEmailEditText, Regex.EMAIL, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeDataCollectPhoneEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    public void addValidatorsWithClient(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mStepThreeDataCollectEmailEditText, Regex.EMAIL, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeDataCollectPhoneEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeDataCollectNumberAccountEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    public boolean inputLimitDigits(){
        if(mStepThreeDataCollectPhoneEditText.getText().toString().length() == 10){
            return true;
        }else{
            return false;
        }
    }

    @OnCheckedChanged(R.id.mClientCheckBoxButton)
    public void OnChangeCheckedClient(boolean b){
        if(b){
            mContentAccountNumberLinearLayout.setVisibility(View.VISIBLE);
            addValidatorsWithClient();
            isWithAccount = true;
        }else{
            mContentAccountNumberLinearLayout.setVisibility(View.GONE);
            addValidators();
            isWithAccount = false;
        }
    }

    @OnClick(R.id.mContinueDataUserClientButton)
    public void OnClickContinueUser(){
        if (mFormValidator.isValid()) {
            if(inputLimitDigits()) {
                if (isWithAccount) {
                    mRegisterSellerPresenter.saveDataSellerSectionTwo(mStepThreeRFCEditText.getText().toString().trim(),
                            mStepThreeDataCollectEmailEditText.getText().toString().trim(),
                            mStepThreeDataCollectPhoneEditText.getText().toString().trim(),
                            mStepThreeDataCollectNumberAccountEditText.getText().toString().trim()
                    );
                } else {
                    mRegisterSellerPresenter.saveDataSellerSectionTwo(mStepThreeRFCEditText.getText().toString().trim(),
                            mStepThreeDataCollectEmailEditText.getText().toString().trim(),
                            mStepThreeDataCollectPhoneEditText.getText().toString().trim(), ""
                    );
                }
                startActivity(new Intent(this, StepThreeCompleteFormDataUserActivity.class));
            }else{
                MessageUtils.toast(this,"Es neceasario ingresar los 10 digitos de su número télefonico");
            }
        }
    }

    @OnClick(R.id.mDataUserCancelClientButton)
    public void OnClickCancelUserButton(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
