package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.EditText;

import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.library.utils.StringUtils;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.ReferredPresenter;
import com.totalplay.sociodigital.view.dialogfragments.RecyclerDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CapturedReferredActivity extends BaseActivity implements ReferredPresenter.RegisterReferredCallback{
    @BindView(R.id.mCapturedReferredNameEditText)
    EditText mCapturedReferredNameEditText;

    @BindView(R.id.mCapturedReferredLastNameEditText)
    EditText mCapturedReferredLastNameEditText;

    @BindView(R.id.mCapturedReferredSecondLastNameEditText)
    EditText mCapturedReferredSecondLastNameEditText;

    @BindView(R.id.mCapturedReferredPhoneEditText)
    EditText mCapturedReferredPhoneEditText;

    @BindView(R.id.mCapturedReferrerEmailEditText)
    EditText mCapturedReferrerEmailEditText;

    public FormValidator mFormValidator;

    public ReferredPresenter mReferredPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.capture_referred_activity);
        ButterKnife.bind(this);
        addValidators();
        OnEditEmail();

        mCapturedReferredNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mCapturedReferredLastNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mCapturedReferredSecondLastNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mCapturedReferrerEmailEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);

        mFormValidator.addValidators(
                new EditTextValidator(mCapturedReferredNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCapturedReferredLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCapturedReferredSecondLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCapturedReferredPhoneEditText, Regex.NUMERIC, R.string.dialog_error_empty)
                );
    }

    public void OnEditEmail(){
        mCapturedReferrerEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                final String[] emailCompose = text.toString().split("\\@");
                if (text.toString().contains("@") && emailCompose.length != 2) {
                    RecyclerDialogFragment dialog = RecyclerDialogFragment.newInstance();
                    dialog.setArrayList(StringUtils.getEmailCommonDomain());
                    dialog.setSelectedListener((index, result) -> mCapturedReferrerEmailEditText.setText(String.format("%s@%s", emailCompose[0], result)));
                    dialog.show(getSupportFragmentManager(), "SelectedDomain");
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    @OnClick(R.id.mSendRequestReferredButton)
    public void OnClickSendRequestReferred(){
        if (mFormValidator.isValid()) {
            if(inputLimitDigits()) {
                mReferredPresenter.capturedReferred(
                        "",
                        mCapturedReferredNameEditText.getText().toString().trim(),
                        mCapturedReferredLastNameEditText.getText().toString().trim(),
                        mCapturedReferredSecondLastNameEditText.getText().toString().trim(),
                        mCapturedReferredPhoneEditText.getText().toString().trim(),
                        mCapturedReferrerEmailEditText.getText().toString().trim());
            }else{
                MessageUtils.toast(this,"Es neceasario ingresar los 10 digitos de su número télefonico");
            }
        }
    }

    public boolean inputLimitDigits(){
        if(mCapturedReferredPhoneEditText.getText().toString().length() == 10){
            return true;
        }else{
            return false;
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        mReferredPresenter = new ReferredPresenter(this, this);
        return mReferredPresenter;
    }

    @OnClick(R.id.mBackButton)
    public void OnClickOnBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void OnSuccessRegister() {
        startActivityForResult(new Intent(this, SendRequestReferredDialog.class),1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.finish();
    }
}
