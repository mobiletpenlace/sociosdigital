package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.totalplay.sociodigital.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ivancortes on 24/09/18.
 * SocioDigital
 */

public class DialogAlertTypeSeller extends BaseActivity {

    public static Intent launch(Context context, String typeSeller){
        Intent intent = new Intent(context,DialogAlertTypeSeller.class);
        intent.putExtra("typeSeller",typeSeller);
        return  intent;
    }

    @BindView(R.id.decription_type_seller)
    public TextView mDetailTextView;
    private String typeSeller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_alert_type_seller);
        typeSeller =  getIntent().getStringExtra("typeSeller");
        ButterKnife.bind(this);
        setDetail();
    }

    @OnClick(R.id.accept_alert_type_seller)
    public void onClickBtnAccept(){
        finish();
    }

    private void setDetail(){
        switch (typeSeller){
            case "Cliente TP":
                mDetailTextView.setText("Esta opción te permite obtener increíbles beneficios por referirnos a tus vecinos, para que como tú también vivan la experiencia Totalplay.");
                break;
            case "Socio Negocios":
                mDetailTextView.setText("Esta opción te permite obtener increíbles beneficios al recomendar Totalplay a tus clientes desde tu negocio.");
                break;
            case "Socio Edificios":
                mDetailTextView.setText("Esta opción te permite obtener increíbles beneficios al recomendar Totalplay dentro de tu desarrollo inmobiliario.");
                break;
            case "Socio Totalplay":
                mDetailTextView.setText("Esta opción te permite obtener increíbles beneficios al recomendar Totalplay a tu red de contactos.");
                break;
        }
    }
}
