package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.WindowManager;
import android.widget.EditText;

import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StepThreeDataUserActivity extends BaseActivity{
    @BindView(R.id.mStepThreeDataCollectNameEditText)
    public EditText mStepThreeDataCollectNameEditText;

    @BindView(R.id.mStepThreeDataCollectLastNameEditText)
    public EditText mStepThreeDataCollectLastNameEditText;

    @BindView(R.id.mStepThreeDataCollectSecondLastNameEditText)
    public EditText mStepThreeDataCollectSecondLastNameEditText;

    public FormValidator mFormValidator;

    private RegisterSellerPresenter mRegisterSellerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setToolbarEnabled(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_three_data_user_collect_activity);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        addValidators();

        mStepThreeDataCollectNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), (src, start, end, dst, dstart, dend) -> {
            if(src.equals("")){ // for backspace
                return src;
            }
            if(src.toString().matches("[a-zA-Z ]+")){
                return src;
            }
            return "";
        }});
        mStepThreeDataCollectLastNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), (src, start, end, dst, dstart, dend) -> {
            if(src.equals("")){ // for backspace
                return src;
            }
            if(src.toString().matches("[a-zA-Z ]+")){
                return src;
            }
            return "";
        }});
        mStepThreeDataCollectSecondLastNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), (src, start, end, dst, dstart, dend) -> {
            if(src.equals("")){ // for backspace
                return src;
            }
            if(src.toString().matches("[a-zA-Z ]+")){
                return src;
            }
            return "";
        }});
    }

    public void addValidators(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mStepThreeDataCollectNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeDataCollectLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStepThreeDataCollectSecondLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    @OnClick(R.id.mContinueDataUserButton)
    public void OnClickContinueDataUser(){
        if (mFormValidator.isValid()) {
            mRegisterSellerPresenter.saveDataSellerSectionOne(
                    mStepThreeDataCollectNameEditText.getText().toString().trim(),
                    mStepThreeDataCollectLastNameEditText.getText().toString().trim(),
                    mStepThreeDataCollectSecondLastNameEditText.getText().toString().trim()
            );
            startActivity(new Intent(this, StepThreeDataUserClientActivity.class));
        }
    }

    @OnClick(R.id.mDataUserCancelButton)
    public void OnClickCancelDataUser(){
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mBackButton)
    public void OnBackButton(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
