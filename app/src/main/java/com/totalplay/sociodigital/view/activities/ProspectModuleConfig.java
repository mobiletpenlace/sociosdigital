package com.totalplay.sociodigital.view.activities;

import android.content.Context;

import com.resources.utils.Prefs;

import java.security.SecureRandom;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class ProspectModuleConfig {
    private Context context;

    public ProspectModuleConfig(Context context) {
        this.context = context;
    }

    public void setup() {
        initRealm();
    }

    private void initRealm() {
        byte[] key = new byte[64];
        new SecureRandom(key);
        Prefs.setDefaultContext(context);
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("prospect.app")
                .deleteRealmIfMigrationNeeded()
                .encryptionKey(key).build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
