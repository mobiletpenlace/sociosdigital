package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.presenter.callbacks.RecoverPassCallbak;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;
import com.totalplay.sociodigital.presenter.implementations.RecoverPassPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by imacbookpro on 13/09/18.
 * SocioDigital
 */

public class RecoverPassActivity extends BaseActivity implements RecoverPassCallbak{
    public static Intent launch(Context context){
        Intent intent = new Intent(context, RecoverPassActivity.class);
        return  intent;
    }

    @BindView(R.id.act_recover_pass_no_socio)
    public EditText noSocio;
    @BindView(R.id.dialog_recover_pass_content)
    public ViewGroup dialogContent;

    private RecoverPassPresenter mRecoverPassPresenter;
    private ViewGroup mBackToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.recover_pass_activity);
        ButterKnife.bind(this);
        mRecoverPassPresenter = new RecoverPassPresenter(this,this);
        dialogContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mBackToolbar =  findViewById(R.id.mBackButton);
        mBackToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getSocio(){
        if (noSocio.getText().length() > 0)
            mRecoverPassPresenter.getRecoverPass(CryptoUtils.crypt(noSocio.getText().toString().trim()));
        else
            MessageUtils.toast(this,"Debe introducir su número de socio para continuar");
    }

    @OnClick(R.id.dialog_recover_pass_acccept)
    public void onClickAccept(){
        dialogContent.setVisibility(View.GONE);
        finish();
    }

    @OnClick(R.id.act_recovery_pass_send)
    public void onClick(){
        getSocio();
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"Error de conexión");
    }

    @Override
    public void successRecoverPass() {
        MessageUtils.stopProgress();
        dialogContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onErrorRecoverPass() {
        MessageUtils.stopProgress();
        MessageUtils.toast(this,"No se ha podido consultar la información");
    }

    @Override
    public void onLoadRecoverPass() {
        MessageUtils.progress(this,"Consultando información");
    }

}
