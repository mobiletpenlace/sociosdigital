package com.totalplay.sociodigital.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.background.ws.response.FamilyPackagesResponse;
import com.totalplay.sociodigital.model.PmPlanesSocios.PlanTotalPlay;

import java.util.ArrayList;

/**
 * Created by imacbookpro on 31/08/18.
 * SocioDigital
 */

public class SelectPackageAdapter extends RecyclerView.Adapter<SelectPackageAdapter.RecyclerViewSimpleTextViewHolder> {

    private ArrayList<PlanTotalPlay> packageList = new ArrayList<>();
    private int positionActiva = 0;
    private Context mContext;

    public SelectPackageAdapter(ArrayList<PlanTotalPlay> packageList, Context context) {
        this.packageList = packageList;
        this.mContext = context;
    }

    public void setPageActivated(int positionActiva){
        this.positionActiva = positionActiva;
    }

    @Override
    public SelectPackageAdapter.RecyclerViewSimpleTextViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_activated_pager_addons, viewGroup, false);
        return new SelectPackageAdapter.RecyclerViewSimpleTextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SelectPackageAdapter.RecyclerViewSimpleTextViewHolder holder, int position) {
        if(position==positionActiva){
            holder.act.setVisibility(View.VISIBLE);
            int newHeight = 25;
            int newWidth = 25;
            holder.act.requestLayout();
            holder.act.getLayoutParams().height = newHeight;
            holder.act.getLayoutParams().width = newWidth;
            holder.act.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.act.setImageDrawable(mContext.getResources().getDrawable(R.drawable.background_circle_blue));
        }else{
            int newHeight = 15;
            int newWidth = 15;
            holder.act.requestLayout();
            holder.act.getLayoutParams().height = newHeight;
            holder.act.getLayoutParams().width = newWidth;
            holder.act.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.act.setImageDrawable(mContext.getResources().getDrawable(R.drawable.background_circle_gray));
        }
    }

    @Override
    public int getItemCount() {
        return this.packageList.size();
    }

    public class RecyclerViewSimpleTextViewHolder extends RecyclerView.ViewHolder {
        ImageView act;
        public RecyclerViewSimpleTextViewHolder(View itemView) {
            super(itemView);
            act= itemView.findViewById(R.id.item_activated_addons_control);
        }
    }

}
