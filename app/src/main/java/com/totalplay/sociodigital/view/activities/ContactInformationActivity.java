package com.totalplay.sociodigital.view.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.jaredrummler.android.device.DeviceName;
import com.josketres.rfcfacil.Rfc;
import com.resources.utils.CryptoUtils;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.resources.utils.validators.TextViewValidator;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.library.utils.SocioDigitalKeys;
import com.totalplay.sociodigital.library.utils.StringUtils;
import com.totalplay.sociodigital.model.entities.AdditionalInfoEntity;
import com.totalplay.sociodigital.model.entities.ContactEntity;
import com.totalplay.sociodigital.model.entities.SellerEntity;
import com.totalplay.sociodigital.presenter.callbacks.ContactInformationCallback;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.ContactInformationPresenter;
import com.totalplay.sociodigital.view.dialogfragments.RecyclerDialogFragment;

import java.util.regex.Pattern;

import static android.view.View.GONE;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class ContactInformationActivity extends BaseActivity implements ContactInformationCallback {

    public static Intent launch (Context context, String city){
        Intent intent =  new Intent(context, ContactInformationActivity.class);
        intent.putExtra("getCity", city);
        return intent;
    }

    private TextView mTitleTextView;
    private TextView mTypePersonTextView;
    private TextView mTypeIdentificationTextView;
    private TextView mIdentificationNumberTextView;
    private TextView mNameTextView;
    private TextView mLastNameTextView;
    private TextView mSecondLastNameTextView;
    private TextView mDateBirthTextView;
    private TextView mRFCTextView;
    private TextView mPhoneTextView;
    private TextView mCellphoneTextView;
    private TextView mEmailTextView;
    private EditText mTypePersonEditText;
    private EditText mTypeIdentificationEditText;
    private EditText mIdentificationNumberEditText;
    private EditText mNameEditText;
    private EditText mLastNameEditText;
    private EditText mSeocndLastNameEditText;
    private EditText mDateBirthEditText;
    private EditText mRFCEditText;
    private EditText mPhoneEditText;
    private EditText mCellphoneEditText;
    private EditText mEmailEditText;
    private Button mNextButton;
    private String city;
    private TextView mSocialReasonTextView;
    private EditText mSocialReasonEditext;
    private ViewGroup mSocialReasonContent;

    //CustomDialog
    private TextView mIneCustomDialogTextView;
    private TextView mLicenseCustomDialogTextView;
    private TextView mPassportCustomDialogTextView;
    private TextView mCeduleCustomDialogTextView;
    private TextView mFMMCustomDialogTextView;
    private ViewGroup mContentCustomDialog;
    private TextView mTitleDialogTextView;

    //CustomDialogTypePerson
    private TextView mPhysicalCustomDialogTypePerson;
    private TextView mMoralCustomDialogTypePerson;
    private ViewGroup mContentCustomDialogTypePerson;
    private TextView mTitleCustomDialogTypePerson;

    private int typeIdentificationSelected;
    private int typePersonSelected;

    private ViewGroup mBackToolbar;
    private ContactInformationPresenter mContactInformationPresenter;
    private FormValidator mFormValidatorFisica;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.contact_information_activity);
        city = getIntent().getStringExtra("getCity");
        bindResources();
        setFonts();
        setOnClicks();
        typeIdentificationSelected = 0;
        typePersonSelected = 1;
        configDateAnnEmailEditext();
        presetTypePerson();
        createValidator();

        mIdentificationNumberEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mLastNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mSeocndLastNameEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mRFCTextView.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mEmailEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mSocialReasonEditext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @Override
    protected BasePresenter getPresenter() {
        return mContactInformationPresenter = new ContactInformationPresenter(this, this);
    }


    private void bindResources(){
        mTitleTextView = findViewById(R.id.act_contact_information_title);
        mTypePersonTextView = findViewById(R.id.act_contact_information_type_person);
        mTypePersonEditText = findViewById(R.id.act_contact_information_type_person_editext);
        mTypeIdentificationTextView = findViewById(R.id.act_contact_information_type_identification);
        mTypeIdentificationEditText = findViewById(R.id.act_contact_information_type_identification_editext);
        mIdentificationNumberTextView =  findViewById(R.id.act_contact_information_number_identification);
        mIdentificationNumberEditText = findViewById(R.id.act_contact_information_number_identification_editext);
        mNameTextView =  findViewById(R.id.act_contact_information_name);
        mNameEditText = findViewById(R.id.act_contact_information_name_editext);
        mLastNameTextView =  findViewById(R.id.act_contact_information_lastname);
        mLastNameEditText = findViewById(R.id.act_contact_information_lastname_editext);
        mSecondLastNameTextView =  findViewById(R.id.act_contact_information_second_lastname);
        mSeocndLastNameEditText =  findViewById(R.id.act_contact_information_second_lastname_editext);
        mDateBirthTextView = findViewById(R.id.act_contact_information_date_birth);
        mDateBirthEditText = findViewById(R.id.act_contact_information_date_birth_editext);
        mRFCEditText =  findViewById(R.id.act_contact_information_rfc_editext);
        mRFCTextView = findViewById(R.id.act_contact_information_rfc);
        mPhoneTextView = findViewById(R.id.act_contact_information_phone);
        mPhoneEditText = findViewById(R.id.act_contact_information_phone_editext);
        mCellphoneTextView = findViewById(R.id.act_contact_information_cellphone);
        mCellphoneEditText = findViewById(R.id.act_contact_information_cellphone_editext);
        mEmailTextView = findViewById(R.id.act_contact_information_email);
        mEmailEditText = findViewById(R.id.act_contact_information_email_editext);
        mNextButton = findViewById(R.id.act_contact_information_next_button);
        mSocialReasonTextView =  findViewById(R.id.act_contact_information_social_reason);
        mSocialReasonEditext = findViewById(R.id.act_contact_information_social_reason_editext);
        mSocialReasonContent = findViewById(R.id.act_contact_information_social_reason_content);
        //CustomDialog
        mIneCustomDialogTextView =  findViewById(R.id.custom_selector_dialog_ine);
        mLicenseCustomDialogTextView =  findViewById(R.id.custom_selector_dialog_licence);
        mPassportCustomDialogTextView =  findViewById(R.id.custom_selector_dialog_passport);
        mCeduleCustomDialogTextView =  findViewById(R.id.custom_selector_dialog_cedule);
        mFMMCustomDialogTextView =  findViewById(R.id.custom_selector_dialog_fmm);
        mContentCustomDialog =  findViewById(R.id.custom_selector_dialog_content);
        mTitleDialogTextView =  findViewById(R.id.custom_selector_dialog_title);
        //CustomDialog type person
        mMoralCustomDialogTypePerson =  findViewById(R.id.custom_selector_dialog_type_person_moral);
        mPhysicalCustomDialogTypePerson =  findViewById(R.id.custom_selector_dialog_type_person_physical);
        mContentCustomDialogTypePerson =  findViewById(R.id.custom_selector_dialog_type_person_content);
        mTitleCustomDialogTypePerson =  findViewById(R.id.custom_selector_dialog_type_person_title);
        mBackToolbar =  findViewById(R.id.mBackButton);

    }

    private void setFonts(){
        Typeface monserratMedium = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.ttf");
        Typeface monserratRegular = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        Typeface monserratBold = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");

        mTitleTextView.setTypeface(monserratRegular);
        mTypePersonTextView.setTypeface(monserratBold);
        mTypeIdentificationTextView.setTypeface(monserratBold);
        mIdentificationNumberTextView.setTypeface(monserratBold);
        mNameTextView.setTypeface(monserratBold);
        mLastNameTextView.setTypeface(monserratBold);
        mSecondLastNameTextView.setTypeface(monserratBold);
        mDateBirthTextView.setTypeface(monserratBold);
        mRFCTextView.setTypeface(monserratBold);
        mPhoneTextView.setTypeface(monserratBold);
        mCellphoneTextView.setTypeface(monserratBold);
        mEmailTextView.setTypeface(monserratBold);
        mTypePersonEditText.setTypeface(monserratRegular);
        mTypeIdentificationEditText.setTypeface(monserratRegular);
        mIdentificationNumberEditText.setTypeface(monserratRegular);
        mNameEditText.setTypeface(monserratRegular);
        mLastNameEditText.setTypeface(monserratRegular);
        mSeocndLastNameEditText.setTypeface(monserratRegular);
        mDateBirthEditText.setTypeface(monserratRegular);
        mRFCEditText.setTypeface(monserratRegular);
        mPhoneEditText.setTypeface(monserratRegular);
        mCellphoneEditText.setTypeface(monserratRegular);
        mEmailEditText.setTypeface(monserratRegular);
        mNextButton.setTypeface(monserratMedium);
        mSocialReasonTextView.setTypeface(monserratBold);
        mSocialReasonEditext.setTypeface(monserratRegular);
        //CustomDialog
        mIneCustomDialogTextView.setTypeface(monserratMedium);
        mLicenseCustomDialogTextView.setTypeface(monserratMedium);
        mPassportCustomDialogTextView.setTypeface(monserratMedium);
        mCeduleCustomDialogTextView.setTypeface(monserratMedium);
        mFMMCustomDialogTextView.setTypeface(monserratMedium);

    }

    private void setOnClicks(){
        mNextButton.setOnClickListener(v -> validateDataForm());

        mTypePersonEditText.setOnClickListener(v -> mContentCustomDialogTypePerson.setVisibility(View.VISIBLE));

        mTypeIdentificationEditText.setOnClickListener(v -> mContentCustomDialog.setVisibility(View.VISIBLE));

        mIneCustomDialogTextView.setOnClickListener(v -> {
            typeIdentificationSelected = 1;
            mTypeIdentificationEditText.setText(mIneCustomDialogTextView.getText().toString().toUpperCase());
            mContentCustomDialog.setVisibility(GONE);
        });

        mLicenseCustomDialogTextView.setOnClickListener(v -> {
            typeIdentificationSelected = 2;
            mTypeIdentificationEditText.setText(mLicenseCustomDialogTextView.getText().toString().toUpperCase());
            mContentCustomDialog.setVisibility(GONE);
        });

        mPassportCustomDialogTextView.setOnClickListener(v -> {
            typeIdentificationSelected = 3;
            mTypeIdentificationEditText.setText(mPassportCustomDialogTextView.getText().toString().toUpperCase());
            mContentCustomDialog.setVisibility(GONE);
        });

        mCeduleCustomDialogTextView.setOnClickListener(v -> {
            typeIdentificationSelected = 4;
            mTypeIdentificationEditText.setText(mCeduleCustomDialogTextView.getText().toString().toUpperCase());
            mContentCustomDialog.setVisibility(GONE);
        });

        mFMMCustomDialogTextView.setOnClickListener(v -> {
            typeIdentificationSelected = 5;
            mTypeIdentificationEditText.setText(mFMMCustomDialogTextView.getText().toString().toUpperCase());
            mContentCustomDialog.setVisibility(GONE);
        });

        mPhysicalCustomDialogTypePerson.setOnClickListener(v -> {
            typePersonSelected = 1;
            mTypePersonEditText.setText(mPhysicalCustomDialogTypePerson.getText().toString().toUpperCase());
            mContentCustomDialogTypePerson.setVisibility(GONE);
            presetTypePerson();
        });

        mMoralCustomDialogTypePerson.setOnClickListener(v -> {
            typePersonSelected = 2;
            mTypePersonEditText.setText(mMoralCustomDialogTypePerson.getText().toString().toUpperCase());
            mContentCustomDialogTypePerson.setVisibility(GONE);
            presetTypePerson();
        });

        mBackToolbar.setOnClickListener(v -> finish());

        //
        mContentCustomDialog.setOnClickListener(v -> {
            //
        });

        mContentCustomDialogTypePerson.setOnClickListener(v -> {
            //
        });
    }

    private void configDateAnnEmailEditext(){
        SimpleMaskFormatter smf = new SimpleMaskFormatter("NN/NN/NNNN");
        MaskTextWatcher mtw = new MaskTextWatcher(mDateBirthEditText, smf);
        mDateBirthEditText.addTextChangedListener(mtw);


        mEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                final String[] emailCompose = text.toString().split("\\@");
                if (text.toString().contains("@") && emailCompose.length != 2) {
                    RecyclerDialogFragment dialog = RecyclerDialogFragment.newInstance();
                    dialog.setArrayList(StringUtils.getEmailCommonDomain());
                    dialog.setSelectedListener((index, result) -> mEmailEditText.setText(String.format("%s@%s", emailCompose[0], result)));
                    dialog.show(getSupportFragmentManager(), "SelectedDomain");
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        mDateBirthEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mDateBirthEditText.getText().length() == 10) {
                    if (typePersonSelected == 1) {
                        String name = mNameEditText.getText().toString();
                        String lastNameP = mLastNameEditText.getText().toString();
                        String lastNameM = mSeocndLastNameEditText.getText().toString();
                        String[] date = mDateBirthEditText.getText().toString().split("/");
                        if (!name.isEmpty() && !lastNameP.isEmpty() && !lastNameM.isEmpty() && date.length > 2) {
                            try {
                                Rfc rfc = new Rfc.Builder()
                                        .name(name)
                                        .firstLastName(lastNameP)
                                        .secondLastName(lastNameM)
                                        .birthday(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]))
                                        .build();
                                //String rfcTemp = rfc.toString().substring(0,8) + rfc.toString().substring(10);
                                mRFCEditText.setText(rfc.toString());
                                try {
                                    TextInputLayout textInputLayout = (TextInputLayout) mRFCEditText.getParent();
                                    textInputLayout.setErrorEnabled(false);
                                    //textInputLayout.setError("");
                                } catch (ClassCastException ignored) {
                                    //mRFCEditText.setError("");
                                }
                            } catch (Exception e) {
                                //mRFCEditText.setError("Ingrese RFC Manualmente");
                            }
                        }
                    } else {
                        String name = mSocialReasonEditext.getText().toString();
                        String lastNameP = mLastNameEditText.getText().toString();
                        String lastNameM = mSeocndLastNameEditText.getText().toString();
                        String[] date = mDateBirthEditText.getText().toString().split("/");
                        if (!name.isEmpty() && !lastNameP.isEmpty() && !lastNameM.isEmpty() && date.length > 2) {
                            try {
                                Rfc rfc = new Rfc.Builder()
                                        .legalName(name)
                                        .birthday(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]))
                                        .build();
                                //String rfcTemp2 = rfc.toString().substring(0,8) + rfc.toString().substring(10);
                                mRFCEditText.setText(rfc.toString());
                                try {
                                    TextInputLayout textInputLayout = (TextInputLayout) mRFCEditText.getParent();
                                    textInputLayout.setErrorEnabled(false);
                                    //textInputLayout.setError("");
                                } catch (ClassCastException ignored) {
                                    //mRFCEditText.setError("");
                                }
                            } catch (Exception e) {
                                //mRFCEditText.setError("Ingrese RFC Manualmente");
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

   /* public void calculateRFCM(CharSequence text) {
        String name = reasonSocialMoral.getText().toString();
        String[] date = dateBirthMoral.getText().toString().split("-");
        if (!name.isEmpty() && date.length > 2) {
            try {
                Rfc rfc = new Rfc.Builder()
                        .legalName(name)
                        .birthday(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]))
                        .build();
                rfcMoral.setText(rfc.toString());
            } catch (Exception e) {
                rfcMoral.setError(getString(R.string.manually_rfc));
            }

        }
    }*/


    private void presetTypePerson(){
        if (typePersonSelected == 2){
            mDateBirthTextView.setText("*Fecha de constitución");
            mSocialReasonContent.setVisibility(View.VISIBLE);
        }else if (typePersonSelected == 1){
            mDateBirthTextView.setText("*Fecha de nacimiento");
            mSocialReasonContent.setVisibility(GONE);
        }
    }

    private boolean isValidPhone(String target)
    {
        return Pattern.compile("^[1-9]\\d{9}$").matcher(target).matches();
    }

    private void getDataForm(){
        AdditionalInfoEntity vAdditionalInfoEntity = new AdditionalInfoEntity();
        ContactEntity vContactEntity = new ContactEntity();
        if (typePersonSelected == 1) {
            Prefs.instance().putString(SocioDigitalKeys.TYPE_PERSON, "physical");
            vContactEntity.typePerson = ("Física");
        }
        else if(typePersonSelected == 2) {
            Prefs.instance().putString(SocioDigitalKeys.TYPE_PERSON, "moral");
            vContactEntity.typePerson = ("Moral");
            vContactEntity.socialReason = CryptoUtils.crypt(mSocialReasonEditext.getText().toString().trim());
        }

        vContactEntity.name = (CryptoUtils.crypt(mNameEditText.getText().toString().trim()));
        Prefs.instance().putString("ownerName",mNameEditText.getText().toString());
        vContactEntity.fatherLastName = (CryptoUtils.crypt(mLastNameEditText.getText().toString().trim()));
        Prefs.instance().putString("ownerLastName",mLastNameEditText.getText().toString());
        vContactEntity.motherLastName = (CryptoUtils.crypt(mSeocndLastNameEditText.getText().toString().trim()));
        Prefs.instance().putString("ownerSecondLastName",mSeocndLastNameEditText.getText().toString());
        vContactEntity.phone = (CryptoUtils.crypt(mPhoneEditText.getText().toString().trim()));
        vContactEntity.otherPhone = (CryptoUtils.crypt(mPhoneEditText.getText().toString().trim()));
        vContactEntity.cellphone = (CryptoUtils.crypt(mCellphoneEditText.getText().toString().trim()));
        vContactEntity.email = (CryptoUtils.crypt(mEmailEditText.getText().toString().trim()));
        vContactEntity.otherEmail = (CryptoUtils.crypt(mEmailEditText.getText().toString().trim()));
        vContactEntity.rfc = (CryptoUtils.crypt(mRFCEditText.getText().toString().trim()));
        String[] date = mDateBirthEditText.getText().toString().split("/");
        String newDate = date[2]+"-"+ date[1] + "-" + date[0];
        vContactEntity.birthDay = CryptoUtils.crypt(newDate);
        //vContactEntity.medioContacto = (CryptoUtils.crypt(idMeansOfTypeEditText.getText().toString().trim()));
        vAdditionalInfoEntity.typeID = mTypeIdentificationEditText.getText().toString().trim();
        Prefs.instance().putString(SocioDigitalKeys.TYPE_IDENTIFICATION, mTypeIdentificationEditText.getText().toString().trim());
        vAdditionalInfoEntity.infoID = mIdentificationNumberEditText.getText().toString().trim();
        SellerEntity seller = new SellerEntity();
        seller.chanel = "Ecommerce";

        seller.subChanel = "AppMovil";
        seller.ticketContract = "";
        //TODO: Extra info phone
        //seller.idDevice = (mPrefs.getString(ProspectKeys.TOKEN_DEVICE_ID));
        //seller.imei = (mPrefs.getString(ProspectKeys.EMEI_PHONE));
        /*

        if (BuildConfig.DEBUG) {
            seller.idEmployee = "a0hQ0000003hcv9";
            seller.loggedUser = "00000000";
        } else {
            seller.idEmployee = "a0I610000056SBk";
            seller.loggedUser = "000002";
        }
        */

        seller.idEmployee = "a0I610000056SBk";
        seller.loggedUser = "000002";

        seller.employee = "NO COMISIONABLE";
        seller.idDistrict = "";
        seller.idEmployeeDistrict = "";
        seller.isApp = "true";
        seller.saleExpress = "false";
        seller.approvalSaleExpress = "NA";
        seller.isCommission = "false";

        seller.deviceModel = (DeviceName.getDeviceName());
        mContactInformationPresenter.saveContactInformation(vContactEntity, vAdditionalInfoEntity, seller);
    }

    private void validateDataForm(){
        String[] date = mDateBirthEditText.getText().toString().split("/");
        if (typePersonSelected == 1)
            mSocialReasonEditext.setText("dummy");

        if (isValidPhone(mPhoneEditText.getText().toString()) && isValidPhone(mCellphoneEditText.getText().toString())){

            if (validateDate(Integer.parseInt(date[0]),Integer.parseInt(date[1]),Integer.parseInt(date[2]))){
                if (mFormValidatorFisica.isValid())
                    getDataForm();
            }else
                MessageUtils.toast(this,"Introduzca una fecha válida");

        }else
            MessageUtils.toast(this,"Compruebe que los números telefónicos sean válidos");
    }

    private void createValidator(){
        mFormValidatorFisica = new FormValidator(this, true);
        mFormValidatorFisica.addValidators(
                new EditTextValidator(mTypeIdentificationEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mIdentificationNumberEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                //new EditTextValidator(idMeansOfTypeEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mSocialReasonEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mSeocndLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new TextViewValidator(mDateBirthEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                //new EditTextValidator(mRFCEditText, Regex.NOT_EMPTY, R.string.dialog_error_rfc),
                //  new EditTextValidator(physicalHomePhoneEditText, Regex.NUMBERPHONE, R.string.dialog_error_number_phone),
                //  new EditTextValidator(physicalCellphoneEditText, Regex.NUMBERPHONE, R.string.dialog_error_number_phone),
                new EditTextValidator(mEmailEditText, Regex.EMAIL, R.string.dialog_error_email)
        );
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.toast(this, "Ha ocurrido un error al guardar la información. Intente nuevamente");
    }

    @Override
    public void onSaveSuccess() {
        //Finish activity
        startActivity(TypeProfileActivity.launch(getApplicationContext(),city));
    }


    public boolean validateFieldRFC(){
        if(mRFCEditText.getText().toString().length() <= 13 && !mRFCEditText.getText().toString().isEmpty()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mContentCustomDialog.setVisibility(GONE);
        mContentCustomDialogTypePerson.setVisibility(GONE);
    }

    private boolean validateDate(int day, int month, int year){
        boolean validDate = false;
        if (day > 0 && day < 32 && month > 0 && month < 13 && year > 1918)
            validDate =  true;

        return validDate;
    }
}
