package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.resources.utils.MessageUtils;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.RegisterSellerPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TypeSellerActivity extends BaseActivity {
    @BindView(R.id.mClientTotalPlayImageView)
    ImageView mClientTotalPlayImageView;

    @BindView(R.id.mHaveEnterpriseImageView)
    ImageView mHaveEnterpriseImageView;

    @BindView(R.id.mManagerBuildingImageView)
    ImageView mManagerBuildingImageView;

    @BindView(R.id.mSellerIndieImageView)
    ImageView mSellerIndieImageView;

    private String mTypeSeller = "";
    private String rollId = "";
    private RegisterSellerPresenter mRegisterSellerPresenter;
    boolean isChecket = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setToolbarEnabled(false);
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.type_seller_activity);
        setHomeAsUpEnabled(false);
        ButterKnife.bind(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        mRegisterSellerPresenter = new RegisterSellerPresenter(this);
        return mRegisterSellerPresenter;
    }

    @OnClick(R.id.mCancelTypeSellerButton)
    public void OnClickCancelarSellerButton() {
        startActivity(new Intent(this, CancelRegisterDialog.class));
    }

    @OnClick(R.id.mContinueTypeSellerButton)
    public void OnClickContinueTypeSeller() {
        if (isChecket){
            typeLaunchSeller();
        }else{
            MessageUtils.toast(this, "Selecciona un tipo de vendedor para continuar");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void typeLaunchSeller() {
        switch (mTypeSeller) {
            case "1":
                mRegisterSellerPresenter.saveTypeSeller("Cliente TP", "1506");
                //startActivity(new Intent(this, TypeSellerClientTotalPlayActivity.class));
                startActivity(new Intent(this, StepOneSelfieActivity.class));
                break;
            case "2":
                mRegisterSellerPresenter.saveTypeSeller("Socio Negocios", "1507");
                //startActivity(new Intent(this, TypeSellerHaveEnterpriseActivity.class));
                startActivity(new Intent(this, StepOneSelfieActivity.class));
                break;
            case "3":
                mRegisterSellerPresenter.saveTypeSeller("Socio Edificios", "1508");
                //startActivity(new Intent(this, TypeSellerManagerBuildingActivity.class));
                startActivity(new Intent(this, StepOneSelfieActivity.class));
                break;
            case "4":
                mRegisterSellerPresenter.saveTypeSeller("Socio Totalplay", "1509");
                startActivity(new Intent(this, StepOneSelfieActivity.class));
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.mOptionClientTotalPlayLY)
    public void mOptionClientTotalPlay() {
        mTypeSeller = "1";
        mClientTotalPlayImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
        mHaveEnterpriseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mManagerBuildingImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mSellerIndieImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        isChecket = true;
    }

    @OnClick(R.id.mOptionHaveEnterpriseLY)
    public void mOptionHaveEnterprise() {
        mTypeSeller = "2";
        mClientTotalPlayImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mHaveEnterpriseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
        mManagerBuildingImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mSellerIndieImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        isChecket = true;
    }

    @OnClick(R.id.mOptionBuildManagerLY)
    public void mOptionBuildManager() {
        mTypeSeller = "3";
        mClientTotalPlayImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mHaveEnterpriseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mManagerBuildingImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
        mSellerIndieImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        isChecket = true;
    }

    @OnClick(R.id.mOptionSellerIndieLY)
    public void mOptionSellerIndie() {
        mTypeSeller = "4";
        mClientTotalPlayImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mHaveEnterpriseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mManagerBuildingImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheckd));
        mSellerIndieImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkd));
        isChecket = true;
    }

    @OnClick(R.id.type_seller_alert_1)
    public void onClickAlert1(){
        startActivity(DialogAlertTypeSeller.launch(this,"Cliente TP"));
    }

    @OnClick(R.id.type_seller_alert_2)
    public void onClickAlert2(){
        startActivity(DialogAlertTypeSeller.launch(this,"Socio Negocios"));
    }

    @OnClick(R.id.type_seller_alert_3)
    public void onClickAlert3(){
        startActivity(DialogAlertTypeSeller.launch(this,"Socio Edificios"));
    }

    @OnClick(R.id.type_seller_alert_4)
    public void onClickAlert4(){
        startActivity(DialogAlertTypeSeller.launch(this,"Socio Totalplay"));
    }

}
