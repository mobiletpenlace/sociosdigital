package com.totalplay.sociodigital.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.resources.utils.MessageUtils;
import com.resources.utils.Prefs;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.sociodigital.R;
import com.totalplay.sociodigital.model.pojos.InfoSocioBean;
import com.totalplay.sociodigital.presenter.implementations.BasePresenter;
import com.totalplay.sociodigital.presenter.implementations.DownloadFilePresenter;
import com.totalplay.sociodigital.presenter.implementations.LoginPresenter;

import java.io.File;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity  implements DownloadFilePresenter.DownloadImageCallback{
    @BindView(R.id.mBackImageView)
    ImageView mBackImageView;
    @BindView(R.id.act_main_drawer_layout)
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle toggle;

    @BindView(R.id.mProfileImageView)
    ImageView mProfileImageView;

    @BindView(R.id.mNameSocioTextView)
    public TextView mNameSocioTextView;

    @BindView(R.id.mNoSocioTextView)
    public TextView mNoSocioTextView;

    @BindView(R.id.mPuestoTextView)
    public TextView mPuestoTextView;

    @BindView(R.id.mCiudadTextView)
    public TextView mCiudadTextView;

    @BindView(R.id.mAreaTextView)
    public TextView mAreaTextView;

    @BindView(R.id.mReferredButton)
    public Button referredButton;

    @BindView(R.id.mStartSaleButton)
    public Button mStartSale;

    @BindView(R.id.dialog_register_card_con)
    public ViewGroup registerCardContent;

    @BindView(R.id.dialog_register_card_re)
    public Button registerCardButton;
    @BindView(R.id.dialog_register_card_number)
    public EditText cardNumberEditext;
    @BindView(R.id.dialog_register_card_vigence_month)
    public EditText vigenceMonthEditext;
    @BindView(R.id.dialog_register_card_vigence_year)
    public EditText vigenceYearEditext;
    @BindView(R.id.dialog_register_card_detail)
    public TextView detailRegisterCard;
    /*@BindView(R.id.act_home_replace_card)
    public TextView replaceCardTextView;
*/
    private LoginPresenter mLoginPresenter;

    private InfoSocioBean mInfoSocioBean;
    private DownloadFilePresenter mDownloadFilePresenter;
    private String isAnyCardRegister;
    private FormValidator mFormValidator;
    private TextWatcher alternateTextWatcher;
    private String tempNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        ButterKnife.bind(this);
        setValidator();
        mBackImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_only));
        mLoginPresenter =  new LoginPresenter(this);
        mDownloadFilePresenter = new DownloadFilePresenter(this,this);
        retreiveInfoSocio();
        setTextWatcher();
    }

    private void setTextWatcher(){
        alternateTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            //1234 1234 1234 1234

            @Override
            public void afterTextChanged(Editable s) {
                String text = cardNumberEditext.getText().toString();
                if (text.length() > 0) {
                    if (!(tempNumber.length() > text.length())) {
                        if (text.length() == 4 || text.length() == 9 || text.length() == 14){
                            text =  text + " ";
                            tempNumber = tempNumber + text.substring(text.length() - 2);
                        }else{
                            tempNumber = tempNumber + text.substring(text.length() - 1);
                        }
                    } else {
                        tempNumber = tempNumber.substring(0, tempNumber.length() - 1);
                    }

                    if (text.length() > 0 && text.length() < 16)
                        text = text.replaceAll("[0-9]", "•");

                    setNumber(text);
                }else {
                    tempNumber = "";
                }
                //MessageUtils.toast(getApplicationContext(), tempNumber);
            }
        };
        cardNumberEditext.addTextChangedListener(alternateTextWatcher);
    }

    private void setNumber(String text){

        cardNumberEditext.removeTextChangedListener(alternateTextWatcher);
        cardNumberEditext.setText(text);
        cardNumberEditext.addTextChangedListener(alternateTextWatcher);
        if(text.length() < 19)
            cardNumberEditext.setSelection(text.length());

    }

    private void setValidator(){
        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(cardNumberEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(vigenceMonthEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(vigenceYearEditext, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    public void retreiveInfoSocio(){
        InfoSocioBean infoSocioBean = (InfoSocioBean) Prefs.instance().object("saveLogin", InfoSocioBean.class);
        if (infoSocioBean != null){
            if (infoSocioBean.mNoEmpleado.length() > 0){
                putDataSocio(infoSocioBean);
            }
            else {
                mInfoSocioBean = mLoginPresenter.retrieveInfoSocio();
                putDataSocio(mInfoSocioBean);
            }
        }else {
            mInfoSocioBean = mLoginPresenter.retrieveInfoSocio();
            putDataSocio(mInfoSocioBean);
        }
    }

    private void putDataSocio(InfoSocioBean infoSocioBean){
        /*if (infoSocioBean.mIsAnyCardRegister.equals("0"))
            replaceCardTextView.setText("Registrar método de cobro");
        else if(infoSocioBean.mIsAnyCardRegister.equals("1"))
            replaceCardTextView.setText("Cambiar método de cobro");*/

        mNameSocioTextView.setText(infoSocioBean.mNombre);
        mNoSocioTextView.setText(infoSocioBean.mNoEmpleado);
        //mPuestoTextView.setText(mInfoSocioBean.mCanal);
        mCiudadTextView.setText(infoSocioBean.mCiudad);
        mAreaTextView.setText(infoSocioBean.mSubCanal);
        if (infoSocioBean.mPathSelfie != null){
            if (infoSocioBean.mPathSelfie.length()> 0)
                mDownloadFilePresenter.fullImage(infoSocioBean.mPathSelfie,"Selfie");
        }

        if(infoSocioBean.mSubCanal.equals("Cliente TP")){
            mStartSale.setVisibility(View.GONE);
        }
        mLoginPresenter.saveDataInfoSocio(infoSocioBean);
        isAnyCardRegister = infoSocioBean.mIsAnyCardRegister;
    }

    @OnClick(R.id.dialog_register_card_re)
    public void onClickRegisterCard(){
        validateRegisterForm();
    }

    @OnClick(R.id.mStartSaleButton)
    public void OnClickStartSale(){
        if(isAnyCardRegister.equals("0")){
            detailRegisterCard.setText("Es necesario registrar una tarjeta  para realizar una venta o registrar un recomendado.");
            registerCardContent.setVisibility(View.VISIBLE);
        }else{
            startActivity(new Intent(this,ValidateCoverageMapActivity.class));
        }
    }

    @OnClick(R.id.mBackButton)
    public void mOnClickMenu(){
        if (registerCardContent.getVisibility() == View.VISIBLE){
            registerCardContent.setVisibility(View.GONE);
        }else{
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                mDrawerLayout.closeDrawer(GravityCompat.START);
            else
                mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @OnClick(R.id.mCloseMenuImageView)
    public void OnCloseMenu(){
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @OnClick(R.id.mReferredButton)
    public void OnClickRefeer(){
        if(isAnyCardRegister.equals("0")){
            detailRegisterCard.setText("Es necesario registrar una tarjeta  para realizar una venta o registrar un recomendado.");
            registerCardContent.setVisibility(View.VISIBLE);
        }else{
            startActivity(new Intent(this,CapturedReferredActivity.class));
        }
    }

    @OnClick(R.id.mCloseSessionButton)
    public void OnClickCloseSession(){
        startActivity(new Intent(this, CloseSessionDialog.class));
    }

    @OnClick(R.id.mChangePasswordButton)
    public void OnClickChangePassword(){
        startActivity(new Intent(this, ChangePasswordActivity.class));
    }

    @OnClick(R.id.mContentMyIncatorsLinearLayout)
    public void OnClickIndicator(){
        startActivity(new Intent(this, ReferredCarruselActivity.class));
    }

    @OnClick(R.id.dialog_register_card_con)
    public void onClickContentDummy(){
        //nothing
    }

  /*  @OnClick(R.id.act_home_replace_card)
    public void onClickReplaceCard(){
        if (replaceCardTextView.getText().toString().equals("Cambiar método de cobro")){
            detailRegisterCard.setText("El registro de la tarjeta reemplazará a su actual método de cobro.");
        }else{
            detailRegisterCard.setText("Es necesario registrar una tarjeta  para realizar una venta o registrar un recomendado.");
        }
        registerCardContent.setVisibility(View.VISIBLE);
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }
*/
    @OnClick(R.id.content_menu)
    public void onClickContentTest(){

    }

    @OnClick(R.id.mContentCommercialOffertLayout)
    public void onClickCommercialOffert() {
        startActivity(CommercialOffertActivity.launch(this));
    }

    @OnClick(R.id.mCapacitacionButton)
    public void OnClickCapacitacion(){
        startActivity(new Intent(this,VideosCapacitacionActivity.class));
    }

    @Override
    public void postImage(File file) {
        if (file != null){
            switch (file.getName()){
                case "Selfie.jpg":
                    Glide.with(this).load(file).into(mProfileImageView);
                    break;
            }
        }
    }

    @Override
    public void onErrorNotSuccessPostImageResponse() {
        MessageUtils.toast(this,"No se pudo optener la imagen de perfil");
    }

    public boolean validateMonth(){
        int month = Integer.parseInt(vigenceMonthEditext.getText().toString());

        if(month > 12 || month == 0 ){
            return false;
        }else{
            return true;
        }
    }

    public boolean validateYear() {
        final Calendar c = Calendar.getInstance();
        int mAnio = c.get(Calendar.YEAR);
        int current = Integer.parseInt(vigenceYearEditext.getText().toString()) + 2000;

        int maxAnio = mAnio + 10;
        if(mAnio < current && current < maxAnio){
            return true;
        }else{
            return false;
        }
    }

    private boolean validateCarNumber(){
        boolean isValidNumber =  false;
        String result = setTypeCard(tempNumber.trim().replace(" ", ""));
        if (result.equals("1") || result.equals("2")  || result.equals("3")){
            isValidNumber =  true;
        }
        return isValidNumber;
    }

    private void validateRegisterForm(){
        if (mFormValidator.isValid()){
            if (validateMonth()){
                if (validateYear()){
                    if(validateCarNumber()){
                        //RegisterCard
                        InfoSocioBean infoSocioBean = (InfoSocioBean) Prefs.instance().object("saveLogin", InfoSocioBean.class);
                        infoSocioBean.mIsAnyCardRegister = "1";
                        isAnyCardRegister = "1";
                        mLoginPresenter.saveDataInfoSocio(infoSocioBean);
                        registerCardContent.setVisibility(View.GONE);
                        cardNumberEditext.setText("");
                        vigenceMonthEditext.setText("");
                        vigenceYearEditext.setText("");
                        //replaceCardTextView.setText("Cambiar método de cobro");
                        tempNumber = "";
                    }else{
                        MessageUtils.toast(this,"Ingrese un número de tarjeta válido");
                    }
                }else{
                    MessageUtils.toast(this, "El año no es válido");
                }
            }else{
                MessageUtils.toast(this, "El mes es incorrecto");
            }
        }

    }

    public String setTypeCard(String mNumberCad){
        return com.totalplay.sociodigital.library.utils.StringUtils.getIdTypeCard(mNumberCad);
    }

    @Override
    public void onBackPressed() {
        if (registerCardContent.getVisibility() == View.VISIBLE){
            registerCardContent.setVisibility(View.GONE);
        }else
            super.onBackPressed();
    }
}
