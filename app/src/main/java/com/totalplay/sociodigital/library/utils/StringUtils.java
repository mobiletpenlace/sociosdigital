package com.totalplay.sociodigital.library.utils;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mahegots on 18/04/17.
 * SocioDigital
 */

public class StringUtils {

    public static String validateMonth(int expiryMonth) {
        switch (expiryMonth) {
            case 1:
                return "01";

            case 2:
                return "02";

            case 3:
                return "03";

            case 4:
                return "03";

            case 5:
                return "05";

            case 6:
                return "06";

            case 7:
                return "07";

            case 8:
                return "08";

            case 9:
                return "09";

            case 10:
                return "10";

            case 11:
                return "11";

            case 12:
                return "12";

            default:
                return "01";

        }

    }


    public static ArrayList<String> convertStringArrayToArrayList(String[] strArr) {
        ArrayList<String> stringList = new ArrayList<String>();
        Collections.addAll(stringList, strArr);
        return stringList;
    }

    public static ArrayList<String> getEmailCommonDomain() {
        ArrayList<String> objs = new ArrayList<>();
        objs.add("gmail.com");
        objs.add("hotmail.com");
        objs.add("outlook.com");
        objs.add("live.com.mx");
        objs.add("yahoo.com");
        objs.add("icloud.com");
        objs.add("aol.com");
        objs.add("yahoo.com.mx");
        objs.add("prodigy.net.mx");
        objs.add("terra.com.mx");
        objs.add("elektra.com.mx");
        objs.add("totalplay.com.mx");
        objs.add("bancoazteca.com.mx");
        return objs;
    }

    public static String getTypeCard(String card) {
        String regexVisa = "(4[0-9]{12}(?:[0-9]{3})?)";
        String regexMasterC = "(5[1-5][0-9]{14})";
        String regexAmEx = "(3[47][0-9]{13})";

        Pattern patternVisa = Pattern.compile(regexVisa);
        Pattern patternMasterC = Pattern.compile(regexMasterC);
        Pattern patternAmEx = Pattern.compile(regexAmEx);

        Matcher matcherV = patternVisa.matcher(card);
        Matcher matcherM = patternMasterC.matcher(card);
        Matcher matcherAmEx = patternAmEx.matcher(card);


        if (matcherV.matches())
            return "Visa";
        else if (matcherM.matches())
            return "Master Card";
        else if (matcherAmEx.matches())
            return "American Express";
        else
            return "";
    }


    public static String getIdTypeCard(String card) {
        String regexVisa = "(4[0-9]{12}(?:[0-9]{3})?)";
        String regexMasterC = "(5[1-5][0-9]{14})";
        String regexAmEx = "(3[47][0-9]{13})";

        Pattern patternVisa = Pattern.compile(regexVisa);
        Pattern patternMasterC = Pattern.compile(regexMasterC);
        Pattern patternAmEx = Pattern.compile(regexAmEx);

        Matcher matcherV = patternVisa.matcher(card);
        Matcher matcherM = patternMasterC.matcher(card);
        Matcher matcherAmEx = patternAmEx.matcher(card);


        if (matcherM.matches())
            return "3";
        else if (matcherV.matches())
            return "2";
        else if (matcherAmEx.matches())
            return "1";
        else
            return "0";
    }

    public static void unlineTextView(String textInput, TextView textView){
        SpannableString text = new SpannableString(textInput);
        text.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        textView.setText(text);
    }

}
