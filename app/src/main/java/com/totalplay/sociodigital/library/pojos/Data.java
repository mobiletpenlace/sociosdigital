package com.totalplay.sociodigital.library.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Data implements Serializable {
    @SerializedName("Instalados")
    public ArrayList<InfoDetail> installedArray =  new ArrayList<>();
    @SerializedName("porContactar")
    public ArrayList<InfoDetail> forContactArray =  new ArrayList<>();
    @SerializedName("porInstalar")
    public ArrayList<InfoDetail> forInstallArray =  new ArrayList<>();
    @SerializedName("sinExito")
    public ArrayList<InfoDetail> failArray =  new ArrayList<>();
}
