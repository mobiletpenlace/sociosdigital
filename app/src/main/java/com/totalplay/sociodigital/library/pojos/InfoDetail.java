package com.totalplay.sociodigital.library.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InfoDetail implements Serializable {
    @SerializedName("anioEnvio")
    public String sendYear;
    @SerializedName("codigoVenta")
    public String codeSale;
    @SerializedName("cuentaRecomendador")
    public String recomendAccount;
    @SerializedName("fechaActivacion")
    public String activateDate;
    @SerializedName("fechaEnvio")
    public String sendDate;
    @SerializedName("mesEnvio")
    public String sendMonth;
    @SerializedName("nombre")
    public String name;
    @SerializedName("recomendador")
    public String recomend;
    @SerializedName("telefono")
    public String phone;
    @SerializedName("tipo")
    public String type;
    @SerializedName("tipoVenta")
    public String typeSale;
}
