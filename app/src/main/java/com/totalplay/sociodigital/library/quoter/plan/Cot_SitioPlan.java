package com.totalplay.sociodigital.library.quoter.plan;

import com.totalplay.sociodigital.library.quoter.Producto.CotServicioProducto;
import com.totalplay.sociodigital.library.quoter.Servicio.CotPlanServicio;
import com.totalplay.sociodigital.library.quoter.utils.Constans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class Cot_SitioPlan {
    private List<CotPlanServicio> listCot_PlanServicio= new ArrayList<CotPlanServicio>();
    private double descuentoRenta__c;
    private double descuentoCargoUnico__c;
    private double dP_Plan;

    public double Cot_PlanServicio(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.precioProductosAdicionales__c();

        }
        return acumulador;
    }

    public 	double precioRecurrenteDescuentos__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.precioRecurrenteDescuento__c();

        }
        return acumulador;

    }

    public double precioProductosIncluidos__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.precioProductosIncluidos__c();

        }
        return acumulador;
    }

    public double precioRecurrentePromociones__c (){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.precioPromociones__c();

        }
        return acumulador;

    }

    public double impuestosProductosAdicionales__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.precioProductosAdicionales__c();

        }
        return acumulador;
    }

    public double impuestosCU_Total__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.impuestosCargosUnicos__c();

        }
        return acumulador;
    }

    public double impuestosProductosIncluidos__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.impuestosProductosIncluidos__c();

        }
        return acumulador;
    }

    public double precioRecurrenteSubtotal__c(){

        return precioProductosAdicionales__c() + precioProductosIncluidos__c() +  precioRecurrentePromociones__c();
    }

    public double precioProductosAdicionales__c (){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.precioProductosAdicionales__c();

        }
        return acumulador;
    }

    public double impuestoRecurrente__c(){
        return impuestosProductosAdicionales__c() + impuestosProductosIncluidos__c() +  impuestosPromociones__c();
    }

    public double impuestosPromociones__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.impuestosPromociones__c();

        }
        return acumulador;
    }

    public double 	totalRenta__c(){
        return precioRecurrenteSubtotal__c() *(1- descuentoRenta__c );
    }

    public double precio_Pronto_Pago__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            //System.out.println("a:"+lsCot_PlanServicio.totalIncluidos__c()+" - "+lsCot_PlanServicio.precioPromociones__c());

            //acumulador+=(lsCot_PlanServicio.totalIncluidos__c());

            for(CotServicioProducto lsCot_ServicioProducto: lsCot_PlanServicio.getList_cot_ServicioProducto()){
                //System.out.println(lsCot_ServicioProducto.getTipoProducto()+" "+lsCot_ServicioProducto.getPrecioUnitario_ProntoPago__c()+"  "+lsCot_ServicioProducto.getPrecioUnitarioBase__c()+"  "+lsCot_ServicioProducto.getImpuesto2__c());
                //System.out.println(" ");



                if((lsCot_ServicioProducto.getTipoProducto().equals(Constans.PRODUCTO_INCLUIDO)||lsCot_ServicioProducto.getTipoProducto().equals(Constans.PRODUCTO_ADICIONAL))
                        &&lsCot_ServicioProducto.getPrecioUnitarioBase__c()>0){
                    double aux=lsCot_ServicioProducto.getPrecioUnitario_ProntoPago__c();
                    if(lsCot_ServicioProducto.getImpuesto2__c()>0){
                        //System.out.println(x);
                        aux=1.03*aux;
                    }

                    acumulador+=aux;

                }



            }


        }
        acumulador=acumulador*1.16;

        return acumulador;
    }

    public double totalPromociones__c(){
        double acumulador=0;
        for(CotPlanServicio lsCot_PlanServicio:listCot_PlanServicio){
            acumulador+=lsCot_PlanServicio.totalPromociones__c();

        }
        return acumulador;
    }

    public List<CotPlanServicio> getListCot_PlanServicio() {
        return listCot_PlanServicio;
    }

    public void setListCot_PlanServicio(List<CotPlanServicio> listCot_PlanServicio) {
        this.listCot_PlanServicio = listCot_PlanServicio;
    }

    public double getDescuentoRenta__c() {
        return descuentoRenta__c;
    }

    public void setDescuentoRenta__c(double descuentoRenta__c) {
        this.descuentoRenta__c = descuentoRenta__c;
    }

    public double getDescuentoCargoUnico__c() {
        return descuentoCargoUnico__c;
    }

    public void setDescuentoCargoUnico__c(double descuentoCargoUnico__c) {
        this.descuentoCargoUnico__c = descuentoCargoUnico__c;
    }

    public double getdP_Plan() {
        return dP_Plan;
    }

    public void setdP_Plan(double dP_Plan) {
        this.dP_Plan = dP_Plan;
    }



    @Override
    public String toString() {
        return "Cot_SitioPlan [listCot_PlanServicio=" + listCot_PlanServicio + ", descuentoRenta__c="
                + descuentoRenta__c + ", descuentoCargoUnico__c=" + descuentoCargoUnico__c + ", dP_Plan=" + dP_Plan
                + ", CotPlanServicio()=" + Cot_PlanServicio() + ", precioRecurrenteDescuentos__c()="
                + precioRecurrenteDescuentos__c() + ", precioProductosIncluidos__c()=" + precioProductosIncluidos__c()
                + ", precioRecurrentePromociones__c()=" + precioRecurrentePromociones__c()
                + ", impuestosProductosAdicionales__c()=" + impuestosProductosAdicionales__c()
                + ", impuestosCU_Total__c()=" + impuestosCU_Total__c() + ", impuestosProductosIncluidos__c()="
                + impuestosProductosIncluidos__c() + ", precioRecurrenteSubtotal__c()=" + precioRecurrenteSubtotal__c()
                + ", precioProductosAdicionales__c()=" + precioProductosAdicionales__c() + ", impuestoRecurrente__c()="
                + impuestoRecurrente__c() + ", impuestosPromociones__c()=" + impuestosPromociones__c()
                + ", totalRenta__c()=" + totalRenta__c() + ", precio_Pronto_Pago__c()=" + precio_Pronto_Pago__c()
                + ", totalPromociones__c()=" + totalPromociones__c() + ", getListCot_PlanServicio()="
                + getListCot_PlanServicio() + ", getDescuentoRenta__c()=" + getDescuentoRenta__c()
                + ", getDescuentoCargoUnico__c()=" + getDescuentoCargoUnico__c() + ", getdP_Plan()=" + getdP_Plan()
                + ", totalRenta_ConImpuesto__c()=" + totalRenta_ConImpuesto__c() + "]";
    }

    public double totalRenta_ConImpuesto__c(){
        return totalRenta__c() +  impuestoRecurrente__c();

    }
}
