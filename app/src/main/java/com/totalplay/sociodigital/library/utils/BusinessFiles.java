package com.totalplay.sociodigital.library.utils;

import android.os.Environment;

import java.io.File;

public class BusinessFiles {

    private static final String SOCIO_DIGITAL_FILES_FOLDER = "SociosDigital/";

    public static File newDocument(String name) {
        if (SystemUtils.isStorageAvailable()) {
            return new File(
                    Environment.getExternalStorageDirectory(), "/" + name + ".jpg"
            );
        }
        return null;
    }
}
