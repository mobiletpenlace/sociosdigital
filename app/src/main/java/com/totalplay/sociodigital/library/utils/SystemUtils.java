package com.totalplay.sociodigital.library.utils;

import android.os.Environment;

class SystemUtils {
    public static boolean isStorageAvailable() {
        return isExternalStorageWritable() && isExternalStorageReadable();
    }

    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }


}
