package com.totalplay.sociodigital.library.quoter.Servicio;

import com.totalplay.sociodigital.library.quoter.Producto.CotServicioProducto;
import com.totalplay.sociodigital.library.quoter.utils.Constans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class CotPlanServicio {
    private List<CotServicioProducto> list_cot_ServicioProducto= new ArrayList<CotServicioProducto>();
    public String name;

    public double totalRenta__c(){
        return subtotalRenta__c() +  precioRecurrenteDescuento__c();
    }

    public double totalAdicionales__c(){
        return precioProductosAdicionales__c() + impuestosProductosAdicionales__c();
    }

    public double totalIncluidos__c(){
        return impuestosProductosIncluidos__c() +  precioProductosIncluidos__c();
    }

    public double totalPromociones__c(){
        return precioPromociones__c() + impuestosPromociones__c();
    }

    public 	double totalCU__c(){
        return subtotalCU__c() +  precioCU_Descuento__c();
    }

    public double subtotalRenta__c(){
        return precioProductosAdicionales__c() + precioProductosIncluidos__c() +  precioPromociones__c();
    }

    public double subtotalCU__c(){
        return precioCU__c() + precioCU_Promociones__c();
    }

    public double precioCU_Promociones__c(){
        double acumulador=0;

        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.getTipoProducto()!=null&&lsCot_ServicioProducto.getTipoProducto().equals(Constans.PROMOCION)&&lsCot_ServicioProducto.isEsCargoUnico()&&!lsCot_ServicioProducto.isEsProntoPago()){
                acumulador+=lsCot_ServicioProducto.precioTotal__c();
            }

        }

        return acumulador;

    }

    public double impuestosPromociones__c(){

        double acumulador=0;

        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.getTipoProducto()!=null && lsCot_ServicioProducto.getTipoProducto().equals(Constans.PROMOCION)&& !lsCot_ServicioProducto.isEsCargoUnico()){
                acumulador+=lsCot_ServicioProducto.montoImpuestos__c();
            }
        }



        return acumulador;
    }

    public double precioPromociones__c(){

        double acumulador=0;
        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.getTipoProducto()!=null && lsCot_ServicioProducto.getTipoProducto().equals(Constans.PROMOCION)&&!lsCot_ServicioProducto.isEsCargoUnico()&&!lsCot_ServicioProducto.isEsProntoPago()){
                acumulador+=lsCot_ServicioProducto.precioTotal__c();
            }


        }

        return acumulador;
    }

    public double impuestosProductosIncluidos__c (){
        double acumulador=0;
        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(!lsCot_ServicioProducto.isEsProductoAdicional() && !lsCot_ServicioProducto.isEsCargoUnico() && lsCot_ServicioProducto.getTipoProducto()!=null&&
                    lsCot_ServicioProducto.getTipoProducto().equals(Constans.PRODUCTO_INCLUIDO)){
                acumulador+=lsCot_ServicioProducto.montoImpuestos__c();
            }
        }

        return acumulador;

    }

    public double impuestosCargosUnicos__c(){
        double acumulador=0;
        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.isEsCargoUnico()){
                acumulador+= lsCot_ServicioProducto.montoImpuestos__c();
            }
        }

        return acumulador;
    }

    public double impuestosProductosAdicionales__c(){

        double acumulador=0;
        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.isEsProductoAdicional() && !lsCot_ServicioProducto.isEsCargoUnico()){
                acumulador+= lsCot_ServicioProducto.montoImpuestos__c();
            }

        }

        return acumulador;

    }

    public double precioRecurrenteDescuento__c(){
        double acumulador=0;
        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.getTipoProducto()!=null&&lsCot_ServicioProducto.equals(Constans.DESCUENTO)&&!lsCot_ServicioProducto.isEsCargoUnico()&&!lsCot_ServicioProducto.isEsProntoPago() ){
                acumulador+= lsCot_ServicioProducto.precioTotal__c();
            }

        }


        return acumulador;
    }



    public double precioCU_Descuento__c(){

        double acumulador=0;
        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.getTipoProducto()!=null&&lsCot_ServicioProducto.equals(Constans.DESCUENTO)&&lsCot_ServicioProducto.isEsCargoUnico()&&!lsCot_ServicioProducto.isEsProntoPago()){
                acumulador +=lsCot_ServicioProducto.montoImpuestos__c();
            }

        }

        return acumulador;
    }

    public double precioProductosAdicionales__c(){
        double acumulador=0;
        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.isEsProductoAdicional()&&!lsCot_ServicioProducto.isEsCargoUnico()&&!lsCot_ServicioProducto.isEsProntoPago()){
                acumulador+= lsCot_ServicioProducto.precioTotal__c();
            }

        }


        return acumulador;
    }

    public double precioProductosIncluidos__c(){
        double acumulador=0;

        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.getTipoProducto()!=null&&lsCot_ServicioProducto.getTipoProducto().equals(Constans.PRODUCTO_INCLUIDO)&&!lsCot_ServicioProducto.isEsCargoUnico()&&!lsCot_ServicioProducto.isEsProntoPago()&&!lsCot_ServicioProducto.isEsProductoAdicional()){

                acumulador+= lsCot_ServicioProducto.precioTotal__c();
            }
        }



        return acumulador;
    }


    public double precioCU__c(){
        double acumulador=0;

        for(CotServicioProducto lsCot_ServicioProducto:   list_cot_ServicioProducto){
            if(lsCot_ServicioProducto.isEsCargoUnico()&&lsCot_ServicioProducto.getTipoProducto()!=null&&lsCot_ServicioProducto.getTipoProducto().equals(Constans.PRODUCTO_INCLUIDO)&&!lsCot_ServicioProducto.isEsProntoPago()){
                acumulador+= lsCot_ServicioProducto.precioTotal__c();
            }

        }


        return acumulador;
    }

    @Override
    public String toString() {
        return "CotPlanServicio [list_cot_ServicioProducto=" + list_cot_ServicioProducto + ", totalRenta__c()="
                + totalRenta__c() + ", totalAdicionales__c()=" + totalAdicionales__c() + ", totalIncluidos__c()="
                + totalIncluidos__c() + ", totalPromociones__c()=" + totalPromociones__c() + ", totalCU__c()="
                + totalCU__c() + ", subtotalRenta__c()=" + subtotalRenta__c() + ", subtotalCU__c()=" + subtotalCU__c()
                + ", precioCU_Promociones__c()=" + precioCU_Promociones__c() + ", impuestosPromociones__c()="
                + impuestosPromociones__c() + ", precioPromociones__c()=" + precioPromociones__c()
                + ", impuestosProductosIncluidos__c()=" + impuestosProductosIncluidos__c()
                + ", impuestosCargosUnicos__c()=" + impuestosCargosUnicos__c() + ", impuestosProductosAdicionales__c()="
                + impuestosProductosAdicionales__c() + ", precioRecurrenteDescuento__c()="
                + precioRecurrenteDescuento__c() + ", precioCU_Descuento__c()=" + precioCU_Descuento__c()
                + ", precioProductosAdicionales__c()=" + precioProductosAdicionales__c()
                + ", precioProductosIncluidos__c()=" + precioProductosIncluidos__c() + ", precioCU__c()="
                + precioCU__c() + "]";
    }

    public List<CotServicioProducto> getList_cot_ServicioProducto() {
        return list_cot_ServicioProducto;
    }

    public void setList_cot_ServicioProducto(List<CotServicioProducto> list_cot_ServicioProducto) {
        this.list_cot_ServicioProducto = list_cot_ServicioProducto;
    }

}
