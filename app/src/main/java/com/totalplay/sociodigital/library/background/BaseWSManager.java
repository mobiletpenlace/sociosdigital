package com.totalplay.sociodigital.library.background;

import android.content.Context;

import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.resources.background.WSBaseRequestInterface;
import com.resources.background.WSBaseResponseInterface;
import com.resources.background.WSCallback;
import com.resources.utils.ConnectionUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.totalplay.sociodigital.R;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public abstract class BaseWSManager {
    private Context mContext;
    private WSCallback mWSCallback;
    private Call<ResponseBody> mBaseResponseCall;

    public BaseWSManager settings() {
        return this;
    }

    public BaseWSManager settings(WSCallback WSCallback) {
        mContext = (Context) WSCallback;
        mWSCallback = WSCallback;
        return this;
    }

    public BaseWSManager settings(Context context, WSCallback WSCallback) {
        mContext = context;
        mWSCallback = WSCallback;
        return this;
    }

    abstract Call<ResponseBody> getWebService(String webServiceValue, WSBaseRequestInterface WSBaseRequest);

    abstract Call<ResponseBody> getQueryWebService(String webServiceValue, String requestValue);

    abstract String getJsonDebug(String requestUrl);

    abstract boolean getDebugEnabled();

    public <R extends WSBaseResponseInterface> R requestWsSync(Class<R> tClass, String webServiceKey, WSBaseRequestInterface wsBaseRequest) {
        if (getDebugEnabled()) {
            Gson gson = new Gson();
            Logger.d(gson.toJson(wsBaseRequest));
            return gson.fromJson(getJsonDebug(webServiceKey), tClass);
        }
        try {
            mBaseResponseCall = getWebService(webServiceKey, wsBaseRequest);
            Response<ResponseBody> bodyResponse = mBaseResponseCall.execute();
            if (bodyResponse.isSuccessful()) {
                String json = bodyResponse.body().string();
                Gson gson = new Gson();
                return gson.fromJson(json, tClass);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public <R extends WSBaseResponseInterface> BaseWSManager requestWs(Class<R> tClass, String webServiceKey, WSBaseRequestInterface wsBaseRequest) {
        if (getDebugEnabled()) {
            Gson gson = new Gson();
            R response;
            response = gson.fromJson(getJsonDebug(webServiceKey), tClass);
            Logger.d(gson.toJson(wsBaseRequest));
            mWSCallback.onSuccessLoadResponse(webServiceKey, response);
            return this;
        }
        if (ConnectionUtils.isConnected(mContext)) {
            mWSCallback.onRequestWS(webServiceKey);
            mBaseResponseCall = getWebService(webServiceKey, wsBaseRequest);

            mBaseResponseCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            String json = response.body().string();
                            Gson gson = new Gson();
                            mWSCallback.onSuccessLoadResponse(webServiceKey, gson.fromJson(json, tClass));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mWSCallback.onErrorLoadResponse(webServiceKey, "");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    if (!call.isCanceled())
                        mWSCallback.onErrorLoadResponse(webServiceKey, "");
                }
            });
        } else {
            mWSCallback.onErrorConnection();
        }
        return this;
    }

    public <R extends WSBaseResponseInterface> BaseWSManager requestWs(Class<R> tClass, String webServiceKey, String requestValue) {
        if (ConnectionUtils.isConnected(mContext)) {
            mWSCallback.onRequestWS(webServiceKey);
            mBaseResponseCall = getQueryWebService(webServiceKey, requestValue);

            mBaseResponseCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            String json = response.body().string();
                            if (isJSONValid(json)) {
                                Gson gson = new Gson();
                                mWSCallback.onSuccessLoadResponse(webServiceKey, gson.fromJson(json, tClass));
                            } else {
                                mWSCallback.onErrorLoadResponse(webServiceKey, "Ha ocurrido un error al procesar la información. Intente nuevamente.");
                            }
                        } catch (IOException e) {
                            mWSCallback.onErrorLoadResponse(webServiceKey, "Ha ocurrido un error al procesar la información. Intente nuevamente.");
                            e.printStackTrace();
                        }
                    } else {
                        mWSCallback.onErrorLoadResponse(webServiceKey, "Ha ocurrido un error al procesar la información. Intente nuevamente.");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    mWSCallback.onErrorLoadResponse(webServiceKey, "Ha ocurrido un error al procesar la información. Intente nuevamente.");
                }
            });
        } else {
            mWSCallback.onErrorConnection();
        }
        return this;
    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public void onDestroy() {
        if (mBaseResponseCall != null && mBaseResponseCall.isExecuted()) mBaseResponseCall.cancel();
    }
}
