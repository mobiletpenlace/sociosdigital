package com.totalplay.sociodigital.library.utils;

/**
 * Created by imacbookpro on 28/08/18.
 * SocioDigital
 */

public class SocioDigitalKeys {
    public static final String ADDRESS = "address_object";
    public static final String EXTRA_DIRECCION_BEAN = "extraDireccionBean";
    public static final String CONTENT_TYPE = "image/jpeg";
    public static final String DEBUG = "debug_config";
    public static final String EXTRA_BRM = "BRM_ACCOUNT";
    public static final String EXTRA_ID_OPPORTUNITY = "idOpportunity";
    public static final String OFFERT = "selectectPakcage";
    public static final String CITY = "saveCity";
    public static final String COLONY = "saveColony";
    public static final String TYPE_PERSON = "typePerson";
    public static final String TYPE_IDENTIFICATION = "typeIdentification";
    public static final String INSTALATION_COST = "installationCost";
    public static final String CONCEP_INSTALLATION = "concepInstallation";

    public static final String USER = "25631";
    public static final String SECRET_WORD = "Middle100$";
    public static final String IP = "10.10.10.10";
}
