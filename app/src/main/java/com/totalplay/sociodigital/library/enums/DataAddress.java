package com.totalplay.sociodigital.library.enums;

import java.io.Serializable;

/**
 * Created by imacbookpro on 04/09/18.
 * SocioDigital
 */

public class DataAddress implements Serializable {
    public String city;
    public String colony;
    public String delegation;
    public String state;

    public DataAddress(String city, String colony, String delegation, String state){
        this.city = city;
        this.colony = colony;
        this.delegation = delegation;
        this.state = state;
    }
}
