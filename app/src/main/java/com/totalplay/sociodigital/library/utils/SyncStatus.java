package com.totalplay.sociodigital.library.utils;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class SyncStatus {
    public static final int NOT_SYNCHRONIZED = 1;
    public static final int SYNCHRONIZING = 2;
    public static final int SYNCHRONIZED = 3;
}
