package com.totalplay.sociodigital.library.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PersistenData {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public PersistenData(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public void saveData(String key,String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void removeData(String key){
        editor.remove(key).commit();
    }

    public String getData(String key){
        return sharedPreferences.getString(key, "");
    }
}
