package com.totalplay.sociodigital.library.quoter.utils;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class Constans {
    public static final String PRODUCTO_INCLUIDO = "Producto incluido";
    public static final String PRODUCTO_ADICIONAL = "Producto adicional";
    public static final String PROMOCION = "Promocion";
    public static final String DESCUENTO = "Descuento";
    public static final String INCLUIDO_SIN_AGREGAR = "Incluido sin agregar";

    //Login Presenter
    public static final String KEY_SAVE_LOGIN = "saveLogin";
    public static final String KEY_INFO_SOCIO = "infoSocio";
    public static final String KEY_CHECKED = "check";

    //URLAPICP
    public static final String URL_CP = "https://api-codigos-postales.herokuapp.com/v2/codigo_postal/";

}
