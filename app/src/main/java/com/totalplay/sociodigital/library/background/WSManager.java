package com.totalplay.sociodigital.library.background;

import android.content.Context;

import com.resources.background.WSBaseRequestInterface;
import com.resources.background.WSBaseResponse;
import com.totalplay.sociodigital.background.ws.WebServices;
import com.totalplay.sociodigital.background.ws.WebServicesProd;
import com.totalplay.sociodigital.background.ws.definitions.WebServiceImageDefinitions;
import com.totalplay.sociodigital.background.ws.definitions.WebServicesDefinition;
import com.totalplay.sociodigital.background.ws.definitions.WebServicesDefinitionsReferred;
import com.totalplay.sociodigital.background.ws.request.ACDRequest;
import com.totalplay.sociodigital.background.ws.request.ConsultAddonsTpRequest;
import com.totalplay.sociodigital.background.ws.request.ConsultaCPRequest;
import com.totalplay.sociodigital.background.ws.request.ConsultationSaleTPRequest;
import com.totalplay.sociodigital.background.ws.request.FamilyPackagesRequest;
import com.totalplay.sociodigital.background.ws.request.IndicatorRecomendRequest;
import com.totalplay.sociodigital.background.ws.request.IndicatorSaleRequest;
import com.totalplay.sociodigital.background.ws.request.LoginRequest;
import com.totalplay.sociodigital.background.ws.request.LoginSocioRequest;
import com.totalplay.sociodigital.background.ws.request.PMPlanesSociosRequest;
import com.totalplay.sociodigital.background.ws.request.PlanDetailRequest;
import com.totalplay.sociodigital.background.ws.request.ProductMasterPlainsRequest;
import com.totalplay.sociodigital.background.ws.request.RecoverPassRequest;
import com.totalplay.sociodigital.background.ws.request.ReferredRequest;
import com.totalplay.sociodigital.background.ws.request.RegisterSellerRequest;
import com.totalplay.sociodigital.background.ws.request.ValidateCoverageRequest;
import com.totalplay.sociodigital.background.ws.request.ValidateSocioRequest;
import com.totalplay.sociodigital.background.ws.request.document.DocumentFilesRequest;
import com.totalplay.sociodigital.model.entities.FormalityEntity;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by imacbookpro on 29/08/18.
 * SocioDigital
 */

public class WSManager extends BaseWSManager {
    private Context mContext;

    public static WSManager init(Context context) {
        return new WSManager(context);
    }

    public WSManager(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    boolean getDebugEnabled() {
        return false;
    }

    public class WS {
        public static final String VALIDATE_COVERAGE = "validateCoverage";
        public static final String PM_PLANES = "pmPlanes";
        public static final String FAMILY_PACKAGES = "familyPackages";
        public static final String PLAIN_DETAIL = "planDetail";
        public static final String SEND_OPPORTUNITY = "sendOpportunity";
        public static final String SEDN_ADC = "sendADC";
        public static final String UPLOAD_DOCUMENT = "uploadDocument";
        public static final String CONSULTA_CP = "consultaCP";

        public static final String PM_PLANES_SOCIOS = "pmPlanesSocios";
        public static final String CONSULT_ADDONS_TP = "consultAddonsTp";
        public static final String CREATE_SALE = "createSale";
        public static final String REGISTER_SELLER = "registerSeller";
        public static final String LOGIN = "login";
        public static final String INDICATOR_SALE = "indicatorSale";
        public static final String INDICATOR_RECOMEND = "indicatorRecomend";
        public static final String REGISTER_REFERRED = "register_referred";
        public static final String RECOVER_PASS =  "recoverPass";
        public static final String VALIDATE_SOCIO = "validateSocio";
    }


    @Override
    Call<ResponseBody> getWebService(String webServiceValue, WSBaseRequestInterface WSBaseRequest) {
        Call<ResponseBody> call = null;
        WebServicesDefinition webServicesDefinition = WebServices.services();
        WebServicesDefinitionsReferred webServicesDefinitionreferred = WebServices.serviceRegisterReferred();
        WebServiceImageDefinitions webServicesDefinitionImages = WebServices.servicesImages();
        WebServicesDefinition webServicesDefinition2 = WebServicesProd.services();
        switch (webServiceValue) {
            case WS.VALIDATE_COVERAGE:
                call = webServicesDefinition2.validateCoverage((ValidateCoverageRequest) WSBaseRequest);
                break;
            case WS.PM_PLANES:
                call = webServicesDefinition.productMasterPlains((ProductMasterPlainsRequest) WSBaseRequest);
                break;
            case WS.FAMILY_PACKAGES:
                call = webServicesDefinition.familyPackages((FamilyPackagesRequest) WSBaseRequest);
                break;
            case WS.SEND_OPPORTUNITY:
                call = webServicesDefinition.sendOpportunity((FormalityEntity) WSBaseRequest);
                break;
            case WS.SEDN_ADC:
                call = webServicesDefinition.acd((ACDRequest) WSBaseRequest);
                break;
            case WS.UPLOAD_DOCUMENT:
                call = webServicesDefinition.queryUploadDocument((DocumentFilesRequest) WSBaseRequest);
                break;
            case WS.PLAIN_DETAIL:
                call = webServicesDefinition.planDetail((PlanDetailRequest) WSBaseRequest);
                break;
            case WS.CONSULTA_CP:
                call = webServicesDefinition.consultaCP((ConsultaCPRequest)WSBaseRequest);
                break;
            case WS.PM_PLANES_SOCIOS:
                call = webServicesDefinition.pmPlanesSocios((PMPlanesSociosRequest)WSBaseRequest);
                break;
            case WS.CONSULT_ADDONS_TP:
                call = webServicesDefinition.consultAddonsTp((ConsultAddonsTpRequest) WSBaseRequest);
                break;
            case WS.CREATE_SALE:
                call = webServicesDefinition.createSale((ConsultationSaleTPRequest) WSBaseRequest);
                break;
            case WS.REGISTER_SELLER:
                call = webServicesDefinition.registerSeller((RegisterSellerRequest)WSBaseRequest);
                break;
            case WS.LOGIN:
                call = webServicesDefinition.login((LoginSocioRequest) WSBaseRequest);
                break;
            case WS.INDICATOR_SALE:
                call = webServicesDefinition.indicatorSale((IndicatorSaleRequest) WSBaseRequest);
                break;
            case WS.INDICATOR_RECOMEND:
                call = webServicesDefinition.indicatorRecomend((IndicatorRecomendRequest) WSBaseRequest);
                break;
            case WS.REGISTER_REFERRED:
                call = webServicesDefinitionreferred.registerReferred((ReferredRequest) WSBaseRequest);
                break;
            case WS.RECOVER_PASS:
                call = webServicesDefinition.recoverPass((RecoverPassRequest) WSBaseRequest);
                break;
            case WS.VALIDATE_SOCIO:
                call =  webServicesDefinition.validateSocio((ValidateSocioRequest) WSBaseRequest);
            break;
        }
        return call;
    }

    @Override
    Call<ResponseBody> getQueryWebService(String webServiceValue, String requestValue) {
        return null;
    }

    @Override
    String getJsonDebug(String requestUrl) {
        String json = "";

        return json;
    }
}
