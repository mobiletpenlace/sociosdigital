package com.totalplay.sociodigital.library.quoter.Producto;

/**
 * Created by imacbookpro on 03/09/18.
 * SocioDigital
 */

public class CotServicioProducto {
    private double precioUnitarioBase__c; //DIVISA(13,5)
    private double precioUnitario_ProntoPago__c; //DIVISA(13,5)
    private double precioUnitario__c;           //DIVISA(13,5)
    private int cantidad__c;
    private double descuento__c; //Tipo Descuento longitud 4 numero de decimales 4
    private double impuesto1__c; // Tipo IVA longitud 2 numero de decimales 3
    private double impuesto2__c; //Tipo IEPS longitud 2 numero de decimales 3

    String tipoProducto;
    boolean esCargoUnico;
    boolean esProntoPago;
    boolean esProductoAdicional;



    public boolean isEsProductoAdicional() {
        return esProductoAdicional;
    }

    public void setEsProductoAdicional(boolean esProductoAdicional) {
        this.esProductoAdicional = esProductoAdicional;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public boolean isEsCargoUnico() {
        return esCargoUnico;
    }

    public void setEsCargoUnico(boolean esCargoUnico) {
        this.esCargoUnico = esCargoUnico;
    }

    public boolean isEsProntoPago() {
        return esProntoPago;
    }

    public void setEsProntoPago(boolean esProntoPago) {
        this.esProntoPago = esProntoPago;
    }

    public double precioTotal__c(){

        return precioUnitario__c*cantidad__c;

    }

    public double precioTotal_ConDescuento__c(){ // 5 digitos

        return precioTotal__c()*(1-descuento__c);
    }



    public double montoImpuestos__c(){

        return (precioTotal_ConDescuento__c() * impuesto1__c) +( ((precioTotal_ConDescuento__c() * impuesto1__c) + precioTotal_ConDescuento__c()) * impuesto2__c);
    }




    @Override
    public String toString() {
        return "CotServicioProducto [precioUnitarioBase__c=" + precioUnitarioBase__c+"\n"
                + ", precioUnitario_ProntoPago__c=" + precioUnitario_ProntoPago__c +"\n"+ ", precioUnitario__c="
                + precioUnitario__c +"\n"+ ", cantidad__c=" + cantidad__c +"\n"+ ", descuento__c=" + descuento__c+"\n"
                + ", impuesto1__c=" + impuesto1__c+"\n" + ", impuesto2__c=" + impuesto2__c+"\n" + ", precioTotal__c()="
                + precioTotal__c()+"\n" + ", precioTotal_ConDescuento__c()=" + precioTotal_ConDescuento__c()+"\n"
                + ", montoImpuestos__c()=" + montoImpuestos__c()+"\n"
                + ", getPrecioUnitarioBase__c()=" + getPrecioUnitarioBase__c() +"\n"+ ", getPrecioUnitario_ProntoPago__c()="
                + getPrecioUnitario_ProntoPago__c()+"\n" + ", getPrecioUnitario__c()=" + getPrecioUnitario__c()+"\n"
                + ", getCantidad__c()=" + getCantidad__c() +"\n"+ ", getDescuento__c()=" + getDescuento__c()+"\n"
                + ", getImpuesto1__c()=" + getImpuesto1__c() +"\n"+ ", getImpuesto2__c()=" + getImpuesto2__c() + "]";

    }

    public double getPrecioUnitarioBase__c() {
        return precioUnitarioBase__c;
    }

    public void setPrecioUnitarioBase__c(double precioUnitarioBase__c) {
        this.precioUnitarioBase__c = precioUnitarioBase__c;
    }

    public double getPrecioUnitario_ProntoPago__c() {
        return precioUnitario_ProntoPago__c;
    }

    public void setPrecioUnitario_ProntoPago__c(double precioUnitario_ProntoPago__c) {
        this.precioUnitario_ProntoPago__c = precioUnitario_ProntoPago__c;
    }

    public double getPrecioUnitario__c() {
        return precioUnitario__c;
    }

    public void setPrecioUnitario__c(double precioUnitario__c) {
        this.precioUnitario__c = precioUnitario__c;
    }

    public int getCantidad__c() {
        return cantidad__c;
    }

    public void setCantidad__c(int cantidad__c) {
        this.cantidad__c = cantidad__c;
    }

    public double getDescuento__c() {
        return descuento__c;
    }

    public void setDescuento__c(double descuento__c) {
        this.descuento__c = descuento__c;
    }

    public double getImpuesto1__c() {
        return impuesto1__c;
    }

    public void setImpuesto1__c(double impuesto1__c) {
        this.impuesto1__c = impuesto1__c;
    }

    public double getImpuesto2__c() {
        return impuesto2__c;
    }

    public void setImpuesto2__c(double impuesto2__c) {
        this.impuesto2__c = impuesto2__c;
    }
}
