package com.totalplay.sociodigital.handlers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;
import android.widget.Toast;
import com.totalplay.sociodigital.view.activities.HomeActivity;

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
    private CancellationSignal cancellationSignal;
    private Context context;
    private boolean flagCancel;
    private Callback mCallback;
    private TextView mErrorTextView;


    public FingerprintHandler(Context mContext,Callback callback, TextView errorTextView) {
        context = mContext;
        mCallback =callback;
        mErrorTextView = errorTextView;
    }


    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    public void stopListening(){
        if(cancellationSignal !=null){
            flagCancel = true;
            cancellationSignal.cancel();
            cancellationSignal = null;
           // android.util.Log.e("entra aqui","deberia parar el fingerprint");
        }
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        if(!flagCancel){
            showError(errString);
            mCallback.onError();
        }
    }

    @Override
    public void onAuthenticationFailed() {
        showError("Huella no reconocida. Intenta nuevamente");
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        showError(helpString);
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        mCallback.onAuthenticated();
    }

    private void showError(CharSequence error) {
        Toast.makeText(context,error,Toast.LENGTH_LONG).show();
        mErrorTextView.removeCallbacks(mResetErrorTextRunnable);
        mErrorTextView.postDelayed(mResetErrorTextRunnable,1000);
    }

    private Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {

        }
    };

    public interface Callback {

        void onAuthenticated();

        void onError();
    }
}
